﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using GAdUPIN.Application.Test.Fake;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GAdUPIN.Application.Test
{
    [TestClass]
    public class ProvinciaServiceTests
    {
        [TestMethod]
        public void AddTest()
        {
            //Arrange
            IDBContext contextFake = new DBContextFake();
            IProvinciaService target = new ProvinciaService(contextFake);

            //Act
            target.Add(new Provincia()
            {
                Id = Guid.NewGuid(), 
                NombreProvincia = "TestProv",
                ComunidadAutonoma = new ComunidadAutonoma()
                {
                    Id = Guid.NewGuid(),
                    NombreComunidadAutonoma = "TestComunidad"
                },
            });

            //Assert
            Assert.AreEqual(1, target.GetAll().Count());
        }
    }
}
