﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GAdUPIN.Application.Test.Fake
{
    public class DBSetProvinciaFake<Provincia> : IDbSet<Provincia> where Provincia : class
    {
        IList<Provincia> internalList = new List<Provincia>();

        public DBSetProvinciaFake()
        {

        }

        public DBSetProvinciaFake(List<Provincia> internalList)
        {
            this.internalList = internalList;
        }

        public Provincia Add(Provincia entity)
        {
            internalList.Add(entity);
            return entity;
        }

        public IEnumerator<Provincia> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public Expression Expression { get; private set; }
        public Type ElementType { get; private set; }
        public IQueryProvider Provider { get; private set; }
        public Provincia Find(params object[] keyValues)
        {
            throw new NotImplementedException();
        }

        

        public Provincia Remove(Provincia entity)
        {
            throw new NotImplementedException();
        }

        public Provincia Attach(Provincia entity)
        {
            throw new NotImplementedException();
        }

        public Provincia Create()
        {
            throw new NotImplementedException();
        }

        public TDerivedEntity Create<TDerivedEntity>() where TDerivedEntity : class, Provincia
        {
            throw new NotImplementedException();
        }

        public ObservableCollection<Provincia> Local { get; private set; }
    }
}
