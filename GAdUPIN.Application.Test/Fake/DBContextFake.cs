﻿using System;
using System.Data.Entity;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GAdUPIN.Application.Test.Fake
{
    [TestClass]
    public class DBContextFake : IDBContext
    {
        DBSetComunidadFake<ComunidadAutonoma> comunidadList = new DBSetComunidadFake<ComunidadAutonoma>();

        public IDbSet<ComunidadAutonoma> Comunidades
        {
            get
            {
                return comunidadList;
            }
            set
            {
                comunidadList = new DBSetComunidadFake<ComunidadAutonoma>(value.ToList());
            }

        }

        public IDbSet<Candidato> Candidatos { get; set; }
        public IDbSet<Admin> Admins { get; set; }
        public IDbSet<Colegio> Colegios { get; set; }
        public IDbSet<ContactoColegio> ContactosDelColegio { get; set; }
        public IDbSet<Encargado> Encargados { get; set; }
        public IDbSet<Entrevistador> Entrevistadores { get; set; }
        public IDbSet<Experiencia> Experiencias { get; set; }
        public IDbSet<PlazaColegio> PlazasDelColegio { get; set; }
        public IDbSet<ComunidadAutonoma> ComunidadesAutonomas { get; set; }

        public IDbSet<Provincia> Provincias
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public IDbSet<TipoAlojamiento> TipoAlojamientos { get; set; }
        public IDbSet<Estatus> Estatuses { get; set; }
        public IDbSet<Nacionalidad> Nacionalidades { get; set; }
        public IDbSet<InformeEntrevistaEvaluacion> InformeEntrevistaEvaluaciones { get; set; }
        public IDbSet<Procedencia> Procedencias { get; set; }
        public IDbSet<DocAdicional> DocsAdjuntos { get; set; }
        public IDbSet<EtapaEducativa> EtapasEducativas { get; set; }
        public IDbSet<DocumentacionRequeridaCandidato> DocumentacionRequeridaCandidatos { get; set; }

        public int SaveChanges()
        {
            return 0;
        }
    }
}
