﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Objects;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GAdUPIN.Application.Test.Fake
{
    
    public class DBSetComunidadFake<ComunidadAutonoma> : IDbSet<ComunidadAutonoma> where ComunidadAutonoma : class 
    {
        IList<ComunidadAutonoma> internalList = new List<ComunidadAutonoma>();

        public DBSetComunidadFake()
        {

        }

        public DBSetComunidadFake(List<ComunidadAutonoma> internalList)
        {
            this.internalList = internalList;
        }

        public ComunidadAutonoma Add(ComunidadAutonoma entity)
        {
            internalList.Add(entity);
            return entity;
        }

        public ComunidadAutonoma Attach(ComunidadAutonoma entity)
        {
            return Add(entity);
        }

        public TDerivedEntity Create<TDerivedEntity>() where TDerivedEntity : class, ComunidadAutonoma
        {
            throw new NotImplementedException();
        }

        public ComunidadAutonoma Create()
        {
            throw new NotImplementedException();
        }

        public ComunidadAutonoma Find(params object[] keyValues)
        {
            throw new NotImplementedException();
        }

        public ObservableCollection<ComunidadAutonoma> Local
        {
            get {return new ObservableCollection<ComunidadAutonoma>(internalList);}
        }

        public ComunidadAutonoma Remove(ComunidadAutonoma entity)
        {
            if (internalList.Remove(entity))
            {
                return entity;
            }
            return null;
        }

        public IEnumerator<ComunidadAutonoma> GetEnumerator()
        {
            return internalList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return internalList.GetEnumerator();
        }

        public Type ElementType
        {
            get { return internalList.AsQueryable().ElementType; }
        }

        public System.Linq.Expressions.Expression Expression
        {
            get { return internalList.AsQueryable().Expression; }
        }

        public IQueryProvider Provider
        {
            get { return internalList.AsQueryable().Provider; }
        }
    }
}
