﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using GAdUPIN.Application.Test.Fake;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GAdUPIN.Application.Test
{
    /// <summary>
    /// Summary description for ComunidadAutonomaServiceTests
    /// </summary>
    [TestClass]
    public class ComunidadAutonomaServiceTests
    {
        [TestMethod]
        public void GetAllTest()
        {
            //Arrange
            IDBContext contextFake = new DBContextFake();
            IComunidadAutonomaService target = new ComunidadAutonomaService(contextFake);

            //Act
            var result = target.GetAll();

            //Assert
            Assert.AreEqual(0, result.Count());
        }

        [TestMethod]
        public void AddTest()
        {
            //Arrange
            IDBContext contextFake = new DBContextFake();
            IComunidadAutonomaService target = new ComunidadAutonomaService(contextFake);

            //Act
            target.Add(new ComunidadAutonoma() { Id = Guid.NewGuid(), NombreComunidadAutonoma = "Test" });

            //Assert
            Assert.AreEqual(1, target.GetAll().Count());
        }
    }
}
