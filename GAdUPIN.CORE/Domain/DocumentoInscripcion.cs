﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace GAdUPIN.CORE.Domain
{
    /// <summary>
    /// Endidad de dominio de los documentos adjuntos a un candidato
    /// </summary>
    [Serializable]
    public class DocumentoInscripcion
    {
        /// <summary>
        /// Identificador interno del Documento Adjunto
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// NombreEncargado del documento
        /// </summary>
        [Required(ErrorMessage = "Este campo es obligatorio")]
        [DisplayName("Documento")]
        public string NombreDocumento { get; set; }

        /// <summary>
        /// Fecha de subida del documento
        /// </summary>
        [DisplayName("Fecha")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? FechaSubida { get; set; }

        /// <summary>
        /// Comentarios sobre el documento
        /// </summary>
        public string Comentarios { get; set; }

        /// <summary>
        /// Estado de la documentación
        /// </summary>
        public string Estado { get; set; }

        /// <summary>
        /// Candidato al que pertenece el documento
        /// </summary>
        public virtual Candidato Candidato { get; set; }
    }
}
