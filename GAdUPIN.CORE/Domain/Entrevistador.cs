﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.ComponentModel.DataAnnotations;
//using System.Linq;
//using System.Text;

//namespace GAdUPIN.CORE.Domain
//{
//    /// <summary>
//    /// Endidad de dominio del entrevistador de candidatos
//    /// </summary>
//    //[Serializable
//    public class Entrevistador
//    {
//        /// <summary>
//        /// Identificador interno del entrevistador
//        /// </summary>
//        public Guid Id { get; set; }

//        /// <summary>
//        /// NombreEncargado del entrevistador
//        /// </summary>
//        [Required(ErrorMessage = "Debe incluir el nombre del entrevistador")]
//        public string NombreEntrevistador { get; set; }

//        /// <summary>
//        /// Primer Apellido del entrevistador
//        /// </summary>
//        [Required(ErrorMessage = "Debe incluir un apellido")]
//        public string ApellidoEntrevistador { get; set; }

//        /// <summary>
//        /// Segundo Apellido del entrevistador
//        /// </summary>
//        public string ApellidoEntrevistador2 { get; set; }

//        /// <summary>
//        /// Correo electronico del entrevistador
//        /// </summary>
//        [Required(ErrorMessage = "Debe incluir un email")]
//        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Email no válido")]
//        public string Email { get; set; }

//        /// <summary>
//        /// Telefono de contacto del entrevistador
//        /// </summary>
//        [Required(ErrorMessage = "Debe incluir un telefono de contacto")]
//        [RegularExpression("([0-9]+)", ErrorMessage = "El campo solo puede contener números")]
//        public string TelefonoDeContacto { get; set; }

//        /// <summary>
//        /// NombreEncargado de usuario del entrevistador para el login
//        /// </summary>
//        public string Usuario { get; set; }

//        /// <summary>
//        /// Contraseña del entrevistador para el login
//        /// </summary>
//        public string Password { get; set; }

//        /// <summary>
//        /// Cuenta de Skype del entrevistador
//        /// </summary>
//        public string Skype { get; set; }
//    }
//}
