﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;



namespace GAdUPIN.CORE.Domain
{
    /// <summary>
    /// Endidad de dominio de la plaza que registra un colegio
    /// </summary>
    //[Serializable
    public class PlazaColegio
    {
        /// <summary>
        /// Identificador interno de la plaza del colegio
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Fecha de inicio
        /// </summary>
        
        [DisplayName("Fecha Inicio")]
        [Required(ErrorMessage = "Debe indicar una fecha de inicio")]
        [DisplayFormatAttribute(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FechaInicio { get; set; }

        /// <summary>
        /// Fecha fin 
        /// </summary>
        
        [DisplayName("Fecha Fin")]
        [Required(ErrorMessage = "Debe indicar una fecha de fin")]
        [DisplayFormatAttribute(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FechaFin { get; set; }

        /// <summary>
        /// Campo antiguo de cuantia de la beca 
        /// </summary>
        [Obsolete("Campo antiguo, BecasCuantia")]
        public int CuantiaBeca { get; set; }

        

        /// <summary>
        /// Preferencia sobre el sexo del profesor
        /// </summary>
        [DisplayName("Sexo Preferente")]
        public string PreferenciaSexo { get; set; }

        /// <summary>
        /// Listado de rangos de edades disponibles para elegir
        /// </summary>
        
        [DisplayName("Etapa Educativa")]
        public virtual EtapaEducativa EtapaEducativa { get; set; }

        /// <summary>
        /// Indica si la entrevista se ha realizado con el candidato. 
        /// </summary>
        [DisplayName("Entrevista Realizada")]
        public bool Entrevista { get; set; }

        /// <summary>
        /// Indicar si se han firmado y recibido las condiciones firmadas por el candidato
        /// </summary>
        [DisplayName("Condiciones Firmadas")]
        public bool CondicionesFirmadas { get; set; }

        /// <summary>
        /// Comentarios adicionales sobre la plaza del colegio
        /// </summary>
        public string Comentarios { get; set; }

        /// <summary>
        /// Colegio al que pertenece la plaza
        /// </summary>
        
        public virtual Colegio Colegio { get; set; }

        /// <summary>
        /// Indica si la plaza ha sido contratada 
        /// </summary>
        
        [DisplayName("Cubierta")]
        public Boolean PlazaContratada { get; set; }

        /// <summary>
        /// Indica el candidato al que se le ha asignado la plaza 
        /// </summary>
        [DisplayName("Candidato Asignado")]
        public virtual Candidato CandidatoAsignado { get; set; }

        
        [Obsolete("Campo antiguo, utilizar NumeroHorasPlaza")]
        public int NumeroHoras { get; set; }

        /// <summary>
        /// Preferencia de nacionalidad para la plaza
        /// </summary>
        [DisplayName("Nacionalidad preferente")]
        public virtual Nacionalidad NacionalidadPreferente { get; set; }

        /// <summary>
        /// Codigo de la plaza
        /// </summary>
        [DisplayName("Código")]
        public string CodigoPlaza { get; set; }

        /// <summary>
        /// Cuantía de la beca
        /// </summary>
        [DisplayName("Cuantía Beca")]
        public string BecaCuantia { get; set; }

        [DisplayName("Número de horas")]
        public string NumeroHorasPlaza { get; set; }

        [DisplayName("Adicional")]
        public string DatoAdicional { get; set; }

        [DisplayName("Condiciones Ofrecidas")]
        public string CondicionesOfrecidas { get; set; }
    }
}
