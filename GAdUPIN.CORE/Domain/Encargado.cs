﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;


namespace GAdUPIN.CORE.Domain
{
    /// <summary>
    /// El encargado es la persona encargada de ayudar al candidato cuando llega a España
    /// </summary>
    public class Encargado
    {

        public Guid Id { get; set; }

        [DisplayName("DNI")]
        public string DNI { get; set; }

        [Required(ErrorMessage = "Este campo es obligatorio")]
        public string Nombre { get; set; }

        [DisplayName("1er Apellido")]
        [Required(ErrorMessage = "Este campo es obligatorio")]
        public string Apellido1 { get; set; }

        
        [DisplayName("2do Apellido")]
        public string Apellido2 { get; set; }

        [Required(ErrorMessage = "Este campo es obligatorio")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Email no válido")]
        public string Email { get; set; }

        [DisplayName("Teléfono de contacto")]
        [Required(ErrorMessage = "Este campo es obligatorio")]
        //[RegularExpression("([0-9]+)", ErrorMessage = "El campo solo puede contener números")]
        public string TelefonoDeContacto { get; set; }


        [DisplayName("Contacto Principal")]
        public bool EsContactoPrincipal { get; set; }

        [DisplayName("Comunidad Autonoma")]
        public virtual ComunidadAutonoma ComunidadAutonoma { get; set; }

        /// <summary>
        /// Listado de candidatos de los que es responsable
        /// </summary>
        public virtual IList<Candidato> Candidatos { get; set; }
       
    }
}
