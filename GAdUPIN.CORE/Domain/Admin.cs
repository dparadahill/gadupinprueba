﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace GAdUPIN.CORE.Domain
{
    /// <summary>
    /// Endidad de dominio del administrador
    /// </summary>
    //[Serializable
    public class Admin
    {
        /// <summary>
        /// Identificador interno del administrador
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// NombreEncargado del administrador
        /// </summary>
        public string NombreAdmin { get; set; }

        /// <summary>
        /// Primer Apellido del administrador
        /// </summary>
        public string Apellido1 { get; set; }

        /// <summary>
        /// Segundo Apellido del administrador
        /// </summary>
        public string Apellido2 { get; set; }

        /// <summary>
        /// Correo electronico del administrador
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Telefono de contacto del administrador
        /// </summary>
        public string TelefonoDeContacto { get; set; }

        /// <summary>
        /// NombreEncargado de usuario del administrador para el login
        /// </summary>
        public string Usuario { get; set; }

        /// <summary>
        /// Contraseña del administrador para el login
        /// </summary>
        public string Password { get; set; }
    }
}
