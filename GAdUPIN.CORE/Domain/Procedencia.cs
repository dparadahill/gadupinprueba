﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace GAdUPIN.CORE.Domain
{
    public class Procedencia
    {
    public Guid Id { get; set; }


    
    [DisplayName("Canal de procedencia")]
    [Required(ErrorMessage = "Debe incluir el nombre de la procedencia")]
    public string NombreProcedencia { get; set; }

    }
}
