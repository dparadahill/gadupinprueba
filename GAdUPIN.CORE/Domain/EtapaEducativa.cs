﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace GAdUPIN.CORE.Domain
{
    [Serializable]
    public class EtapaEducativa
    {
        public Guid Id { get; set; }

        
        [DisplayName("Etapa Educativa")]
        [Required(ErrorMessage = "Debe incluir el nombre de la etapa educativa")]
        public string EtapaEducativaNombre { get; set; }
    }
}
