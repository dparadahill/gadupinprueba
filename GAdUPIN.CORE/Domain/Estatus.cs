﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace GAdUPIN.CORE.Domain
{
    [Serializable]
    public class Estatus
    {
        public Guid Id { get; set; }

        
        [DisplayName("Estatus")]
        [Required(ErrorMessage = "Debe incluir el nombre del estatus")]
        public string EstatusNombre { get; set; }
    }
}
