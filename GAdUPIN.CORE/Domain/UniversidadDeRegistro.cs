﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace GAdUPIN.CORE.Domain
{
    [Serializable]
    public class UniversidadDeRegistro
    {
        /// <summary>
        /// Identificador interno de la universidad de registro
        /// </summary>
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// Nombre de la universidad de registro
        /// </summary>
        [DisplayName("Universidad de registro")]
        [Required(ErrorMessage = "Debe incluir el nombre de la universidad")]
        public string NombreUniversidadDeRegistro { get; set; }

        [DisplayName("¿Visible?")]
        public bool Visible { get; set; }
    }
}
