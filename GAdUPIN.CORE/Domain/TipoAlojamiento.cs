﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace GAdUPIN.CORE.Domain
{
    /// <summary>
    /// Tipo de alojamiento 
    /// </summary>
    [Serializable]
    public class TipoAlojamiento
    {
        public Guid Id { get; set; }

        
        [DisplayName("Tipo de alojamiento")]
        [Required(ErrorMessage = "Debe incluir el nombre del tipo de alojamiento")]
        public string TipoAlojamientoNombre { get; set; }
    }
}
