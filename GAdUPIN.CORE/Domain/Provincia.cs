﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Dynamic;
using System.Linq;
using System.Text;

namespace GAdUPIN.CORE.Domain
{
    /// <summary>
    /// Entidad de dominio de provincias en españa
    /// </summary>
    [Serializable]
    public class Provincia
    {
        /// <summary>
        /// ID interno de la provincia
        /// </summary>
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// Nombre de la provincia
        /// </summary>
        
        [DisplayName("Provincia")]
        [Required(ErrorMessage = "Debe incluir el nombre de la provincia")]
        public string NombreProvincia { get; set; }




        /// <summary>
        /// Comunidad Autonoma a la que pertenece la provincia
        /// </summary>
        [ForeignKey("ComunidadAutonoma")]
        public Guid ComunidadAutonomaId { get; set; }
        [DisplayName("Comunidad Autónoma")]
        public virtual ComunidadAutonoma ComunidadAutonoma { get; set; }

    }
}
