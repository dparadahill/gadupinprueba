﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace GAdUPIN.CORE.Domain
{
    [Serializable]
    public class MotivoCierreFicha
    {
        /// <summary>
        /// Identificador interno
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Campo de texto indicando el motivo de cierre. 
        /// </summary>
        public string Motivo { get; set; }
    }
}
