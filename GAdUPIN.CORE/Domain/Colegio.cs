﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Server;

namespace GAdUPIN.CORE.Domain
{
    /// <summary>
    /// Endidad de dominio del colegio
    /// </summary>
    [Serializable]
    public class Colegio
    {
        /// <summary>
        /// Identificador interno del colegio
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// CIF del colegio
        /// </summary>
        public string CIF { get; set; }

        /// <summary>
        /// Nombre del colegio
        /// </summary>
        [Required(ErrorMessage = "Debe incluir el nombre del colegio")]
        [DisplayName("Colegio")]
        public string NombreColegio { get; set; }

        /// <summary>
        /// Direccion del colegio
        /// </summary>
        [Required(ErrorMessage = "Debe incluir la dirección")]
        [DisplayName("Dirección")]
        public string DireccionColegio { get; set; }

        /// <summary>
        /// Codigo Postal del colegio
        /// </summary>
        [Required(ErrorMessage = "Debe incluir el codigo postal")]
        [DisplayName("C.P.")]
        [Range(0, Int32.MaxValue, ErrorMessage = "Cantidad inválida")]
        [MinLength(5, ErrorMessage = "Debe ser un numero de 5 dígitos")]
        public string CodigoPostal { get; set; }

        /// <summary>
        /// Comunidad Autonoma del colegio
        /// </summary>
        [DisplayName("Comunidad Autónoma")]
        public virtual ComunidadAutonoma ComunidadAutonoma { get; set; }

        /// <summary>
        /// Provincia de la comunidad autonoma
        /// </summary>
        [DisplayName("Provincia")]
        public virtual Provincia Provincia { get; set; }

        /// <summary>
        /// Localidad del colegio
        /// </summary>
        [DisplayName("Localidad")]
        public string Localidad { get; set; }

        /// <summary>
        /// Telefono de contacto del colegio
        /// </summary>
        [DisplayName("Teléfono")]
        //[RegularExpression("([0-9]+)", ErrorMessage = "El campo solo puede contener números")]
        public string Telefono { get; set; }

        /// <summary>
        /// Numero de plazas disponibles del colegio
        /// </summary>
        [DisplayName("Total de plazas")]
        public int PlazasDisponibles { get; set; }

        /// <summary>
        /// Indica si el colegio ha sido contratado o no.
        /// </summary>
        [DisplayName("Contratado")]
        public Boolean ColegioContratado { get; set; }

        /// <summary>
        /// Fecha de contrato del colegio
        /// </summary>
        [DisplayName("Fecha de contrato")]
        [DisplayFormatAttribute(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? FechaDeContrato { get; set;  }

        /// <summary>
        /// Listado de plazas que tiene el colegio
        /// </summary>
        [DisplayName("Plazas")]
        public virtual List<PlazaColegio> PlazasDelColegio { get; set; }

        /// <summary>
        /// Listado de los contactos del colegio
        /// </summary>
        [DisplayName("Contactos")]
        public virtual List<ContactoColegio> ContactosColegio { get; set; }

        public Guid UserId { get; set; }

        /// <summary>
        /// Comentarios del colegio
        /// </summary>
        [DisplayName("Comentarios")]
        public string Comentarios { get; set; }

        /// <summary>
        /// Nombre del del colegio para el login
        /// </summary>
        public string Usuario { get; set; }

        /// <summary>
        /// Contraseña del colegio para el login
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Indica si el colegio no es un colegio con el que ya no se trata
        /// </summary>
        public bool Desactivado { get; set; }

        /// <summary>
        /// Página web del colegio
        /// </summary>
        public string Web { get; set; }

        /// <summary>
        /// Email del colegio
        /// </summary>
        public string Email { get; set; }

    }
}
