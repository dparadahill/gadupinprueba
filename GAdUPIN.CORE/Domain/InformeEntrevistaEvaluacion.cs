﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Server;

namespace GAdUPIN.CORE.Domain
{
    /// <summary>
    /// Endidad de dominio de la entrevista personal realizada 
    /// </summary>
    //[Serializable
    public class InformeEntrevistaEvaluacion
    {
        /// <summary>
        /// Identificador interno del informe realizado despues de la entrevista personal
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Fecha de realizacion de la entrevista
        /// </summary>
        
        [DisplayName("Última actualización")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        //[Required(ErrorMessage = "Campo Obligatorio")]
        public DateTime? Fecha { get; set; }

        /// <summary>
        /// Titulacion del candidato
        /// </summary>
        
        [DisplayName("Titulación")]
        public string Titulacion { get; set; }

        /// <summary>
        /// Observaciones generales sobre la entrevista
        /// </summary>
        [DisplayName("Observaciones generales")]
        [DataType(DataType.MultilineText)]
        public string ObsGenerales { get; set; }

        /// <summary>
        /// Analisis sobre el candidato por parte de UP International
        /// </summary>
        [DisplayName("Análisis Equipo UP")]
        [DataType(DataType.MultilineText)]
        public string AnalisisEquipoUP { get; set; }

      #region Valoracion
        /// <summary>
        /// Indica si el candidato cumple con los requisitos para ser profesor
        /// </summary>
        
        [DisplayName("Cumple requisitos")]
        public Boolean CumpleRequisitos { get; set; }

        /// <summary>
        /// Valoracion global del candidato
        /// </summary>
        
        [DisplayName("Valoración global")]
        public int ValGlobal { get; set; }

        /// <summary>
        /// Valoracion del cumplimiento de los requisitos del candidato
        /// </summary>
        [DisplayName("Valoración de requisitos")]
        public int ValRequisitos { get; set; }

        /// <summary>
        /// Valoracion del interes del candidato por el programa
        /// </summary>
        [DisplayName("Valoración del interés")]
        public int ValInteres { get; set; }

        /// <summary>
        /// Valoracion de la educacion que recibio el candidato
        /// </summary>
        [DisplayName("Valoración de educación")]
        public int ValEducacion { get; set; }

        /// <summary>
        /// Valoracion de las habilidades profesionales del candidato
        /// </summary>
        [DisplayName("Valoración de habilidades profesionales")]
        public int ValHabProfesionales { get; set; }

        /// <summary>
        /// Comentarios sobre el interes que muestra el candidato por el programa
        /// </summary>
        [DisplayName("Comentarios de interés")]
        [DataType(DataType.MultilineText)]
        public string ComentariosInteres { get; set; }
      #endregion
        
      #region Analisis Linguistico
        /// <summary>
        /// Valoracion del claridad de comunicacion del candidato
        /// </summary>
        [DisplayName("Valoración de la claridad del idioma")]
        public int ValClaridadDelIdioma { get; set; }

        /// <summary>
        /// Valoracion de la expresividad y soltura del candidato
        /// </summary>
        [DisplayName("Valoración de expresividad y soltura")]
        public int ValExpresividadSoltura { get; set; }

        /// <summary>
        /// Valoracion de la confianza personal que demuestra el candidato
        /// </summary>
        [DisplayName("Valoración de confianza personal")]
        public int ValConfianzaPersonal { get; set; }

        /// <summary>
        /// Comentarios sobre el analisis linguistico
        /// </summary>
        [DisplayName("Comentarios")]
        [DataType(DataType.MultilineText)]
        public string ComentariosAnalisisLinguistico { get; set; }
      #endregion

      #region Otros datos
        /// <summary>
        /// Idiomas que conoce el candidato
        /// </summary>
        [DisplayName("Idiomas")]
        [DataType(DataType.MultilineText)]
        public string Idiomas { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("Condiciones del programa")]
        [DataType(DataType.MultilineText)]
        public string CondicionesDelPrograma { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("Referencias")]
        [DataType(DataType.MultilineText)]
        public string Referencias { get; set; }

        /// <summary>
        /// Situacion economica del candidato
        /// </summary>
        [DisplayName("Situación económica")]
        [DataType(DataType.MultilineText)]
        public string SituacionEconomica { get; set; }

        /// <summary>
        /// Datos relativos a alguna dieta especial 
        /// </summary>
        [DisplayName("Dieta / Alimentación")]
        [DataType(DataType.MultilineText)]
        public string DietaAlimentacion { get; set; }

        /// <summary>
        /// Religion que practica
        /// </summary>
        [DisplayName("Religión")]
        [DataType(DataType.MultilineText)]
        public string Religion { get; set; }

        /// <summary>
        /// Otros datos importantes que se deseen mencionar sobre el candidato
        /// </summary>
        [DisplayName("Otros")]
        [DataType(DataType.MultilineText)]
        public string Otros { get; set; }
      #endregion

        [DisplayName("Comp")]
        public bool Completado { get; set; }

        ///// <summary>
        ///// Entrevistador al que le corresponde rellenar la entrevista
        ///// </summary>
        
        //[DisplayName("Entrevistador")]
        //public virtual Entrevistador Entrevistador { get; set; }

        /// <summary>
        /// Candidato sobre el que se realiza la entrevista
        /// </summary>

        [DisplayName("Candidato")]
        public virtual Candidato Candidato { get; set; }

        public Guid UserId { get; set; }

        
        //NOTA: Procedencia y comentarios sobre la procedencia se guardan en candidato

    }
}
