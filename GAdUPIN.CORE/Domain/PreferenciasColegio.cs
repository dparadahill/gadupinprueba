﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace GAdUPIN.CORE.Domain
{
    /// <summary>
    /// Endidad de dominio de las preferencias del colegio que tiene el candidato
    /// </summary>
    [Serializable]
    public class PreferenciasColegio
    {
        /// <summary>
        /// Identificador interno de las preferencias del colegio
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Listado de rangos de edades disponibles 
        /// </summary>
        [DisplayName("Etapa educativa")]
        public EtapaEducativa EtapaEducativa { get; set; }

        /// <summary>
        /// Listado del tipo de alojamientos disponibles
        /// </summary>
        [DisplayName("Tipo de alojamiento")]
        public TipoAlojamiento TipoDeAlojamiento { get; set; }

        /// <summary>
        /// Listado de localidades disponibles
        /// </summary>
        [DisplayName("Comunidad Autonoma")]
        public ComunidadAutonoma ComunidadAutonoma { get; set; }

        /// <summary>
        /// Listado de localidades disponibles
        /// </summary>
        [DisplayName("Provincia")]
        public Provincia Provincia { get; set; }

        /// <summary>
        /// Comentarios adicionales sobre las preferencias
        /// </summary>
        [DisplayName("Comentarios")]
        public string Comentarios { get; set; }

        public virtual Candidato Candidato { get; set; } 

    }
}
