﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace GAdUPIN.CORE.Domain
{
    /// <summary>
    /// Entidad de dominio de las comunidades autonomas de españa
    /// </summary>
    [Serializable]
    public class ComunidadAutonoma
    {
        /// <summary>
        /// ID interno de la comunidad autonoma
        /// </summary>
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// Nombre de la comunidad autonoma
        /// </summary>
        
        [DisplayName("Comunidad Autónoma")]
        [Required(ErrorMessage = "Debe incluir un nombre")]
        public string NombreComunidadAutonoma { get; set; }

        /// <summary>
        /// Provincias que pertenecen a la Comunidad Autonoma
        /// </summary>
        public virtual IList<Provincia> Provincias { get; set; }
    }
}
