﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace GAdUPIN.CORE.Domain
{
    /// <summary>
    /// Endidad de dominio de la experiencia que tiene un candidato
    /// </summary>
    [Serializable]
    [Obsolete("Usar NuevaExperiencia")]
    public class Experiencia
    {
        /// <summary>
        /// Identificador interno de la experiencia del candidato
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Booleano para indicar si el candidato tiene experiencia con colegios. 0 = no y 1 = si
        /// </summary>
        public bool Colegio { get; set; }
        
        /// <summary>
        /// Booleano para indicar si el candidato tiene experiencia con niños. 0 = no y 1 = si
        /// </summary>
        public bool Ninos { get; set; }

        /// <summary>
        /// Booleano para indicar si el candidato tiene experiencia en Academias. 0 = no y 1 = si
        /// </summary>
        public bool Academia { get; set; }

        /// <summary>
        /// Booleano para indicar si el candidato tiene experiencia con Up. 0 = no y 1 = si
        /// </summary>
        public bool Up { get; set; }

        /// <summary>
        /// Booleano para indicar si el candidato tiene experiencia con campamentos de verano. 0 = no y 1 = si
        /// </summary>
        public bool Campamento { get; set; }

        /// <summary>
        /// Booleano para indicar si el candidato tiene experiencia en trabajar con adultos. 0 = no y 1 = si
        /// </summary>
        public bool Adultos { get; set; }

        /// <summary>
        /// Booleano para indicar si el candidato tiene experiencia con volutariados de idioma. 0 = no y 1 = si
        /// </summary>
        public bool Voluntariado { get; set; }

        /// <summary>
        /// Booleano para indicar si el candidato tiene certificado TEFL. 0 = no y 1 = si
        /// </summary>
        public bool TEFL { get; set; }

        [DisplayName("Abroad")]
        public bool Paises { get; set; }
    }
}
