﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace GAdUPIN.CORE.Domain
{
    /// <summary>
    /// Nacionalidad del candidato
    /// </summary>
    [Serializable]
    public class Nacionalidad
    {
        public Guid Id { get; set; }

        
        [DisplayName("Nacionalidad")]
        [Required(ErrorMessage = "Debe incluir el nombre de la nacionalidad")]
        public string NombreNacionalidad { get; set; }

        
        [DisplayName("Requiere visado")]
        public Boolean Visado { get; set; }
    }
}
