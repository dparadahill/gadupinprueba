﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Mime;
using System.Reflection;
using System.Text;



namespace GAdUPIN.CORE.Domain
{
    /// <summary>
    /// Endidad de dominio del candidato
    /// </summary>
    [Serializable]
    //[Table("Candidato")]
    public class Candidato
    {
        /// <summary>
        /// Identificador interno del candidato
        /// </summary>
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// Codigo del usuario que debera ser guardad con formato aa####  (a de año y luego un numero incremental)
        /// </summary>
        [DisplayName("Código")]
        public string Codigo { get; set; }

        /// <summary>
        /// Pasaporte del candidato
        /// </summary>
        [DisplayName("Pasaporte")]
        public bool Pasaporte { get; set; }

        /// <summary>
        /// Numero del pasaporte del candidato
        /// </summary>
        [DisplayName("Número de Pasaporte")]
        public string NumeroPasaporte { get; set; }

        /// <summary>
        /// Colegio al que pertenece el candidato
        /// </summary>
        [DisplayName("Colegio")]
        public Colegio Colegio { get; set; }

        /// <summary>
        /// Nombre del candidato
        /// </summary>
        [DisplayName("Nombre")]
        [Required(ErrorMessage = "Debe incluir el nombre del candidato")]
        public string Nombre { get; set; }

        /// <summary>
        /// Primer Apellido del candidato
        /// </summary>
        [DisplayName("Apellidos")]
        [Required(ErrorMessage = "Debe incluir el apellido del candidato")]
        public string Apellido1 { get; set; }

        /// <summary>
        /// Segundo Apellido del candidato
        /// </summary>
        [DisplayName("Vive en")]
        public string Apellido2 { get; set; }

        /// <summary>
        /// Correo electronico del candidato
        /// </summary>
        [DisplayName("Email")]
        [Required(ErrorMessage = "Debe incluir un email")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Email no válido")]
        public string Email { get; set; }

        /// <summary>
        /// Telefono de contacto del candidato
        /// </summary>
        [DisplayName("Teléfono extranjero")]
        //[RegularExpression("([0-9]+)", ErrorMessage = "El campo solo puede contener números")]
        public string TelefonoDeContacto { get; set; }

        /// <summary>
        /// Telefono Espanol del Candidato
        /// </summary>
        [DisplayName("Teléfono español")]
        //[RegularExpression("([0-9]+)", ErrorMessage = "El campo solo puede contener números")]
        public string TelefonoEspanol { get; set; }

        /// <summary>
        /// Nombre de usuario del candidato para el login
        /// </summary>
        public string Usuario { get; set; }

        /// <summary>
        /// Contraseña del candidato para el login
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Edad del candidato
        /// </summary>

        [DisplayName("Fecha de nacimiento")]
        [Required(ErrorMessage = "Debe indicar una fecha de nacimiento")]
        [DisplayFormatAttribute(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FechaNac { get; set; }

        /// <summary>
        /// Nacionalidad del candidato
        /// </summary>

        [DisplayName("Nacionalidad")]
        public Nacionalidad Nacionalidad { get; set; }

        /// <summary>
        /// Skype del candidato
        /// </summary>
        [DisplayName("Skype")]
        public string Skype { get; set; }

        /// <summary>
        /// Canal de procedencia del candidato
        /// </summary>

        [DisplayName("Canal de procedencia")]
        public Procedencia Procedencia { get; set; }

        /// <summary>
        /// Comentarios sobre la procedencia del candidato
        /// </summary>
        [DisplayName("Comentarios sobre sus preferencias")]
        public string ComentariosProcedencia { get; set; }

        /// <summary>
        /// Sexo del candidato, 0 es masculino y 1 es femenino
        /// </summary>
        [DisplayName("Sexo")]
        public bool Sexo { get; set; }

        /// <summary>
        /// Carrera del candidato
        /// </summary>
        [DisplayName("Carrera")]
        public string Carrera { get; set; }

        /// <summary>
        /// Fecha en la que el candidato se dio de alta
        /// </summary>

        [DisplayName("Fecha de alta")]
        [Required(ErrorMessage = "Debe indicar una fecha de alta")]
        [DisplayFormatAttribute(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FechaAlta { get; set; }

        /// <summary>
        /// Fecha en la que el candidato llega a españa
        /// </summary>

        [DisplayName("Fecha de llegada a España")]
        [DisplayFormatAttribute(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? FechaLlegadaEspana { get; set; }

        /// <summary>
        /// Experiencia que tiene el candidato
        /// </summary>
        [DisplayName("Experiencia")]
        [Obsolete]
        public virtual Experiencia Experiencia { get; set; }

        /// <summary>
        /// Indica si el candidato tiene una cuenta bancaria en Espana
        /// </summary>
        [DisplayName("Banco")]
        public Boolean Banco { get; set; }

        /// <summary>
        /// Indica si el candidato tiene ya su tarjeta sanitaria
        /// </summary>
        [DisplayName("Tarjeta Sanitaria")]
        public Boolean TarjetaSanitaria { get; set; }

        /// <summary>
        /// Indica la fecha de la futura disponibilidad del candidato
        /// </summary>
        [DisplayName("Fecha de disponibilidad")]
        [DisplayFormatAttribute(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? FechaDisponibilidad { get; set; }

        /// <summary>
        /// Indica el estado del candidato en cuanto a su contratacion al programa
        /// </summary>
        [DisplayName("Estatus")]
        public Estatus EstatusCandidato { get; set; }



        /// <summary>
        /// Encargado del candidato
        /// </summary>
        [DisplayName("Encargado")]
        public virtual Encargado Encargado { get; set; }

        /// <summary>
        /// Listado de la documentacion requerida por ese candidato
        /// </summary>
        [DisplayName("Documentacion del candidato")]
        public virtual List<DocumentoDeTramite> DocumentosDeTramite { get; set; }

        /// <summary>
        /// Listado de documentos adicionales relacionados a ese candidato
        /// </summary>
        [DisplayName("Documentos adicionales")]
        public virtual List<DocumentoInscripcion> DocumentosInscripcion { get; set; }

        ///// <summary>
        ///// Indica si tiene pendiente realizar una entrevista
        ///// </summary>
        //[DisplayName("Entrevista pendiente")]
        //public Boolean PendienteEntrevista { get; set; }


        /// <summary>
        /// Indica si la entrevista fue realizada por el entrevistador nativo
        /// </summary>
        public bool EntrevistaConNativo { get; set; }

        /// <summary>
        /// Indica la fecha de la entrevista (ya sea realizada o programada) del agente nativo con el candidato
        /// </summary>
        [DisplayFormatAttribute(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? FechaEntrevistaNativo { get; set; }

        

        /// <summary>
        /// Indica si la entrevista fue realizada por UP
        /// </summary>
        public bool EntrevistaConUp { get; set; }

        /// <summary>
        /// Indica la fecha de la entrevista (ya sea realizada o programada) del agente nativo con el candidato
        /// </summary>
        [DisplayFormatAttribute(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? FechaEntrevistaUP { get; set; }


        
        /// <summary>
        /// Datos de la entrevista
        /// </summary>
        [DisplayName("Informe de entrevista")]
        public virtual IList<InformeEntrevistaEvaluacion> InformeEntrevistaEvaluaciones { get; set; }

        /// <summary>
        /// Listado de plazas asignadas al candidato
        /// </summary>
        [DisplayName("Plazas Cubiertas")]
        public virtual IList<PlazaColegio> PlazasDeColegiosAsignadas { get; set; }

        /// <summary>
        /// % de ajuste al puesto que se obtiene del test
        /// </summary>
        [DisplayName("Ajuste al puesto")]
        public int AjusteAlPuesto { get; set; }

        /// <summary>
        /// % de deseabilidad social que se obtiene del test
        /// </summary>
        [DisplayName("Ajuste al puesto")]
        public int DeseabilidadSocial { get; set; }

        /// <summary>
        /// Se indica si ha realizado el test o no
        /// </summary>
        [DisplayName("Test realizado")]
        public bool TestRealizado { get; set; }

        /// <summary>
        /// Campo para dejar comentarios sobre el test
        /// </summary>
        [DisplayName("Comentarios Test")]
        public string ComentariosTest { get; set; }

        /// <summary>
        /// Campo para dejar comentarios generales sobre el candidato
        /// </summary>
        [DisplayName("Comentarios generales")]
        public string ComentariosGenerales { get; set; }


        #region Formación

        /// <summary>
        /// Campo para indicar si al candidato se le va a formar con TEFL
        /// </summary>
        [DisplayName("TEFL / CELTA")]
        [Obsolete("Use TC")]
        public bool TeflCelta { get; set; }

        /// <summary>
        /// Campo para indicar si al candidato se le va a formar CLIL
        /// </summary>
        [DisplayName("CLIL")]
        public bool CLIL { get; set; }

        /// <summary>
        /// Campo para indicar si al candidato se le va a formar Español
        /// </summary>
        [DisplayName("Español")]
        public bool Espanol { get; set; }

        #endregion


        #region Preferencias

        [DisplayName("Tipo de alojamiento")]
        public virtual TipoAlojamiento TipoAlojamientoPreferido { get; set; }

        [DisplayName("Comunidad Autónoma Preferida")]
        public virtual ComunidadAutonoma ComunidadAutonomaPreferida { get; set; }

        [DisplayName("Provincia Preferida")]
        public virtual Provincia ProvinciaPreferida { get; set; }

        [DisplayName("Etapa Educativa Pref.")]
        public virtual EtapaEducativa EtapaEducativaPreferida { get; set; }

        #endregion


        #region Visado

        [DisplayName("Estado del visado")]
        public virtual EstadoVisado EstadoVisado { get; set; }

        [DisplayName("Comentarios sobre visado")]
        public string ComentariosVisado { get; set; }

        #endregion

        #region CerrarFicha

        [DisplayName("Cerrar ficha")]
        public Boolean fichaCerrada { get; set; }

        /// <summary>
        /// Motivo del cierre de la ficha. Puede ser temporalmente (contactar) o permanentemente (lista negra o fin de periodo)
        /// </summary>
        [DisplayName("Motivo de cierre")]
        public virtual MotivoCierreFicha MotivoCierreFicha { get; set; }

        /// <summary>
        /// Comentarios sobre el motivo de cierre de la ficha
        /// </summary>
        [DisplayName("Comentarios sobre el motivo de cierre")]
        public string ComentariosMotivoCierre { get; set; }

        /// <summary>
        /// Campo que se mostrará si el motivo de cierre es contactar con el candidato en el futuro
        /// </summary>
        [DisplayName("Año a contactar")]
        [DisplayFormatAttribute(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? anyoContacto { get; set; }

        #endregion

        /// <summary>
        /// Universidad de registro del candidato
        /// </summary>
        [DisplayName("Universidad de registro")]
        public UniversidadDeRegistro UniversidadDeRegistro { get; set; }


        public bool TituloRecibido { get; set; }
        public bool CertificadoPenalesRecibido { get; set; }

        //[DisplayName("Condiciones firmadas")]
        //public bool CondicionesColegioFirmadas { get; set; }

        [DisplayName("Entrevista")]
        public DateTime? EntrevistaColegio { get; set; }


        public string fotoPerfilPath { get; set; }

        /// <summary>
        /// Comentarios relacionados al colegio ofrecido/asignado al candidato
        /// </summary>
        [DisplayName("Comentarios sobre el colegio")]
        public String ComentariosColegio { get; set; }


        /// <summary>
        /// Plaza Ofrecida
        /// </summary>
        [DisplayName("Plaza ofrecida al candidato")]
        public virtual PlazaColegio PlazaOfrecida { get; set; }

        public int prueba1Migration { get; set; }

        public int prueba2Migration { get; set; }

        /// <summary>
        /// Indica el estado de la ficha, e.g. condiciones enviadas, etc.
        /// </summary>
        [DisplayName("Estado de la ficha")]
        public string EstadoFicha { get; set; }

        [DisplayName("Hora")]
        public string HoraEntrevistaNativo { get; set; }
        public string HoraEntrevistaUp { get; set; }
        public string HoraEntrevistaColegio { get; set; }

        //public virtual NuevaExperiencia NuevaExperiencia { get; set; }


        [DisplayName("Experiencia")]
        public string CantidadDeExperiencia { get; set; }

        [DisplayName("Abroad")]
        public bool Abroad { get; set; }

        /// <summary>
        /// TEFL o CELTA
        /// </summary>
        [DisplayName("T/C")]
        public bool TC { get; set; }

        public bool Voluntario { get; set; }

        public string Destacar { get; set; }

        public string ComentariosExperiencia { get; set; }
    }

}