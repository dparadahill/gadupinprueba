﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;


namespace GAdUPIN.CORE.Domain
{
    [Serializable]
    public class ContactoColegio
    {   
        /// <summary>
        /// Del Contacto del colegio
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Del Contacto del colegio
        /// </summary>
        
        [DisplayName("Nombre")]
        public string NombreContactoColegio { get; set; }

        /// <summary>
        /// Del Contacto del colegio
        /// </summary>
        [DisplayName("Apellido")]
        public string Apellido1 { get; set; }

        /// <summary>
        /// Del Contacto del colegio
        /// </summary>
        [DisplayName("2do Apellido")]
        public string Apellido2 { get; set; }

        /// <summary>
        /// Del Contacto del colegio
        /// </summary>
        
        [DisplayName("Email")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Email no válido")]
        public string Email { get; set; }

        /// <summary>
        /// Del Contacto del colegio
        /// </summary>
        [DisplayName("Teléfono")]
        //[RegularExpression("([0-9]+)", ErrorMessage = "El campo solo puede contener números")]
        public string TelefonoDeContacto { get; set; }

        /// <summary>
        /// Indica si es el contacto principal del colegio
        /// </summary>
        [DisplayName("Contacto Principal")]
        public bool ContactoPrincipal { get; set; }

        /// <summary>
        /// Colegio al que pertenece el contacto
        /// </summary>
        [DisplayName("Colegio")]
        public Colegio Colegio { get; set; }

        /// <summary>
        /// DNI del contacto del colegio
        /// </summary>
        [DisplayName("DNI")]
        public string DNI { get; set; }

        /// <summary>
        /// Tipo de contacto (p.e. Tutor, profesor, etc)
        /// </summary>
        [DisplayName("Tipo")]
        public string Tipo { get; set; }

        /// <summary>
        /// Comentarios sobre el contacto de colegio
        /// </summary>
        [DisplayName("Comentarios")]
        public string ComentariosContacto { get; set; }
    }
}
