﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace GAdUPIN.CORE.Domain
{
    [Serializable]
    public class EstadoVisado
    {
        public Guid Id { get; set; }

        [DisplayName("Estado")]
        [Required(ErrorMessage = "Debe haber texto")]
        public string EstadoVisadoNombre { get; set; }

    }
}
