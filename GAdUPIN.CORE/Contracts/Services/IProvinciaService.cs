﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.CORE.Contracts.Services
{
    public interface IProvinciaService
    {
        void Add(Provincia provincia, bool autosave = true);
        void Remove(Provincia provincia, bool autosave = true);
        void Update(Provincia provincia, bool autosave = true);
        Provincia Get(Guid id);
        IQueryable<Provincia> GetAll();
        List<Provincia> GetByComunidadAutonoma(Guid id);
        Provincia GetWithComunidad(Guid id);
    }
}
