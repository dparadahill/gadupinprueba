﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
 
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.CORE.Contracts.Services
{
    interface IDocAdjuntoService
    {
        void Add(DocumentoInscripcion documentoInscripcion, bool autosave = true);
        void Remove(DocumentoInscripcion documentoInscripcion, bool autosave = true);
        void Update(DocumentoInscripcion documentoInscripcion, bool autosave = true);
        DocumentoInscripcion Get(Guid id);
        IQueryable<DocumentoInscripcion> GetAll();
    }
}
