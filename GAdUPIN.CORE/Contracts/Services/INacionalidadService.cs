﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.CORE.Contracts.Services
{
    public interface INacionalidadService
    {
        void Add(Nacionalidad nacionalidad, bool autosave = true);
        void Remove(Guid Id, bool autosave = true);
        void Update(Nacionalidad nacionalidad, bool autosave = true);
        Nacionalidad Get(Guid id);
        IQueryable<Nacionalidad> GetAll();

    }
}
