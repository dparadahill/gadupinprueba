﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
 
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.CORE.Contracts.Services
{
    public interface IPlazaColegioService
    {
        void Add(PlazaColegio plazaColegio, bool autosave = true);
        void Remove(PlazaColegio plazaColegio, bool autosave = true);
        void Update(PlazaColegio plazaColegio, bool autosave = true);
        PlazaColegio Get(Guid id);
        IQueryable<PlazaColegio> GetAll();
        List<PlazaColegio> GetByColegio(Guid Id);
        PlazaColegio GetWithColegio(Guid id);
        PlazaColegio GetWithEtapaEducativa(Guid id);
        void Desasignar(Guid id, bool autosave = true);
        
    }
}
