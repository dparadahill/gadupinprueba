﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
 
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.CORE.Contracts.Services
{
    public interface ICandidatoService
    {
        void Add(Candidato candidato, bool autosave = true);
        void Remove(Candidato candidato, bool autosave = true);
        void Update(Candidato candidato, bool autosave = true);
        Candidato Get(Guid id);
        Candidato GetIncNacionalidad(Guid id);
        IQueryable<Candidato> GetAll();
        IQueryable<Candidato> GetAllWithInformes();
        IQueryable<Candidato> GetAllWithColegios();
        IQueryable<Candidato> GetByColegio(Colegio colegio);
        IQueryable<Candidato> GetByEncargado(Guid id);
        IQueryable<Candidato> GetAllButCerradosOrSeleccionados();
        IQueryable<Candidato> GetAllSeleccionados();
        bool EmailNoExiste(Candidato candidato);
        void DesasignarEncargado(Guid id, bool autosave = true);
        //void DesasignarPlaza(Guid id, bool autosave = true);
    }
}
