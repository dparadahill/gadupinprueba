﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.CORE.Contracts.Services
{
    public interface IComunidadAutonomaService
    {
        void Add(ComunidadAutonoma comunidadAutonoma, bool autosave = true);
        void Remove(ComunidadAutonoma comunidadAutonoma, bool autosave = true);
        void Update(ComunidadAutonoma comunidadAutonoma, bool autosave = true);
        ComunidadAutonoma Get(Guid id );
        IQueryable<ComunidadAutonoma> GetAll();
        ComunidadAutonoma GetWithProvincias(Guid id);
    }
}
