﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.CORE.Contracts.Services
{
    public interface IEstadoVisadoService
    {
        void Add(EstadoVisado estadosVisado, bool autosave = true);
        void Remove(EstadoVisado estadosVisado, bool autosave = true);
        void Update(EstadoVisado estadosVisado, bool autosave = true);
        EstadoVisado Get(Guid id);
        IQueryable<EstadoVisado> GetAll();
    }
}
