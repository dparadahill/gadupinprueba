﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.CORE.Contracts.Services
{
    public interface IProcedenciaService
    {
        void Add(Procedencia procedencia, bool autosave = true);
        void Remove(Guid Id, bool autosave = true);
        void Update(Procedencia procedencia, bool autosave = true);
        Procedencia Get(Guid id);
        IQueryable<Procedencia> GetAll();
    }
}
