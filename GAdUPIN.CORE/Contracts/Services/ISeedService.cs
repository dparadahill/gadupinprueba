﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.DAL;

namespace GAdUPIN.CORE.Contracts.Services
{
    public interface ISeedService
    {
       void Seed(IDBContext context);
    }
}
