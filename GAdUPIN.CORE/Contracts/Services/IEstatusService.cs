﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.CORE.Contracts.Services
{
    public interface IEstatusService
    {
        void Add(Estatus estatus, bool autosave = true);
        void Remove(Estatus estatus, bool autosave = true);
        void Update(Estatus estatus, bool autosave = true);
        Estatus Get(Guid id);
        IQueryable<Estatus> GetAll();

    }
}
