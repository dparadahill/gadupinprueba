﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.CORE.Contracts.Services
{
    public interface IContactoColegioService
    {
        void Add(ContactoColegio contactoColegio, bool autosave = true);
        void Remove(Guid Id, bool autosave = true);
        void Update(ContactoColegio contactoColegio, bool autosave = true);
        ContactoColegio Get(Guid id);
        IQueryable<ContactoColegio> GetAll();
        
        //IQueryable<ContactoColegio> GetByColegio(Guid id);
        List<ContactoColegio> GetByColegio(Guid Id);
    }
}
