﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.CORE.Contracts.Services
{
    public interface IMotivoCierreFichaService
    {
        void Add(MotivoCierreFicha motivoCierreFicha, bool autosave = true);
        void Remove(MotivoCierreFicha motivoCierreFicha, bool autosave = true);
        void Update(MotivoCierreFicha motivoCierreFicha, bool autosave = true);
        MotivoCierreFicha Get(Guid id);
        IQueryable<MotivoCierreFicha> GetAll();
    }
}
