﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.CORE.Contracts.Services
{
    public interface IEncargadoService
    {
        void Add(Encargado encargado, bool autosave = true);
        void Remove(Guid Id, bool autosave = true);
        void Update(Encargado encargado, bool autosave = true);
        Encargado Get(Guid id);
        IEnumerable<Encargado> GetAll();
        
        
        
    }
}
