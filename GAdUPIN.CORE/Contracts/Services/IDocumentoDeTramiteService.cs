﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.CORE.Contracts.Services
{
    public interface IDocumentoDeTramiteService
    {
        void Add(DocumentoDeTramite documentoDeTramite, bool autosave = true);
        void Remove(Guid documentoDeTramite, bool autosave = true);
        void Update(DocumentoDeTramite documentoDeTramite, bool autosave = true);
        DocumentoDeTramite Get(Guid id);
        IQueryable<DocumentoDeTramite> GetAll();
        
        List<DocumentoDeTramite> GetByCandidato(Guid id);
    }
}
