﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using GAdUPIN.CORE.Domain;

namespace GAdUPIN.CORE.Contracts.Services
{
    public interface IColegioService
    {
        void Add(Colegio colegio, bool autosave = true);
        void Remove(Colegio colegio, bool autosave = true);
        void Desactivar(Colegio colegio, bool autosave = true);
        void Activar(Colegio colegio, bool autosave = true);
        void Update(Colegio colegio, bool autosave = true);
        Colegio Get(Guid id);
        IQueryable<Colegio> GetAll();
        Colegio GetWithPlazasDelColegio(Guid id);
        
    }
}
