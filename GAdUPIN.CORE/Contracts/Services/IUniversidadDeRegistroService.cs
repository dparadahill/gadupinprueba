﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.CORE.Contracts.Services
{
    public interface IUniversidadDeRegistroService
    {
        void Add(UniversidadDeRegistro universidadDeRegistro, bool autosave = true);
        void Remove(Guid Id, bool autosave = true);
        void Update(UniversidadDeRegistro universidadDeRegistro, bool autosave = true);
        UniversidadDeRegistro Get(Guid id);
        IQueryable<UniversidadDeRegistro> GetAll();
        IQueryable<UniversidadDeRegistro> GetAllButInvisible();
    }
}
