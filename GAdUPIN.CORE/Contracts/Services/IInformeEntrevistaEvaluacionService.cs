﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.CORE.Contracts.Services
{
    public interface IInformeEntrevistaEvaluacionService
    {
        void Add(InformeEntrevistaEvaluacion informeEntrevistaEvaluacion, bool autosave = true);
        void Remove(InformeEntrevistaEvaluacion informeEntrevistaEvaluacion, bool autosave = true);
        void Update(InformeEntrevistaEvaluacion informeEntrevistaEvaluacion, bool autosave = true);
        InformeEntrevistaEvaluacion Get(Guid id);
        IQueryable<InformeEntrevistaEvaluacion> GetAll();
        //IQueryable<InformeEntrevistaEvaluacion> GetByEntrevistador(Entrevistador entrevistador);
    }
}
