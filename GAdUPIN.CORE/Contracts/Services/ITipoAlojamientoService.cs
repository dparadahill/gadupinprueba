﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.CORE.Contracts.Services
{
    public interface ITipoAlojamientoService
    {
        void Add(TipoAlojamiento tipoAlojamiento, bool autosave = true);
        void Remove(Guid Id, bool autosave = true);
        void Update(TipoAlojamiento tipoAlojamiento, bool autosave = true);
        TipoAlojamiento Get(Guid id);
        IQueryable<TipoAlojamiento> GetAll();
     
    }
}
