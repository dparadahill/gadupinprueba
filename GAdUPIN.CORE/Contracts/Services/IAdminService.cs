﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using GAdUPIN.CORE.Domain;

namespace GAdUPIN.CORE.Contracts.Services
{
    public interface IAdminService
    {
        void Add(Admin admin, bool autosave = true);
        void Remove(Admin admin, bool autosave = true);
        void Update(Admin admin, bool autosave = true);
        Admin Get(Guid id);
        IQueryable<Admin> GetAll();
    }
}
