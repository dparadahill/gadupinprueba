﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.CORE.Contracts.Services
{
    public interface IEtapaEducativaService
    {
        void Add(EtapaEducativa etapaEducativa, bool autosave = true);
        void Remove(Guid Id, bool autosave = true);
        void SoftDelete(EtapaEducativa etapaEducativa, bool autosave = true);
        void Update(EtapaEducativa etapaEducativa, bool autosave = true);
        EtapaEducativa Get(Guid id);
        IQueryable<EtapaEducativa> GetAll();
    }
}
