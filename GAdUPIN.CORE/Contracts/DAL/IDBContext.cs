﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.CORE.DAL
{
    public interface IDBContext
    {

        DbSet<Candidato> Candidatos { get; set; }

        IDbSet<Admin> Admins { get; set; }

        IDbSet<Colegio> Colegios { get; set; }

        IDbSet<ContactoColegio> ContactosDelColegio { get; set; }

        IDbSet<Encargado> Encargados { get; set; }

        //IDbSet<Entrevistador> Entrevistadores { get; set; }

        //IDbSet<NuevaExperiencia> Experiencias { get; set; }

        IDbSet<PlazaColegio> PlazasDelColegio { get; set; }

        IDbSet<ComunidadAutonoma> ComunidadesAutonomas { get; set; }

        IDbSet<Provincia> Provincias { get; set; }

        IDbSet<TipoAlojamiento> TipoAlojamientos { get; set; }

        IDbSet<Estatus> Estatuses { get; set; }

        IDbSet<Nacionalidad> Nacionalidades { get; set; }

        IDbSet<InformeEntrevistaEvaluacion> InformeEntrevistaEvaluaciones { get; set; }

        IDbSet<Procedencia> Procedencias { get; set; }

        IDbSet<EtapaEducativa> EtapasEducativas { get; set; }

        IDbSet<EstadoVisado> EstadosVisado { get; set; }

        IDbSet<MotivoCierreFicha> MotivosCierreFicha { get; set; }

        IDbSet<UniversidadDeRegistro> UniversidadesDeRegistro { get; set; }

        IDbSet<DocumentoDeTramite> DocumentosDeTramite { get; set; }
        
        IDbSet<DocumentoInscripcion> DocumentosInscripcion { get; set; } 

        int SaveChanges();
    }
}
