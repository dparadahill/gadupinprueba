﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GAdUPIN.Infrastructure.Logging
{
    public interface INLogger
    {
        void Info(string message);
        void Error(string message, string location);
    }
}
