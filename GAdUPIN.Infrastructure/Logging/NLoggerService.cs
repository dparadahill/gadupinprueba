﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
namespace GAdUPIN.Infrastructure.Logging
{
    public class NLoggerService : INLogger
    {
        private Logger _logger;

        public NLoggerService()
        {
            _logger = LogManager.GetCurrentClassLogger();
        }


        public void Info(string message)
        {
            _logger.Info(message);
        }


        public void Error(string message, string location)
        {
            _logger.Error(message);
        }
    }


}
