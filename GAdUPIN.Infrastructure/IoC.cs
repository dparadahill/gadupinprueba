﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Text;
//using System.Web.Management;
//using GAdUPIN.CORE.DAL;
//using Microsoft.Practices.Unity;
//using System.Configuration;
//using System.Collections.Specialized;
//using GAdUPIN.CORE.Contracts.Services;
//using GAdUPIN.CORE.Contracts;
//using GAdUPIN.Infrastructure.Logging;

//namespace GAdUPIN.Infrastructure
//{
//    public class IoC
//    {
//        protected IUnityContainer container;

//        #region Constructores

//        /// <summary>
//        /// Prevent "before field init",this is  only for
//        /// singleton pattern
//        /// </summary>
//        static IoC()
//        {
//        }

//        /// <summary>
//        /// Incializa una instancia de <see cref="IoCFactory"/>
//        /// </summary>
//        protected IoC()
//        {
//            container = new UnityContainer();

//            new LogEvent("message to myself").Raise();

//            Type typeContext = Type.GetType("GAdUPIN.DAL.DBContext, GAdUPIN.DAL");
//            if (typeContext == null)
//                new LogEvent("GAdUPIN.DAL.DBContext, GAdUPIN.DAL no encontrado ").Raise();
//            else
//                container.RegisterType(typeof(IDBContext),
//                                                   typeContext,
//                                                   new ContainerControlledLifetimeManager(), new InjectionConstructor("ApplicationServices"));

//            typeContext = Type.GetType("GAdUPIN.Application.CandidatoService, GAdUPIN.Application");
//            if (typeContext == null)
//                new LogEvent("GAdUPIN.Application.CandidatoService, GAdUPIN.Application.CandidatoService no encontrado ").Raise();
//            else
//                container.RegisterType(typeof(ICandidatoService),
//                                    Type.GetType("GAdUPIN.Application.CandidatoService, GAdUPIN.Application"),
//                                    new TransientLifetimeManager());

//            typeContext = Type.GetType("GAdUPIN.Application.AdminService, GAdUPIN.Application");
//            if (typeContext == null)
//                new LogEvent("GAdUPIN.Application.AdminService, GAdUPIN.Application.AdminService no encontrado ").Raise();
//            else
//                container.RegisterType(typeof(IAdminService),
//                                        typeContext,
//                                        new TransientLifetimeManager());

//            typeContext = Type.GetType("GAdUPIN.Application.ColegioService, GAdUPIN.Application");
//            if (typeContext == null)
//                new LogEvent("GAdUPIN.Application.ColegioService, GAdUPIN.Application.ColegioService no encontrado ").Raise();
//            else
//                container.RegisterType(typeof(IColegioService),
//                                        typeContext,
//                                        new TransientLifetimeManager());

//            typeContext = Type.GetType("GAdUPIN.Application.ContactoColegioService, GAdUPIN.Application");
//            if (typeContext == null)
//                new LogEvent("GAdUPIN.Application.ContactoColegioService, GAdUPIN.Application.ContactoColegioService no encontrado ").Raise();
//            else
//                container.RegisterType(typeof(IContactoColegioService),
//                                        typeContext,
//                                        new TransientLifetimeManager());

//            typeContext = Type.GetType("GAdUPIN.Application.EncargadoService, GAdUPIN.Application");
//            if (typeContext == null)
//                new LogEvent("GAdUPIN.Application.EncargadoService, GAdUPIN.Application.EncargadoService no encontrado ").Raise();
//            else
//                container.RegisterType(typeof(IEncargadoService),
//                                        typeContext,
//                                        new TransientLifetimeManager());

//            typeContext = Type.GetType("GAdUPIN.Application.EntrevistadorService, GAdUPIN.Application");
//            if (typeContext == null)
//                new LogEvent("GAdUPIN.Application.EntrevistadorService, GAdUPIN.Application.EntrevistadorService no encontrado ").Raise();
//            else
//                container.RegisterType(typeof(IEntrevistadorService),
//                                        typeContext,
//                                        new TransientLifetimeManager());

//            typeContext = Type.GetType("GAdUPIN.Application.ExperienciaService, GAdUPIN.Application");
//            if (typeContext == null)
//                new LogEvent("GAdUPIN.Application.ExperienciaService, GAdUPIN.Application.ExperienciaService no encontrado ").Raise();
//            else
//                container.RegisterType(typeof(IExperienciaService),
//                                        typeContext,
//                                        new TransientLifetimeManager());

//            typeContext = Type.GetType("GAdUPIN.Application.InformeEntrevistaEvaluacionService, GAdUPIN.Application");
//            if (typeContext == null)
//                new LogEvent("GAdUPIN.Application.InformeEntrevistaEvaluacionService, GAdUPIN.Application.InformeEntrevistaEvaluacionService no encontrado ").Raise();
//            else
//                container.RegisterType(typeof(IInformeEntrevistaEvaluacionService),
//                                        typeContext,
//                                        new TransientLifetimeManager());

//            typeContext = Type.GetType("GAdUPIN.Application.PlazaColegioService, GAdUPIN.Application");
//            if (typeContext == null)
//                new LogEvent("GAdUPIN.Application.PlazaColegioService, GAdUPIN.Application.PlazaColegioService no encontrado ").Raise();
//            else
//                container.RegisterType(typeof(IPlazaColegioService),
//                                        typeContext,
//                                        new TransientLifetimeManager());

//           typeContext = Type.GetType("GAdUPIN.Application.ComunidadAutonomaService, GAdUPIN.Application");
//            if (typeContext == null)
//                new LogEvent("GAdUPIN.Application.ComunidadAutonomaService, GAdUPIN.Application.ComunidadAutonomaService no encontrado ").Raise();
//            else
//                container.RegisterType(typeof(IComunidadAutonomaService),
//                                        typeContext,
//                                        new TransientLifetimeManager());

//            typeContext = Type.GetType("GAdUPIN.Application.ProvinciaService, GAdUPIN.Application");
//            if (typeContext == null)
//                new LogEvent("GAdUPIN.Application.ProvinciaService, GAdUPIN.Application.ProvinciaService no encontrado ").Raise();
//            else
//                container.RegisterType(typeof(IProvinciaService),
//                                        typeContext,
//                                        new TransientLifetimeManager());

//            typeContext = Type.GetType("GAdUPIN.Application.TipoAlojamientoService, GAdUPIN.Application");
//            if (typeContext == null)
//                new LogEvent("GAdUPIN.Application.TipoAlojamientoService, GAdUPIN.Application.TipoAlojamientoService no encontrado ").Raise();
//            else
//                container.RegisterType(typeof(ITipoAlojamientoService),
//                                        typeContext,
//                                        new TransientLifetimeManager());

//            typeContext = Type.GetType("GAdUPIN.Application.EstatusService, GAdUPIN.Application");
//            if (typeContext == null)
//                new LogEvent("GAdUPIN.Application.EstatusService, GAdUPIN.Application.EstatusService no encontrado ").Raise();
//            else
//                container.RegisterType(typeof(IEstatusService),
//                                        typeContext,
//                                        new TransientLifetimeManager());

//            typeContext = Type.GetType("GAdUPIN.Application.NacionalidadService, GAdUPIN.Application");
//            if (typeContext == null)
//                new LogEvent("GAdUPIN.Application.NacionalidadService, GAdUPIN.Application.NacionalidadService no encontrado ").Raise();
//            else
//                container.RegisterType(typeof(INacionalidadService),
//                                        typeContext,
//                                        new TransientLifetimeManager());

//            typeContext = Type.GetType("GAdUPIN.Application.ProcedenciaService, GAdUPIN.Application");
//            if (typeContext == null)
//                new LogEvent("GAdUPIN.Application.ProcedenciaService, GAdUPIN.Application.ProcedenciaService no encontrado ").Raise();
//            else
//                container.RegisterType(typeof(IProcedenciaService),
//                                        typeContext,
//                                        new TransientLifetimeManager());

//            typeContext = Type.GetType("GAdUPIN.Application.EtapaEducativaService, GAdUPIN.Application");
//            if (typeContext == null)
//                new LogEvent("GAdUPIN.Application.EtapaEducativaService, GAdUPIN.Application.EtapaEducativaService no encontrado ").Raise();
//            else
//                container.RegisterType(typeof(IEtapaEducativaService),
//                                        typeContext,
//                                        new TransientLifetimeManager());

//            typeContext = Type.GetType("GAdUPIN.Application.DocumentacionRequeridaCandidatoService, GAdUPIN.Application");
//            if (typeContext == null)
//                new LogEvent("GAdUPIN.Application.DocumentacionRequeridaCandidatoService, GAdUPIN.Application.DocumentacionRequeridaCandidatoService no encontrado ").Raise();
//            else
//                container.RegisterType(typeof(IDocumentoDeTramite),
//                                        typeContext,
//                                        new TransientLifetimeManager());


//            typeContext = Type.GetType("GAdUPIN.Infrastructure.Logging.NLoggerService, GAdUPIN.Infrastructure");
//            if (typeContext == null)
//                new LogEvent("GAdUPIN.Application.NLogger, GAdUPIN.Application.NLogger no encontrado ").Raise();
//            else
//                container.RegisterType(typeof(INLogger),
//                                        typeContext,
//                                        new TransientLifetimeManager());

//        }

//        #endregion

//        #region Singleton

//        /// <summary>
//        /// Singleton instance
//        /// </summary>
//        static readonly IoC current = new IoC();

//        /// <summary>
//        /// Returns conversation manager 
//        /// </summary>
//        public static IoC Current
//        {
//            get
//            {
//                return current;
//            }
//        }

//        #endregion

//        public IUnityContainer GetContainer()
//        {
//            return container;
//        }
       
//    }
//}
