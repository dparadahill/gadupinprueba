using System;
using System.Web.Mvc;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.DAL;
using GAdUPIN.Infrastructure.Logging;
using Microsoft.Practices.Unity;
using Unity.Mvc3;

namespace GAdUPIN.Infrastructure
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();      
            new LogEvent("message to myself").Raise();

            Type typeContext = Type.GetType("GAdUPIN.DAL.DBContext, GAdUPIN.DAL");
            if (typeContext == null)
                new LogEvent("GAdUPIN.DAL.DBContext, GAdUPIN.DAL no encontrado ").Raise();
            //else
            //    container.RegisterType(typeof(IDBContext),
            //                                       typeContext,
            //                                       new ContainerControlledLifetimeManager(), new InjectionConstructor("ApplicationServices"));
            else
                container.RegisterType(typeof(IDBContext),
                                                   typeContext,
                                                   new ContainerControlledLifetimeManager(), new InjectionConstructor());


            typeContext = Type.GetType("GAdUPIN.Application.CandidatoService, GAdUPIN.Application");
            if (typeContext == null)
                new LogEvent("GAdUPIN.Application.CandidatoService, GAdUPIN.Application.CandidatoService no encontrado ").Raise();
            else
                container.RegisterType(typeof(ICandidatoService),
                                    Type.GetType("GAdUPIN.Application.CandidatoService, GAdUPIN.Application"),
                                    new TransientLifetimeManager());

            typeContext = Type.GetType("GAdUPIN.Application.AdminService, GAdUPIN.Application");
            if (typeContext == null)
                new LogEvent("GAdUPIN.Application.AdminService, GAdUPIN.Application.AdminService no encontrado ").Raise();
            else
                container.RegisterType(typeof(IAdminService),
                                        typeContext,
                                        new TransientLifetimeManager());

            typeContext = Type.GetType("GAdUPIN.Application.ColegioService, GAdUPIN.Application");
            if (typeContext == null)
                new LogEvent("GAdUPIN.Application.ColegioService, GAdUPIN.Application.ColegioService no encontrado ").Raise();
            else
                container.RegisterType(typeof(IColegioService),
                                        typeContext,
                                        new TransientLifetimeManager());

            typeContext = Type.GetType("GAdUPIN.Application.ContactoColegioService, GAdUPIN.Application");
            if (typeContext == null)
                new LogEvent("GAdUPIN.Application.ContactoColegioService, GAdUPIN.Application.ContactoColegioService no encontrado ").Raise();
            else
                container.RegisterType(typeof(IContactoColegioService),
                                        typeContext,
                                        new TransientLifetimeManager());

            typeContext = Type.GetType("GAdUPIN.Application.EncargadoService, GAdUPIN.Application");
            if (typeContext == null)
                new LogEvent("GAdUPIN.Application.EncargadoService, GAdUPIN.Application.EncargadoService no encontrado ").Raise();
            else
                container.RegisterType(typeof(IEncargadoService),
                                        typeContext,
                                        new TransientLifetimeManager());

            //typeContext = Type.GetType("GAdUPIN.Application.EntrevistadorService, GAdUPIN.Application");
            //if (typeContext == null)
            //    new LogEvent("GAdUPIN.Application.EntrevistadorService, GAdUPIN.Application.EntrevistadorService no encontrado ").Raise();
            //else
            //    container.RegisterType(typeof(IEntrevistadorService),
            //                            typeContext,
            //                            new TransientLifetimeManager());

            //typeContext = Type.GetType("GAdUPIN.Application.ExperienciaService, GAdUPIN.Application");
            //if (typeContext == null)
            //    new LogEvent("GAdUPIN.Application.ExperienciaService, GAdUPIN.Application.ExperienciaService no encontrado ").Raise();
            //else
            //    container.RegisterType(typeof(IExperienciaService),
            //                            typeContext,
            //                            new TransientLifetimeManager());

            typeContext = Type.GetType("GAdUPIN.Application.InformeEntrevistaEvaluacionService, GAdUPIN.Application");
            if (typeContext == null)
                new LogEvent("GAdUPIN.Application.InformeEntrevistaEvaluacionService, GAdUPIN.Application.InformeEntrevistaEvaluacionService no encontrado ").Raise();
            else
                container.RegisterType(typeof(IInformeEntrevistaEvaluacionService),
                                        typeContext,
                                        new TransientLifetimeManager());

            typeContext = Type.GetType("GAdUPIN.Application.PlazaColegioService, GAdUPIN.Application");
            if (typeContext == null)
                new LogEvent("GAdUPIN.Application.PlazaColegioService, GAdUPIN.Application.PlazaColegioService no encontrado ").Raise();
            else
                container.RegisterType(typeof(IPlazaColegioService),
                                        typeContext,
                                        new TransientLifetimeManager());

            typeContext = Type.GetType("GAdUPIN.Application.ComunidadAutonomaService, GAdUPIN.Application");
            if (typeContext == null)
                new LogEvent("GAdUPIN.Application.ComunidadAutonomaService, GAdUPIN.Application.ComunidadAutonomaService no encontrado ").Raise();
            else
                container.RegisterType(typeof(IComunidadAutonomaService),
                                        typeContext,
                                        new TransientLifetimeManager());

            typeContext = Type.GetType("GAdUPIN.Application.ProvinciaService, GAdUPIN.Application");
            if (typeContext == null)
                new LogEvent("GAdUPIN.Application.ProvinciaService, GAdUPIN.Application.ProvinciaService no encontrado ").Raise();
            else
                container.RegisterType(typeof(IProvinciaService),
                                        typeContext,
                                        new TransientLifetimeManager());

            typeContext = Type.GetType("GAdUPIN.Application.TipoAlojamientoService, GAdUPIN.Application");
            if (typeContext == null)
                new LogEvent("GAdUPIN.Application.TipoAlojamientoService, GAdUPIN.Application.TipoAlojamientoService no encontrado ").Raise();
            else
                container.RegisterType(typeof(ITipoAlojamientoService),
                                        typeContext,
                                        new TransientLifetimeManager());

            typeContext = Type.GetType("GAdUPIN.Application.EstatusService, GAdUPIN.Application");
            if (typeContext == null)
                new LogEvent("GAdUPIN.Application.EstatusService, GAdUPIN.Application.EstatusService no encontrado ").Raise();
            else
                container.RegisterType(typeof(IEstatusService),
                                        typeContext,
                                        new TransientLifetimeManager());

            typeContext = Type.GetType("GAdUPIN.Application.NacionalidadService, GAdUPIN.Application");
            if (typeContext == null)
                new LogEvent("GAdUPIN.Application.NacionalidadService, GAdUPIN.Application.NacionalidadService no encontrado ").Raise();
            else
                container.RegisterType(typeof(INacionalidadService),
                                        typeContext,
                                        new TransientLifetimeManager());

            typeContext = Type.GetType("GAdUPIN.Application.ProcedenciaService, GAdUPIN.Application");
            if (typeContext == null)
                new LogEvent("GAdUPIN.Application.ProcedenciaService, GAdUPIN.Application.ProcedenciaService no encontrado ").Raise();
            else
                container.RegisterType(typeof(IProcedenciaService),
                                        typeContext,
                                        new TransientLifetimeManager());

            typeContext = Type.GetType("GAdUPIN.Application.EtapaEducativaService, GAdUPIN.Application");
            if (typeContext == null)
                new LogEvent("GAdUPIN.Application.EtapaEducativaService, GAdUPIN.Application.EtapaEducativaService no encontrado ").Raise();
            else
                container.RegisterType(typeof(IEtapaEducativaService),
                                        typeContext,
                                        new TransientLifetimeManager());

            typeContext = Type.GetType("GAdUPIN.Application.EstadoVisadoService, GAdUPIN.Application");
            if (typeContext == null)
                new LogEvent("GAdUPIN.Application.EstadoVisadoService, GAdUPIN.Application.EstadoVisadoService no encontrado ").Raise();
            else
                container.RegisterType(typeof(IEstadoVisadoService),
                                        typeContext,
                                        new TransientLifetimeManager());

            typeContext = Type.GetType("GAdUPIN.Application.MotivoCierreFichaService, GAdUPIN.Application");
            if (typeContext == null)
                new LogEvent("GAdUPIN.Application.MotivoCierreFichaService, GAdUPIN.Application.MotivoCierreFichaService no encontrado ").Raise();
            else
                container.RegisterType(typeof(IMotivoCierreFichaService),
                                        typeContext,
                                        new TransientLifetimeManager());
            
            typeContext = Type.GetType("GAdUPIN.Application.UniversidadDeRegistroService, GAdUPIN.Application");
            if (typeContext == null)
                new LogEvent("GAdUPIN.Application.UniversidadDeRegistroService, GAdUPIN.Application.UniversidadDeRegistroService no encontrado ").Raise();
            else
                container.RegisterType(typeof(IUniversidadDeRegistroService),
                                        typeContext,
                                        new TransientLifetimeManager());

            //typeContext = Type.GetType("GAdUPIN.Application.PlazaSexoPreferenteService, GAdUPIN.Application");
            //if (typeContext == null)
            //    new LogEvent("GAdUPIN.Application.PlazaSexoPreferenteService, GAdUPIN.Application.PlazaSexoPreferenteService no encontrado ").Raise();
            //else
            //    container.RegisterType(typeof(IPlazaSexoPreferenteService),
            //                            typeContext,
            //                            new TransientLifetimeManager());


            typeContext = Type.GetType("GAdUPIN.Application.DocumentoDeTramiteService, GAdUPIN.Application");
            if (typeContext == null)
                new LogEvent("GAdUPIN.Application.DocumentoDeTramiteService, GAdUPIN.Application.DocumentoDeTramiteService no encontrado ").Raise();
            else
                container.RegisterType(typeof(IDocumentoDeTramiteService),
                                        typeContext,
                                        new TransientLifetimeManager());

            typeContext = Type.GetType("GAdUPIN.Application.DocumentoInscripcionService, GAdUPIN.Application");
            if (typeContext == null)
                new LogEvent("GAdUPIN.Application.DocumentoInscripcionService, GAdUPIN.Application.DocumentoInscripcionService no encontrado ").Raise();
            else
                container.RegisterType(typeof(IDocumentoInscripcionService),
                                        typeContext,
                                        new TransientLifetimeManager());




            typeContext = Type.GetType("GAdUPIN.Infrastructure.Logging.NLoggerService, GAdUPIN.Infrastructure");
            if (typeContext == null)
                new LogEvent("GAdUPIN.Application.NLogger, GAdUPIN.Application.NLogger no encontrado ").Raise();
            else
                container.RegisterType(typeof(INLogger),
                                        typeContext,
                                        new TransientLifetimeManager());



            return container;
        }
    }
}