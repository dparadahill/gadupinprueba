﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace GAdUPIN.WEB
{
    public static class Utils
    {
        public static JsonDataTable CreateJsDataTable(IEnumerable<object> list)
        {
            JsonDataTable jsDT = new JsonDataTable();

            var firstElement = list.FirstOrDefault();
            if (firstElement != null)
            {
                System.Reflection.PropertyInfo[] prps = firstElement.GetType().GetProperties();
                foreach (var property in prps)
                {
                    jsDT.add_Column(new JsonDataTable.JsDataColumn() { Title = property.Name, Class = property.PropertyType.Name});
                }
                foreach (var element in list)
                {
                    List<object> v1 = new List<object>();
                    foreach (var property in prps)
                    {
                        v1.Add(property.GetValue(element, null));
                    }
                    jsDT.add_Row(v1);
                }
            }
            return jsDT;
        }
    }
}
