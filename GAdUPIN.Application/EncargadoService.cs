﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.Application
{
    public class EncargadoService: IEncargadoService
    {
         private INLogger _NLoggerService;

        IDBContext _dbContext;
        public EncargadoService(IDBContext dbContext)
        {
            _dbContext = dbContext;
            //_NLoggerService =
            //    IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
            //    as INLogger;
        }

        public void Add(Encargado encargado, bool autosave = true)
        {
            _dbContext.Encargados.Add(encargado);
            if (autosave)
            {
                _dbContext.SaveChanges();
            }

        }

        public void Remove(Guid Id, bool autosave = true)
        {
            Encargado encargadodb = Get(Id);
            if (encargadodb != null)
            {
                _dbContext.Encargados.Remove(encargadodb);
                if (autosave)
                {
                    _dbContext.SaveChanges();
                }
            }
        }

        public void Update(Encargado encargado, bool autosave = true)
        {
            Encargado encargadodb = Get(encargado.Id);
            if (encargadodb != null)
            {
                encargadodb.DNI = (encargado.DNI ?? encargadodb.DNI);
                encargadodb.Nombre = (encargado.Nombre ?? encargadodb.Nombre);
                encargadodb.Apellido1 = (encargado.Apellido1 ?? encargadodb.Apellido1);
                encargadodb.Apellido2 = (encargado.Apellido2 ?? encargadodb.Apellido2);
                encargadodb.Email = (encargado.Email ?? encargadodb.Email);
                encargadodb.TelefonoDeContacto = (encargado.TelefonoDeContacto ?? encargadodb.TelefonoDeContacto);
                encargadodb.ComunidadAutonoma = (encargado.ComunidadAutonoma ?? encargadodb.ComunidadAutonoma);
                encargadodb.Candidatos = (encargado.Candidatos ?? encargadodb.Candidatos);
                encargadodb.EsContactoPrincipal = (encargado.EsContactoPrincipal != encargadodb.EsContactoPrincipal ? encargado.EsContactoPrincipal : encargadodb.EsContactoPrincipal);

                if(autosave)
                {
                    _dbContext.SaveChanges();
                }
            else
                {
                    throw new KeyNotFoundException(Messages.ContactoColegioNotFound);
                }

            }
        }

        public Encargado Get(Guid id)
        {
            return _dbContext.Encargados.Find(id);
        }

        public IEnumerable<Encargado> GetAll()
        {
            return _dbContext.Encargados;
        }



        
        //public IQueryable<Encargado> GetByCandidatos(Candidato candidato)
        //{
        //    return _dbContext.Encargados.Where(e => e.Candidato.Id == candidato.Id).Include(e => e.Candidato);
        //}

    }
}
