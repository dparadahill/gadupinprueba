﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.Application
{
    class EstatusService: IEstatusService
    {
        private INLogger _NLoggerService;

        IDBContext _dbContext;

        public EstatusService(IDBContext dbContext)
        {
            _dbContext = dbContext;
            //_NLoggerService =
            //    IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
            //    as INLogger;
        }
        public void Add(Estatus estatus, bool autosave = true)
        {
            _dbContext.Estatuses.Add(estatus);
            if (autosave) { _dbContext.SaveChanges(); }
        }

        public void Remove(Estatus estatus, bool autosave = true)
        {
            Estatus estatusdb = Get(estatus.Id);
            if (estatusdb != null)
            {
                _dbContext.Estatuses.Remove(estatusdb);
                if (autosave) { _dbContext.SaveChanges(); }
            }
        }

        public void Update(Estatus estatus, bool autosave = true)
        {
            Estatus estatusdb = Get(estatus.Id);
            if (estatusdb != null)
            {

                estatusdb.EstatusNombre = (estatus.EstatusNombre ?? estatusdb.EstatusNombre);
                
                if (autosave) { _dbContext.SaveChanges(); }
                else
                {
                    throw new KeyNotFoundException(Messages.GenericNotFound);
                }

            }
        }

        public Estatus Get(Guid id)
        {
            return _dbContext.Estatuses.Find(id);
        }

        
        public IQueryable<Estatus> GetAll()
        {
            return _dbContext.Estatuses;
            
        }

    }
}
