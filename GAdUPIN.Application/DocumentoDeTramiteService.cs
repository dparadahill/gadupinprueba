﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.Application
{
    public class DocumentoDeTramiteService: IDocumentoDeTramiteService
    {
        private INLogger _NLoggerService;

        IDBContext _dbContext;

        public DocumentoDeTramiteService(IDBContext dbContext)
        {
            _dbContext = dbContext;
            //_NLoggerService =
            //    IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
            //    as INLogger;
        }
        public void Add(CORE.Domain.DocumentoDeTramite documentoDeTramite, bool autosave = true)
        {
            _dbContext.DocumentosDeTramite.Add(documentoDeTramite);
            if (autosave) { _dbContext.SaveChanges(); }
        }


        public void Remove(Guid documentoDeTramite, bool autosave = true)
        {
            CORE.Domain.DocumentoDeTramite documentoDeTramitedb = Get(documentoDeTramite);
            if (documentoDeTramitedb != null)
            {
                _dbContext.DocumentosDeTramite.Remove(documentoDeTramitedb);
                if (autosave) { _dbContext.SaveChanges(); }
            }
        }

        public void Update(CORE.Domain.DocumentoDeTramite documentoDeTramite, bool autosave = true)
        {
            CORE.Domain.DocumentoDeTramite documentoDeTramitedb = Get(documentoDeTramite.Id);
            if (documentoDeTramitedb != null)
            {
                documentoDeTramitedb.NombreDocumento = (documentoDeTramite.NombreDocumento ?? documentoDeTramitedb.NombreDocumento);
                documentoDeTramitedb.FechaSubida = (documentoDeTramite.FechaSubida != documentoDeTramitedb.FechaSubida ? documentoDeTramite.FechaSubida : documentoDeTramitedb.FechaSubida);
                documentoDeTramitedb.Comentarios = (documentoDeTramite.Comentarios ?? documentoDeTramitedb.Comentarios);
                documentoDeTramitedb.Estado = (documentoDeTramite.Estado ?? documentoDeTramitedb.Estado);
                documentoDeTramitedb.Candidato = (documentoDeTramite.Candidato ?? documentoDeTramitedb.Candidato);

                if (autosave) { _dbContext.SaveChanges(); }
                else
                {
                    throw new KeyNotFoundException(Messages.GenericNotFound);
                }

            }
        }

        public CORE.Domain.DocumentoDeTramite Get(Guid id)
        {
            return _dbContext.DocumentosDeTramite.Find(id);
        }

        
        public IQueryable<CORE.Domain.DocumentoDeTramite> GetAll()
        {
            return _dbContext.DocumentosDeTramite;
            
        }

        public List<DocumentoDeTramite> GetByCandidato(Guid id)
        {
            return _dbContext.DocumentosDeTramite.Where(e => e.Candidato.Id == id).ToList();
            //return _dbContext.DocumentosDeTramite.Where(e => e.Candidato.Id == id).Include(e => e.NombreDocumento);
        }
    }
}
