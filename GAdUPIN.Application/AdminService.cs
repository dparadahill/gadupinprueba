﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.Application
{
    public class AdminService: IAdminService
      {
            private INLogger _NLoggerService;

            IDBContext _dbContext;
            public AdminService(IDBContext dbContext)
            {
                _dbContext = dbContext;
                //_NLoggerService =
                //    IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
                //    as INLogger;
            }

            public void Add(Admin admin, bool autosave = true)
            {
                _dbContext.Admins.Add(admin);
                if (autosave)
                {
                    _dbContext.SaveChanges();
                }
            }

            public void Remove(Admin admin, bool autosave = true)
            {
                Admin admindb = Get(admin.Id);
                if (admindb != null)
                {
                    _dbContext.Admins.Remove(admindb);
                    if (autosave)
                    {
                        _dbContext.SaveChanges();
                    }
                }
                else
                {
                    throw new ApplicationException(
                            string.Format(Messages.AdminNotFound, admin.Id),
                            new KeyNotFoundException());
                }
            }

            public void Update(Admin admin, bool autosave = true)
            {
                Admin admindb = Get(admin.Id);
                if (admindb != null)
                {

                    admindb.NombreAdmin = admin.NombreAdmin;
                    admindb.Apellido1 = admin.Apellido1;
                    admindb.Apellido2 = admin.Apellido2;
                    admindb.Email = admin.Email;
                    admindb.TelefonoDeContacto = admin.TelefonoDeContacto;
                    admindb.Usuario = admin.Usuario;
                    admindb.Password = admin.Password;
                
                    if (autosave)
                    {
                        _dbContext.SaveChanges();
                    }
                }
                else
                {
                    throw new KeyNotFoundException(Messages.AdminNotFound);
                }
            }

            public Admin Get(Guid id)
            {
                return _dbContext.Admins.Find(id);
            }

            public IQueryable<Admin> GetAll()
            {
                return _dbContext.Admins;
            }



            // private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        }
    }
