﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.Application
{
    public class MotivoCierreFichaService: IMotivoCierreFichaService
    {
         IDBContext _dbContext;

        public MotivoCierreFichaService(IDBContext dbContext)
        {
            _dbContext = dbContext;
        }



        public void Add(MotivoCierreFicha motivoCierreFicha, bool autosave = true)
        {
            _dbContext.MotivosCierreFicha.Add(motivoCierreFicha);
            if (autosave) { _dbContext.SaveChanges(); }
        }

        public void Remove(MotivoCierreFicha motivoCierreFicha, bool autosave = true)
        {
            MotivoCierreFicha motivoCierreFichadb = Get(motivoCierreFicha.Id);
            if (motivoCierreFichadb != null)
            {
                _dbContext.MotivosCierreFicha.Remove(motivoCierreFichadb);
                if (autosave) { _dbContext.SaveChanges(); }
            }
        }

        public void Update(MotivoCierreFicha motivoCierreFicha, bool autosave = true)
        {
            MotivoCierreFicha motivoCierreFichadb = Get(motivoCierreFicha.Id);
            if (motivoCierreFichadb != null)
            {

                motivoCierreFichadb.Motivo = (motivoCierreFicha.Motivo ?? motivoCierreFichadb.Motivo);

                if (autosave) { _dbContext.SaveChanges(); }
                else
                {
                    throw new KeyNotFoundException(Messages.GenericNotFound);
                }

            }
        }

        public MotivoCierreFicha Get(Guid id)
        {
            return _dbContext.MotivosCierreFicha.Find(id);
        }

        public IQueryable<MotivoCierreFicha> GetAll()
        {
            return _dbContext.MotivosCierreFicha;
        }
    }
}
