﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.Application
{
    public class ProvinciaService: IProvinciaService
    {
         private INLogger _NLoggerService;

            IDBContext _dbContext;
            public ProvinciaService(IDBContext dbContext)
            {
                _dbContext = dbContext;
                //_NLoggerService =
                //    IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
                //    as INLogger;
            }

            public void Add(Provincia provincia, bool autosave = true)
            {
                _dbContext.Provincias.Add(provincia);
                if (autosave)
                {
                    _dbContext.SaveChanges();
                }
            }

            public void Remove(Provincia provincia, bool autosave = true)
            {
                Provincia provinciadb = Get(provincia.Id);
                if (provinciadb != null)
                {
                    _dbContext.Provincias.Remove(provinciadb);
                    if (autosave)
                    {
                        _dbContext.SaveChanges();
                    }
                }
                else
                {
                    throw new ApplicationException(
                            string.Format(Messages.PlazaAntiguaNotFound, provincia.Id),
                            new KeyNotFoundException());
                }
            }

            public void Update(Provincia provincia, bool autosave = true)
            {
                Provincia provinciadb = Get(provincia.Id);
                if (provinciadb != null)
                {

                    
                    provinciadb.NombreProvincia = provincia.NombreProvincia ?? provinciadb.NombreProvincia;
                    provinciadb.ComunidadAutonoma = provincia.ComunidadAutonoma ?? provinciadb.ComunidadAutonoma;
                
                    if (autosave)
                    {
                        _dbContext.SaveChanges();
                    }
                }
                else
                {
                    throw new KeyNotFoundException(Messages.ProvinciaNotFound);
                }
            }

            public Provincia Get(Guid id)
            {
                return _dbContext.Provincias.Find(id);
            }


            public IQueryable<Provincia> GetAll()
            {
                return _dbContext.Provincias;
            }

            
            public List<Provincia> GetByComunidadAutonoma(Guid id)
            {
                return _dbContext.Provincias.Where(e => e.ComunidadAutonoma.Id == id).ToList();
            }

            public Provincia GetWithComunidad(Guid id)
            {
                return _dbContext.Provincias
                    .Include(i => i.ComunidadAutonoma).FirstOrDefault(x => x.ComunidadAutonoma.Id == id);
                
            }
            
        // private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
    }
}
