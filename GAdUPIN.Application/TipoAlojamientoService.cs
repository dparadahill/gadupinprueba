﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.Application
{
    class TipoAlojamientoService: ITipoAlojamientoService
    {
        private INLogger _NLoggerService;

        IDBContext _dbContext;

        public TipoAlojamientoService(IDBContext dbContext)
        {
            _dbContext = dbContext;
            //_NLoggerService =
            //    IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
            //    as INLogger;
        }
        public void Add(TipoAlojamiento tipoAlojamiento, bool autosave = true)
        {
            _dbContext.TipoAlojamientos.Add(tipoAlojamiento);
            if (autosave) { _dbContext.SaveChanges(); }
        }

        public void Remove(Guid Id, bool autosave = true)
        {
            TipoAlojamiento tipoAlojamientodb = Get(Id);
            if (tipoAlojamientodb != null)
            {
                _dbContext.TipoAlojamientos.Remove(tipoAlojamientodb);
                if (autosave) { _dbContext.SaveChanges(); }
            }
        }

        public void Update(TipoAlojamiento tipoAlojamiento, bool autosave = true)
        {
            TipoAlojamiento tipoAlojamientodb = Get(tipoAlojamiento.Id);
            if (tipoAlojamientodb != null)
            {

                tipoAlojamientodb.TipoAlojamientoNombre = (tipoAlojamiento.TipoAlojamientoNombre ?? tipoAlojamientodb.TipoAlojamientoNombre);
                
                if (autosave) { _dbContext.SaveChanges(); }
                else
                {
                    throw new KeyNotFoundException(Messages.GenericNotFound);
                }

            }
        }

        public TipoAlojamiento Get(Guid id)
        {
            return _dbContext.TipoAlojamientos.Find(id);
        }

        
        public IQueryable<TipoAlojamiento> GetAll()
        {
            return _dbContext.TipoAlojamientos;
            
        }

    }
}
