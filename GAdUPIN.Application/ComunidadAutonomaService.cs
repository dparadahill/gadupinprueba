﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.Application
{
    public class ComunidadAutonomaService: IComunidadAutonomaService
    {  private INLogger _NLoggerService;

        IDBContext _dbContext;
        public ComunidadAutonomaService(IDBContext dbContext)
        {
            _dbContext = dbContext;
            //_NLoggerService =
            //    IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
            //    as INLogger;
        }

        public void Add(ComunidadAutonoma comunidadAutonoma, bool autosave = true)
        {
            _dbContext.ComunidadesAutonomas.Add(comunidadAutonoma);
            if (autosave){ _dbContext.SaveChanges();}
        }

        public void Remove(ComunidadAutonoma comunidadAutonoma, bool autosave = true)
        {
            ComunidadAutonoma comunidadAutonomadb = Get(comunidadAutonoma.Id);
            if (comunidadAutonomadb != null)
            {
                _dbContext.ComunidadesAutonomas.Remove(comunidadAutonomadb);
                if (autosave) { _dbContext.SaveChanges(); }
            }
        }

        public void Update(ComunidadAutonoma comunidadAutonoma, bool autosave = true)
        {
            ComunidadAutonoma comunidadAutonomadb = Get(comunidadAutonoma.Id);
            if (comunidadAutonomadb != null)
            {
                comunidadAutonomadb.NombreComunidadAutonoma = comunidadAutonoma.NombreComunidadAutonoma ?? comunidadAutonomadb.NombreComunidadAutonoma;
                comunidadAutonomadb.Provincias = comunidadAutonoma.Provincias ?? comunidadAutonomadb.Provincias;
                
                if(autosave) { _dbContext.SaveChanges(); }
            else
                {
                    throw new KeyNotFoundException(Messages.ComunidadNotFound);
                }

            }
        }

        public ComunidadAutonoma Get(Guid id)
        {
            return _dbContext.ComunidadesAutonomas.Find(id);
        }

        public ComunidadAutonoma GetWithProvincias(Guid id)
        {
            return _dbContext.ComunidadesAutonomas.Include("Provincia")
                .Where(p => p.Id == id).SingleOrDefault();
        }

        public IQueryable<ComunidadAutonoma> GetAll()
        {
            return _dbContext.ComunidadesAutonomas;
        }

      
    }
}
