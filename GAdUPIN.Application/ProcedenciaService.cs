﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.Application
{
    class ProcedenciaService:IProcedenciaService
    {
        private INLogger _NLoggerService;

        IDBContext _dbContext;

        public ProcedenciaService(IDBContext dbContext)
        {
            _dbContext = dbContext;
            //_NLoggerService =
            //    IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
            //    as INLogger;
        }
        public void Add(Procedencia procedencia, bool autosave = true)
        {
            try
            {
                _dbContext.Procedencias.Add(procedencia);
                if (autosave)
                {
                    _dbContext.SaveChanges();
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        public void Remove(Guid Id, bool autosave = true)
        {
            Procedencia procedenciadb = Get(Id);
            if (procedenciadb != null)
            {
                _dbContext.Procedencias.Remove(procedenciadb);
                if (autosave) { _dbContext.SaveChanges(); }
            }
        }

        public void Update(Procedencia procedencia, bool autosave = true)
        {
            Procedencia procedenciadb = Get(procedencia.Id);
            if (procedenciadb != null)
            {

                procedenciadb.NombreProcedencia = (procedencia.NombreProcedencia ?? procedenciadb.NombreProcedencia);
                if (autosave) { _dbContext.SaveChanges(); }
                else
                {
                    throw new KeyNotFoundException(Messages.GenericNotFound);
                }

            }
        }

        public Procedencia Get(Guid id)
        {
            return _dbContext.Procedencias.Find(id);
        }

        
        public IQueryable<Procedencia> GetAll()
        {
            return _dbContext.Procedencias;
            
        }

    }
}
