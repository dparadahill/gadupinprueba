﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using GAdUPIN.CORE.Contracts.Services;
//using GAdUPIN.CORE.DAL;
//using GAdUPIN.CORE.Domain;

//namespace GAdUPIN.Application
//{
//    public class PlazaSexoPreferenteService: IPlazaSexoPreferenteService
//    {
//    IDBContext _dbContext;

//        public PlazaSexoPreferenteService(IDBContext dbContext)
//        {
//            _dbContext = dbContext;
//        }



//        public void Add(PlazaSexoPreferente plazaSexoPreferente, bool autosave = true)
//        {
//            _dbContext.PlazaSexosPreferentes.Add(plazaSexoPreferente);
//            if (autosave) { _dbContext.SaveChanges(); }
//        }

//        public void Remove(PlazaSexoPreferente plazaSexoPreferente, bool autosave = true)
//        {
//            PlazaSexoPreferente plazaSexoPreferentedb = Get(plazaSexoPreferente.Id);
//            if (plazaSexoPreferentedb != null)
//            {
//                _dbContext.PlazaSexosPreferentes.Remove(plazaSexoPreferentedb);
//                if (autosave) { _dbContext.SaveChanges(); }
//            }
//        }

//        public void Update(PlazaSexoPreferente plazaSexoPreferente, bool autosave = true)
//        {
//            PlazaSexoPreferente plazaSexoPreferentedb = Get(plazaSexoPreferente.Id);
//            if (plazaSexoPreferentedb != null)
//            {

//                plazaSexoPreferentedb.NombreSexoPreferente = (plazaSexoPreferente.NombreSexoPreferente ?? plazaSexoPreferentedb.NombreSexoPreferente);

//                if (autosave) { _dbContext.SaveChanges(); }
//                else
//                {
//                    throw new KeyNotFoundException(Messages.GenericNotFound);
//                }

//            }
//        }

//        public PlazaSexoPreferente Get(Guid id)
//        {
//            return _dbContext.PlazaSexosPreferentes.Find(id);
//        }

//        public IQueryable<PlazaSexoPreferente> GetAll()
//        {
//            return _dbContext.PlazaSexosPreferentes;
//        }
//    }
//}
