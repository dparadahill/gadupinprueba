﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.Application
{
    public class EtapaEducativaService: IEtapaEducativaService
    {
         private INLogger _NLoggerService;

        IDBContext _dbContext;

        public EtapaEducativaService(IDBContext dbContext)
        {
            _dbContext = dbContext;
            //_NLoggerService =
            //    IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
            //    as INLogger;
        }
        public void Add(EtapaEducativa etapaEducativa, bool autosave = true)
        {
            _dbContext.EtapasEducativas.Add(etapaEducativa);
            if (autosave) { _dbContext.SaveChanges(); }
        }

        public void Remove(Guid Id, bool autosave = true)
        {
            EtapaEducativa etapaEducativadb = Get(Id);
            if (etapaEducativadb != null)
            {
                _dbContext.EtapasEducativas.Remove(etapaEducativadb);
                if (autosave) { _dbContext.SaveChanges(); }
            }
        }

        public void SoftDelete(EtapaEducativa etapaEducativa, bool autosave = true)
        {
            EtapaEducativa etapaEducativadb = Get(etapaEducativa.Id);
            if (etapaEducativadb != null)
            {

                etapaEducativadb.EtapaEducativaNombre = "*Borrado*";
                if (autosave) { _dbContext.SaveChanges(); }
                else
                {
                    throw new KeyNotFoundException(Messages.GenericNotFound);
                }

            }
        }

        public void Update(EtapaEducativa etapaEducativa, bool autosave = true)
        {
            EtapaEducativa etapaEducativadb = Get(etapaEducativa.Id);
            if (etapaEducativadb != null)
            {

                etapaEducativadb.EtapaEducativaNombre = (etapaEducativa.EtapaEducativaNombre ?? etapaEducativadb.EtapaEducativaNombre);
                if (autosave) { _dbContext.SaveChanges(); }
                else
                {
                    throw new KeyNotFoundException(Messages.GenericNotFound);
                }

            }
        }

        public EtapaEducativa Get(Guid id)
        {
            return _dbContext.EtapasEducativas.Find(id);
        }

        
        public IQueryable<EtapaEducativa> GetAll()
        {
            return _dbContext.EtapasEducativas;
            
        }

    }
}

 
