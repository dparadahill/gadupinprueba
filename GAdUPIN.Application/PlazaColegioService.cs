﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.Application
{
    public class PlazaColegioService: IPlazaColegioService
    {
    private INLogger _NLoggerService;

        IDBContext _dbContext;
        public PlazaColegioService(IDBContext dbContext)
        {
            _dbContext = dbContext;
            //_NLoggerService =
            //    IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
            //    as INLogger;
        }

        public void Add(PlazaColegio plazaColegio, bool autosave = true)
        {
            _dbContext.PlazasDelColegio.Add(plazaColegio);
            if (autosave)
            {
                _dbContext.SaveChanges();
            }
        }

        public void Remove(PlazaColegio plazaColegio, bool autosave = true)
        {
            PlazaColegio plazaColegiodb = Get(plazaColegio.Id);
            if (plazaColegiodb != null)
            {
                _dbContext.PlazasDelColegio.Remove(plazaColegiodb);
                if (autosave)
                {
                    _dbContext.SaveChanges();
                }
            }
            else
            {
                throw new ApplicationException(
                        string.Format(Messages.PlazaAntiguaNotFound, plazaColegio.Id),
                        new KeyNotFoundException());
            }
        }

        public void Update(PlazaColegio plazaColegio, bool autosave = true)
        {
            PlazaColegio plazaColegiodb = Get(plazaColegio.Id);
            if (plazaColegiodb != null)
            {
                plazaColegiodb.FechaInicio = (plazaColegio.FechaInicio != plazaColegiodb.FechaInicio ? plazaColegio.FechaInicio : plazaColegiodb.FechaInicio);
                plazaColegiodb.FechaFin = (plazaColegio.FechaFin != plazaColegiodb.FechaFin ? plazaColegio.FechaFin : plazaColegiodb.FechaFin);
                plazaColegiodb.BecaCuantia = (plazaColegio.BecaCuantia != plazaColegiodb.BecaCuantia ? plazaColegio.BecaCuantia : plazaColegiodb.BecaCuantia);
                plazaColegiodb.PreferenciaSexo = (plazaColegio.PreferenciaSexo != plazaColegiodb.PreferenciaSexo ? plazaColegio.PreferenciaSexo : plazaColegiodb.PreferenciaSexo);
                plazaColegiodb.EtapaEducativa = (plazaColegio.EtapaEducativa != plazaColegiodb.EtapaEducativa ? plazaColegio.EtapaEducativa : plazaColegiodb.EtapaEducativa);
                plazaColegiodb.Entrevista = (plazaColegio.Entrevista != plazaColegiodb.Entrevista ? plazaColegio.Entrevista : plazaColegiodb.Entrevista);
                plazaColegiodb.CondicionesFirmadas = (plazaColegio.CondicionesFirmadas != plazaColegiodb.CondicionesFirmadas ? plazaColegio.CondicionesFirmadas : plazaColegiodb.CondicionesFirmadas);
                plazaColegiodb.Comentarios = (plazaColegio.Comentarios != plazaColegiodb.Comentarios ? plazaColegio.Comentarios : plazaColegiodb.Comentarios);
                plazaColegiodb.Colegio = (plazaColegio.Colegio != plazaColegiodb.Colegio ? plazaColegio.Colegio : plazaColegiodb.Colegio);
                plazaColegiodb.PlazaContratada = (plazaColegio.PlazaContratada != plazaColegiodb.PlazaContratada ? plazaColegio.PlazaContratada : plazaColegiodb.PlazaContratada);
                plazaColegiodb.CandidatoAsignado = (plazaColegio.CandidatoAsignado != plazaColegiodb.CandidatoAsignado ? plazaColegio.CandidatoAsignado : plazaColegiodb.CandidatoAsignado);
                plazaColegiodb.NumeroHorasPlaza = (plazaColegio.NumeroHorasPlaza != plazaColegiodb.NumeroHorasPlaza ? plazaColegio.NumeroHorasPlaza : plazaColegiodb.NumeroHorasPlaza);

                plazaColegiodb.NacionalidadPreferente = (plazaColegio.NacionalidadPreferente ?? plazaColegiodb.NacionalidadPreferente);
                plazaColegiodb.CodigoPlaza = (plazaColegio.CodigoPlaza ?? plazaColegiodb.CodigoPlaza);
                plazaColegiodb.DatoAdicional = (plazaColegio.DatoAdicional ?? plazaColegiodb.DatoAdicional);
                plazaColegiodb.CondicionesOfrecidas = (plazaColegio.CondicionesOfrecidas ?? plazaColegiodb.CondicionesOfrecidas);
                

                if (autosave)
                {
                    _dbContext.SaveChanges();
                }
            }
            else
            {
                throw new KeyNotFoundException(Messages.PlazaAntiguaNotFound);
            }
        }

        public PlazaColegio Get(Guid id)
        {
            //return _dbContext.PlazasDelColegio.Find(id);
            //return _dbContext.PlazasDelColegio.Where(e => e.Id == id).FirstOrDefault().Include(e => e.CandidatoAsignado);
            //return _dbContext.PlazasDelColegio.Include("CandidatoAsignado").Where(p => p.Id == id).SingleOrDefault();
            return _dbContext.PlazasDelColegio.Include("CandidatoAsignado").SingleOrDefault(p => p.Id == id);
                

        }

        public IQueryable<PlazaColegio> GetAll()
        {
            return _dbContext.PlazasDelColegio;
        }

            
        public List<PlazaColegio> GetByColegio(Guid Id)
        {
                
            return _dbContext.PlazasDelColegio.Where(e => e.Colegio.Id == Id).Include(e => e.Colegio).ToList();
        }

        public PlazaColegio GetWithColegio(Guid id)
        {
            return _dbContext.PlazasDelColegio
                .Include(i => i.Colegio).FirstOrDefault(x => x.Colegio.Id == id);
        }

        public PlazaColegio GetWithEtapaEducativa(Guid id)
        {
                
            return _dbContext.PlazasDelColegio.Include(x => x.EtapaEducativa).FirstOrDefault();
        }



        public void Desasignar(Guid id, bool autosave = true)
        {
            PlazaColegio plazaColegiodb = Get(id);
            if (plazaColegiodb != null)
            {
                plazaColegiodb.PlazaContratada = false;
                plazaColegiodb.CandidatoAsignado = null;
                
                if (autosave)
                {
                    _dbContext.SaveChanges();
                }
            }
            else
            {
                throw new KeyNotFoundException(Messages.PlazaAntiguaNotFound);
            }
        }
        

        // private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
    }
}
