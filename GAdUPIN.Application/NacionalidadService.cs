﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.Application
{
    class NacionalidadService: INacionalidadService
    {
        private INLogger _NLoggerService;

        IDBContext _dbContext;

        public NacionalidadService(IDBContext dbContext)
        {
            _dbContext = dbContext;
            //_NLoggerService =
            //    IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
            //    as INLogger;
        }
        public void Add(Nacionalidad nacionalidad, bool autosave = true)
        {
            try
            {
                _dbContext.Nacionalidades.Add(nacionalidad);
                if (autosave)
                {
                    _dbContext.SaveChanges();
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        public void Remove(Guid Id, bool autosave = true)
        {
            Nacionalidad nacionalidaddb = Get(Id);
            if (nacionalidaddb != null)
            {
                _dbContext.Nacionalidades.Remove(nacionalidaddb);
                if (autosave) { _dbContext.SaveChanges(); }
            }
        }

        public void Update(Nacionalidad nacionalidad, bool autosave = true)
        {
            Nacionalidad nacionalidaddb = Get(nacionalidad.Id);
            if (nacionalidaddb != null)
            {

                nacionalidaddb.NombreNacionalidad = (nacionalidad.NombreNacionalidad ?? nacionalidaddb.NombreNacionalidad);
                nacionalidaddb.Visado = (nacionalidad.Visado != nacionalidaddb.Visado ? nacionalidad.Visado : nacionalidaddb.Visado);
                if (autosave) { _dbContext.SaveChanges(); }
                else
                {
                    throw new KeyNotFoundException(Messages.GenericNotFound);
                }

            }
        }

        public Nacionalidad Get(Guid id)
        {
            return _dbContext.Nacionalidades.Find(id);
        }

        
        public IQueryable<Nacionalidad> GetAll()
        {
            return _dbContext.Nacionalidades;
            
        }

    }
}
