﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.Application
{
    public class InformeEntrevistaEvaluacionService : IInformeEntrevistaEvaluacionService
    {

    private INLogger _NLoggerService;

            IDBContext _dbContext;
            public InformeEntrevistaEvaluacionService(IDBContext dbContext)
            {
                _dbContext = dbContext;
                //_NLoggerService =
                //    IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
                //    as INLogger;
            }

            public void Add(InformeEntrevistaEvaluacion informeEntrevistaEvaluacion, bool autosave = true)
            {
                _dbContext.InformeEntrevistaEvaluaciones.Add(informeEntrevistaEvaluacion);
                if (autosave)
                {
                    _dbContext.SaveChanges();
                }
            }

            public void Remove(InformeEntrevistaEvaluacion informeEntrevistaEvaluacion, bool autosave = true)
            {
                InformeEntrevistaEvaluacion informeEntrevistaEvaluaciondb = Get(informeEntrevistaEvaluacion.Id);
                if (informeEntrevistaEvaluaciondb != null)
                {
                    _dbContext.InformeEntrevistaEvaluaciones.Remove(informeEntrevistaEvaluaciondb);
                    if (autosave)
                    {
                        _dbContext.SaveChanges();
                    }
                }
                else
                {
                    throw new ApplicationException(
                            string.Format(Messages.AdminNotFound, informeEntrevistaEvaluacion.Id),
                            new KeyNotFoundException());
                }
            }

            public void Update(InformeEntrevistaEvaluacion informeEntrevistaEvaluacion, bool autosave = true)
            {
                InformeEntrevistaEvaluacion informeEntrevistaEvaluaciondb = Get(informeEntrevistaEvaluacion.Id);
                if (informeEntrevistaEvaluaciondb != null)
                {

                    informeEntrevistaEvaluaciondb.Fecha = ( informeEntrevistaEvaluacion.Fecha != informeEntrevistaEvaluaciondb.Fecha ? informeEntrevistaEvaluacion.Fecha : informeEntrevistaEvaluaciondb.Fecha);
                    informeEntrevistaEvaluaciondb.Titulacion = (informeEntrevistaEvaluacion.Titulacion ?? informeEntrevistaEvaluaciondb.Titulacion);
                    informeEntrevistaEvaluaciondb.ObsGenerales = ( informeEntrevistaEvaluacion.ObsGenerales ?? informeEntrevistaEvaluaciondb.ObsGenerales);
                    informeEntrevistaEvaluaciondb.AnalisisEquipoUP = ( informeEntrevistaEvaluacion.AnalisisEquipoUP ?? informeEntrevistaEvaluaciondb.AnalisisEquipoUP);
                    informeEntrevistaEvaluaciondb.CumpleRequisitos = (informeEntrevistaEvaluacion.CumpleRequisitos != informeEntrevistaEvaluaciondb.CumpleRequisitos ? informeEntrevistaEvaluacion.CumpleRequisitos : informeEntrevistaEvaluaciondb.CumpleRequisitos);
                    informeEntrevistaEvaluaciondb.ValGlobal = (informeEntrevistaEvaluacion.ValGlobal != informeEntrevistaEvaluaciondb.ValGlobal ? informeEntrevistaEvaluacion.ValGlobal : informeEntrevistaEvaluaciondb.ValGlobal);
                    informeEntrevistaEvaluaciondb.ValRequisitos = (informeEntrevistaEvaluacion.ValRequisitos != informeEntrevistaEvaluaciondb.ValRequisitos ? informeEntrevistaEvaluacion.ValRequisitos : informeEntrevistaEvaluaciondb.ValRequisitos);
                    informeEntrevistaEvaluaciondb.ValInteres = (informeEntrevistaEvaluacion.ValInteres != informeEntrevistaEvaluaciondb.ValInteres ? informeEntrevistaEvaluacion.ValInteres : informeEntrevistaEvaluaciondb.ValInteres);
                    informeEntrevistaEvaluaciondb.ValEducacion = ( informeEntrevistaEvaluacion.ValEducacion!= informeEntrevistaEvaluaciondb.ValEducacion ? informeEntrevistaEvaluacion.ValEducacion : informeEntrevistaEvaluaciondb.ValEducacion);
                    informeEntrevistaEvaluaciondb.ValHabProfesionales = ( informeEntrevistaEvaluacion.ValHabProfesionales!= informeEntrevistaEvaluaciondb.ValHabProfesionales ? informeEntrevistaEvaluacion.ValHabProfesionales : informeEntrevistaEvaluaciondb.ValHabProfesionales);
                    informeEntrevistaEvaluaciondb.ComentariosInteres = ( informeEntrevistaEvaluacion.ComentariosInteres ?? informeEntrevistaEvaluaciondb.ComentariosInteres);
                    informeEntrevistaEvaluaciondb.ValClaridadDelIdioma = (informeEntrevistaEvaluacion.ValClaridadDelIdioma != informeEntrevistaEvaluaciondb.ValClaridadDelIdioma ? informeEntrevistaEvaluacion.ValClaridadDelIdioma : informeEntrevistaEvaluaciondb.ValClaridadDelIdioma);
                    informeEntrevistaEvaluaciondb.ValExpresividadSoltura = (informeEntrevistaEvaluacion.ValExpresividadSoltura != informeEntrevistaEvaluaciondb.ValExpresividadSoltura ? informeEntrevistaEvaluacion.ValExpresividadSoltura : informeEntrevistaEvaluaciondb.ValExpresividadSoltura);
                    informeEntrevistaEvaluaciondb.ValConfianzaPersonal = (informeEntrevistaEvaluacion.ValConfianzaPersonal != informeEntrevistaEvaluaciondb.ValConfianzaPersonal ? informeEntrevistaEvaluacion.ValConfianzaPersonal : informeEntrevistaEvaluaciondb.ValConfianzaPersonal);
                    informeEntrevistaEvaluaciondb.ComentariosAnalisisLinguistico = ( informeEntrevistaEvaluacion.ComentariosAnalisisLinguistico ?? informeEntrevistaEvaluaciondb.ComentariosAnalisisLinguistico);
                    informeEntrevistaEvaluaciondb.Idiomas = ( informeEntrevistaEvaluacion.Idiomas ?? informeEntrevistaEvaluaciondb.Idiomas);
                    informeEntrevistaEvaluaciondb.CondicionesDelPrograma = ( informeEntrevistaEvaluacion.CondicionesDelPrograma?? informeEntrevistaEvaluaciondb.CondicionesDelPrograma);
                    informeEntrevistaEvaluaciondb.Referencias = ( informeEntrevistaEvaluacion.Referencias?? informeEntrevistaEvaluaciondb.Referencias);
                    informeEntrevistaEvaluaciondb.SituacionEconomica = ( informeEntrevistaEvaluacion.SituacionEconomica?? informeEntrevistaEvaluaciondb.SituacionEconomica);
                    informeEntrevistaEvaluaciondb.DietaAlimentacion = ( informeEntrevistaEvaluacion.DietaAlimentacion?? informeEntrevistaEvaluaciondb.DietaAlimentacion);
                    informeEntrevistaEvaluaciondb.Religion = ( informeEntrevistaEvaluacion.Religion?? informeEntrevistaEvaluaciondb.Religion);
                    informeEntrevistaEvaluaciondb.Otros = ( informeEntrevistaEvaluacion.Otros?? informeEntrevistaEvaluaciondb.Otros);
                    informeEntrevistaEvaluaciondb.Completado = (informeEntrevistaEvaluacion.Completado != informeEntrevistaEvaluaciondb.Completado ? informeEntrevistaEvaluacion.Completado : informeEntrevistaEvaluaciondb.Completado);
                    //informeEntrevistaEvaluaciondb.Entrevistador = ( informeEntrevistaEvaluacion.Entrevistador ?? informeEntrevistaEvaluaciondb.Entrevistador);
                    informeEntrevistaEvaluaciondb.Candidato = ( informeEntrevistaEvaluacion.Candidato ?? informeEntrevistaEvaluaciondb.Candidato);
                    
                
                    if (autosave)
                    {
                        _dbContext.SaveChanges();
                    }
                }
                else
                {
                    throw new KeyNotFoundException(Messages.InformeNotFound);
                }
            }

            public InformeEntrevistaEvaluacion Get(Guid id)
            {
                return _dbContext.InformeEntrevistaEvaluaciones.Find(id);
            }

            public IQueryable<InformeEntrevistaEvaluacion> GetAll()
            {
                return _dbContext.InformeEntrevistaEvaluaciones
                    .Include(c => c.Candidato);
            }

            //public IQueryable<InformeEntrevistaEvaluacion> GetByEntrevistador(Entrevistador entrevistador)
            //{
            //    return _dbContext.InformeEntrevistaEvaluaciones.Where(e => e.Entrevistador.Id == entrevistador.Id).Include(e => e.Entrevistador);
            //}



            // private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        }
    }
