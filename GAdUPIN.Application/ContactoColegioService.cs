﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.Application
{
    public class ContactoColegioService: IContactoColegioService
    {
        private INLogger _NLoggerService;

        IDBContext _dbContext;
        public ContactoColegioService(IDBContext dbContext)
        {
            _dbContext = dbContext;
            //_NLoggerService =
            //    IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
            //    as INLogger;
        }

        public void Add(ContactoColegio contactoColegio, bool autosave = true)
        {
            _dbContext.ContactosDelColegio.Add(contactoColegio);
            if (autosave)
            {
                _dbContext.SaveChanges();
            }

        }

        public void Remove(Guid Id, bool autosave = true)
        {
            ContactoColegio contactoColegiodb = Get(Id);
            if (contactoColegiodb != null)
            {
                _dbContext.ContactosDelColegio.Remove(contactoColegiodb);
                if (autosave)
                {
                    _dbContext.SaveChanges();
                }
            }
        }

        public void Update(ContactoColegio contactoColegio, bool autosave = true)
        {
            ContactoColegio contactoColegiodb = Get(contactoColegio.Id);
            if (contactoColegiodb != null)
            {
                contactoColegiodb.NombreContactoColegio = contactoColegio.NombreContactoColegio ?? contactoColegiodb.NombreContactoColegio;
                contactoColegiodb.Apellido1 = contactoColegio.Apellido1 ?? contactoColegiodb.Apellido1;
                contactoColegiodb.Apellido2 = contactoColegio.Apellido2 ?? contactoColegiodb.Apellido2;
                contactoColegiodb.Email = contactoColegio.Email ?? contactoColegiodb.Email;
                contactoColegiodb.TelefonoDeContacto = contactoColegio.TelefonoDeContacto ?? contactoColegiodb.TelefonoDeContacto;
                contactoColegiodb.ContactoPrincipal = contactoColegio.ContactoPrincipal != contactoColegiodb.ContactoPrincipal ? contactoColegio.ContactoPrincipal : contactoColegiodb.ContactoPrincipal;
                contactoColegiodb.Colegio = contactoColegio.Colegio ?? contactoColegiodb.Colegio;
                contactoColegiodb.DNI = contactoColegio.DNI ?? contactoColegiodb.DNI;
                contactoColegiodb.Tipo = contactoColegio.Tipo ?? contactoColegiodb.Tipo;
                contactoColegiodb.ComentariosContacto = contactoColegio.ComentariosContacto ?? contactoColegiodb.ComentariosContacto;

                if(autosave)
                {
                    _dbContext.SaveChanges();
                }
            else
                {
                    throw new KeyNotFoundException(Messages.ContactoColegioNotFound);
                }

            }
        }

        public ContactoColegio Get(Guid id)
        {
            //return _dbContext.ContactosDelColegio.Find(id);

            return _dbContext.ContactosDelColegio.Include("Colegio").SingleOrDefault(p => p.Id == id);
        }

        public IQueryable<ContactoColegio> GetAll()
        {
            return _dbContext.ContactosDelColegio;
        }

        public List<ContactoColegio> GetByColegio(Guid Id)
        {
            return _dbContext.ContactosDelColegio.Where(e => e.Colegio.Id == Id).Include(e => e.Colegio).ToList();
        }
    }
}
