﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using GAdUPIN.CORE.Contracts.Services;
//using GAdUPIN.CORE.DAL;
//using GAdUPIN.CORE.Domain;
//using GAdUPIN.Infrastructure;
//using GAdUPIN.Infrastructure.Logging;

//namespace GAdUPIN.Application
//{
//    public class ExperienciaService: IExperienciaService
//    {
//     private INLogger _NLoggerService;

//            IDBContext _dbContext;
//            public ExperienciaService(IDBContext dbContext)
//            {
//                _dbContext = dbContext;
//                //_NLoggerService =
//                //    IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
//                //    as INLogger;
//            }

//            public void Add(NuevaExperiencia nuevaExperiencia, bool autosave = true)
//            {
//                _dbContext.Experiencias.Add(nuevaExperiencia);
//                if (autosave)
//                {
//                    _dbContext.SaveChanges();
//                }
//            }

//            public void Remove(NuevaExperiencia nuevaExperiencia, bool autosave = true)
//            {
//                NuevaExperiencia nuevaExperienciadb = Get(nuevaExperiencia.Id);
//                if (nuevaExperienciadb != null)
//                {
//                    _dbContext.Experiencias.Remove(nuevaExperienciadb);
//                    if (autosave)
//                    {
//                        _dbContext.SaveChanges();
//                    }
//                }
//                else
//                {
//                    throw new ApplicationException(
//                            string.Format(Messages.CandidatoNotFound, nuevaExperiencia.Id),
//                            new KeyNotFoundException());
//                }
//            }

//            public void Update(NuevaExperiencia nuevaExperiencia, bool autosave = true)
//            {
//                NuevaExperiencia nuevaExperienciadb = Get(nuevaExperiencia.Id);
//                if (nuevaExperienciadb != null)
//                {
//                    nuevaExperienciadb.CantidadDeExperiencia = (nuevaExperiencia.CantidadDeExperiencia ?? nuevaExperienciadb.CantidadDeExperiencia);
//                    nuevaExperienciadb.Abroad = (nuevaExperiencia.Abroad != nuevaExperienciadb.Abroad ? nuevaExperiencia.Abroad : nuevaExperienciadb.Abroad);
//                    nuevaExperienciadb.TC = (nuevaExperiencia.TC != nuevaExperienciadb.TC ? nuevaExperiencia.TC : nuevaExperienciadb.TC);
//                    nuevaExperienciadb.Voluntario = (nuevaExperiencia.Voluntario != nuevaExperienciadb.Voluntario ? nuevaExperiencia.Voluntario : nuevaExperienciadb.Voluntario);
//                    nuevaExperienciadb.Destacar = (nuevaExperiencia.Destacar ?? nuevaExperienciadb.Destacar);
//                    nuevaExperienciadb.ComentariosExperiencia = (nuevaExperiencia.ComentariosExperiencia ?? nuevaExperienciadb.ComentariosExperiencia);
                    
//                    if (autosave)
//                    {
//                        _dbContext.SaveChanges();
//                    }
//                }
//                else
//                {
//                    throw new KeyNotFoundException(Messages.ExperienciaNotFound);
//                }
//            }

//            public NuevaExperiencia Get(Guid id)
//            {
//                return _dbContext.Experiencias.Find(id);
//            }

//            public IQueryable<NuevaExperiencia> GetAll()
//            {
//                return _dbContext.Experiencias;
//            }



//            // private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
//        }
//    }
