﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.DAL;

namespace GAdUPIN.CORE.Domain
{
    public class SeedData : DropCreateDatabaseIfModelChanges<DbContext>
    {
        IDBContext _dbContext;
        public SeedData(IDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        protected override void Seed(DbContext context)
        {
            new List<ComunidadAutonoma>
            {
                new ComunidadAutonoma {Id = Guid.NewGuid(), NombreComunidadAutonoma = "Aragón"},
                new ComunidadAutonoma {Id = Guid.NewGuid(), NombreComunidadAutonoma = "Galicia"},
                new ComunidadAutonoma {Id = Guid.NewGuid(), NombreComunidadAutonoma = "Cataluña"},
                new ComunidadAutonoma {Id = Guid.NewGuid(), NombreComunidadAutonoma = "Navarra"},
                new ComunidadAutonoma {Id = Guid.NewGuid(), NombreComunidadAutonoma = "Com. de Madrid"},

            }.ForEach(i => _dbContext.ComunidadesAutonomas.Add(i));
        }

    }
}