﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Contracts;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;
using System.Web.Mvc;


namespace GAdUPIN.Application
{
    class ColegioService: IColegioService
    {
        private INLogger _NLoggerService;

        IDBContext _dbContext;
        public ColegioService(IDBContext dbContext)
        {
            _dbContext = dbContext;
            //_NLoggerService =
            //    IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
            //    as INLogger;
        }

        public void Add(Colegio colegio, bool autosave = true)
        {
            _dbContext.Colegios.Add(colegio);
            if (autosave){ _dbContext.SaveChanges();}
        }

        public void Remove(Colegio colegio, bool autosave = true)
        {
            Colegio colegiodb = Get(colegio.Id);
            if (colegiodb != null)
            {
                _dbContext.Colegios.Remove(colegiodb);
                if (autosave) { _dbContext.SaveChanges(); }
            }
        }

        public void Desactivar(Colegio colegio, bool autosave = true)
        {
            Colegio colegiodb = Get(colegio.Id);
            if (colegiodb != null)
            {

                colegiodb.Desactivado = true;

                if (autosave) { _dbContext.SaveChanges(); }
                else
                {
                    throw new KeyNotFoundException(Messages.ColegioNotFound);
                }

            }
        }

        public void Activar(Colegio colegio, bool autosave = true)
        {
            Colegio colegiodb = Get(colegio.Id);
            if (colegiodb != null)
            {

                colegiodb.Desactivado = false;

                if (autosave) { _dbContext.SaveChanges(); }
                else
                {
                    throw new KeyNotFoundException(Messages.ColegioNotFound);
                }

            }
        }

        public void Update(Colegio colegio, bool autosave = true)
        {
            Colegio colegiodb = Get(colegio.Id);
            if (colegiodb != null)
            {

                colegiodb.CIF = (colegio.CIF ?? colegiodb.CIF);
                colegiodb.NombreColegio = (colegio.NombreColegio ?? colegiodb.NombreColegio);
                colegiodb.DireccionColegio = (colegio.DireccionColegio ?? colegiodb.DireccionColegio);
                colegiodb.CodigoPostal = (colegio.CodigoPostal ?? colegiodb.CodigoPostal);
                colegiodb.ComunidadAutonoma = (colegio.ComunidadAutonoma ?? colegiodb.ComunidadAutonoma);
                colegiodb.Provincia = (colegio.Provincia ?? colegiodb.Provincia);
                colegiodb.Telefono = (colegio.Telefono ?? colegiodb.Telefono );
                colegiodb.PlazasDisponibles = (colegio.PlazasDisponibles != colegiodb.PlazasDisponibles ? colegio.PlazasDisponibles : colegiodb.PlazasDisponibles);
                colegiodb.ColegioContratado = (colegio.ColegioContratado != colegiodb.ColegioContratado ? colegio.ColegioContratado : colegiodb.ColegioContratado);
                colegiodb.PlazasDelColegio = (colegio.PlazasDelColegio ?? colegiodb.PlazasDelColegio);
                colegiodb.ContactosColegio = (colegio.ContactosColegio ?? colegiodb.ContactosColegio);
                colegiodb.Comentarios = (colegio.Comentarios ?? colegiodb.Comentarios);
                colegiodb.Desactivado = (colegio.Desactivado != colegiodb.Desactivado ? colegio.Desactivado : colegiodb.Desactivado);
                colegiodb.Localidad = (colegio.Localidad ?? colegiodb.Localidad);

                colegiodb.FechaDeContrato = (colegio.FechaDeContrato ?? colegiodb.FechaDeContrato);
                colegiodb.Web = (colegio.Web ?? colegiodb.Web);
                colegiodb.Email = (colegio.Email ?? colegiodb.Email);


                if(autosave) { _dbContext.SaveChanges(); }
            else
                {
                    throw new KeyNotFoundException(Messages.ColegioNotFound);
                }

            }
        }

        public Colegio Get(Guid id)
        {
            return _dbContext.Colegios.Find(id);
        }

        public Colegio GetWithPlazasDelColegio(Guid id)
        {
            
            return _dbContext.Colegios.Include("PlazasDelColegio")
                .Where(p => p.Id == id).SingleOrDefault();
        }

        public IQueryable<Colegio> GetAll()
        {
            return _dbContext.Colegios
                .Include(c => c.ComunidadAutonoma);
                
        }


    }
}
