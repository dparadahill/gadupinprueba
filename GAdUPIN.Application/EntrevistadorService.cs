﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using GAdUPIN.CORE.Contracts.Services;
//using GAdUPIN.CORE.DAL;
//using GAdUPIN.CORE.Domain;
//using GAdUPIN.Infrastructure;
//using GAdUPIN.Infrastructure.Logging;

//namespace GAdUPIN.Application
//{
//    public class EntrevistadorService: IEntrevistadorService
//    {          private INLogger _NLoggerService;

//            IDBContext _dbContext;
//            public EntrevistadorService(IDBContext dbContext)
//            {
//                _dbContext = dbContext;
//                //_NLoggerService =
//                //    IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
//                //    as INLogger;
//            }

//            public void Add(Entrevistador entrevistador, bool autosave = true)
//            {
//                _dbContext.Entrevistadores.Add(entrevistador);
//                if (autosave)
//                {
//                    _dbContext.SaveChanges();
//                }
//            }

//            public void Remove(Entrevistador entrevistador, bool autosave = true)
//            {
//                Entrevistador entrevistadordb = Get(entrevistador.Id);
//                if (entrevistadordb != null)
//                {
//                    _dbContext.Entrevistadores.Remove(entrevistadordb);
//                    if (autosave)
//                    {
//                        _dbContext.SaveChanges();
//                    }
//                }
//                else
//                {
//                    throw new ApplicationException(
//                            string.Format(Messages.CandidatoNotFound, entrevistador.Id),
//                            new KeyNotFoundException());
//                }
//            }

//            public void Update(Entrevistador entrevistador, bool autosave = true)
//            {
//                Entrevistador entrevistadordb = Get(entrevistador.Id);
//                if (entrevistadordb != null)
//                {

//                    entrevistadordb.NombreEntrevistador = entrevistador.NombreEntrevistador ?? entrevistadordb.NombreEntrevistador;
//                    entrevistadordb.ApellidoEntrevistador = entrevistador.ApellidoEntrevistador ?? entrevistadordb.ApellidoEntrevistador;
//                    entrevistadordb.ApellidoEntrevistador2 = entrevistador.ApellidoEntrevistador2 ?? entrevistadordb.ApellidoEntrevistador2;
//                    entrevistadordb.Email = entrevistador.Email ?? entrevistadordb.Email;
//                    entrevistadordb.TelefonoDeContacto = entrevistador.TelefonoDeContacto ?? entrevistadordb.TelefonoDeContacto;
//                    entrevistadordb.Skype = entrevistador.Skype ?? entrevistadordb.Skype;
                
//                    if (autosave)
//                    {
//                        _dbContext.SaveChanges();
//                    }
//                }
//                else
//                {
//                    throw new KeyNotFoundException(Messages.AdminNotFound);
//                }
//            }

//            public Entrevistador Get(Guid id)
//            {
//                return _dbContext.Entrevistadores.Find(id);
//            }

//            public IQueryable<Entrevistador> GetAll()
//            {
//                return _dbContext.Entrevistadores;
//            }



//            // private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
//        }
//    }
