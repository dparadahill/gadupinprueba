﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.Application
{
    public class DocumentoInscripcionService : IDocumentoInscripcionService
    {
        private INLogger _NLoggerService;

        IDBContext _dbContext;

        public DocumentoInscripcionService(IDBContext dbContext)
        {
            _dbContext = dbContext;
            //_NLoggerService =
            //    IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
            //    as INLogger;
        }
        public void Add(DocumentoInscripcion documentoInscripcion, bool autosave = true)
        {
            _dbContext.DocumentosInscripcion.Add(documentoInscripcion);
            if (autosave) { _dbContext.SaveChanges(); }
        }


        public void Remove(Guid id, bool autosave = true)
        {
            CORE.Domain.DocumentoInscripcion documentoInscripciondb = Get(id);
            if (documentoInscripciondb != null)
            {
                _dbContext.DocumentosInscripcion.Remove(documentoInscripciondb);
                if (autosave) { _dbContext.SaveChanges(); }
            }
        }

        public void Update(CORE.Domain.DocumentoInscripcion documentoInscripcion, bool autosave = true)
        {
            CORE.Domain.DocumentoInscripcion documentoInscripciondb = Get(documentoInscripcion.Id);
            if (documentoInscripciondb != null)
            {
                documentoInscripciondb.NombreDocumento = (documentoInscripcion.NombreDocumento ?? documentoInscripciondb.NombreDocumento);
                documentoInscripciondb.FechaSubida = (documentoInscripcion.FechaSubida != documentoInscripciondb.FechaSubida ? documentoInscripcion.FechaSubida : documentoInscripciondb.FechaSubida);
                documentoInscripciondb.Comentarios = (documentoInscripcion.Comentarios ?? documentoInscripciondb.Comentarios);
                documentoInscripciondb.Estado = (documentoInscripcion.Estado ?? documentoInscripciondb.Estado);
                documentoInscripciondb.Candidato = (documentoInscripcion.Candidato ?? documentoInscripciondb.Candidato);

                if (autosave) { _dbContext.SaveChanges(); }
                else
                {
                    throw new KeyNotFoundException(Messages.GenericNotFound);
                }

            }
        }

        public CORE.Domain.DocumentoInscripcion Get(Guid id)
        {
            return _dbContext.DocumentosInscripcion.Find(id);
        }


        public IQueryable<CORE.Domain.DocumentoInscripcion> GetAll()
        {
            return _dbContext.DocumentosInscripcion;

        }

        public List<DocumentoInscripcion> GetByCandidato(Guid id)
        {
            return _dbContext.DocumentosInscripcion.Where(e => e.Candidato.Id == id).ToList();
            //return _dbContext.DocumentosDeTramite.Where(e => e.Candidato.Id == id).Include(e => e.NombreDocumento);
        }

    }
}
