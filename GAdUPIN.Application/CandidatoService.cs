﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Web.DynamicData;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.Application
{
    public class CandidatoService : ICandidatoService
        {
            private INLogger _NLoggerService;

            IDBContext _dbContext;
            public CandidatoService(IDBContext dbContext)
            {
                _dbContext = dbContext;
                //_NLoggerService =
                //    IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
                //    as INLogger;
            }

            public void Add(Candidato candidato, bool autosave = true)
            {
                _dbContext.Candidatos.Add(candidato);
                if (autosave)
                {
                    _dbContext.SaveChanges();
                }
            }

            public void Remove(Candidato candidato, bool autosave = true)
            {
                Candidato candidatodb = Get(candidato.Id);
                if (candidatodb != null)
                {
                    _dbContext.Candidatos.Remove(candidatodb);
                    if (autosave)
                    {
                        _dbContext.SaveChanges();
                    }
                }
                else
                {
                    throw new ApplicationException(
                            string.Format(Messages.CandidatoNotFound, candidato.Id),
                            new KeyNotFoundException());
                }
            }

            public void Update(Candidato candidato, bool autosave = true)
            {
                Candidato candidatodb = Get(candidato.Id);
                if (candidatodb != null)
                {
                    candidatodb.Codigo = (candidato.Codigo ?? candidatodb.Codigo);
                    candidatodb.Pasaporte = (candidato.Pasaporte != candidatodb.Pasaporte ? candidato.Pasaporte : candidatodb.Pasaporte);
                    candidatodb.Colegio = (candidato.Colegio ?? candidatodb.Colegio);
                    candidatodb.Nombre = (candidato.Nombre ?? candidatodb.Nombre);
                    candidatodb.Apellido1 = (candidato.Apellido1 ?? candidatodb.Apellido1);
                    candidatodb.Apellido2 = (candidato.Apellido2 ?? candidatodb.Apellido2);
                    candidatodb.Email = (candidato.Email ?? candidatodb.Email);
                    candidatodb.TelefonoDeContacto = (candidato.TelefonoDeContacto ?? candidatodb.TelefonoDeContacto);
                    candidatodb.TelefonoEspanol = (candidato.TelefonoEspanol ?? candidatodb.TelefonoEspanol);
                    candidatodb.Usuario = (candidato.Usuario ?? candidatodb.Usuario);
                    candidatodb.Password = (candidato.Password ?? candidatodb.Password);
                    candidatodb.FechaNac = (candidato.FechaNac != candidatodb.FechaNac ? candidato.FechaNac : candidatodb.FechaNac);
                    candidatodb.Nacionalidad = (candidato.Nacionalidad ?? candidatodb.Nacionalidad);
                    candidatodb.Skype = (candidato.Skype ?? candidatodb.Skype);
                    candidatodb.Procedencia = (candidato.Procedencia ?? candidatodb.Procedencia);
                    candidatodb.ComentariosProcedencia = (candidato.ComentariosProcedencia ?? candidatodb.ComentariosProcedencia);
                    candidatodb.Sexo = (candidato.Sexo != candidatodb.Sexo ? candidato.Sexo : candidatodb.Sexo);
                    candidatodb.FechaAlta = (candidato.FechaAlta != candidatodb.FechaAlta ? candidato.FechaAlta : candidatodb.FechaAlta);
                    candidatodb.FechaLlegadaEspana = (candidato.FechaLlegadaEspana != candidatodb.FechaLlegadaEspana ? candidato.FechaLlegadaEspana : candidatodb.FechaLlegadaEspana);
                    

                    //candidatodb.Experiencia.Academia = (candidato.Experiencia.Academia != candidatodb.Experiencia.Academia ? candidato.Experiencia.Academia : candidatodb.Experiencia.Academia);
                    //candidatodb.Experiencia.Adultos = (candidato.Experiencia.Adultos != candidatodb.Experiencia.Adultos ? candidato.Experiencia.Adultos : candidatodb.Experiencia.Adultos);
                    //candidatodb.Experiencia.Campamento = (candidato.Experiencia.Campamento != candidatodb.Experiencia.Campamento ? candidato.Experiencia.Campamento : candidatodb.Experiencia.Campamento);
                    //candidatodb.Experiencia.Colegio = (candidato.Experiencia.Colegio != candidatodb.Experiencia.Colegio ? candidato.Experiencia.Colegio : candidatodb.Experiencia.Colegio);
                    //candidatodb.Experiencia.Ninos = (candidato.Experiencia.Ninos != candidatodb.Experiencia.Ninos ? candidato.Experiencia.Ninos : candidatodb.Experiencia.Ninos);
                    //candidatodb.Experiencia.Up = (candidato.Experiencia.Up != candidatodb.Experiencia.Up ? candidato.Experiencia.Up : candidatodb.Experiencia.Up);
                    //candidatodb.Experiencia.Voluntariado = (candidato.Experiencia.Voluntariado != candidatodb.Experiencia.Voluntariado ? candidato.Experiencia.Voluntariado : candidatodb.Experiencia.Voluntariado);
                    //candidatodb.Experiencia.Paises = (candidato.Experiencia.Paises != candidatodb.Experiencia.Paises ? candidato.Experiencia.Paises : candidatodb.Experiencia.Paises);

                    candidatodb.Banco = (candidato.Banco != candidatodb.Banco ? candidato.Banco : candidatodb.Banco);
                    candidatodb.TarjetaSanitaria = (candidato.TarjetaSanitaria != candidatodb.TarjetaSanitaria ? candidato.TarjetaSanitaria : candidatodb.TarjetaSanitaria);
                    candidatodb.EstatusCandidato = (candidato.EstatusCandidato ?? candidatodb.EstatusCandidato);
                    candidatodb.FechaDisponibilidad = (candidato.FechaDisponibilidad != candidatodb.FechaDisponibilidad ? candidato.FechaDisponibilidad : candidatodb.FechaDisponibilidad);
                    candidatodb.Encargado = (candidato.Encargado ?? candidatodb.Encargado);
                    candidatodb.DocumentosDeTramite = (candidato.DocumentosDeTramite ?? candidatodb.DocumentosDeTramite);
                    candidatodb.DocumentosInscripcion = (candidato.DocumentosInscripcion ?? candidatodb.DocumentosInscripcion);
                    //candidatodb.PendienteEntrevista = (candidato.PendienteEntrevista != candidatodb.PendienteEntrevista ? candidato.PendienteEntrevista : candidatodb.PendienteEntrevista);
                    candidatodb.InformeEntrevistaEvaluaciones = (candidato.InformeEntrevistaEvaluaciones ?? candidatodb.InformeEntrevistaEvaluaciones);
                    candidatodb.PlazasDeColegiosAsignadas = (candidato.PlazasDeColegiosAsignadas ?? candidatodb.PlazasDeColegiosAsignadas);

                    candidatodb.AjusteAlPuesto = (candidato.AjusteAlPuesto != candidatodb.AjusteAlPuesto ? candidato.AjusteAlPuesto : candidatodb.AjusteAlPuesto);
                    candidatodb.TestRealizado = (candidato.TestRealizado != candidatodb.TestRealizado ? candidato.TestRealizado : candidatodb.TestRealizado);
                    candidatodb.ComentariosTest = (candidato.ComentariosTest ?? candidatodb.ComentariosTest);
                    candidatodb.DeseabilidadSocial = (candidato.DeseabilidadSocial != candidatodb.DeseabilidadSocial ? candidato.DeseabilidadSocial : candidatodb.DeseabilidadSocial);
                    candidatodb.TipoAlojamientoPreferido = (candidato.TipoAlojamientoPreferido ?? candidatodb.TipoAlojamientoPreferido);
                    candidatodb.ComunidadAutonomaPreferida = (candidato.ComunidadAutonomaPreferida ?? candidatodb.ComunidadAutonomaPreferida);
                    candidatodb.ProvinciaPreferida = (candidato.ProvinciaPreferida ?? candidatodb.ProvinciaPreferida);
                    candidatodb.EtapaEducativaPreferida = (candidato.EtapaEducativaPreferida ?? candidatodb.EtapaEducativaPreferida);
                    candidatodb.ComentariosGenerales = (candidato.ComentariosGenerales ?? candidatodb.ComentariosGenerales);
                    
                    candidatodb.TeflCelta = (candidato.TeflCelta != candidatodb.TeflCelta ? candidato.TeflCelta : candidatodb.TeflCelta);
                    candidatodb.CLIL = (candidato.CLIL != candidatodb.CLIL ? candidato.CLIL : candidatodb.CLIL);
                    candidatodb.Espanol = (candidato.Espanol != candidatodb.Espanol ? candidato.Espanol : candidatodb.Espanol);

                    candidatodb.EstadoVisado = (candidato.EstadoVisado ?? candidatodb.EstadoVisado);
                    candidatodb.ComentariosVisado = (candidato.ComentariosVisado ?? candidatodb.ComentariosVisado);

                    candidatodb.fichaCerrada = (candidato.fichaCerrada != candidatodb.fichaCerrada ? candidato.fichaCerrada : candidatodb.fichaCerrada);
                    candidatodb.MotivoCierreFicha = (candidato.MotivoCierreFicha ?? candidatodb.MotivoCierreFicha);
                    candidatodb.ComentariosMotivoCierre = (candidato.ComentariosMotivoCierre ?? candidatodb.ComentariosMotivoCierre);
                    candidatodb.anyoContacto = (candidato.anyoContacto != candidatodb.anyoContacto ? candidato.anyoContacto : candidatodb.anyoContacto);

                    candidatodb.Carrera = (candidato.Carrera ?? candidatodb.Carrera);
                    candidatodb.UniversidadDeRegistro = (candidato.UniversidadDeRegistro ?? candidatodb.UniversidadDeRegistro);

                    candidatodb.FechaEntrevistaNativo = (candidato.FechaEntrevistaNativo ?? candidatodb.FechaEntrevistaNativo);
                    candidatodb.FechaEntrevistaUP = (candidato.FechaEntrevistaUP ?? candidatodb.FechaEntrevistaUP);
                    candidatodb.EntrevistaConNativo = (candidato.EntrevistaConNativo != candidatodb.EntrevistaConNativo ? candidato.EntrevistaConNativo : candidatodb.EntrevistaConNativo);
                    candidatodb.EntrevistaConUp = (candidato.EntrevistaConUp != candidatodb.EntrevistaConUp ? candidato.EntrevistaConUp : candidatodb.EntrevistaConUp);

                    candidatodb.NumeroPasaporte = (candidato.NumeroPasaporte ?? candidatodb.NumeroPasaporte);

                    candidatodb.TituloRecibido = (candidato.TituloRecibido != candidatodb.TituloRecibido ? candidato.TituloRecibido : candidatodb.TituloRecibido);
                    candidatodb.CertificadoPenalesRecibido = (candidato.CertificadoPenalesRecibido != candidatodb.CertificadoPenalesRecibido ? candidato.CertificadoPenalesRecibido : candidatodb.CertificadoPenalesRecibido);

                    
                    candidatodb.EntrevistaColegio = (candidato.EntrevistaColegio ?? candidatodb.EntrevistaColegio);

                    candidatodb.fotoPerfilPath = (candidato.fotoPerfilPath ?? candidatodb.fotoPerfilPath);

                    candidatodb.ComentariosColegio = (candidato.ComentariosColegio ?? candidatodb.ComentariosColegio);
                    candidatodb.PlazaOfrecida = (candidato.PlazaOfrecida ?? candidatodb.PlazaOfrecida);

                    candidatodb.EstadoFicha = (candidato.EstadoFicha ?? candidatodb.EstadoFicha);
                    candidatodb.HoraEntrevistaColegio = (candidato.HoraEntrevistaColegio ?? candidatodb.HoraEntrevistaColegio);

                    candidatodb.HoraEntrevistaNativo = (candidato.HoraEntrevistaNativo ?? candidatodb.HoraEntrevistaNativo);
                    candidatodb.HoraEntrevistaUp = (candidato.HoraEntrevistaUp ?? candidatodb.HoraEntrevistaUp);

                    //candidatodb.NuevaExperiencia = (candidato.NuevaExperiencia ?? candidatodb.NuevaExperiencia);
                    candidatodb.CantidadDeExperiencia = (candidato.CantidadDeExperiencia ?? candidatodb.CantidadDeExperiencia);
                    candidatodb.Abroad = (candidato.Abroad != candidatodb.Abroad ? candidato.Abroad : candidatodb.Abroad);
                    candidatodb.TC = (candidato.TC != candidatodb.TC ? candidato.TC : candidatodb.TC);
                    candidatodb.Voluntario = (candidato.Voluntario != candidatodb.Voluntario ? candidato.Voluntario : candidatodb.Voluntario);
                    candidatodb.Destacar = (candidato.Destacar ?? candidatodb.Destacar);
                    candidatodb.ComentariosExperiencia = (candidato.ComentariosExperiencia ?? candidatodb.ComentariosExperiencia);

                    if (autosave)
                    {
                        _dbContext.SaveChanges();
                    }
                }
                else
                {
                    throw new KeyNotFoundException(Messages.CandidatoNotFound);
                }
            }

            public Candidato Get(Guid id)
            {
                // return _dbContext.Candidatos.Find(id);
                return _dbContext.Candidatos.Include("Nacionalidad").SingleOrDefault(c => c.Id == id);
            }

            public Candidato GetIncNacionalidad(Guid id)
            {
                return _dbContext.Candidatos.Include("Nacionalidad").SingleOrDefault(c => c.Id == id);
            } 

            public IQueryable<Candidato> GetAll()
            {
                return _dbContext.Candidatos
                     .Include(c => c.Nacionalidad);
            }

               

            public IQueryable<Candidato> GetAllWithInformes()
            {
                return _dbContext.Candidatos
                     .Include(c => c.InformeEntrevistaEvaluaciones);
            }

            public IQueryable<Candidato> GetAllWithColegios()
            {
                return _dbContext.Candidatos.Include(e => e.Colegio);
            }

            
            public IQueryable<Candidato> GetByColegio(Colegio colegio)
            {
                return _dbContext.Candidatos.Where(e => e.Colegio.Id == colegio.Id).Include(e => e.Colegio);
            }

            public IQueryable<Candidato> GetByEncargado(Guid id)
            {
                return _dbContext.Candidatos.Where(e => e.Encargado.Id == id).Where(e => !e.fichaCerrada).Include(e => e.Encargado);
                

            }

        public bool EmailNoExiste(Candidato candidato)
        {
            
            if(_dbContext.Candidatos.Count(c => c.Email == candidato.Email) != 0)
            {
                
                return false;
            }
            
            return true;    
            
        }



        // private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();


        public IQueryable<Candidato> GetAllButCerradosOrSeleccionados()
        {
            return _dbContext.Candidatos
                     .Where(c => !c.fichaCerrada).Where(c => c.PlazasDeColegiosAsignadas.Count == 0);
        }





        public void DesasignarEncargado(Guid id, bool autosave = true)
            {
                Candidato candidatodb = Get(id);
                if (candidatodb != null)
                {
                    candidatodb.Encargado = null;

                    if (autosave)
                    {
                        _dbContext.SaveChanges();
                    }
                }
                else
                {
                    throw new KeyNotFoundException(Messages.PlazaAntiguaNotFound);
                }
            }

        //public void DesasignarPlaza(Guid id, bool autosave = true)
        //{
        //    Candidato candidatodb = GetIncNacionalidad(id);
        //    if (candidatodb != null)
        //    {
        //        candidatodb.PlazasDeColegiosAsignadas = null;

        //        if (autosave)
        //        {
        //            _dbContext.SaveChanges();
        //        }
        //    }
        //    else
        //    {
        //        throw new KeyNotFoundException(Messages.PlazaAntiguaNotFound);
        //    }
        //}


        public IQueryable<Candidato> GetAllSeleccionados()
        {
            return _dbContext.Candidatos
                     .Where(c => c.PlazasDeColegiosAsignadas.Count != 0);
        }

       


        }
    }
