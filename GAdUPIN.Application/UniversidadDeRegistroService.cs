﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.Application
{
    public class UniversidadDeRegistroService:IUniversidadDeRegistroService
    {
    private INLogger _NLoggerService;

        IDBContext _dbContext;

        public UniversidadDeRegistroService(IDBContext dbContext)
        {
            _dbContext = dbContext;
            //_NLoggerService =
            //    IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
            //    as INLogger;
        }
        public void Add(UniversidadDeRegistro universidadDeRegistro, bool autosave = true)
        {
            try
            {
                _dbContext.UniversidadesDeRegistro.Add(universidadDeRegistro);
                if (autosave)
                {
                    _dbContext.SaveChanges();
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        public void Remove(Guid Id, bool autosave = true)
        {
            UniversidadDeRegistro universidadDeRegistrodb = Get(Id);
            if (universidadDeRegistrodb != null)
            {
                _dbContext.UniversidadesDeRegistro.Remove(universidadDeRegistrodb);
                if (autosave) { _dbContext.SaveChanges(); }
            }
        }

        public void Update(UniversidadDeRegistro universidadDeRegistro, bool autosave = true)
        {
            UniversidadDeRegistro universidadDeRegistrodb = Get(universidadDeRegistro.Id);
            if (universidadDeRegistrodb != null)
            {

                universidadDeRegistrodb.NombreUniversidadDeRegistro = (universidadDeRegistro.NombreUniversidadDeRegistro ?? universidadDeRegistrodb.NombreUniversidadDeRegistro);
                universidadDeRegistrodb.Visible = universidadDeRegistro.Visible != universidadDeRegistrodb.Visible ? universidadDeRegistro.Visible : universidadDeRegistrodb.Visible;

                if (autosave) { _dbContext.SaveChanges(); }
                else
                {
                    throw new KeyNotFoundException(Messages.GenericNotFound);
                }

            }
        }

        public UniversidadDeRegistro Get(Guid id)
        {
            return _dbContext.UniversidadesDeRegistro.Find(id);
        }

        
        public IQueryable<UniversidadDeRegistro> GetAll()
        {
            return _dbContext.UniversidadesDeRegistro;
            
        }



        public IQueryable<UniversidadDeRegistro> GetAllButInvisible()
        {
            return _dbContext.UniversidadesDeRegistro.Where(c => c.Visible);
        }
    }
}