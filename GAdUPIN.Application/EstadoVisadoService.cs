﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.Application
{
    public class EstadoVisadoService:IEstadoVisadoService
    {
        IDBContext _dbContext;

        public EstadoVisadoService(IDBContext dbContext)
        {
            _dbContext = dbContext;
        }



        public void Add(EstadoVisado estadoVisado, bool autosave = true)
        {
            _dbContext.EstadosVisado.Add(estadoVisado);
            if (autosave) { _dbContext.SaveChanges(); }
        }

        public void Remove(EstadoVisado estadoVisado, bool autosave = true)
        {
            EstadoVisado estadoVisadodb = Get(estadoVisado.Id);
            if (estadoVisadodb != null)
            {
                _dbContext.EstadosVisado.Remove(estadoVisadodb);
                if (autosave) { _dbContext.SaveChanges(); }
            }
        }

        public void Update(EstadoVisado estadoVisado, bool autosave = true)
        {
            EstadoVisado estadoVisadodb = Get(estadoVisado.Id);
            if (estadoVisadodb != null)
            {

                estadoVisadodb.EstadoVisadoNombre = (estadoVisado.EstadoVisadoNombre ?? estadoVisadodb.EstadoVisadoNombre);

                if (autosave) { _dbContext.SaveChanges(); }
                else
                {
                    throw new KeyNotFoundException(Messages.GenericNotFound);
                }

            }
        }

        public EstadoVisado Get(Guid id)
        {
            return _dbContext.EstadosVisado.Find(id);
        }

        public IQueryable<EstadoVisado> GetAll()
        {
            return _dbContext.EstadosVisado;
        }
    }
}
