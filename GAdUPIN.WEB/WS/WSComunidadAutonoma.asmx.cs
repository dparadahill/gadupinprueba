﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;

namespace GAdUPIN.WEB.WS
{
    /// <summary>
    /// Summary description for WSComunidadAutonoma
    /// </summary>
    //[WebService(Namespace = "http://tempuri.org/")]
    [WebService(Namespace = "http://gadupin.org/", Name = "WSComunidadAutonoma")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class WSComunidadAutonoma : System.Web.Services.WebService
    {

        private static IComunidadAutonomaService _comunidadAutonomaService;

        public WSComunidadAutonoma()
        {
            //_comunidadAutonomaService =
            //    IoC.Current.GetContainer().Resolve(typeof(IComunidadAutonomaService), "")
            //        as IComunidadAutonomaService;
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public JsonDataTable GetComunidadesAutonomas()
        {
            try
            {
                MembershipUser user = Membership.GetUser();
                Guid userId = user == null ? Guid.Empty : (Guid)user.ProviderUserKey;

                IEnumerable<ComunidadAutonoma> list = _comunidadAutonomaService.GetAll();
                return Utils.CreateJsDataTable(list);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }


    }
}