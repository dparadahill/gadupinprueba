﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;

namespace GAdUPIN.WEB.WS
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        private static IColegioService _colegioService;

        public WebService1()
        {
        //    _colegioService =
        //        IoC.Current.GetContainer().Resolve(typeof (IColegioService), "")
        //            as IColegioService;
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public static JsonDataTable GetColegios()
        {
            try
            {
                MembershipUser user = Membership.GetUser();
                Guid userId = user == null ? Guid.Empty : (Guid) user.ProviderUserKey;

                IEnumerable<Colegio> list = _colegioService.GetAll();
                return Utils.CreateJsDataTable(list);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
    }
}
