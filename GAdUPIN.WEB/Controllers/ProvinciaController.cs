﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;
using GAdUPIN.WEB.ViewModels;

namespace GAdUPIN.WEB.Controllers
{
    public class ProvinciaController : Controller
    {  
        private IProvinciaService _provinciaService;
        private IComunidadAutonomaService _comunidadAutonomaService;
        //private INLogger _NLoggerService;
        

        public ProvinciaController( IProvinciaService provinciaService, IComunidadAutonomaService comunidadAutonomaService)
        {
            _provinciaService = provinciaService;
            _comunidadAutonomaService = comunidadAutonomaService;

            //_NLoggerService =
            //IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
            //as INLogger;
        }


        public ActionResult Index(Guid id)
        {
           // MembershipUser user = Membership.GetUser();
            //Guid userId = user == null ? Guid.Empty : (Guid)user.ProviderUserKey;
            ViewData["IdComunidad"] = id;
            IList<ProvinciaListVM> list = CreateViewModelList(id);
            //_NLoggerService.Info("Visita a la página de provincias registrada");
            return View(list);
        }

        private IList<ProvinciaListVM> CreateViewModelList(Guid id)
        {
            IList<ProvinciaListVM> result = new List<ProvinciaListVM>();
            
            foreach (var provincia in _provinciaService.GetByComunidadAutonoma(id))
            {
                result.Add(new ProvinciaListVM()
                {
                    Id = provincia.Id, 
                    IdComunidadAutonoma = provincia.ComunidadAutonoma.Id,
                    NombreComunidadAutonoma = provincia.ComunidadAutonoma.NombreComunidadAutonoma != null ? provincia.ComunidadAutonoma.NombreComunidadAutonoma : string.Empty,
                    NombreProvincia = string.Format("{0}", provincia.NombreProvincia)
                });
            }


            return result;
        }

        // GET: /Provincia/Create
        public ActionResult Create(Guid comunidadA )
        {
            ProvinciaNewEditVM provincia = CreateViewModelNewEdit(comunidadA);
            return View(provincia);

        }
        
        private ProvinciaNewEditVM CreateViewModelNewEdit(Guid id )
        {
            ProvinciaNewEditVM result = new ProvinciaNewEditVM();
            
                result.Provincia = new Provincia() { Id = Guid.NewGuid() };
                result.Provincia.ComunidadAutonoma = _comunidadAutonomaService.Get(id);
                result.SelectedComunidadAutonoma = id;
                result.IdComunidadAutonoma = id;
                result.NombreComunidadAutonoma = result.Provincia.ComunidadAutonoma.NombreComunidadAutonoma;
                
                
            return result;
        } 

        //
        // POST: /Provincia/Create

        [HttpPost]
        public ActionResult Create(ProvinciaNewEditVM provinciaVM)
        {
            try
            {
                provinciaVM.Provincia.ComunidadAutonoma = _comunidadAutonomaService.Get(provinciaVM.Provincia.ComunidadAutonoma.Id);
                _provinciaService.Add(provinciaVM.Provincia);
                return RedirectToAction("Index", new
                {
                    id = provinciaVM.Provincia.ComunidadAutonoma.Id
                });

            }
            catch
            {
                return View();
            }
        }


        //// GET: /Provincia/Create
        //public ActionResult CreateWOComunidad()
        //{
        //    ProvinciaNewEditVM provincia = CreateViewModelNewEditWOComunidad();
        //    return View(provincia);

        //}

        //private ProvinciaNewEditVM CreateViewModelNewEditWOComunidad()
        //{
        //    ProvinciaNewEditVM result = new ProvinciaNewEditVM();

        //    result.Provincia = new Provincia() { Id = Guid.NewGuid() };
        //    result.ComunidadesAutonomas = _comunidadAutonomaService.GetAll();

        //    return result;
        //}

        ////
        //// POST: /Provincia/Create

        //[HttpPost]
        //public ActionResult CreateWOComunidad(ProvinciaNewEditVM provinciaVM)
        //{
        //    try
        //    {
        //            provinciaVM.Provincia.ComunidadAutonoma =
        //                _comunidadAutonomaService.Get(provinciaVM.IdComunidadAutonoma);
        //        if (ModelState.IsValid)
        //        {
        //            provinciaVM.Provincia.ComunidadAutonoma =
        //                _comunidadAutonomaService.Get(provinciaVM.IdComunidadAutonoma);
        //            _provinciaService.Add(provinciaVM.Provincia);
        //            return RedirectToAction("Index", "Provincia", new
        //            {
        //                id = provinciaVM.IdComunidadAutonoma
        //            });
        //        }

        //            return RedirectToAction("Index", "Provincia", new
        //            {
        //                id = provinciaVM.IdComunidadAutonoma
        //            });
        //    }
        //    catch
        //    {
        //        return View("CreateWOComunidad");
        //    }
        //}



        //
        // GET: /Provincia/Edit/5
        public ActionResult Edit(Guid id)
        {
            ProvinciaNewEditVM vm = new ProvinciaNewEditVM();
            vm.Provincia = _provinciaService.Get(id);
            vm.ComunidadesAutonomas = _comunidadAutonomaService.GetAll();
            vm.SelectedComunidadAutonoma = vm.Provincia.ComunidadAutonoma.Id;
            return View(vm);
        }

        //
        // POST: /Provincia/Edit/5

        [HttpPost]
        public ActionResult Edit(ProvinciaNewEditVM provinciaVM)
        {
            try
            {
                provinciaVM.Provincia.ComunidadAutonoma = _comunidadAutonomaService.Get(provinciaVM.SelectedComunidadAutonoma);
                _provinciaService.Update(provinciaVM.Provincia);

                return RedirectToAction("Index", new
                {
                    id = provinciaVM.SelectedComunidadAutonoma
                });
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Provincia/Delete/5

        public ActionResult Delete(Guid id)
        {
            return View(_provinciaService.Get(id));
        }

        //
        // POST: /Provincia/Delete/5

        [HttpPost]
        public ActionResult Delete(Provincia provincia)
        {
            try
            {
                var idComunidadA = _provinciaService.Get(provincia.Id).ComunidadAutonoma.Id;
                // TODO: Add delete logic here
                _provinciaService.Remove(provincia);
                return RedirectToAction("Index", new
                {
                    id = idComunidadA
                });
            }
            catch
            {
                return View();
            }
        }
    }
}



       