﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Services.Description;
using GAdUPIN.CORE.Contracts.Services;

using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;
using GAdUPIN.WEB.ViewModels;


namespace GAdUPIN.WEB.Controllers
{
    //[Authorize(Roles = "admin")]
    public class CandidatoController : Controller
    {

        private ICandidatoService _candidatoService;
        private INacionalidadService _nacionalidadService;
        private IProcedenciaService _procedenciaService;
        //private IPreferenciasColegioService _preferenciaService;
        private IProvinciaService _provinciaService;
        private IComunidadAutonomaService _comunidadAutonomaService;
        private IEtapaEducativaService _etapaEducativaService;
        private ITipoAlojamientoService _tipoAlojamientoService;
        private IDocumentoDeTramiteService _documentoDeTramite;
        private IEstadoVisadoService _estadoVisadoService;
        private IMotivoCierreFichaService _motivoCierreFichaService;
        private IUniversidadDeRegistroService _universidadDeRegistroService;
        //private IPlazaSexoPreferenteService _plazaSexoPreferenteService;

        //private INLogger _NLoggerService;

        public CandidatoController(ICandidatoService candidatoService, INacionalidadService  nacionalidadService, IProcedenciaService procedenciaService, 
                                   IProvinciaService provinciaService, IComunidadAutonomaService comunidadAutonomaService, IEtapaEducativaService etapaEducativaService, 
                                   ITipoAlojamientoService tipoAlojamientoService, IDocumentoDeTramiteService documentacionRequeridaService,
                                   IEstadoVisadoService estadoVisadoService, IMotivoCierreFichaService motivoCierreFichaService,
                                   IUniversidadDeRegistroService universidadDeRegistroService) 
        {
            _candidatoService = candidatoService;
            _nacionalidadService = nacionalidadService;
            _procedenciaService = procedenciaService;
            _provinciaService = provinciaService;
            _comunidadAutonomaService = comunidadAutonomaService;
            _etapaEducativaService = etapaEducativaService;
            _tipoAlojamientoService = tipoAlojamientoService;
            _documentoDeTramite = documentacionRequeridaService;
            _estadoVisadoService = estadoVisadoService;
            _motivoCierreFichaService = motivoCierreFichaService;
            _universidadDeRegistroService = universidadDeRegistroService;
            //_plazaSexoPreferenteService = plazaSexoPreferenteService;
        }
        //
        // GET: /Candidato/

        public ActionResult Index()
        {
            return View(_candidatoService.GetAllButCerradosOrSeleccionados());
        }

        //
        // GET: /Candidato/Details/5

        public ActionResult DetailsCandidato(Guid id)
        {
            CandidatoDetailsVM vm = new CandidatoDetailsVM();
            vm.Candidato = _candidatoService.GetIncNacionalidad(id);
            vm.Nacionalidades = _nacionalidadService.GetAll();
            vm.NacionalidadActual = vm.Candidato.Nacionalidad;
            vm.SexoActual = vm.Candidato.Sexo;
            vm.Procedencias = _procedenciaService.GetAll();
            vm.EstadosVisado = _estadoVisadoService.GetAll();
            vm.MotivosCierreFicha = _motivoCierreFichaService.GetAll();
            vm.UniversidadesDeRegistro = _universidadDeRegistroService.GetAllButInvisible();
            


            //vm.ComunidadesAutonomas = _comunidadAutonomaService.GetAll();
            var ComunidadesAutonomas = new SelectList(_comunidadAutonomaService.GetAll(), "Id", "NombreComunidadAutonoma");
            vm.ComunidadesAutonomas = ComunidadesAutonomas;

            var Provincias = new SelectList(_provinciaService.GetAll(), "Id", "NombreProvincia");
            vm.Provincias = Provincias;

            //vm.Provincias = _provinciaService.GetAll();
            vm.EtapasEducativas = _etapaEducativaService.GetAll();
            vm.TiposAlojamientos = _tipoAlojamientoService.GetAll();

            vm.IdCandidato = vm.Candidato.Id;
            


            // Se revisa si tiene un informe creado
            if (vm.Candidato.InformeEntrevistaEvaluaciones == null || vm.Candidato.InformeEntrevistaEvaluaciones.Count == 0)
            {
                vm.IdInforme = Guid.Empty;
                vm.InformeCompletado = false;
                //vm.InformeFechaCompletado = Convert.ToDateTime("0001/01/01");
                //vm.InformeFechaCompletado = null;
                vm.InformeValoracionGlobal = 0;
            }
            else
            {
                foreach (var informe in vm.Candidato.InformeEntrevistaEvaluaciones)
                {
                    vm.IdInforme = informe.Id;
                    vm.InformeCompletado = informe.Completado;
                    vm.InformeFechaCompletado = informe.Fecha;
                    vm.InformeValoracionGlobal = informe.ValGlobal;
                }
            }

            // Se revisa si tiene una plaza asignada
            if (vm.Candidato.PlazasDeColegiosAsignadas == null || vm.Candidato.PlazasDeColegiosAsignadas.Count == 0)
            {
                vm.IdPlaza = Guid.Empty;
                vm.IdColegio = Guid.Empty;
                vm.NombreColegioPlazaCubierta = "";
                vm.CodigoPlazaCubierta = "";
                //vm.CondicionesColegio = "N/A";
                vm.EntrevistaColegio = "N/A";
            }
            else
            {
                foreach (var plaza in vm.Candidato.PlazasDeColegiosAsignadas)
                {
                    vm.IdPlaza = plaza.Id;
                    vm.IdColegio = plaza.Colegio.Id;
                    vm.NombreColegioPlazaCubierta = plaza.Colegio.NombreColegio;
                    vm.CodigoPlazaCubierta = plaza.CodigoPlaza;
                    vm.EntrevistaColegio = "Desarrollar upload de docs";
                }
            }

            if (vm.Candidato.DocumentosDeTramite != null)
            {

                foreach (var documento in vm.Candidato.DocumentosDeTramite)
                {
                    //vm.Pasaporte = documento.NombreDocumento.Contains("bean");

                }
            }


            if (vm.SelectedEtapaEducativa == Guid.Parse("00000000-0000-0000-0000-000000000000"))
            {
                vm.SelectedEtapaEducativa = Guid.Parse("3319A8B4-EB68-4055-B8CA-A8DECC2189E8");
            }
            else
            {
                vm.SelectedEtapaEducativa = vm.Candidato.EtapaEducativaPreferida.Id;
            }


            if (vm.SelectedTipoAlojamiento == Guid.Parse("00000000-0000-0000-0000-000000000000") && vm.Candidato.TipoAlojamientoPreferido == null)
            {
                vm.SelectedTipoAlojamiento = Guid.Parse("3319A8B4-EB68-4055-B8CA-A8DECC2189A8");
            }
            else
            {
                vm.SelectedTipoAlojamiento = vm.Candidato.TipoAlojamientoPreferido.Id;
            }

            if (vm.ProcedenciaActual == Guid.Parse("00000000-0000-0000-0000-000000000000") && vm.Candidato.Procedencia == null)
            {
                vm.ProcedenciaActual = _procedenciaService.Get(Guid.Parse("76D08182-8560-496A-85A4-48C775D997EE")).Id;
            }
            else
            {
                vm.ProcedenciaActual = vm.Candidato.Procedencia.Id;
            }

            if (vm.SelectedEstadoVisado == Guid.Parse("00000000-0000-0000-0000-000000000000") && vm.Candidato.EstadoVisado == null)
            {
                vm.SelectedEstadoVisado = vm.Candidato.Nacionalidad.Visado ?
                       Guid.Parse("66ECE39B-E566-4DC1-BCDB-4D5A48DCE0FE")  // Requiere visado
                     : Guid.Parse("66ECE39B-E566-4DC1-BCDB-4D5A48DCE0FA"); // No requiere visado
            }
            else
            {
                vm.SelectedEstadoVisado = vm.Candidato.EstadoVisado.Id;
            }


            if (vm.SelectedComunidadAutonoma == Guid.Parse("00000000-0000-0000-0000-000000000000") && vm.Candidato.ComunidadAutonomaPreferida == null)
            {
                vm.SelectedComunidadAutonoma = Guid.Parse("DB7C9B1A-0CF9-46D4-A7EE-524532A87D8D"); //Ficha no cerrada
            }
            else
            {
                vm.SelectedComunidadAutonoma = vm.Candidato.ComunidadAutonomaPreferida.Id;
            }
            
            vm.SelectedProvincia = vm.Candidato.ProvinciaPreferida.Id;

            if (vm.SelectedMotivoCierreFicha == Guid.Parse("00000000-0000-0000-0000-000000000000") && vm.Candidato.MotivoCierreFicha == null)
            {
                vm.SelectedMotivoCierreFicha = Guid.Parse("DB7C9B1A-0CF9-46D4-A7EE-524532A87D8D"); //Ficha no cerrada
            }
            else
            {
                vm.SelectedMotivoCierreFicha = vm.Candidato.MotivoCierreFicha.Id;
            }

            if (vm.SelectedUniversidadDeRegistro == Guid.Parse("00000000-0000-0000-0000-000000000000") && vm.Candidato.UniversidadDeRegistro == null)
            {
                vm.SelectedUniversidadDeRegistro = Guid.Parse("154BEBC5-A2A1-4034-976D-9E4EFADDF7B4"); 
                    // Aún no registrado
            }
            else
            {
                vm.SelectedUniversidadDeRegistro = vm.Candidato.UniversidadDeRegistro.Id;
            }

            
            

            return View(vm);
        }

       

        //
        // POST: /Candidato/Details
        [HttpPost]
        public ActionResult DetailsCandidato(CandidatoDetailsVM candidatoVM)
        {
            try
            {
                //ModelState["Candidato.Procedencia.NombreProcedencia"].Errors.Clear();
                //ModelState["Candidato.Procedencia"].Errors.Clear();
                ModelState["Candidato.Nacionalidad.NombreNacionalidad"].Errors.Clear();
                ModelState["Candidato.Sexo"].Errors.Clear();
                candidatoVM.Candidato.Nacionalidad = _nacionalidadService.Get(candidatoVM.Candidato.Nacionalidad.Id);
                //candidatoVM.Candidato.Procedencia = _procedenciaService.Get(candidatoVM.Candidato.Procedencia.Id);
                candidatoVM.Candidato.Procedencia = _procedenciaService.Get(candidatoVM.ProcedenciaActual);
                candidatoVM.Candidato.TipoAlojamientoPreferido = _tipoAlojamientoService.Get(candidatoVM.SelectedTipoAlojamiento);
                candidatoVM.Candidato.ComunidadAutonomaPreferida = _comunidadAutonomaService.Get(candidatoVM.SelectedComunidadAutonoma);
                candidatoVM.Candidato.ProvinciaPreferida = _provinciaService.Get(candidatoVM.SelectedProvincia);
                candidatoVM.Candidato.EtapaEducativaPreferida = _etapaEducativaService.Get(candidatoVM.SelectedEtapaEducativa);
                candidatoVM.Candidato.EstadoVisado = _estadoVisadoService.Get(candidatoVM.SelectedEstadoVisado);
                candidatoVM.Candidato.MotivoCierreFicha =_motivoCierreFichaService.Get(candidatoVM.SelectedMotivoCierreFicha);
                candidatoVM.Candidato.UniversidadDeRegistro = _universidadDeRegistroService.Get(candidatoVM.SelectedUniversidadDeRegistro);

                //if (candidatoVM.Candidato.FechaLlegadaEspana < Convert.ToDateTime("01/01/1900 00:00:00"))
                //{
                //    candidatoVM.Candidato.FechaLlegadaEspana = Convert.ToDateTime("1753-01-01 00:00:00");
                //}

                //if (candidatoVM.Candidato.anyoContacto < Convert.ToDateTime("01/01/1900 00:00:00"))
                //{
                //    candidatoVM.Candidato.anyoContacto = Convert.ToDateTime("1753-01-01 00:00:00");
                //}

                if (ModelState.IsValid)
                {
                    _candidatoService.Update(candidatoVM.Candidato);
                    return RedirectToAction("DetailsCandidato", new
                    {
                        id = candidatoVM.Candidato.Id
                    });
                }
                return View("DetailsCandidato", candidatoVM);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return View();
            }
        }


        //
        // GET: /Candidato/Create

        public ActionResult Create()
        {
            
            CandidatoNewEditVM candidato = CreateViewModelNewedit();
            return View(candidato);
        }

        private CandidatoNewEditVM CreateViewModelNewedit()
        {
            CandidatoNewEditVM result = new CandidatoNewEditVM();
            result.Candidato = new Candidato() { Id = Guid.NewGuid() };
            result.Nacionalidades = _nacionalidadService.GetAll();
            result.Procedencias = _procedenciaService.GetAll();
            result.Candidato.FechaAlta = DateTime.Now;
            result.Candidato.FechaNac = DateTime.Now;
            result.Candidato.FechaDisponibilidad = DateTime.Now;
            result.Candidato.FechaLlegadaEspana = DateTime.Now;
            int proxNumSerie = _candidatoService.GetAll().Count() + 1;
            result.Candidato.Codigo = DateTime.Now.ToString("yy") + (proxNumSerie.ToString("D4"));

            return result;
        }

        //
        // POST: /Candidato/Create
        /// <summary>
        /// Dentro de esta accion es donde se crean la mayoria de valores relacionados a un candidato, se revisa si existen en la 
        /// base de datos y si no existen se crea el registro. 
        /// </summary>
        /// <param name="candidatoVM"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(CandidatoNewEditVM candidatoVM)
        {
            try
            {
                int proxNumSerie = _candidatoService.GetAll().Count() + 1;
                candidatoVM.Candidato.Codigo = DateTime.Now.ToString("yy") + (proxNumSerie.ToString("D4"));

                candidatoVM.Candidato.Procedencia = _procedenciaService.Get(candidatoVM.Candidato.Procedencia.Id);
                ModelState["Candidato.Procedencia.NombreProcedencia"].Errors.Clear();

                
                candidatoVM.Candidato.Nacionalidad =
                    _nacionalidadService.Get(candidatoVM.Candidato.Nacionalidad.Id);
                ModelState["Candidato.Nacionalidad.NombreNacionalidad"].Errors.Clear();


                candidatoVM.Candidato.TipoAlojamientoPreferido =
                        _tipoAlojamientoService.Get(Guid.Parse("3319A8B4-EB68-4055-B8CA-A8DECC2189A8"));

                candidatoVM.Candidato.EtapaEducativaPreferida =
                        _etapaEducativaService.Get(Guid.Parse("3319A8B4-EB68-4055-B8CA-A8DECC2189E8"));


                candidatoVM.Candidato.ComunidadAutonomaPreferida =
                       _comunidadAutonomaService.Get(Guid.Parse("5873E2BD-0FBE-4C5B-8918-34E878017AD9"));

                candidatoVM.Candidato.ProvinciaPreferida =
                        _provinciaService.Get(Guid.Parse("5873E2BD-0FBE-4C5B-8918-34E878017AD8"));

                candidatoVM.Candidato.EstadoVisado = 
                    _estadoVisadoService.Get(candidatoVM.Candidato.Nacionalidad.Visado 
                        ?Guid.Parse("66ECE39B-E566-4DC1-BCDB-4D5A48DCE0FE") 
                        :Guid.Parse("66ECE39B-E566-4DC1-BCDB-4D5A48DCE0FD"));


                if (_candidatoService.EmailNoExiste(candidatoVM.Candidato) == false)
                {
                    // ModelState["Candidato.Procedencia.NombreProcedencia"].Errors.Add("Email ya existe");
                    ModelState.AddModelError("Candidato.Email", "Email Ya existe. ¿Ya existe el candidato?");
                    candidatoVM.Nacionalidades = _nacionalidadService.GetAll();
                    candidatoVM.Procedencias = _procedenciaService.GetAll();
                    return View(candidatoVM);
                }

                 //Crear los valores del dropdownlist de motivos de cierre de ficha
                //if (_motivoCierreFichaService.Get(Guid.Parse("DB7C9B1A-0CF9-46D4-A7EE-524532A87D8A")) == null)
                //{
                //    MotivoCierreFicha listaNegra = new MotivoCierreFicha();
                //    listaNegra.Id = Guid.Parse("DB7C9B1A-0CF9-46D4-A7EE-524532A87D8A");
                //    listaNegra.Motivo = "Lista Negra";
                //    _motivoCierreFichaService.Add(listaNegra);
                //}

                

                if (ModelState.IsValid)
                {
                    
                    //candidatoVM.Candidato.NuevaExperiencia = new NuevaExperiencia() { Id = Guid.NewGuid() };
                    candidatoVM.Candidato.UniversidadDeRegistro = _universidadDeRegistroService.Get(Guid.Parse("154BEBC5-A2A1-4034-976D-9E4EFADDF7B4"));
                    
                    _candidatoService.Add(candidatoVM.Candidato);
                    
                }
                return RedirectToAction("DetailsCandidato", "Candidato", new
                {
                    id = candidatoVM.Candidato.Id
                });
            }
            catch (Exception e)
            {
                //_NLoggerService.Error(e.Message.ToString(), e.Message);

                return View("Error");
                //throw new Exception("Ha ocurrido un error.");
            }
        }


        //
        // GET: /Candidato/Preferencias

        public ActionResult Preferencias()
        {
            IList<CandidatoPreferenciasListVM> list = CreateViewModelList();
            return View(list);
        }

        private IList<CandidatoPreferenciasListVM> CreateViewModelList()
        {
            IList<CandidatoPreferenciasListVM> result = new List<CandidatoPreferenciasListVM>();

            foreach (var candidato in _candidatoService.GetAllButCerradosOrSeleccionados())
            {
                CandidatoPreferenciasListVM preferencia = new CandidatoPreferenciasListVM();
                preferencia.IdCandidato = candidato.Id;
                preferencia.CodigoCandidato = candidato.Codigo;
                preferencia.CandidatoNombre = candidato.Nombre ?? string.Empty;
                preferencia.CandidatoApellido1 = candidato.Apellido1 ?? string.Empty;
                //preferencia.CandidatoApellido2 = candidato.Apellido2 ?? string.Empty;
                preferencia.FechaDisponibilidad = candidato.FechaDisponibilidad;

                if (candidato.TipoAlojamientoPreferido == null)
                { preferencia.TipoAlojamiento = string.Empty; }
                else
                { preferencia.TipoAlojamiento = candidato.TipoAlojamientoPreferido.TipoAlojamientoNombre; }

                if (candidato.ComunidadAutonomaPreferida == null)
                { preferencia.ComunidadAutonoma = string.Empty; }
                else
                { preferencia.ComunidadAutonoma = candidato.ComunidadAutonomaPreferida.NombreComunidadAutonoma; }

                if (candidato.ProvinciaPreferida == null)
                { preferencia.Provincia = string.Empty; }
                else
                { preferencia.Provincia = candidato.ProvinciaPreferida.NombreProvincia; }

                if (candidato.EtapaEducativaPreferida == null)
                { preferencia.EtapaEducativa = string.Empty; }
                else
                { preferencia.EtapaEducativa = candidato.EtapaEducativaPreferida.EtapaEducativaNombre; }

                
                result.Add(preferencia);
            }
            return result;
        }

        //
        // POST: /Candidato/Preferencias

        [HttpPost]
        public ActionResult Preferencias(Guid id)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        //
        // GET: /Candidato/Preferencias

        public ActionResult Cerrados()
        {
            IList<CandidatosCerradosListVM> list = CreateCerradosVM();
            return View(list);
        }

        private IList<CandidatosCerradosListVM> CreateCerradosVM()
        {
            IList<CandidatosCerradosListVM> result = new List<CandidatosCerradosListVM>();

            foreach (var candidato in _candidatoService.GetAll())
            {
                if (candidato.fichaCerrada)
                {
                    CandidatosCerradosListVM candidatoCerrado = new CandidatosCerradosListVM();
                    candidatoCerrado.IdCandidato = candidato.Id;
                    candidatoCerrado.CodigoCandidato = candidato.Codigo;
                    candidatoCerrado.CandidatoNombre = candidato.Nombre ?? string.Empty;
                    candidatoCerrado.CandidatoApellido1 = candidato.Apellido1 ?? string.Empty;
                    //candidatoCerrado.CandidatoApellido2 = candidato.Apellido2 ?? string.Empty;
                    candidatoCerrado.MotivoCierreFicha = candidato.MotivoCierreFicha.Motivo;
                    candidatoCerrado.AnyoContacto = candidato.anyoContacto;

                 
                    result.Add(candidatoCerrado);    
                }
                
            }
            return result;
        }

        //
        // POST: /Candidato/Preferencias

        [HttpPost]
        public ActionResult Cerrados(Guid id)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        // GET: /Candidato/Seleccionados

        public ActionResult Seleccionados()
        {
            IList<Candidato> list = CreateSeleccionadosList();
            return View(list);
        }

        private IList<Candidato> CreateSeleccionadosList()
        {
            IList<Candidato> result = new List<Candidato>();
            foreach (var candidato in _candidatoService.GetAllSeleccionados())
            {
                    Candidato candidatoSeleccionado = new Candidato();
                    candidatoSeleccionado = candidato;
                    result.Add(candidatoSeleccionado);
            }
            return result;
        }

        public ActionResult EntrevistasList()
        {
            IList<CandidatoEntrevistasListVM> list = CreateEntrevistasListVM();
            return View(list);
        }

        private IList<CandidatoEntrevistasListVM> CreateEntrevistasListVM()
        {

            IList<CandidatoEntrevistasListVM> result = new List<CandidatoEntrevistasListVM>();
            foreach (var candidato in _candidatoService.GetAllButCerradosOrSeleccionados())
            if ((candidato.FechaEntrevistaNativo != null || candidato.FechaEntrevistaUP != null) && (candidato.EntrevistaConNativo != true || candidato.EntrevistaConUp != true)) 
            {
                CandidatoEntrevistasListVM entrevistasDelCandidato = new CandidatoEntrevistasListVM();
                entrevistasDelCandidato.Id = candidato.Id;
                entrevistasDelCandidato.Codigo = candidato.Codigo;
                entrevistasDelCandidato.Nombre = candidato.Nombre;
                entrevistasDelCandidato.Apellidos = candidato.Apellido1;
                entrevistasDelCandidato.Email = candidato.Email;
                entrevistasDelCandidato.Skype = candidato.Skype;
                entrevistasDelCandidato.EntrevistaNativo = candidato.EntrevistaConNativo;
                entrevistasDelCandidato.EntrevistaUp = candidato.EntrevistaConUp;
                entrevistasDelCandidato.FechaEntrevistaNativo = candidato.FechaEntrevistaNativo;
                entrevistasDelCandidato.FechaEntrevistaUp = candidato.FechaEntrevistaUP;
                entrevistasDelCandidato.HoraEntrevistaNativo = candidato.HoraEntrevistaNativo;
                entrevistasDelCandidato.HoraEntrevistaUp = candidato.HoraEntrevistaUp;

                result.Add(entrevistasDelCandidato);
            }
            return result;
        }



        public ActionResult PerfilesList()
        {
            IList<CandidatoPerfilesListVM> list = CreatePerfilesListVM();
            return View(list);
        }

        private IList<CandidatoPerfilesListVM> CreatePerfilesListVM()
        {

            IList<CandidatoPerfilesListVM> result = new List<CandidatoPerfilesListVM>();
            foreach (var candidato in _candidatoService.GetAllButCerradosOrSeleccionados())
                
                {
                    CandidatoPerfilesListVM perfilDelCandidato = new CandidatoPerfilesListVM();
                    perfilDelCandidato.Id = candidato.Id;
                    perfilDelCandidato.Codigo = candidato.Codigo;
                    perfilDelCandidato.Nombre = candidato.Nombre + " " +candidato.Apellido1 + candidato.Apellido2;
                    perfilDelCandidato.Edad = DateTime.Today.Year - candidato.FechaNac.Year;
                    perfilDelCandidato.Carrera = candidato.Carrera;
                    perfilDelCandidato.CantidadExperiencia = candidato.CantidadDeExperiencia;
                    perfilDelCandidato.TC = candidato.TC;
                    //perfilDelCandidato.Test = candidato.InformeEntrevistaEvaluaciones.Count != 0 ? candidato.InformeEntrevistaEvaluaciones.First().ValGlobal : 99;
                    perfilDelCandidato.Test = candidato.AjusteAlPuesto + " " + candidato.DeseabilidadSocial + "%";
                    
                    perfilDelCandidato.Destacar = candidato.Destacar;

                    result.Add(perfilDelCandidato);
                }
            return result;
        }

    }
}
 