﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.WEB.Controllers
{
    [HandleError]
    public class HomeController : Controller
    {
        //private INLogger _NLoggerService;


        public HomeController()
        {
            //_NLoggerService =
                //IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
                //as INLogger;
        }

        public ActionResult Index()
        {
           
           
            //ViewData["Message2"] = "NLogger: Visita a la página principal registrada";
            //_NLoggerService.Info("Visita a la página principal registrada");
            return RedirectToAction("LogOn", "Account");
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
