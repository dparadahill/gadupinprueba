﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.WEB.Controllers
{
    public class TipoAlojamientoController : Controller
    {
        private ITipoAlojamientoService _tipoAlojamientoService;
        //private INLogger _NLoggerService;

        public TipoAlojamientoController( ITipoAlojamientoService tipoAlojamientoService)
        {
            _tipoAlojamientoService = tipoAlojamientoService;
            //_NLoggerService =
            //IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
            //as INLogger;
        }
        //
        // GET: /TipoAlojamiento/

        public ActionResult Index()
        {
            return View(_tipoAlojamientoService.GetAll());
        }


        //
        // GET: /TipoAlojamiento/Create

        public ActionResult Create()
        {
            TipoAlojamiento tipoAlojamiento = new TipoAlojamiento();
            tipoAlojamiento.Id = Guid.NewGuid();
            return View(tipoAlojamiento);
        }

        //
        // POST: /TipoAlojamiento/Create

        [HttpPost]
        public ActionResult Create(TipoAlojamiento tipoAlojamiento)
        {
            try
            {

                _tipoAlojamientoService.Add(tipoAlojamiento);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /TipoAlojamiento/Edit/5

        public ActionResult Edit(Guid id)
        {
            TipoAlojamiento tipoAlojamiento = new TipoAlojamiento();
            tipoAlojamiento = _tipoAlojamientoService.Get(id);
            return View(tipoAlojamiento);
        }

        //
        // POST: /TipoAlojamiento/Edit/5

        [HttpPost]
        public ActionResult Edit(TipoAlojamiento tipoAlojamiento)
        {
            try
            {
                _tipoAlojamientoService.Update(tipoAlojamiento);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /TipoAlojamiento/Delete/5

        public ActionResult Delete(Guid id)
        {
            try
            {
                
                return View(_tipoAlojamientoService.Get(id));
            }
            catch
            {
                return RedirectToAction("Index");
            }

        }

        //
        // POST: /TipoAlojamiento/Delete/5

        [HttpPost]
        public ActionResult Delete(TipoAlojamiento tipoAlojamiento)
        {
            try
            {
                _tipoAlojamientoService.Remove(tipoAlojamiento.Id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}