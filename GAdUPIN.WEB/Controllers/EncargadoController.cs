﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.Domain;
using GAdUPIN.WEB.ViewModels;

namespace GAdUPIN.WEB.Controllers
{
    public class EncargadoController: Controller
    {
         private IEncargadoService _encargadoService;
         private IComunidadAutonomaService _comunidadAutonomaService;
         private ICandidatoService _candidatoService;
        //private INLogger _NLoggerService;

        public EncargadoController(IEncargadoService encargadoService, IComunidadAutonomaService comunidadAutonomaService, 
                                   ICandidatoService candidatoService)
        {
            _encargadoService = encargadoService;
            _comunidadAutonomaService = comunidadAutonomaService;
            _candidatoService = candidatoService;
            //_NLoggerService =
            //IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
            //as INLogger;
        }
        //
        // GET: /Encargado/

        public ActionResult Index()
        {
            return View(_encargadoService.GetAll());
        }


        //
        // GET: /Encargado/Create

        public ActionResult Create()
        {
            EncargadoNewEditVM encargado = CreateViewModelNewEdit();
            return View(encargado);
        }

        private EncargadoNewEditVM CreateViewModelNewEdit()
        {
            EncargadoNewEditVM result = new EncargadoNewEditVM();
            result.Encargado = new Encargado() { Id = Guid.NewGuid() };
            result.ComunidadesAutonomas = _comunidadAutonomaService.GetAll();
            //int proxNumSerie = _candidatoService.GetAll().Count() + 1;
            //result.Candidato.Codigo = DateTime.Now.ToString("yy") + (proxNumSerie.ToString("D4"));

            return result;
        }

        //
        // POST: /Encargado/Create

        [HttpPost]
        public ActionResult Create(EncargadoNewEditVM encargadoVM)
        {
            try
            {
                encargadoVM.Encargado.ComunidadAutonoma =
                    _comunidadAutonomaService.Get(encargadoVM.Encargado.ComunidadAutonoma.Id);
                _encargadoService.Add(encargadoVM.Encargado);

                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }

        //
        // GET: /Encargado/Edit/5

        public ActionResult Edit(Guid id)
        {
            EncargadoNewEditVM encargado = EditViewModelNewEdit(id);
            return View(encargado);
        }

        private EncargadoNewEditVM EditViewModelNewEdit(Guid id)
        {
            EncargadoNewEditVM result = new EncargadoNewEditVM();
            result.Encargado = _encargadoService.Get(id);
            result.ComunidadesAutonomas = _comunidadAutonomaService.GetAll();
            //int proxNumSerie = _candidatoService.GetAll().Count() + 1;
            //result.Candidato.Codigo = DateTime.Now.ToString("yy") + (proxNumSerie.ToString("D4"));

            return result;
        }

        //
        // POST: /Encargado/Edit/5

        [HttpPost]
        public ActionResult Edit(EncargadoNewEditVM EncargadoVM)
        {
            try
            {
                EncargadoVM.Encargado.ComunidadAutonoma = _comunidadAutonomaService.Get(EncargadoVM.Encargado.ComunidadAutonoma.Id);
                _encargadoService.Update(EncargadoVM.Encargado);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

       

        //
        // GET: /Encargado/Delete/5

        public ActionResult Delete(Guid id)
        {
            return View(_encargadoService.Get(id));
        }

        //
        // POST: /Encargado/Delete/5

        [HttpPost]
        public ActionResult Delete(Guid id, Encargado encargado)
        {
            try
            {
                _encargadoService.Remove(encargado.Id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }



        //
        // GET: /Encargado/ResponsablePor/5

        public ActionResult ResponsablePor(Guid id)
        {
            return View(_candidatoService.GetByEncargado(id));
        }

        //
        // POST: /Encargado/ResponsablePor/5

        [HttpPost]
        public ActionResult ResponsablePor(Guid id, Encargado encargado)
        {
            try
            {
                Candidato candidato = _candidatoService.GetIncNacionalidad(id);
                candidato.Encargado = null;
                _candidatoService.Update(candidato);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        

        //
        // GET: /Encargado/AsignarResponsable
        [HttpGet]
        public ActionResult AsignarEncargado(Guid idCandidato)
        {
            ViewData["idCandidato"] = idCandidato;

            return View(_encargadoService.GetAll());
        }



        //
        // GET: /PlazaColegio/CubrirPlaza

        [HttpGet]
        public ActionResult Asignar(Guid idCandidato, Guid idEncargado)
        {
            try
            {
                Candidato candidato = _candidatoService.Get(idCandidato);
                Encargado encargado = _encargadoService.Get(idEncargado);
                //encargado.Candidatos.Add(candidato);
                candidato.Encargado = encargado;

                _candidatoService.Update(candidato);
                //_encargadoService.Update(encargado);

                return RedirectToAction("DetailsCandidato", "Candidato", new
                {
                    id = idCandidato
                });
            }
            catch
            {
                return RedirectToAction("Index", new
                {
                    id = idEncargado
                });
            }
        }


        //[HttpGet]
        //public ActionResult Desasignar(Guid id)
        //{
        //    return View(_plazaColegioService.Get(id));
        //}

        [HttpGet]
        public ActionResult DesasignarEncargado(Guid idCandidato)
        {

            //var vPlazaColegio = _plazaColegioService.Get(plazaColegio.Id);
            _candidatoService.DesasignarEncargado(idCandidato);

            return RedirectToAction("DetailsCandidato", "Candidato", new
            {
                id = idCandidato
            });
        }

    }
}
