﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Security.AccessControl;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.WEB.ViewModels;

namespace GAdUPIN.WEB.Controllers
{
    public class FotoPerfilController : Controller
    {
        private IDocumentoDeTramiteService _documentoDeTramite;
        private ICandidatoService _candidatoService;

        public FotoPerfilController(IDocumentoDeTramiteService documentoDeTramite, ICandidatoService candidatoService )
        {
            _documentoDeTramite = documentoDeTramite;
            _candidatoService = candidatoService;
        }

        //
        // GET: /Documento/

        //public ActionResult Index(Guid id)
        //{
        //    IList<DocumentacionRequeridaListVM> documentacion = CreateListVM(id);
            

        //    return View(documentacion);
        //}

        //private IList<DocumentacionRequeridaListVM> CreateListVM(Guid id)
        //{
        //    IList<DocumentacionRequeridaListVM> result = new List<DocumentacionRequeridaListVM>();

        //    if (_documentoDeTramite.GetByCandidato(id).Count == 0)
        //    {
        //        result.Add(new DocumentacionRequeridaListVM()
        //        {
        //            NombreDocumento = "Sin subir",
        //            FechaSubida = null,
        //            Link = "",
        //            IdCandidato = id
        //        });
        //    }
        //    else
        //    {
        //        foreach (var documento in _documentoDeTramite.GetByCandidato(id))
        //        {
        //            result.Add(new DocumentacionRequeridaListVM()
        //            {
        //                NombreDocumento = documento.NombreDocumento,
        //                FechaSubida = documento.FechaSubida,
        //                Link = documento.Path,
        //                IdCandidato = id
        //            });

        //        }
        //    }
            
        //    return result;
        //}

        
        public ActionResult SubirFoto()
        {

            return View("SubirFoto");
        }

        [HttpPost]
        public ActionResult SubirFoto(HttpPostedFileBase file, FormCollection collection)
        {
            string a = Request.RawUrl;
            a = a.Replace("/Documento/SubirFoto/", "");
            int imgSize = file.ContentLength;
            //string nombreDocumento = collection["NombreDoc"];
            //DocumentoDeTramite documento = new DocumentoDeTramite()
            //{
            //    Id = Guid.NewGuid(),
            //    NombreDocumento = nombreDocumento,
            //};
            //documento.Candidato = _candidatoService.Get(Guid.Parse(a));
            Candidato candidato = _candidatoService.Get(Guid.Parse(a));
            string nombreDocumento = "fotoPerfil";
            if (file.ContentLength > 0 && file.FileName != "")
            {
                var fileName = Path.GetFileName(file.FileName);
                //string pathCheck = Server.MapPath("~/App_Data/uploads/" + candidato.Codigo);
                string fExtension = Path.GetExtension(fileName);
                //string newFileName = nombreDocumento + fExtension;

                if (imgSize > 2097151)
                {
                    return View("Error"); //File too big.
                }

                if (fExtension.ToUpper().Trim() != ".JPG" && fExtension.ToUpper() != ".PNG" && fExtension.ToUpper() != ".JPEG")
                {
                    //  only .jpg, .png and .jpeg image types!')";
                }
                else
                {

                    string pathCheck = Server.MapPath("~/App_Data/uploads/" + candidato.Codigo);
                    string newFileName = nombreDocumento + fExtension;
                    
                    if (!(System.IO.Directory.Exists(pathCheck)))
                    {
                        System.IO.Directory.CreateDirectory(pathCheck);
                    }
                    var path = Path.Combine(Server.MapPath("~/App_Data/uploads/" + candidato.Codigo), newFileName);
                    file.SaveAs(path);
                    
                    candidato.fotoPerfilPath = path;

                    
                    
                    Response.Write("Image Saved Successfully!");

                }

                    
            }

            
            //documento.Subido = true;
            //documento.FechaSubida = DateTime.Now;
            
            //_documentoDeTramite.Add(documento);
            _candidatoService.Update(candidato);

            return RedirectToAction("DetailsCandidato", "Candidato",new
            {
                id = candidato.Id
            });
        }



        //private void StartUpLoad()
        //{

        //    string imgName = fuimage.FileName;

        //    int imgSize = fuimage.PostedFile.ContentLength;

        //    string ext = System.IO.Path.GetExtension(this.fuimage.PostedFile.FileName);

        //    if (fuimage.PostedFile != null && fuimage.PostedFile.FileName != "")
        //    {

        //        if (imgSize > 2097151)
        //        {
        //            lblimgname.Text = "alert('File is too big.')";
        //        }

        //        if (ext.ToUpper().Trim() != ".JPG" && ext.ToUpper() != ".PNG" && ext.ToUpper() != ".JPEG")
        //        {
        //            lblimgname.Text = "alert('Please choose only .jpg, .png and .jpeg image types!')";
        //        }
        //        else
        //        {

        //            string fileName = fuimage.FileName.ToString();

        //            string uploadFolderPath = "~/staff_image/";

        //            string filePath = HttpContext.Current.Server.MapPath(uploadFolderPath);

        //            fuimage.SaveAs(filePath + "\\" + fileName);

        //            imgstaff.ImageUrl = "~/Event_Image/" + fuimage.FileName.ToString();

        //            lblimgname.Text = fuimage.FileName.ToString();

        //            Response.Write("Image Saved Successfully!");

        //        }

        //    }

        //}



        //public ActionResult Upload()
        //{

        //    return View("Upload");
        //}

        //[HttpPost] public ActionResult Upload(HttpPostedFileBase file, FormCollection collection)
        //{
        //    string a = Request.RawUrl;
        //    a = a.Replace("/Documento/SubirFoto/", "");

        //    string nombreDocumento = "FotoPrincipal";
        //    DocumentoDeTramite documento = new DocumentoDeTramite()
        //    {
        //        Id = Guid.NewGuid(),
        //        NombreDocumento = nombreDocumento,
        //    };
        //    documento.Candidato = _candidatoService.Get(Guid.Parse(a));

        //    if (file.ContentLength > 0)
        //    {
        //        var fileName = Path.GetFileName(file.FileName);
        //        string pathCheck = Server.MapPath("~/App_Data/uploads/" + documento.Candidato.Codigo);
        //        string fExtension = Path.GetExtension(fileName);
        //        string newFileName = nombreDocumento + fExtension;

        //        if (!(System.IO.Directory.Exists(pathCheck)))
        //        {
        //            System.IO.Directory.CreateDirectory(pathCheck);
        //        }
        //        var path = Path.Combine(Server.MapPath("~/App_Data/uploads/" + documento.Candidato.Codigo), newFileName);
        //        file.SaveAs(path);
        //        documento.Path = path;
        //    }


        //    documento.Subido = true;
        //    documento.FechaSubida = DateTime.Now;

        //    _documentoDeTramite.Add(documento);

        //    return RedirectToAction("Index", new
        //    {
        //        id = documento.Candidato.Id
        //    });
        //}


        
        //public ActionResult Descargar(string path, Guid id)
        //{
        //    string a = Request.QueryString["path"];
            
        //    Response.AddHeader("Content-Disposition", "attachment; filename=" + a);
        //    Response.Clear();
        //    Response.ClearContent();
        //    Response.TransmitFile(a);
        //    Response.Flush();
        //    Response.End();
        //    return RedirectToAction("Index", new
        //    {
        //        id
        //    });  
        //}


    }
}
