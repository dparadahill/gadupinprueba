﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;
using GAdUPIN.WEB.ViewModels;

namespace GAdUPIN.WEB.Controllers
{
    public class PlazaColegioController : Controller
    {
        private IPlazaColegioService _plazaColegioService;
        private IColegioService _colegioService;
        private ICandidatoService _candidatoService;
        private IEtapaEducativaService _etapaEducativaService;
        private INacionalidadService _nacionalidadService;
        //private INLogger _NLoggerService;


        public PlazaColegioController( IPlazaColegioService plazaColegioService, IColegioService colegioService, INacionalidadService nacionalidadService,
                                       ICandidatoService candidatoService, IEtapaEducativaService etapaEducativaService)
        {
            _plazaColegioService = plazaColegioService;
            _colegioService = colegioService;
            _nacionalidadService = nacionalidadService;
            _candidatoService = candidatoService;
            _etapaEducativaService = etapaEducativaService;

            //_NLoggerService =
            //IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
            //as INLogger;
        }
       
        //
        // GET: /PlazaColegio/
        public ActionResult Index(Guid id)
        {
            
            IList<PlazaColegioListVM> list = CreateViewModelList(id);
            ViewData["idColegio"] = id;

            return View(list);
        }

        private IList<PlazaColegioListVM> CreateViewModelList(Guid id)
        {
            IList<PlazaColegioListVM> result = new List<PlazaColegioListVM>();
            List<PlazaColegio> plazasColegios = _plazaColegioService.GetByColegio(id).ToList();

            
            foreach (var plazaColegio in plazasColegios) //_plazaColegioService.GetByColegio(id)
            {
               
                    result.Add(new PlazaColegioListVM()
                    {
                        Id = plazaColegio.Id,
                        IdColegio = plazaColegio.Colegio.Id,
                        CuantiaBeca = plazaColegio.BecaCuantia,
                        FechaInicio = plazaColegio.FechaInicio,
                        FechaFin = plazaColegio.FechaFin,
                        PlazaContratada = plazaColegio.PlazaContratada,
                        EtapaEducativa = plazaColegio.EtapaEducativa,
                        PreferenciaSexo = plazaColegio.PreferenciaSexo,
                        NumeroHoras = plazaColegio.NumeroHorasPlaza,
                        CubiertaPor = plazaColegio.CandidatoAsignado, 
                        NombreColegio = plazaColegio.Colegio.NombreColegio, 
                        CodigoPlaza = plazaColegio.CodigoPlaza,
                        DatoAdicional = plazaColegio.DatoAdicional,
                        Condiciones = plazaColegio.CondicionesOfrecidas

                    });
                
            }
            
             
            
            return result;
        }


        //
        // GET: /PlazaColegio/Create

        public ActionResult Create(Guid colegio)
        {
            PlazaColegioNewEditVM plazaColegio = CreateViewModelNewEdit(colegio);
            return View(plazaColegio);
        }

        private PlazaColegioNewEditVM CreateViewModelNewEdit(Guid id)
        {
            PlazaColegioNewEditVM result = new PlazaColegioNewEditVM();

            result.PlazaColegio = new PlazaColegio() { Id = Guid.NewGuid() };
            result.PlazaColegio.Colegio = _colegioService.Get(id);
            //result.SelectedColegio = id;
            result.SelectedColegio = result.PlazaColegio.Colegio.Id;
            result.NombreColegio = result.PlazaColegio.Colegio.NombreColegio;
            result.EtapasEducativas = _etapaEducativaService.GetAll();
            result.Nacionalidades = _nacionalidadService.GetAll();
            int proxNumSerie = _plazaColegioService.GetAll().Count() + 1;
            result.CodigoPlaza = "P-" + DateTime.Now.ToString("yy") + (proxNumSerie.ToString("D4"));
            //result.Candidatos = _candidatoService.GetAll();
            

            return result;
        } 

        //
        // POST: /PlazaColegio/Create

        [HttpPost]
        public ActionResult Create(PlazaColegioNewEditVM plazaColegioVM)
        {
            try
            {
                ModelState["PlazaColegio.EtapaEducativa.EtapaEducativaNombre"].Errors.Clear();
                if (ModelState.IsValid)
                {
                    plazaColegioVM.PlazaColegio.EtapaEducativa = _etapaEducativaService.Get(plazaColegioVM.PlazaColegio.EtapaEducativa.Id);
                    plazaColegioVM.PlazaColegio.Colegio = _colegioService.Get(plazaColegioVM.SelectedColegio);
                    plazaColegioVM.PlazaColegio.NacionalidadPreferente = _nacionalidadService.Get(plazaColegioVM.SelectedNacionalidad);
                    int proxNumSerie = _plazaColegioService.GetAll().Count() + 1;
                    plazaColegioVM.PlazaColegio.CodigoPlaza = "P-" + DateTime.Now.ToString("yy") + (proxNumSerie.ToString("D4"));

                    _plazaColegioService.Add(plazaColegioVM.PlazaColegio);
                    return RedirectToAction("Index", new
                    {
                        id = plazaColegioVM.SelectedColegio
                    });
                }
                return RedirectToAction("Index", new
                {
                    id = plazaColegioVM.SelectedColegio
                });
            }
            catch
            {
                throw new NotImplementedException();
                //return View("Error");
            }
        }

        //// GET: /PlazaColegio/CreateWOColegio
        //public ActionResult CreateWOColegio()
        //{
        //    PlazaColegioNewEditVM plazaColegio = CreateViewModelNewEditWOColegio();
        //    return View(plazaColegio);

        //}

        //private PlazaColegioNewEditVM CreateViewModelNewEditWOColegio()
        //{
        //    PlazaColegioNewEditVM result = new PlazaColegioNewEditVM();

        //    result.PlazaColegio = new PlazaColegio() { Id = Guid.NewGuid() };
        //    result.Colegios = _colegioService.GetAll();
        //    result.EtapasEducativas = _etapaEducativaService.GetAll();

        //    return result;
        //}

        ////
        //// POST: /PlazaColegio/CreateWOColegio

        //[HttpPost]
        //public ActionResult CreateWOColegio(PlazaColegioNewEditVM plazaColegioVM)
        //{
        //    ModelState["PlazaColegio.EtapaEducativa.EtapaEducativaNombre"].Errors.Clear();
            
            
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            plazaColegioVM.PlazaColegio.EtapaEducativa = _etapaEducativaService.Get(plazaColegioVM.PlazaColegio.EtapaEducativa.Id);
        //            plazaColegioVM.PlazaColegio.Colegio = _colegioService.Get(plazaColegioVM.SelectedColegio);
        //            _plazaColegioService.Add(plazaColegioVM.PlazaColegio);

        //        }

        //        return RedirectToAction("Index", "PlazaColegio", new
        //        {
        //            id = plazaColegioVM.SelectedColegio
        //        });
        //    }
        //    catch
        //    {
        //        return View("Error");
        //    }
            
        //}



        //
        // GET: /PlazaColegio/Edit/5
        public ActionResult Edit(Guid id)
        {
            PlazaColegioNewEditVM vm = new PlazaColegioNewEditVM();
            vm.PlazaColegio = _plazaColegioService.Get(id);
            //vm.PlazaColegio.Colegio = _plazaColegioService.GetWithColegio(vm.plazaColegio.Colegio.Id);
            vm.Candidatos = _candidatoService.GetAllButCerradosOrSeleccionados();
            vm.SelectedColegio = vm.PlazaColegio.Colegio.Id;
            vm.PreferenciaSexoActual = vm.PlazaColegio.PreferenciaSexo;
            vm.EtapaEducativaActual = vm.PlazaColegio.EtapaEducativa;
            vm.EtapasEducativas = _etapaEducativaService.GetAll();
            vm.CandidatoAsignado = vm.PlazaColegio.CandidatoAsignado;

            vm.SelectedNacionalidad = vm.PlazaColegio.NacionalidadPreferente.Id;
            vm.Nacionalidades = _nacionalidadService.GetAll();
            vm.CuantiaBeca = vm.PlazaColegio.BecaCuantia;

            
            

            return View(vm);
        }

        //
        // POST: /PlazaColegio/Edit/5

        [HttpPost]
        public ActionResult Edit(PlazaColegioNewEditVM plazaColegioVM)
        {
            try
            {
                plazaColegioVM.PlazaColegio.Colegio = _colegioService.Get(plazaColegioVM.SelectedColegio);
                plazaColegioVM.PlazaColegio.EtapaEducativa = _etapaEducativaService.Get(plazaColegioVM.PlazaColegio.EtapaEducativa.Id);
                plazaColegioVM.PlazaColegio.CandidatoAsignado = plazaColegioVM.CandidatoAsignado;
                
                _plazaColegioService.Update(plazaColegioVM.PlazaColegio);

                return RedirectToAction("Index", new
                {
                    id = plazaColegioVM.PlazaColegio.Colegio.Id
                });
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /PlazaColegio/Delete/5
 
        public ActionResult Delete(Guid id)
        {
            return View(_plazaColegioService.Get(id));
        }

        //
        // POST: /PlazaColegio/Delete/5

        [HttpPost]
        public ActionResult Delete(PlazaColegio plazaColegio)
        {
            try
            {
                var idColegioA = _plazaColegioService.Get(plazaColegio.Id).Colegio.Id;
                _plazaColegioService.Remove(plazaColegio);

                return RedirectToAction("Index", new
                {
                    id = idColegioA
                });
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /PlazaColegio/Details/5

        //public ActionResult Details(int id)
        //{
        //    return View();
        //}


        //
        // GET: /Candidato/
        public ActionResult ListarCandidatosParaAsignacion(Guid idPlaza)
        {

            IList<CandidatoAsignarPlazaListVM> list = CreateVMBasedOnPlaza(idPlaza);

            return View(list);
        }

        private IList<CandidatoAsignarPlazaListVM> CreateVMBasedOnPlaza(Guid idPlaza)
        {
            IList<CandidatoAsignarPlazaListVM> result = new List<CandidatoAsignarPlazaListVM>();
            List<Candidato> candidatos = _candidatoService.GetAllButCerradosOrSeleccionados().ToList();
            

            foreach (var candidato in candidatos) //_plazaColegioService.GetByColegio(id)
            {
                result.Add(new CandidatoAsignarPlazaListVM()
                {
                    Candidato = candidato,
                    IdPlaza = idPlaza
                });
            }



            return result;
        }

        //
        // GET: /PlazaColegio/CubrirPlaza
        [HttpGet]
        public ActionResult CubrirPlaza(Guid idCandidato, Guid idPlaza)
        {
            try
            {
                Candidato candidato = _candidatoService.GetIncNacionalidad(idCandidato);
                PlazaColegio plaza = _plazaColegioService.Get(idPlaza);
                plaza.CandidatoAsignado = candidato;
                plaza.PlazaContratada = true;
               
                
                //_candidatoService.Update(candidato);
                candidato.PlazasDeColegiosAsignadas.Add(plaza);
                
                _candidatoService.Update(candidato);
                _plazaColegioService.Update(plaza);

                return RedirectToAction("DetailsCandidato","Candidato",  new
                {
                    id = idCandidato
                });
            }
            catch
            {
                return RedirectToAction("Index", new
                {
                    id = _plazaColegioService.Get(idPlaza).Colegio.Id
                });
            }
        }


        [HttpGet]
        public ActionResult DesasignarPlaza(Guid id)
        {
            return View(_plazaColegioService.Get(id));
        }

        [HttpPost]
        public ActionResult DesasignarPlaza(PlazaColegio plazaColegio)
        {
            plazaColegio = _plazaColegioService.Get(plazaColegio.Id);
            Candidato candidato = _candidatoService.Get(plazaColegio.CandidatoAsignado.Id);
            candidato.PlazasDeColegiosAsignadas.Remove(plazaColegio);
            plazaColegio.CandidatoAsignado = null;
            plazaColegio.PlazaContratada = false;

            //PlazaColegio  plaza =  _plazaColegioService.Get(plazaColegio.Id);
            _candidatoService.Update(candidato); 
            _plazaColegioService.Update(plazaColegio);

            
            
            return RedirectToAction("Edit", new
            {
                id = plazaColegio.Id
            });
        }

         //[HttpGet]
        //public ActionResult TodasActivas()
        // {
        //     return View(_plazaColegioService.GetAll());
        // }

         //
         // GET: /PlazaColegio/
        [HttpGet]
         public ActionResult TodasActivas()
         {

             IList<PlazaColegioListVM> list = CreatePlazasActivasListVM();

             return View(list);
         }

         private IList<PlazaColegioListVM> CreatePlazasActivasListVM()
         {
             IList<PlazaColegioListVM> result = new List<PlazaColegioListVM>();
             List<PlazaColegio> plazasColegios = _plazaColegioService.GetAll().ToList();


             foreach (var plazaColegio in plazasColegios) //_plazaColegioService.GetByColegio(id)
             {
                if (!plazaColegio.Colegio.Desactivado)
                    if (plazaColegio.CandidatoAsignado == null)
                    {
                        result.Add(new PlazaColegioListVM()
                        {
                            Id = plazaColegio.Id,
                            IdColegio = plazaColegio.Colegio.Id,
                            NombreColegio = plazaColegio.Colegio.NombreColegio,
                            PlazaContratada = plazaColegio.PlazaContratada,
                            CuantiaBeca = plazaColegio.BecaCuantia,
                            NumeroHoras = plazaColegio.NumeroHorasPlaza,
                            FechaInicio = plazaColegio.FechaInicio,
                            EtapaEducativa = plazaColegio.EtapaEducativa,
                            PreferenciaSexo = plazaColegio.PreferenciaSexo, 
                            CodigoCandidato = "",
                            NombreComunidadAutonoma = plazaColegio.Colegio.ComunidadAutonoma.NombreComunidadAutonoma,
                            NacionalidadPreferente = plazaColegio.NacionalidadPreferente,
                            DatoAdicional = plazaColegio.DatoAdicional,
                            Condiciones = plazaColegio.CondicionesOfrecidas
                         });
                    }
                    else
                    {
                        result.Add(new PlazaColegioListVM()
                        {
                            Id = plazaColegio.Id,
                            IdColegio = plazaColegio.Colegio.Id,
                            IdCandidato = plazaColegio.CandidatoAsignado.Id,
                            NombreColegio = plazaColegio.Colegio.NombreColegio,
                            PlazaContratada = plazaColegio.PlazaContratada,
                            CuantiaBeca = plazaColegio.BecaCuantia,
                            NumeroHoras = plazaColegio.NumeroHorasPlaza,
                            FechaInicio = plazaColegio.FechaInicio,
                            EtapaEducativa = plazaColegio.EtapaEducativa,
                            PreferenciaSexo = plazaColegio.PreferenciaSexo, 
                            CodigoCandidato = plazaColegio.CandidatoAsignado.Codigo,
                            //NombreCandidato = plazaColegio.CandidatoAsignado.Nombre + " " + plazaColegio.CandidatoAsignado.Apellido1,
                            NombreComunidadAutonoma = plazaColegio.Colegio.ComunidadAutonoma.NombreComunidadAutonoma
                        });
                    }
               }
                return result;
         }

         //
         // GET: /PlazaColegio/EnviarPlaza
         [HttpGet]
         public ActionResult ListarPlazasParaAsignacion(Guid idCandidato, bool cubrir)
         {
             
             ViewData["idCandidato"] = idCandidato;
             IList<PlazaColegioListVM> list = CreatePlazasParaAsignacionListVM(cubrir);

             return View(list);
         }

         private IList<PlazaColegioListVM> CreatePlazasParaAsignacionListVM(bool cubrir)
         {
             IList<PlazaColegioListVM> result = new List<PlazaColegioListVM>();
             List<PlazaColegio> plazasColegios = _plazaColegioService.GetAll().ToList();


             foreach (var plazaColegio in plazasColegios) //_plazaColegioService.GetByColegio(id)
             {
                 if (!plazaColegio.Colegio.Desactivado)
                     if (plazaColegio.CandidatoAsignado == null)
                     {
                         result.Add(new PlazaColegioListVM()
                         {
                             Id = plazaColegio.Id,
                             IdColegio = plazaColegio.Colegio.Id,
                             NombreColegio = plazaColegio.Colegio.NombreColegio,
                             PlazaContratada = plazaColegio.PlazaContratada,
                             CuantiaBeca = plazaColegio.BecaCuantia,
                             NumeroHoras = plazaColegio.NumeroHorasPlaza,
                             FechaInicio = plazaColegio.FechaInicio,
                             EtapaEducativa = plazaColegio.EtapaEducativa,
                             PreferenciaSexo = plazaColegio.PreferenciaSexo,
                             CodigoCandidato = null,
                             NombreComunidadAutonoma = plazaColegio.Colegio.ComunidadAutonoma.NombreComunidadAutonoma, 
                             CodigoPlaza = plazaColegio.CodigoPlaza,
                             Cubrir = cubrir
                             

                         });
                     }
                     
             }
             return result;
         }

        [HttpGet]
        public ActionResult EnviarPlaza(Guid idCandidato, Guid idPlaza)
        {
            try
            {
                Candidato candidato = _candidatoService.GetIncNacionalidad(idCandidato);
                PlazaColegio plaza = _plazaColegioService.Get(idPlaza);
                candidato.PlazaOfrecida = plaza;
                

                _candidatoService.Update(candidato);

                return RedirectToAction("DetailsCandidato", "Candidato",new
                {
                    id = candidato.Id
                });
            }
            catch
            {
                return RedirectToAction("Index", new
                {
                    id = _plazaColegioService.Get(idPlaza).Colegio.Id
                });
            }
        }
    }
}

