﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Services;
using System.Web.Security;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.WEB.ViewModels;
using Microsoft.Practices.Unity;

namespace GAdUPIN.WEB.Controllers
{
    public class ColegioController : Controller
    {
        private IColegioService _colegioService;
        private IComunidadAutonomaService _comunidadAutonomaService;
        private IProvinciaService _provinciaService;


        public ColegioController( IColegioService colegioService, IComunidadAutonomaService comunidadAutonomaService, IProvinciaService provinciaService)
        {
            _colegioService = colegioService;
            _comunidadAutonomaService = comunidadAutonomaService;
            _provinciaService = provinciaService;
        }

        //
        // GET: /Colegio/

        public ActionResult Index()
        {
            IList<ColegioListVM> list = CreateViewModelList();
            return View(list);
            //return View(_colegioService.GetAll());
        }

        private IList<ColegioListVM> CreateViewModelList()
        {

            IList<ColegioListVM> result = new List<ColegioListVM>();
            List<Colegio> colegios = _colegioService.GetAll().ToList();
            int numPlazasDisponibles = 0;

            foreach (var colegio in colegios)
            {
                if (colegio.Desactivado == false)
                {
                    if (colegio.PlazasDelColegio == null)
                    {
                        colegio.PlazasDisponibles = numPlazasDisponibles;
                    }
                    else
                    {
                        foreach (var plaza in colegio.PlazasDelColegio)
                        {
                            if (!plaza.PlazaContratada)
                            {
                                numPlazasDisponibles ++;
                            }
                        }
                        
                    }

                    result.Add(new ColegioListVM()
                    {
                        IdColegio = colegio.Id,
                        NombreColegio = colegio.NombreColegio,
                        CIF = colegio.CIF,
                        Telefono = colegio.Telefono,
                        NombreComunidadAutonoma = colegio.ComunidadAutonoma.NombreComunidadAutonoma,
                        NombreProvincia = colegio.Provincia.NombreProvincia,
                        //Contratado = colegio.ColegioContratado,
                        PlazasDisponibles = numPlazasDisponibles,
                        PlazasTotales = colegio.PlazasDelColegio != null ? colegio.PlazasDelColegio.Count : 0

                    });
                    numPlazasDisponibles = 0;
                }
            }

            return result;
        }

        //    IList<ColegioListVM> result = new List<ColegioListVM>();
        //    List<Colegio> colegios = _colegioService.GetAll().ToList();

        //    foreach (var colegio in colegios) 
        //    {
        //        if (colegio.Desactivado == false)
        //        {
        //            Provincia a = _provinciaService.GetWithComunidad(colegio.ComunidadAutonoma.Id);
        //            var plazas = _colegioService.Get(colegio.Id);
        //            if (plazas.PlazasDelColegio == null)
        //            {
        //                colegio.PlazasDisponibles = 0;
        //            }
        //            else
        //            {
        //                colegio.PlazasDisponibles = colegio.PlazasDelColegio.Count;
        //            }
        //            Guid idComA = a.ComunidadAutonoma.Id;
        //            result.Add(new ColegioListVM()
        //            {
        //                IdColegio = colegio.Id,
        //                //IdComunidadAutonoma = _comunidadAutonomaService.GetCAfromProvincia(colegio.Provincia.Id).Id,
        //                IdComunidadAutonoma = idComA,
        //                IdProvincia = a.Id,
        //                NombreComunidadAutonoma =
        //                    colegio.ComunidadAutonoma.NombreComunidadAutonoma != null
        //                        ? colegio.ComunidadAutonoma.NombreComunidadAutonoma
        //                        : string.Empty,
        //                NombreProvincia = a.NombreProvincia != null ? a.NombreProvincia : string.Empty,
        //                NombreColegio = colegio.NombreColegio != null ? colegio.NombreColegio : string.Empty,
        //                DireccionColegio = colegio.DireccionColegio != null ? colegio.DireccionColegio : string.Empty,
        //                Telefono = colegio.Telefono != null ? colegio.Telefono : string.Empty,
        //                Contratado = colegio.ColegioContratado,
        //                PlazasDisponibles = colegio.PlazasDisponibles,
        //                CIF = colegio.CIF
        //            });
        //    //    }
        //    //}

        //    //return result;
        //}

        //
        // GET: /Colegio/

        public ActionResult Desactivados()
        {
            IList<ColegioListVM> list = CreateViewModelListDesactivados();

            return View(list);
        }

        private IList<ColegioListVM> CreateViewModelListDesactivados()
        {

            IList<ColegioListVM> result = new List<ColegioListVM>();
            List<Colegio> colegios = _colegioService.GetAll().ToList();
            int numPlazasDisponibles = 0;

            foreach (var colegio in colegios)
            {
                if (colegio.Desactivado)
                {
                    if (colegio.PlazasDelColegio == null)
                    {
                        colegio.PlazasDisponibles = numPlazasDisponibles;
                    }
                    else
                    {
                        foreach (var plaza in colegio.PlazasDelColegio)
                        {
                            if (!plaza.PlazaContratada)
                            {
                                numPlazasDisponibles++;
                            }
                        }

                    }

                    result.Add(new ColegioListVM()
                    {
                        IdColegio = colegio.Id,
                        NombreColegio = colegio.NombreColegio,
                        CIF = colegio.CIF,
                        Telefono = colegio.Telefono,
                        NombreComunidadAutonoma = colegio.ComunidadAutonoma.NombreComunidadAutonoma,
                        NombreProvincia = colegio.Provincia.NombreProvincia,
                        Contratado = colegio.ColegioContratado,
                        PlazasDisponibles = numPlazasDisponibles
                    });
                    numPlazasDisponibles = 0;
                }
            }

            return result;
        }



        //{
        //    IList<ColegioListVM> result = new List<ColegioListVM>();
        //    List<Colegio> colegios = _colegioService.GetAll().ToList();

        //    foreach (var colegio in colegios)
        //    {
        //        if (colegio.Desactivado == true)
        //        {
        //            Provincia a = _provinciaService.GetWithComunidad(colegio.ComunidadAutonoma.Id);
        //            var plazas = _colegioService.Get(colegio.Id);
        //            if (plazas.PlazasDelColegio == null)
        //            {
        //                colegio.PlazasDisponibles = 0;
        //            }
        //            else
        //            {
        //                colegio.PlazasDisponibles = colegio.PlazasDelColegio.Count;
        //            }
        //            Guid idComA = a.ComunidadAutonoma.Id;
        //            result.Add(new ColegioListVM()
        //            {
        //                IdColegio = colegio.Id,
        //                //IdComunidadAutonoma = _comunidadAutonomaService.GetCAfromProvincia(colegio.Provincia.Id).Id,
        //                IdComunidadAutonoma = idComA,
        //                IdProvincia = a.Id,
        //                NombreComunidadAutonoma =
        //                    colegio.ComunidadAutonoma.NombreComunidadAutonoma != null
        //                        ? colegio.ComunidadAutonoma.NombreComunidadAutonoma
        //                        : string.Empty,
        //                NombreProvincia = a.NombreProvincia != null ? a.NombreProvincia : string.Empty,
        //                NombreColegio = colegio.NombreColegio != null ? colegio.NombreColegio : string.Empty,
        //                DireccionColegio = colegio.DireccionColegio != null ? colegio.DireccionColegio : string.Empty,
        //                Telefono = colegio.Telefono != null ? colegio.Telefono : string.Empty,
        //                Contratado = colegio.ColegioContratado,
        //                PlazasDisponibles = colegio.PlazasDisponibles,
        //                CIF = colegio.CIF
        //            });
        //        }
        //    }

        //    return result;
        //}


        //
        // GET: /Colegio/Create
        public ActionResult Create()
        {

            ColegioNewEditVM colegio = CreateViewModelNewEdit();
            return View(colegio);
        }

        private ColegioNewEditVM CreateViewModelNewEdit()
        {

            ColegioNewEditVM result = new ColegioNewEditVM();
            result.Colegio = new Colegio() { Id = Guid.NewGuid() };
            result.ComunidadesAutonomas = _comunidadAutonomaService.GetAll();
            result.Provincias = _provinciaService.GetAll();
            //    result.ContactoColegio = new ContactoColegio() { Id = Guid.NewGuid() };

            return result;
        }



        //
        // POST: /Colegio/Create

        [HttpPost]
        public ActionResult Create(ColegioNewEditVM colegioVM)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    colegioVM.Colegio.ComunidadAutonoma = _comunidadAutonomaService.Get(colegioVM.SelectedComunidadAutonoma);
                    colegioVM.Colegio.Provincia = _provinciaService.Get(colegioVM.SelectedProvincia);
                    _colegioService.Add(colegioVM.Colegio);
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return View();
            }
            var modelStateErrors = this.ModelState.Keys.SelectMany(key => this.ModelState[key].Errors);
            return View(colegioVM);
        }


        [HttpPost]
        public ActionResult GetProvinciasByComunidad(Guid id)
        {
            List<Provincia> provincias = _provinciaService.GetByComunidadAutonoma(id);
            return Json(provincias.Select(p => new { Id = p.Id, Name = p.NombreProvincia }), JsonRequestBehavior.AllowGet);

        }


        //
        // GET: /Colegio/Details/5

        public ActionResult Details(Guid id)
        {

            ColegioDetailsVM vm = new ColegioDetailsVM();
            vm.Colegio = _colegioService.Get(id);
            vm.SelectedComunidadAutonoma = vm.Colegio.ComunidadAutonoma.Id;
            vm.SelectedProvincia = vm.Colegio.Provincia.Id;

            var ComunidadesAutonomas = new SelectList(_comunidadAutonomaService.GetAll(), "Id", "NombreComunidadAutonoma");
            vm.ComunidadesAutonomas = ComunidadesAutonomas;

            var Provincias = new SelectList(_provinciaService.GetAll(), "Id", "NombreProvincia");
            vm.Provincias = Provincias;

            // Inicializamos variables
            vm.TotalDePlazas = 0;
            vm.PlazasCubiertas = 0;
            vm.PlazasDisponibles = 0;

            if (vm.Colegio.PlazasDelColegio != null)
            {
            
                // Realizamos conteo de plazas, dependiendo de si estan cubiertas o no
                foreach (var plaza in vm.Colegio.PlazasDelColegio )
                {
                    vm.TotalDePlazas ++;
                    if (plaza.PlazaContratada)
                    {
                        vm.PlazasCubiertas ++;
                    }
                    else
                    {
                        vm.PlazasDisponibles ++;
                    }
                }
            }

            return View(vm);
        }

        //
        //POST /Colegio/Details
        [HttpPost]
        public ActionResult Details(ColegioDetailsVM colegioDetailsVm)
        {
            colegioDetailsVm.Colegio.ComunidadAutonoma = _comunidadAutonomaService.Get(colegioDetailsVm.SelectedComunidadAutonoma);
            colegioDetailsVm.Colegio.Provincia = _provinciaService.Get(colegioDetailsVm.SelectedProvincia);
            _colegioService.Update(colegioDetailsVm.Colegio);
            return RedirectToAction("Details", new
            {
                id = colegioDetailsVm.Colegio.Id
            });
        }


        ////
        //// POST: /Colegio/Details/5

        //[HttpPost]
        //public ActionResult Details(ColegioDetailsVM colegioVM)
        //{
        //    try
        //    {

        //        _colegioService.Update(colegioVM.Colegio);
        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        ////
        //// GET: /Colegio/Delete/5

        //public ActionResult Delete(Guid id)
        //{
        //    return View(_colegioService.Get(id));
        //}

        ////
        //// POST: /Colegio/Delete/5

        //[HttpPost]
        //public ActionResult Delete(Guid id, Colegio colegio)
        //{
        //    try
        //    {
        //        _colegioService.Remove(colegio);
        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //
        // GET: /Colegio/Delete/5

        public ActionResult ConfirmarDesactivacion(Guid id)
        {
            return View(_colegioService.Get(id));
        }

        //
        // POST: /Colegio/Delete/5

        [HttpPost]
        public ActionResult ConfirmarDesactivacion(Guid id, Colegio colegio)
        {
            try
            {
                _colegioService.Desactivar(colegio);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult ConfirmarActivacion(Guid id)
        {
            return View(_colegioService.Get(id));
        }

        //
        // POST: /Colegio/Delete/5

        [HttpPost]
        public ActionResult ConfirmarActivacion(Guid id, Colegio colegio)
        {
            try
            {
                _colegioService.Activar(colegio);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetProvinciaByComunidad2(Guid? IdComunidadAutonoma)
        {
            var provinciaModel = new ColegioNewEditVM();
            var provincias = provinciaModel.Provincias.AsEnumerable();

            var provinciasFiltradas = provincias.Where(x => x.ComunidadAutonoma.Id == IdComunidadAutonoma);
            var dropDownData = provinciasFiltradas.Select(m => new SelectListItem()
            {
                Text = m.NombreProvincia,
                Value = m.Id.ToString(),
            });
            return Json(dropDownData, JsonRequestBehavior.AllowGet);
        }


       

    }
}
