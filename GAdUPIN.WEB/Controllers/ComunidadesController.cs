﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;

namespace GAdUPIN.WEB.Controllers
{
    public class ComunidadesController : Controller
    {

        private IComunidadAutonomaService _comunidadAutonomaService;


        public ComunidadesController( IComunidadAutonomaService comunidadAutonomaService)
        {
            _comunidadAutonomaService = comunidadAutonomaService;
        }


        public ActionResult Index()
        {
            return View(_comunidadAutonomaService.GetAll());
        }

        //
        // GET: /Comunidad/Details/5

        //public ActionResult Details(Guid id)
        //{
        //    return View(_comunidadAutonomaService.Get(id));
        //}

        //
        // GET: /Comunidad/Create
        public ActionResult Create()
        {
            ComunidadAutonoma comunidadAutonoma = new ComunidadAutonoma();
            comunidadAutonoma = new ComunidadAutonoma() {Id = Guid.NewGuid()};

            return View(comunidadAutonoma);
        }

        //
        // POST: /Comunidad/Create

        [HttpPost]
        public ActionResult Create(ComunidadAutonoma comunidad)
        {
            try
            {
                _comunidadAutonomaService.Add(comunidad);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Comunidad/Edit/5

        public ActionResult Edit(Guid id)
        {
            return View(_comunidadAutonomaService.Get(id));
        }

        //
        // POST: /Comunidad/Edit/5

        [HttpPost]
        public ActionResult Edit(Guid id, ComunidadAutonoma comunidadAutonoma)
        {
            try
            {
                _comunidadAutonomaService.Update(comunidadAutonoma);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Comunidad/Delete/5

        public ActionResult Delete(Guid id)
        {
            return View(_comunidadAutonomaService.Get(id));
        }

        //
        // POST: /Comunidad/Delete/5

        [HttpPost]
        public ActionResult Delete(ComunidadAutonoma comunidadAutonoma, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                _comunidadAutonomaService.Remove(comunidadAutonoma);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}