﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;
using GAdUPIN.WEB.ViewModels;

namespace GAdUPIN.WEB.Controllers
{
    public class InformeEntrevistaController : Controller
    {
        private IInformeEntrevistaEvaluacionService _informeService;
        private ICandidatoService _candidatoService;
        //private INLogger _NLoggerService;

        public InformeEntrevistaController( IInformeEntrevistaEvaluacionService informeEntrevistaEvaluacionService, ICandidatoService candidatoService)
        {
            _informeService = informeEntrevistaEvaluacionService;
            _candidatoService = candidatoService;
            //_NLoggerService =
            //IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
            //as INLogger;
        }
        
        public Dictionary<int,int> Valoracion { get; set; }

        //
        // GET: /InformeEntrevista/Create

        public ActionResult Index()
        {
            IList<InformeEntrevistaListVM> list = CreateViewModelList();
            return View(list);
        }

        private IList<InformeEntrevistaListVM> CreateViewModelList()
        {
            IList<InformeEntrevistaListVM> result = new List<InformeEntrevistaListVM>();
            

            foreach (var candidato in _candidatoService.GetAllWithInformes())
                if (!candidato.fichaCerrada)
                {    
                    {
                        InformeEntrevistaListVM informe = new InformeEntrevistaListVM();
                            informe.NombreCandidato = candidato.Nombre ?? string.Empty;
                            informe.IdCandidato = candidato.Id;
                            informe.Apellido1 = candidato.Apellido1 ?? string.Empty;
                            informe.Apellido2 = candidato.Apellido2 ?? string.Empty;
                            informe.CodigoCandidato = candidato.Codigo ?? string.Empty;

                        if (candidato.InformeEntrevistaEvaluaciones != null)
                        {
                            foreach (var informee in candidato.InformeEntrevistaEvaluaciones)
                            {
                                informe.IdInforme = informee.Id;
                                informe.InformeCompletado = informee.Completado;
                                informe.ValGlobal = informee.ValGlobal;
                                informe.FechaCompletado = informee.Fecha;
                            }
                        }   
                        else
                        {
                            informe.IdInforme = Guid.Empty;
                            informe.InformeCompletado = false;
                            informe.ValGlobal = 0;
                        }
                        result.Add(informe);
                }
            }


            return result;
        }

        //GET: /InformeEntrevista/Create
        public ActionResult Create(Guid id)
        {
            InformeEntrevistaNewEdit entrevista = CreateViewModelNewEdit(id);

            Valoracion = new Dictionary<int, int>()
            {
                {0,0},
                {1,1},
                {2,2},
                {3,3},
                {4,4},
                {5,5},
                {6,6},
                {7,7},
                {8,8},
                {9,9},
            };
            return View(entrevista);
        }

        private InformeEntrevistaNewEdit CreateViewModelNewEdit(Guid id)
        {
            InformeEntrevistaNewEdit result = new InformeEntrevistaNewEdit();
            result.Informe = new InformeEntrevistaEvaluacion(){ Id = Guid.NewGuid()};
            result.Informe.Fecha = DateTime.Now;
            result.Candidato = _candidatoService.Get(id);
            result.CandidatoSeleccionado = id;
            return result;
        }

        // POST: /InformeEntrevista/Create
        [HttpPost]
        public ActionResult Create(InformeEntrevistaNewEdit informeVm)
        {
            informeVm.Candidato = _candidatoService.Get(informeVm.CandidatoSeleccionado);
           
            informeVm.Informe.Candidato = informeVm.Candidato;
            _informeService.Add(informeVm.Informe);
            return RedirectToAction("Index");
        }


        // GET: /InformeEntrevista/Details
        public ActionResult Details(Guid id)
        {
            InformeEntrevistaNewEdit entrevista = CreateViewModelDetails(id);

            Valoracion = new Dictionary<int, int>()
            {
                {0,0},
                {1,1},
                {2,2},
                {3,3},
                {4,4},
                {5,5},
                {6,6},
                {7,7},
                {8,8},
                {9,9},
            };
            return View(entrevista);
        }

        private InformeEntrevistaNewEdit CreateViewModelDetails(Guid id)
        {
            InformeEntrevistaNewEdit result = new InformeEntrevistaNewEdit();
            result.Informe = _informeService.Get(id);
            result.CandidatoSeleccionado = result.Informe.Candidato.Id;
            result.Candidato = result.Informe.Candidato;
            return result;
        }

        // POST: /InformeEntrevista/Details
        [HttpPost]
        public ActionResult Details(InformeEntrevistaNewEdit informeVm)
        {
            informeVm.Candidato = _candidatoService.Get(informeVm.CandidatoSeleccionado);
            informeVm.Informe.Candidato = informeVm.Candidato;
            _informeService.Update(informeVm.Informe);
            return RedirectToAction("Index");
        }
       
    }
}
