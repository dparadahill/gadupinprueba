﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.WEB.Controllers
{
    public class EtapaEducativaController : Controller
    {

        private IEtapaEducativaService _etapaEducativaService;
        //private INLogger _NLoggerService;

        public EtapaEducativaController(IEtapaEducativaService etapaEducativaService)
        {
            _etapaEducativaService = etapaEducativaService;
            //_NLoggerService =
            //IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
            //as INLogger;
        }
        //
        // GET: /EtapaEducativa/

        public ActionResult Index()
        {
            return View(_etapaEducativaService.GetAll());
        }


        //
        // GET: /EtapaEducativa/Create

        public ActionResult Create()
        {
            EtapaEducativa etapaEducativa = new EtapaEducativa();
            etapaEducativa.Id = Guid.NewGuid();
            return View(etapaEducativa);
        }

        //
        // POST: /EtapaEducativa/Create

        [HttpPost]
        public ActionResult Create(EtapaEducativa etapaEducativa)
        {
            try
            {

                _etapaEducativaService.Add(etapaEducativa);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /EtapaEducativa/Edit/5

        public ActionResult Edit(Guid id)
        {
            EtapaEducativa etapaEducativa = new EtapaEducativa();
            etapaEducativa = _etapaEducativaService.Get(id);
            return View(etapaEducativa);
        }

        //
        // POST: /EtapaEducativa/Edit/5

        [HttpPost]
        public ActionResult Edit(EtapaEducativa etapaEducativa)
        {
            try
            {
                _etapaEducativaService.Update(etapaEducativa);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /EtapaEducativa/Delete/5

        //public ActionResult Delete(Guid id)
        //{
        //    try
        //    {
        //        _etapaEducativaService.Remove(id);
        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return RedirectToAction("Index");
        //    }

        //}


        ////
        //// POST: /EtapaEducativa/Delete/5

        //[HttpPost]
        //public ActionResult Delete()
        //{
        //    try
        //    {
        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //
        // GET: /EtapaEducativa/Delete/5

        public ActionResult Delete(Guid id)
        {
            return View(_etapaEducativaService.Get(id));
        }

        //
        // POST: /EtapaEducativa/Delete/5

        [HttpPost]
        public ActionResult Delete(Guid id, EtapaEducativa etapaEducativa)
        {
            try
            {
                _etapaEducativaService.Remove(etapaEducativa.Id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        ////
        //// GET: /EtapaEducativa/Delete/5

        //public ActionResult Delete(Guid id)
        //{
        //    return View(_etapaEducativaService.Get(id));
        //}

        ////
        //// POST: /EtapaEducativa/Delete/5

        //[HttpPost]
        //public ActionResult Delete(Guid id, EtapaEducativa etapaEducativa)
        //{
        //    try
        //    {
        //        _etapaEducativaService.SoftDelete(etapaEducativa);
        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}