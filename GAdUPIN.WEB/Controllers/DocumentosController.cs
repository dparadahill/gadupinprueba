﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.Domain;
using GAdUPIN.WEB.ViewModels;

namespace GAdUPIN.WEB.Controllers
{
    public class DocumentosController : Controller
    {
        private IDocumentoInscripcionService _documentoInscripcionService;
        private IDocumentoDeTramiteService _documentoDeTramiteService;
        private ICandidatoService _candidatoService; 

        public DocumentosController(IDocumentoInscripcionService documentoInscripcionService,
                                    IDocumentoDeTramiteService documentoDeTramiteService,
                                    ICandidatoService candidatoService)
        {
            _documentoInscripcionService = documentoInscripcionService;
            _documentoDeTramiteService = documentoDeTramiteService;
            _candidatoService = candidatoService;
        }

        //
        // GET: /Documentos/Index

        public ActionResult Index(Guid idCandidato, int tipo)
        {
            if (tipo == 1) // 1 = inscripcion, 2 = tramite
            {
                IList<DocumentoInscripcion> list = _documentoInscripcionService.GetByCandidato(idCandidato);
                ViewData["idCandidato"] = idCandidato;
                return View("Inscripcion/Index",list);
            } else if (tipo == 2)
            {
                IList<DocumentoDeTramite> list = _documentoDeTramiteService.GetByCandidato(idCandidato);
                ViewData["idCandidato"] = idCandidato;
                return View("Tramites/Index", list);
            }

            return null;
        }

        //SECCION DE DOCUMENTOS DE INSCRIPCION
        //SECCION DE DOCUMENTOS DE INSCRIPCION
        
        //
        // GET: /DocumentoInscripcion/Create
        public ActionResult CreateDocInscripcion(Guid idCandidato)
        {
            DocumentoInscripcion docInscripcion = new DocumentoInscripcion();
            docInscripcion.Id = Guid.NewGuid();
            ViewData["idCandidato"] = idCandidato;
            return View("Inscripcion/Create", docInscripcion);
        }

        //
        // POST: /DocumentoInscripcion/Create
        [HttpPost]
        public ActionResult CreateDocInscripcion(DocumentoInscripcion docInscripcion, Guid idCandidato)
        {
            try
            {

                if (ModelState.IsValid)
                {

                    docInscripcion.Candidato = _candidatoService.GetIncNacionalidad(idCandidato);
                    _documentoInscripcionService.Add(docInscripcion);
                }
                return RedirectToAction("Index", new {idCandidato, tipo = 1});
            }
            catch
            {
                return RedirectToAction("Index", new { idCandidato, tipo = 1 });
            }
        }

        //
        // GET: /DocumentoInscripcion/Edit/5
        public ActionResult EditDocInscripcion(Guid id, Guid idCandidato)
        {
            DocumentoInscripcion docInscripcion = new DocumentoInscripcion();
            docInscripcion = _documentoInscripcionService.Get(id);
            ViewData["idCandidato"] = idCandidato;
            return View("Inscripcion/Edit", docInscripcion);
        }

        //
        // POST: /DocumentoInscripcion/Edit/5

        [HttpPost]
        public ActionResult EditDocInscripcion(DocumentoInscripcion docInscripcion)
        {
            Guid idCandidato = _documentoInscripcionService.Get(docInscripcion.Id).Candidato.Id;
            try
            {
                
                _documentoInscripcionService.Update(docInscripcion);

                return RedirectToAction("Index", new {idCandidato, tipo = 1});
            }
            catch
            {
                return RedirectToAction("Index", new {idCandidato, tipo = 1});
            }
        }


        //
        // GET: /DocumentoInscripcion/Delete/5

        public ActionResult DeleteDocInscripcion(Guid id, Guid idCandidato)
        {
            ViewData["idCandidato"] = idCandidato;
            try
            {

                return View("Inscripcion/Delete", _documentoInscripcionService.Get(id));
            }
            catch
            {
                return RedirectToAction("Index", new { idCandidato, tipo = 1 });
            }

        }

        //
        // POST: /DocumentoInscripcion/Delete/5

        [HttpPost]
        public ActionResult DeleteDocInscripcion(DocumentoInscripcion docInscripcion)
        {
            DocumentoInscripcion documento = _documentoInscripcionService.Get(docInscripcion.Id);
            Guid idCandidato = documento.Candidato.Id;
            try
            {
                _documentoInscripcionService.Remove(docInscripcion.Id);
                return RedirectToAction("Index", new {idCandidato, tipo = 1});
            }
            catch
            {
                return RedirectToAction("Index", new { idCandidato, tipo = 1 });
            }
        }

        //SECCION DE DOCUMENTOS DE TRAMITES
        //SECCION DE DOCUMENTOS DE TRAMITES

        //
        // GET: /DocumentoDeTramite/Create
        public ActionResult CreateDocDeTramites(Guid idCandidato)
        {
            DocumentoDeTramite docDeTramites = new DocumentoDeTramite();
            docDeTramites.Id = Guid.NewGuid();
            ViewData["idCandidato"] = idCandidato;
            return View("Tramites/Create", docDeTramites);
        }

        //
        // POST: /DocumentoDeTramite/Create
        [HttpPost]
        public ActionResult CreateDocDeTramites(DocumentoDeTramite docDeTramites, Guid idCandidato)
        {
            try
            {

                if (ModelState.IsValid)
                {

                    docDeTramites.Candidato = _candidatoService.GetIncNacionalidad(idCandidato);
                    _documentoDeTramiteService.Add(docDeTramites);
                }
                return RedirectToAction("Index", new { idCandidato, tipo = 2 });
            }
            catch
            {
                return RedirectToAction("Index", new { idCandidato, tipo = 2 });
            }
        }

        //
        // GET: /DocumentoDeTramite/Edit/5
        public ActionResult EditDocDeTramites(Guid id, Guid idCandidato)
        {
            DocumentoDeTramite docDeTramites = new DocumentoDeTramite();
            docDeTramites = _documentoDeTramiteService.Get(id);
            ViewData["idCandidato"] = idCandidato;
            return View("Tramites/Edit", docDeTramites);
        }

        //
        // POST: /DocumentoDeTramite/Edit/5

        [HttpPost]
        public ActionResult EditDocDeTramites(DocumentoDeTramite docDeTramites)
        {
            Guid idCandidato = _documentoDeTramiteService.Get(docDeTramites.Id).Candidato.Id;
            try
            {

                _documentoDeTramiteService.Update(docDeTramites);

                return RedirectToAction("Index", new { idCandidato, tipo = 2 });
            }
            catch
            {
                return RedirectToAction("Index", new { idCandidato, tipo = 2 });
            }
        }


        //
        // GET: /DocumentoDeTramite/Delete/5

        public ActionResult DeleteDocDeTramites(Guid id, Guid idCandidato)
        {
            ViewData["idCandidato"] = idCandidato;
            try
            {

                return View("Tramites/Delete", _documentoDeTramiteService.Get(id));
            }
            catch
            {
                return RedirectToAction("Index", new { idCandidato, tipo = 2 });
            }

        }

        //
        // POST: /DocumentoDeTramite/Delete/5

        [HttpPost]
        public ActionResult DeleteDocDeTramites(DocumentoDeTramite docDeTramites)
        {
            DocumentoDeTramite documento = _documentoDeTramiteService.Get(docDeTramites.Id);
            Guid idCandidato = documento.Candidato.Id;
            try
            {
                _documentoDeTramiteService.Remove(docDeTramites.Id);
                return RedirectToAction("Index", new { idCandidato, tipo = 2 });
            }
            catch
            {
                return RedirectToAction("Index", new { idCandidato, tipo = 2 });
            }
        }
    }
}
