﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.Domain;
using GAdUPIN.WEB.ViewModels;

namespace GAdUPIN.WEB.Controllers
{
    public class UniversidadDeRegistroController : Controller
    {
        private IUniversidadDeRegistroService _universidadDeRegistroService;

        public UniversidadDeRegistroController(IUniversidadDeRegistroService universidadDeRegistroService)
        {
            _universidadDeRegistroService = universidadDeRegistroService;
        }

        //
        //GET: UniversidadDeRegistro/Index
        public ActionResult Index()
        {
            return View(_universidadDeRegistroService.GetAll());
        }

        
        //
        //GET: UniversidadDeRegistro/Create
        public ActionResult Create()
        {
            UniversidadDeRegistroNewEditVM universidad = CreateNewEditVM();
            return View(universidad);

        }

        private UniversidadDeRegistroNewEditVM CreateNewEditVM()
        {
            UniversidadDeRegistroNewEditVM result = new UniversidadDeRegistroNewEditVM();
            result.UniversidadDeRegistro = new UniversidadDeRegistro() {Id = Guid.NewGuid()};

            return result;
        }


        //
        //POST: UniversidadDeRegistro/Create
        [HttpPost]
        public ActionResult Create(UniversidadDeRegistroNewEditVM universidadVM)
        {
            try
            {
                _universidadDeRegistroService.Add(universidadVM.UniversidadDeRegistro);

                return RedirectToAction("Index");
            }
            catch (Exception)
            {

                return View("Error");
            }
        }



        //
        // GET: /UniversidadDeRegistro/Edit/5

        public ActionResult Edit(Guid id)
        {
            UniversidadDeRegistroNewEditVM universidadDeRegistro = EditViewModelNewEdit(id);
            return View(universidadDeRegistro);
        }

        private UniversidadDeRegistroNewEditVM EditViewModelNewEdit(Guid id)
        {
            UniversidadDeRegistroNewEditVM result = new UniversidadDeRegistroNewEditVM();
            result.UniversidadDeRegistro = _universidadDeRegistroService.Get(id);
            return result;
        }

        //
        // POST: /UniversidadDeRegistro/Edit/5

        [HttpPost]
        public ActionResult Edit(UniversidadDeRegistroNewEditVM UniversidadDeRegistroVM)
        {
            try
            {
                _universidadDeRegistroService.Update(UniversidadDeRegistroVM.UniversidadDeRegistro);

                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }



        //
        // GET: /UniversidadDeRegistro/Delete/5

        public ActionResult Delete(Guid id)
        {
            return View(_universidadDeRegistroService.Get(id));
        }

        //
        // POST: /UniversidadDeRegistro/Delete/5

        [HttpPost]
        public ActionResult Delete(Guid id, UniversidadDeRegistro universidadDeRegistro)
        {
            try
            {
                _universidadDeRegistroService.Remove(universidadDeRegistro.Id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }



        



    }
}
