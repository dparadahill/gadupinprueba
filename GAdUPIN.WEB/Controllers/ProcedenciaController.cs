﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.WEB.Controllers
{
    public class ProcedenciaController : Controller
    {
        private IProcedenciaService _procedenciaService;
        //private INLogger _NLoggerService;

        public ProcedenciaController(IProcedenciaService procedenciaService)
        {
            _procedenciaService = procedenciaService;
            //_NLoggerService =
            //IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
            //as INLogger;
        }
        //
        // GET: /Procedencia/

        public ActionResult Index()
        {
            return View(_procedenciaService.GetAll());
        }


        //
        // GET: /Procedencia/Create

        public ActionResult Create()
        {
            Procedencia procedencia = new Procedencia();
            procedencia.Id = Guid.NewGuid();
            return View(procedencia);
        } 

        //
        // POST: /Procedencia/Create

        [HttpPost]
        public ActionResult Create(Procedencia procedencia)
        {
            try
            {
               
                _procedenciaService.Add(procedencia);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /Procedencia/Edit/5
 
        public ActionResult Edit(Guid id)
        {
            Procedencia procedencia = new Procedencia();
            procedencia = _procedenciaService.Get(id);
            return View(procedencia);
        }

        //
        // POST: /Procedencia/Edit/5

        [HttpPost]
        public ActionResult Edit(Procedencia procedencia)
        {
            try
            {
               _procedenciaService.Update(procedencia);
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Procedencia/Delete/5
 
        public ActionResult Delete(Guid id)
        {
            try
            {

                return View(_procedenciaService.Get(id));
            }
            catch
            {
                return RedirectToAction("Index");
            }
           
        }

        //
        // POST: /Procedencia/Delete/5

        [HttpPost]
        public ActionResult Delete(Procedencia procedencia)
        {
            try
            {
                _procedenciaService.Remove(procedencia.Id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
