﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.Domain;
using GAdUPIN.WEB.ViewModels;

namespace GAdUPIN.WEB.Controllers
{
    public class ContactoColegioController : Controller
    {
        private IContactoColegioService _contactoColegioService;
        private IColegioService _colegioService;
        //private IComunidadAutonomaService _comunidadAutonomaService;

        public ContactoColegioController(IContactoColegioService contactoColegioService, IColegioService colegioService)
        {
            _contactoColegioService = contactoColegioService;
            _colegioService = colegioService;

        }
        //
        // GET: /ContactoColegio/Index

        public ActionResult Index(Guid id)
        {
            IList<ContactoColegio> list = _contactoColegioService.GetByColegio(id);
            ViewData["idColegio"] = id;
            return View(list);

            //return View(list);
        }

        //private IList<ContactoColegioListVM> CreateVMList(Guid id)
        //{
        //    IList<ContactoColegioListVM> result = new List<PlazaColegioListVM>();
        //    List<ContactoColegio> contactosColegio = _contactoColegioService.GetByColegio(id).ToList();


        //    foreach (var contacto in contactosColegio)
        //    {

        //        result.Add(new ContactoColegioListVM()
        //        {
        //            Id = contacto.Id,
        //            IdColegio = contacto.Colegio.Id,
        //            Apellido1 = contacto.Apellido1,
        //            Apellido2 = contacto.Apellido2,
        //            Email = contacto.Email,
        //            TelefonoDeContacto = contacto.TelefonoDeContacto,
        //            ContactoPrincipal = contacto.ContactoPrincipal
        //        });
        //    }
        //    return result;
        //}

        //
        // GET: /ContactoColegio/Create

        public ActionResult Create(Guid idColegio)
        {
            ContactoColegioNewEditVM contactoColegio = CreateViewModelNewEdit(idColegio);
            return View(contactoColegio);
        }

        private ContactoColegioNewEditVM CreateViewModelNewEdit(Guid idColegio)
        {
            ContactoColegioNewEditVM result = new ContactoColegioNewEditVM();
            result.ContactoColegio = new ContactoColegio() { Id = Guid.NewGuid() };
            result.IdColegio = idColegio;
            //result.ComunidadesAutonomas = _comunidadAutonomaService.GetAll();

            //int proxNumSerie = _candidatoService.GetAll().Count() + 1;
            //result.Candidato.Codigo = DateTime.Now.ToString("yy") + (proxNumSerie.ToString("D4"));

            return result;
        }

        //
        // POST: /ContactoColegio/Create

        [HttpPost]
        public ActionResult Create(ContactoColegioNewEditVM contactoColegioVM)
        {
            try
            {
                
                contactoColegioVM.ContactoColegio.Colegio = _colegioService.Get(contactoColegioVM.IdColegio);
                Guid IdColegio = contactoColegioVM.ContactoColegio.Colegio.Id;
                _contactoColegioService.Add(contactoColegioVM.ContactoColegio);
                

                return RedirectToAction("Index", new
                {
                    id = IdColegio
                });
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /ContactoColegio/Edit/5

        public ActionResult Edit(Guid id)
        {
            ContactoColegioNewEditVM contactoColegio = EditViewModelNewEdit(id);
            return View(contactoColegio);
        }

        private ContactoColegioNewEditVM EditViewModelNewEdit(Guid id)
        {
            ContactoColegioNewEditVM result = new ContactoColegioNewEditVM();
            result.ContactoColegio = _contactoColegioService.Get(id);
            result.IdColegio = result.ContactoColegio.Colegio.Id;
            //result.ComunidadesAutonomas = _comunidadAutonomaService.GetAll();
            
            return result;
        }

        //
        // POST: /ContactoColegio/Edit/5

        [HttpPost]
        public ActionResult Edit(ContactoColegioNewEditVM contactoColegioVM)
        {
            try
            {
                contactoColegioVM.ContactoColegio.Colegio = _colegioService.Get(contactoColegioVM.IdColegio);
                _contactoColegioService.Update(contactoColegioVM.ContactoColegio);

                return RedirectToAction("Index", new
                {
                    id = contactoColegioVM.IdColegio
                });
            }
            catch
            {
                return View();
            }
        }

       

        //
        // GET: /ContactoColegio/Delete/5

        public ActionResult Delete(Guid id)
        {
            return View(_contactoColegioService.Get(id));
        }

        //
        // POST: /ContactoColegio/Delete/5

        [HttpPost]
        public ActionResult Delete(Guid id, ContactoColegio contactoColegio)
        {
            try
            {

                Guid IdColegio =  _contactoColegioService.Get(id).Colegio.Id;
                _contactoColegioService.Remove(contactoColegio.Id);
                return RedirectToAction("Index", new
                {
                    id = IdColegio
                });
            }
            catch
            {
                return View();
            }
        }


        
    }
}
