﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.WEB.Controllers
{
    public class NacionalidadController : Controller
    {
        private INacionalidadService _nacionalidadService;
        //private INLogger _NLoggerService;

        public NacionalidadController(INacionalidadService nacionalidadService)
        {
            _nacionalidadService = nacionalidadService;
            //_NLoggerService =
            //IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
            //as INLogger;
        }
        //
        // GET: /Nacionalidad/

        public ActionResult Index()
        {
            return View(_nacionalidadService.GetAll());
        }


        //
        // GET: /Nacionalidad/Create

        public ActionResult Create()
        {
            Nacionalidad nacionalidad = new Nacionalidad();
            nacionalidad.Id = Guid.NewGuid();
            return View(nacionalidad);
        } 

        //
        // POST: /Nacionalidad/Create

        [HttpPost]
        public ActionResult Create(Nacionalidad nacionalidad)
        {
            try
            {
                
                if (ModelState.IsValid)
                {
                    _nacionalidadService.Add(nacionalidad);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /Nacionalidad/Edit/5
 
        public ActionResult Edit(Guid id)
        {
            Nacionalidad nacionalidad = new Nacionalidad();
            nacionalidad = _nacionalidadService.Get(id);
            return View(nacionalidad);
        }

        //
        // POST: /Nacionalidad/Edit/5

        [HttpPost]
        public ActionResult Edit(Nacionalidad nacionalidad)
        {
            try
            {
               _nacionalidadService.Update(nacionalidad);
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Nacionalidad/Delete/5
 
        public ActionResult Delete(Guid id)
        {
            try
            {

                return View(_nacionalidadService.Get(id));
             }
            catch
            {
                return RedirectToAction("Index");
            }
           
        }

        //
        // POST: /Nacionalidad/Delete/5

        [HttpPost]
        public ActionResult Delete(Guid id, Nacionalidad nacionalidad)
        {
            try
            {
                _nacionalidadService.Remove(nacionalidad.Id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
