﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.WEB.ViewModels
{
    public class PlazaColegioNewEditVM : Controller
    {
        public PlazaColegio PlazaColegio { get; set; }
        public IQueryable<Colegio> Colegios { get; set; }
        public IQueryable<Candidato> Candidatos { get; set; }
        public IQueryable<EtapaEducativa> EtapasEducativas { get; set; }
        

        public Guid SelectedColegio { get; set; }
        public string NombreColegio { get; set; }
        public Candidato CandidatoAsignado { get; set; }

        public string PreferenciaSexoActual { get; set; }
        public EtapaEducativa EtapaEducativaActual { get; set; }

        public IQueryable<Nacionalidad> Nacionalidades { get; set; }
        public Guid SelectedNacionalidad { get; set; }

        public string CodigoPlaza { get; set; }

        public string DatoAdicional { get; set; }
        public string CuantiaBeca { get; set; }
    }
}
