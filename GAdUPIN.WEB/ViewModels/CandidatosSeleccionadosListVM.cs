﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.WEB.ViewModels
{
    public class CandidatosSeleccionadosListVM
    {

        public Candidato candidatoSeleccionado { get; set; }
    }
}
