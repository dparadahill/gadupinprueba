﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.WEB.ViewModels
{
    public class ProvinciaNewEditVM 
    {
        public Provincia Provincia { get; set; }
        public IQueryable<ComunidadAutonoma> ComunidadesAutonomas { get; set; }
        public Guid SelectedComunidadAutonoma { get; set; }
        public string NombreComunidadAutonoma { get; set;}
        public Guid IdComunidadAutonoma { get; set; }
        
    }
}
