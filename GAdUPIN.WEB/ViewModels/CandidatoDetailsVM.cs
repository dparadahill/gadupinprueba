﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Domain;


namespace GAdUPIN.WEB.ViewModels
{
    public class CandidatoDetailsVM
    {
        public Guid IdInforme { get; set; }
        public Guid IdCandidato { get; set; }
        public Candidato Candidato { get; set; }
        public IQueryable<Nacionalidad> Nacionalidades { get; set; }
        public IQueryable<Procedencia> Procedencias { get; set; }

        public Nacionalidad NacionalidadActual { get; set; }
        public bool SexoActual { get; set; }
        public Guid ProcedenciaActual { get; set; }


    #region Preferencias

        public Guid SelectedProvincia { get; set; }
        //public IQueryable<Provincia> Provincias { get; set; }
        public IEnumerable<SelectListItem> Provincias { get; set; }
    
        public Guid SelectedComunidadAutonoma { get; set; }
        //public IQueryable<ComunidadAutonoma> ComunidadesAutonomas { get; set; }
        public IEnumerable<SelectListItem> ComunidadesAutonomas { get; set; }

        public Guid SelectedEtapaEducativa { get; set; }
        public IQueryable<EtapaEducativa> EtapasEducativas { get; set; }

        public Guid SelectedTipoAlojamiento { get; set; }
        public IQueryable<TipoAlojamiento> TiposAlojamientos { get; set; }

        #endregion

    #region Entrevista

        public bool InformeCompletado { get; set; }
        [DisplayFormatAttribute(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? InformeFechaCompletado { get; set; }
        public int InformeValoracionGlobal { get; set; }

        #endregion

    #region Plaza Asignada

        public Guid IdPlaza { get; set; }
        public Guid IdColegio { get; set; }
        [DisplayName("Colegio")]
        public string NombreColegioPlazaCubierta { get; set; }
        //Historico de colegio
        public string EntrevistaColegio { get; set; }
        [DisplayName("Código")]
        public string CodigoPlazaCubierta { get; set; }

        #endregion

    #region Documentos

        public bool Curriculum { get; set; }
        public bool TestAptitud { get; set; }
        public bool Foto { get; set; }
        public bool Pasaporte { get; set; }
        public string NumeroPasaporte { get; set; }
        public bool Titulo { get; set; }
        public bool CertificadoPenales { get; set; }
        public bool CartaAdmisionUP { get; set; }
        public bool DocInscripcion { get; set; }
        public bool Matricula { get; set; }
        public bool ClavesAccesoUSJ { get; set; }
        public bool AnexoConvenio { get; set; }
        public bool NIE { get; set; }
        public bool Empadronamiento { get; set; }
        public bool AltaSegSocial { get; set; }
        public bool AltaFiscal_Modelo30 { get; set; }
        

        #endregion

    #region Visado

        public Guid SelectedEstadoVisado { get; set; }
        public IQueryable<EstadoVisado> EstadosVisado { get; set; }     

        #endregion


    #region Cierre de Ficha

        public Guid SelectedMotivoCierreFicha { get; set; }
        public IQueryable<MotivoCierreFicha> MotivosCierreFicha { get; set; }

        #endregion

    #region Universidad de Registro

        public Guid SelectedUniversidadDeRegistro { get; set; }
        public IQueryable<UniversidadDeRegistro> UniversidadesDeRegistro { get; set; }

        #endregion
    }
}
