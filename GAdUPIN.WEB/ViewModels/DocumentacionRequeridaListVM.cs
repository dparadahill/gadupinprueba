﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.WEB.ViewModels
{
    public class DocumentacionRequeridaListVM 
    {
        
        public IList<DocumentoDeTramite> Documentos { get; set; }
        public Guid IdDoc { get; set; }
        public Guid IdCandidato { get; set; }

        public DateTime? FechaSubida { get; set; }
        public string NombreDocumento { get; set; }
        public string Link { get; set; }
    }
}
