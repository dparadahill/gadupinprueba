﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Domain;
using GAdUPIN.WEB.Controllers;

namespace GAdUPIN.WEB.ViewModels
{
    public class ColegioNewEditVM 
    {
        public Colegio Colegio { get; set; }
        public ContactoColegio ContactoColegio { get; set; }
        public IQueryable<ComunidadAutonoma> ComunidadesAutonomas { get; set; }
        public IQueryable<Provincia> Provincias { get; set; }

        public Guid SelectedComunidadAutonoma { get; set; }
        public Guid SelectedProvincia { get; set; }

       
    }
}
