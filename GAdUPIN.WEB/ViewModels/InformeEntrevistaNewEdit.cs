﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.WEB.ViewModels
{
    public class InformeEntrevistaNewEdit
    {
        public InformeEntrevistaEvaluacion Informe { get; set; }
        public Candidato Candidato { get; set; }
       
        public Guid CandidatoSeleccionado { get; set; }
    }
}
