﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.WEB.ViewModels
{
    public class ProvinciaListVM : Controller
    {
        public IEnumerable<ComunidadAutonoma> ComunidadesAutonomas { get; set; }
        public IEnumerable<Provincia> Provincias { get; set; }

        public Guid Id { get; set; }
        public Guid IdComunidadAutonoma { get; set; }

        public string NombreComunidadAutonoma { get; set; }
        public string NombreProvincia { get; set; }

    }
}
