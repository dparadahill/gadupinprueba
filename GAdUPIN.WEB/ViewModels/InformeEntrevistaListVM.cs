﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.WEB.ViewModels
{
    public class InformeEntrevistaListVM : Controller
    {
        
        public Guid IdInforme { get; set; }
        public Guid IdCandidato { get; set; }
        public string CodigoCandidato { get; set; }

        public string NombreCandidato { get; set; }
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }

        public int ValGlobal { get; set; }
        public bool InformeCompletado { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? FechaCompletado { get; set; }

    }
}
