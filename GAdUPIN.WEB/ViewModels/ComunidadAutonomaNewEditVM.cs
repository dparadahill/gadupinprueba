﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.WEB.ViewModels
{
    public class ComunidadAutonomaNewEditVM
    {
        public ComunidadAutonoma ComunidadAutonoma { get; set; }
        public IQueryable<ComunidadAutonoma> Comunidades { get; set; }
         
    }
}
