﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.WEB.ViewModels
{
    public class CandidatoNewEditVM : Controller
    {
        public Candidato Candidato { get; set; }
        public IQueryable<Nacionalidad> Nacionalidades { get; set; }
        public IQueryable<Procedencia> Procedencias { get; set; }
        public bool Sexo { get; set; } 
    }
}
