﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.WEB.ViewModels
{
    public class CandidatoEntrevistasListVM
    {
        public Guid Id { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Email { get; set; }
        public string Skype { get; set; }
        public bool EntrevistaNativo { get; set; }
        public string HoraEntrevistaNativo { get; set; }
        public bool EntrevistaUp { get; set; }
        public string HoraEntrevistaUp { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
            public DateTime? FechaEntrevistaNativo { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
            public DateTime? FechaEntrevistaUp { get; set; }
    }
}
