﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GAdUPIN.WEB.ViewModels
{
    public class DocumentoNewEditVM
    {
        public Guid idDocumento { get; set; }
        public string NombreDocumento { get; set; }
        public DateTime? FechaSubida { get; set; }
        public string Estado { get; set; }
        public string Comentarios { get; set; }
        public Guid IdCandidato { get; set; }
    }
}
