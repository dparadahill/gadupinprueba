﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.WEB.ViewModels
{
    public class EncargadoNewEditVM 
    {
        
        public Encargado Encargado { get; set; }
        public IQueryable<ComunidadAutonoma> ComunidadesAutonomas { get; set; }
    }
}
