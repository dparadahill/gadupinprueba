﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.WEB.ViewModels
{
    public class CandidatoPerfilesListVM
    {
        public Guid Id { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        
        public int Edad { get; set; }
        public string Carrera { get; set; }
        public string CantidadExperiencia { get; set; }
        public bool TC { get; set; } //Tefl / Celta
        public string Test { get; set; }
        public string Destacar { get; set; }

    }
}
