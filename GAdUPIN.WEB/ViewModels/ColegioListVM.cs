﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.WEB.ViewModels
{
    public class ColegioListVM
    {
        public List<Colegio> Colegios { get; set; }

        public string CIF { get; set; }
        public Guid IdColegio { get; set; }
        //public Guid IdComunidadAutonoma { get; set; }
        //public Guid IdProvincia { get; set; }

        
        

        public string NombreColegio  { get; set; }
        //public string DireccionColegio { get; set; }
        public string Telefono { get; set; }
        public string NombreComunidadAutonoma { get; set; }
        public string NombreProvincia { get; set; }
        public bool Contratado { get; set; }
        public int PlazasDisponibles { get; set; }
        public int PlazasTotales { get; set; }

    }
}
