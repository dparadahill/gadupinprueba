﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.WEB.ViewModels
{
    public class ColegioDetailsVM
    {
        public Colegio Colegio { get; set; }
        public IQueryable<ContactoColegio> ContactosColegio { get; set; }
        public IQueryable<PlazaColegio> PlazasColegio { get; set; }

        public IEnumerable<SelectListItem> ComunidadesAutonomas { get; set; }
        public IEnumerable<SelectListItem> Provincias { get; set; }

        public Guid SelectedComunidadAutonoma { get; set; }
        public Guid SelectedProvincia { get; set; }

        public int TotalDePlazas { get; set; }
        public int PlazasCubiertas { get; set; }
        public int PlazasDisponibles { get; set; }
    }
}
