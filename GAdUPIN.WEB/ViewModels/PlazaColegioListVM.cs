﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.WEB.ViewModels
{
    public class PlazaColegioListVM
    {
        public Colegio Colegio { get; set; }
        public IEnumerable<Colegio> Colegios { get; set; }
        public IEnumerable<Candidato> Candidatos { get; set; }

        public Guid Id { get; set; } 
        public Guid IdColegio { get; set;  }
        public Guid IdCandidato { get; set; }

        public string NombreColegio { get; set; }
        public string CuantiaBeca { get; set; }
        public string NumeroHoras { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FechaInicio { get; set; }
        [DisplayFormatAttribute(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FechaFin { get; set; }
        public bool PlazaContratada { get; set; }
        public EtapaEducativa EtapaEducativa { get; set; }
        public string PreferenciaSexo { get; set; }
        public Candidato CubiertaPor { get; set; }
        //public string NombreCandidato { get; set; }
        public string NombreComunidadAutonoma { get; set;  }
        public string CodigoPlaza { get; set; }

        public bool Cubrir { get; set; }
        public string CodigoCandidato { get; set; }
        public Nacionalidad NacionalidadPreferente { get; set;  }

        public string DatoAdicional { get; set; }
        public string Condiciones { get; set; }
    }
}
