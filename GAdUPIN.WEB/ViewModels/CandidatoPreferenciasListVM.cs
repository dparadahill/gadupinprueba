﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GAdUPIN.WEB.ViewModels
{
    public class CandidatoPreferenciasListVM : Controller
    {
        public Guid IdCandidato { get; set; }
        public Guid IdPreferencia { get; set; }

        public string CodigoCandidato { get; set; }
        public string CandidatoNombre { get; set; }
        public string CandidatoApellido1 { get; set; }
        public string CandidatoApellido2 { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? FechaDisponibilidad { get; set; }

        public string TipoAlojamiento { get; set; }
        public string ComunidadAutonoma { get; set; }
        public string Provincia { get; set; }
        public string EtapaEducativa { get; set; }
        
    }
}
