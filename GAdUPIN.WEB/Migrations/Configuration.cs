using System.Collections.Generic;
using System.Data.Entity.Migrations.Infrastructure;
using System.Diagnostics;
using GAdUPIN.CORE.Domain;

namespace GAdUPIN.WEB.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<GAdUPIN.DAL.DBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }




        protected override void Seed(GAdUPIN.DAL.DBContext context)
        {
            if (System.Diagnostics.Debugger.IsAttached == false)
                System.Diagnostics.Debugger.Launch();


            //
            // A�adiendo comunidades y sus provincias
            var comunidades = new List<ComunidadAutonoma>
            {
                

                new ComunidadAutonoma {Id = Guid.Parse("5873E2BD-0FBE-4C5B-8918-34E878017AD9"), NombreComunidadAutonoma = "*Indiferente*", Provincias = new List<Provincia>(
                    new List<Provincia>{
                                        new Provincia {Id = Guid.Parse("5873E2BD-0FBE-4C5B-8918-34E878017AD8"), NombreProvincia = "*Indiferente*"}
                })},

                new ComunidadAutonoma {Id = Guid.Parse("255EBAA9-3503-4A3C-B6B8-EA5374DAD18F"), NombreComunidadAutonoma = "Andaluc�a", Provincias = new List<Provincia>(
                    new List<Provincia>{
                                        new Provincia {Id = Guid.Parse("37D9FF8C-6D4D-4E14-A6D2-9ABB9ECB6FDE"), NombreProvincia = "Almer�a"},
                                        new Provincia {Id = Guid.Parse("37D9FF8C-6D4D-4E14-A6D2-9ABB9ECB6FD1"), NombreProvincia = "C�diz"},
                                        new Provincia {Id = Guid.Parse("37D9FF8C-6D4D-4E14-A6D2-9ABB9ECB6FD2"), NombreProvincia = "C�rdoba"},
                                        new Provincia {Id = Guid.Parse("37D9FF8C-6D4D-4E14-A6D2-9ABB9ECB6FD3"), NombreProvincia = "Granada"},
                                        new Provincia {Id = Guid.Parse("37D9FF8C-6D4D-4E14-A6D2-9ABB9ECB6FD4"), NombreProvincia = "Huelva"},
                                        new Provincia {Id = Guid.Parse("37D9FF8C-6D4D-4E14-A6D2-9ABB9ECB6FD5"), NombreProvincia = "Ja�n"},
                                        new Provincia {Id = Guid.Parse("37D9FF8C-6D4D-4E14-A6D2-9ABB9ECB6FD6"), NombreProvincia = "M�laga"},
                                        new Provincia {Id = Guid.Parse("37D9FF8C-6D4D-4E14-A6D2-9ABB9ECB6FD7"), NombreProvincia = "Sevilla"}
                })},


                new ComunidadAutonoma {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0111"), NombreComunidadAutonoma = "Arag�n", Provincias = new List<Provincia>(
                    new List<Provincia>{
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0A11"), NombreProvincia = "Zaragoza"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0A12"), NombreProvincia = "Huesca"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0A13"), NombreProvincia = "Teruel"}
                })},

                new ComunidadAutonoma {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C01A1"), NombreComunidadAutonoma = "Castilla Y Le�n", Provincias = new List<Provincia>(
                    new List<Provincia>{
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0AA1"), NombreProvincia = "�vila"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0AA2"), NombreProvincia = "Burgos"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0AA3"), NombreProvincia = "Le�n"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0AA4"), NombreProvincia = "Palencia"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0AA5"), NombreProvincia = "Salamanca"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0AA6"), NombreProvincia = "Segovia"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0AA7"), NombreProvincia = "Soria"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0AA8"), NombreProvincia = "Valladolid"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0AA9"), NombreProvincia = "Zamora"}
                })},

                new ComunidadAutonoma {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C01B1"), NombreComunidadAutonoma = "Castilla-La Mancha", Provincias = new List<Provincia>(
                    new List<Provincia>{
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0AB1"), NombreProvincia = "Albacete"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0AB2"), NombreProvincia = "Ciudad Real"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0AB3"), NombreProvincia = "Cuenca"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0AB4"), NombreProvincia = "Guadalajara"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0AB5"), NombreProvincia = "Toledo"}
                })},

                new ComunidadAutonoma {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0112"), NombreComunidadAutonoma = "Catalu�a", Provincias = new List<Provincia>(
                    new List<Provincia>{
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0A21"), NombreProvincia = "L�rida"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0A22"), NombreProvincia = "Gerona"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0A23"), NombreProvincia = "Tarragona"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C0A24"), NombreProvincia = "Barcelona"}
                })},

                new ComunidadAutonoma {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C02C1"), NombreComunidadAutonoma = "Ceuta", Provincias = new List<Provincia>(
                    new List<Provincia>{
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C02C2"), NombreProvincia = "Ceuta"}
                })},

                new ComunidadAutonoma {Id = Guid.Parse("23421A51-D727-4368-8015-DF1BE07CE1A9"), NombreComunidadAutonoma = "Comunidad de Madrid", Provincias = new List<Provincia>(
                    new List<Provincia>{
                                        new Provincia {Id = Guid.Parse("23421A51-D727-4368-8015-DF1BE07CE1A8"), NombreProvincia = "Madrid"}
                })},

                new ComunidadAutonoma {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C03C1"), NombreComunidadAutonoma = "Comunidad Valenciana", Provincias = new List<Provincia>(
                    new List<Provincia>{
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C03C2"), NombreProvincia = "Alicante"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C03C3"), NombreProvincia = "Castell�n"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C03C4"), NombreProvincia = "Valencia"}
                })},

                new ComunidadAutonoma {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C04C1"), NombreComunidadAutonoma = "Extremadura", Provincias = new List<Provincia>(
                    new List<Provincia>{
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C04C2"), NombreProvincia = "Badajoz"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C04C3"), NombreProvincia = "C�ceres"}
                })},

                new ComunidadAutonoma {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C05C1"), NombreComunidadAutonoma = "Galicia", Provincias = new List<Provincia>(
                    new List<Provincia>{
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C05C2"), NombreProvincia = "A Coru�a"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C05C3"), NombreProvincia = "Lugo"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C05C4"), NombreProvincia = "Ourense"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C05C5"), NombreProvincia = "Pontevedra"}
                })},

                new ComunidadAutonoma {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C06C1"), NombreComunidadAutonoma = "Illes Balears", Provincias = new List<Provincia>(
                    new List<Provincia>{
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C06C2"), NombreProvincia = "Baleares"},
                })},

                new ComunidadAutonoma {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C07C1"), NombreComunidadAutonoma = "Islas Canarias", Provincias = new List<Provincia>(
                    new List<Provincia>{
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C07C2"), NombreProvincia = "Las Palmas"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C07C3"), NombreProvincia = "Sta. Cruz De Tenerife"}
                })},

                new ComunidadAutonoma {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C08C1"), NombreComunidadAutonoma = "La Rioja", Provincias = new List<Provincia>(
                    new List<Provincia>{
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C08C2"), NombreProvincia = "La Rioja"}
                })},

                new ComunidadAutonoma {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C09C1"), NombreComunidadAutonoma = "Melilla", Provincias = new List<Provincia>(
                    new List<Provincia>{
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C09C2"), NombreProvincia = "Melilla"}
                })},

                new ComunidadAutonoma {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C1AC1"), NombreComunidadAutonoma = "Navarra", Provincias = new List<Provincia>(
                    new List<Provincia>{
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C1AC2"), NombreProvincia = "Navarra"}
                })},

                new ComunidadAutonoma {Id = Guid.Parse("A5F47D56-FABE-4A4B-9A40-6B161A85F8B6"), NombreComunidadAutonoma = "Pa�s Vasco", Provincias = new List<Provincia>(
                    new List<Provincia>{
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C2AC2"), NombreProvincia = "Alava"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C2AC3"), NombreProvincia = "Guipuzcoa"},
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C2AC4"), NombreProvincia = "Vizcaya"}
                })},

                new ComunidadAutonoma {Id = Guid.Parse("A5F47D56-FABE-4A4B-9A40-6B161A85F8B5"), NombreComunidadAutonoma = "Pdo. de Asturias", Provincias = new List<Provincia>(
                    new List<Provincia>{
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C3AC2"), NombreProvincia = "�lava"}
                })},

                new ComunidadAutonoma {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C4AC1"), NombreComunidadAutonoma = "Regi�n de Murcia", Provincias = new List<Provincia>(
                    new List<Provincia>{
                                        new Provincia {Id = Guid.Parse("3B85B456-0A8E-49E7-84EE-87C28E8C4AC2"), NombreProvincia = "Murcia"}
                })},
            };



            //foreach (var comunidadAutonoma in comunidades)
            //{
            //    var comunidadinDB = context.ComunidadesAutonomas.Where(
            //        p => p.Id == comunidadAutonoma.Id).SingleOrDefault();
            //    if (comunidadinDB == null)
            //    {
            //        context.ComunidadesAutonomas.Add(comunidadAutonoma);
            //    }
            //}
            comunidades.ForEach(c => context.ComunidadesAutonomas.AddOrUpdate(a => a.Id, c));
            context.SaveChanges();

            //
            // Procedencias
            var procedencias = new List<Procedencia>
            {
                new Procedencia{ Id = Guid.Parse("76D08182-8560-496A-85A4-48C775D997EE"), NombreProcedencia = "No indicado" }
            };
            foreach (var procedencia in procedencias)
            {
                var entityInDB = context.Procedencias.SingleOrDefault(s => s.Id == procedencia.Id);
                if (entityInDB == null)
                {
                    context.Procedencias.Add(procedencia);
                }
            }
            //procedencias.ForEach(c => context.Procedencias.AddOrUpdate(a => a.Id, c));
            context.SaveChanges();

            //
            // Nacionalidades
            var nacionalidades = new List<Nacionalidad>
            {
                new Nacionalidad { Id = Guid.Parse("C38AB775-1ED3-4DD7-98A0-A5DD2EACE3D5"), NombreNacionalidad = "No indicada", Visado = false }
            };
            foreach (var a in nacionalidades)
            {
                var entityInDB = context.Nacionalidades.SingleOrDefault(s => s.Id == a.Id);
                if (entityInDB == null)
                {
                    context.Nacionalidades.Add(a);
                }
            }
            //nacionalidades.ForEach(c => context.Nacionalidades.AddOrUpdate(a => a.Id, c));
            context.SaveChanges();

            //
            // Tipos de alojamiento
            var tipoAlojamientos = new List<TipoAlojamiento>
            {
                new TipoAlojamiento { Id = Guid.Parse("3319A8B4-EB68-4055-B8CA-A8DECC2189A8"), TipoAlojamientoNombre = "*Indiferente*"}
            };
            foreach (var a in tipoAlojamientos)
            {
                var entityInDB = context.TipoAlojamientos.SingleOrDefault(s => s.Id == a.Id);
                if (entityInDB == null)
                {
                    context.TipoAlojamientos.Add(a);
                }
            }
            //tipoAlojamientos.ForEach(c => context.TipoAlojamientos.AddOrUpdate(a => a.Id, c));
            context.SaveChanges();

            //
            // Etapas educativas
            var etapasEducativa = new List<EtapaEducativa>
            {
                new EtapaEducativa() { Id = Guid.Parse("3319A8B4-EB68-4055-B8CA-A8DECC2189E8"), EtapaEducativaNombre = "*Indiferente*"}
            };
            foreach (var a in etapasEducativa)
            {
                var entityInDB = context.EtapasEducativas.SingleOrDefault(s => s.Id == a.Id);
                if (entityInDB == null)
                {
                    context.EtapasEducativas.Add(a);
                }
            }
            //etapasEducativa.ForEach(c => context.EtapasEducativas.AddOrUpdate(a => a.Id, c));
            context.SaveChanges();

            //
            // Estados del visado
            var estadosVisado = new List<EstadoVisado>
            {
                new EstadoVisado() { EstadoVisadoNombre = "No iniciado", Id = Guid.Parse("66ECE39B-E566-4DC1-BCDB-4D5A48DCE0FB"), },
                new EstadoVisado() { EstadoVisadoNombre = "Denegado", Id = Guid.Parse("66ECE39B-E566-4DC1-BCDB-4D5A48DCE0FE"), },
                new EstadoVisado() { EstadoVisadoNombre = "No necesario", Id = Guid.Parse("66ECE39B-E566-4DC1-BCDB-4D5A48DCE0FD"), },
                new EstadoVisado() { EstadoVisadoNombre = "Concedido", Id = Guid.Parse("66ECE39B-E566-4DC1-BCDB-4D5A48DCE0FA"), },
                new EstadoVisado() { EstadoVisadoNombre = "En tramite", Id = Guid.Parse("66ECE39B-E566-4DC1-BCDB-4D5A48DCE0FC"), },
                
            };
            foreach (var a in estadosVisado)
            {
                var entityInDB = context.EstadosVisado.SingleOrDefault(s => s.Id == a.Id);
                if (entityInDB == null)
                {
                    context.EstadosVisado.Add(a);
                }
            }
            //estadosVisado.ForEach(c => context.EstadosVisado.AddOrUpdate(a => a.Id, c));
            context.SaveChanges();

            //
            // Motivos de cierre de ficha
            var motivosCierreFicha = new List<MotivoCierreFicha>
            {
                new MotivoCierreFicha() { Id = Guid.Parse("DB7C9B1A-0CF9-46D4-A7EE-524532A87D8A"), Motivo = "Lista Negra"},
                new MotivoCierreFicha() { Id = Guid.Parse("DB7C9B1A-0CF9-46D4-A7EE-524532A87D8B"), Motivo = "Volver a contactar"},
                new MotivoCierreFicha() { Id = Guid.Parse("DB7C9B1A-0CF9-46D4-A7EE-524532A87D8C"), Motivo = "Fin de t�rmino"},
                new MotivoCierreFicha() { Id = Guid.Parse("DB7C9B1A-0CF9-46D4-A7EE-524532A87D8D"), Motivo = "Autodescartado"},
                new MotivoCierreFicha() { Id = Guid.Parse("DB7C9B1A-0CF9-46D4-A7EE-524532A87D8E"), Motivo = "Rechazado"},
                
            };
            foreach (var a in motivosCierreFicha)
            {
                var entityInDB = context.MotivosCierreFicha.SingleOrDefault(s => s.Id == a.Id);
                if (entityInDB == null)
                {
                    context.MotivosCierreFicha.Add(a);
                }
            }
            //motivosCierreFicha.ForEach(c => context.MotivosCierreFicha.AddOrUpdate(a => a.Id, c));
            context.SaveChanges();


            //
            // Universidades de registro
            var universidadesDeRegistro = new List<UniversidadDeRegistro>
            {
                new UniversidadDeRegistro() { Id = Guid.Parse("154BEBC5-A2A1-4034-976D-9E4EFADDF7B4"), NombreUniversidadDeRegistro = "A�n no registrado", Visible = true}
            };
            foreach (var a in universidadesDeRegistro)
            {
                var entityInDB = context.UniversidadesDeRegistro.SingleOrDefault(s => s.Id == a.Id);
                if (entityInDB == null)
                {
                    context.UniversidadesDeRegistro.Add(a);
                }
            }
            //universidadesDeRegistro.ForEach(c => context.UniversidadesDeRegistro.AddOrUpdate(a => a.Id, c));
            context.SaveChanges();


        }
    }
}
