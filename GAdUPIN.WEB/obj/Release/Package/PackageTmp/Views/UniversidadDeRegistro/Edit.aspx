﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.WEB.ViewModels.UniversidadDeRegistroNewEditVM>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Editar Universidad de Registro
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Editar Universidad de Registro</h2>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
            <div class="editor-label" style="display: none">
                <%: Html.LabelFor(model => model.UniversidadDeRegistro.Id) %>
            </div>
            <div class="editor-label" style="display: none">
                <%: Html.TextBoxFor(model => model.UniversidadDeRegistro.Id) %>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.UniversidadDeRegistro.NombreUniversidadDeRegistro) %>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.UniversidadDeRegistro.NombreUniversidadDeRegistro) %>
                <%: Html.ValidationMessageFor(model => model.UniversidadDeRegistro.NombreUniversidadDeRegistro) %>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.UniversidadDeRegistro.Visible) %>
            </div>
            <div class="der">
                <%: Html.CheckBoxFor(model => model.UniversidadDeRegistro.Visible) %>
                <%: Html.ValidationMessageFor(model => model.UniversidadDeRegistro.Visible) %>
            </div>
            
            
        </fieldset>
        <p>
                <input type="submit" value="Guardar" class="boton_der" />
        </p>
    <% } %>

    <div>
        <%--<%: Html.ActionLink("Volver", "Index", null, new { @class="boton"}) %>--%>
    </div>

</asp:Content>

