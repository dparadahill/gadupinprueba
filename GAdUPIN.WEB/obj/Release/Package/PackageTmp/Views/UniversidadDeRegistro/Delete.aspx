﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.UniversidadDeRegistro>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Eliminar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Eliminar</h2>

    <h3 class="warning">¡Una vez eliminado, el registro no se puede recuperar!</h3>
    <fieldset>
        <legend>Fields</legend>
        
        <div class="display-label" style="display: none">Id</div>
        <div class="display-field" style="display: none"><%: Model.Id %></div>
        
        <div class="display-label">Nombre de la universidad</div>
        <div class="display-field"><%: Model.NombreUniversidadDeRegistro %></div>
        
        <div class="display-label">Visible</div>
        <div class="display-field"><%: Model.Visible? "Si":"No"%></div>
        
    </fieldset>
    <% using (Html.BeginForm()) { %>
        <p>
		    <input type="submit" value="Eliminar" class="boton_der"/> 
		    <%--<%: Html.ActionLink("Volver", "Index", "UniversidadDeRegistro", null, new { @class = "boton_izq" })%>--%>
        </p>
    <% } %>

</asp:Content>

