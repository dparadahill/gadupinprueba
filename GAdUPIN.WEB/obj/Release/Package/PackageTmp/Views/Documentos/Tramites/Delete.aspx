﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.DocumentoDeTramite>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Eliminar documento
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Eliminar</h2>

    <h3>¿Seguro que quiere eliminar el registro?</h3>
    <fieldset>
        
        <div class="display-label" style="display: none">Id</div>
        <div class="display-field" style="display: none"><%: Model.Id %></div>
        
        <div class="display-label">Nombre</div>
        <div class="display-field"><%: Model.NombreDocumento %></div>
        
        <div class="display-label">Fecha Subida</div>
        <div class="display-field"><%: Model.FechaSubida %></div>
        
        <div class="display-label">Estado</div>
        <div class="display-field"><%: Model.Estado %></div>
        
        <div class="display-label">Comentarios</div>
        <div class="display-field"><%: Model.Comentarios %></div>
    </fieldset>
    <% using (Html.BeginForm()) { %>
        <p>
		    <input type="submit" class="boton_der" value="Confirmar" /> 
		    <%--<%: Html.ActionLink("Volver", "Index", "Documentos", new {idCandidato = ViewData["idCandidato"], tipo = 2 }, new { @class = "boton_izq" })%>--%>
        </p>
    <% } %>

</asp:Content>

