﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<IEnumerable<GAdUPIN.CORE.Domain.DocumentoDeTramite>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Documentos de trámites 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Documentos de trámites</h2>

   <table id="Jtable" class="display" style="text-align: left" >
        <thead>
            <tr>
                <th>
                    Documento
                </th>
                <th>
                    Fecha subida
                </th>
                <th>
                    Estado
                </th>
                <th></th>
            </tr>
        </thead>
        
        <tbody>
            <tr>
            <% foreach (var item in Model) { %>
                <td>
                    <%: item.NombreDocumento %>
                </td>
                <td>
                    <%: Html.DisplayFor(m => item.FechaSubida)%>
                </td>
                <td>
                    <%: item.Estado %>
                </td>
                <td>
                    <%: Html.ActionLink("Editar", "EditDocDeTramites", new { id=item.Id, idCandidato = ViewData["idCandidato"] }) %> |
                    <%: Html.ActionLink("Eliminar", "DeleteDocDeTramites", new { id = item.Id, idCandidato = ViewData["idCandidato"] })%>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
        <script type="text/javascript">
            $(document).ready(function () {
                $.fn.dataTableExt.sErrMode = 'throw';
                $('#Jtable').dataTable({
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    }
                });
            });
    </script>

    <p style="clear: left; margin-top:30px;">
        <%: Html.ActionLink("Añadir", "CreateDocDeTramites", new { idCandidato = ViewData["idCandidato"] }, new { @class = "boton_der" })%>
        <%--<%: Html.ActionLink("Volver", "DetailsCandidato", "Candidato", new { id = ViewData["idCandidato"] }, new { @class = "boton_izq" })%>--%>
    </p>

</asp:Content>

