﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.DocumentoDeTramite>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Editar documento
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Editar documento</h2>
    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
          
            <div class="izq" style="display: none">
                <%: Html.LabelFor(model => model.Id) %>
            </div>
            <div class="der" style="display: none">
                <%: Html.TextBoxFor(model => model.Id) %>
                <%: Html.ValidationMessageFor(model => model.Id) %>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.NombreDocumento) %>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.NombreDocumento) %>
                <%: Html.ValidationMessageFor(model => model.NombreDocumento) %>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.FechaSubida) %>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.FechaSubida)%>
                <%: Html.ValidationMessageFor(model => model.FechaSubida)%>
            </div>
            
            <div class="izq" style="clear:both">
                <%: Html.LabelFor(model => model.Estado)%>
            </div>
            <div class="der">  
                <%: Html.DropDownListFor(model => model.Estado, new SelectList(
                    new[]
                    {
                        new {Value = "Enviado", Text = "Enviado"},      
                        new {Value = "Recibido", Text = "Recibido"},      
                        new {Value = "Reenviado", Text = "Reenviado"},      
                        new {Value = "Archivado", Text = "Archivado"}      
                    },
                    "Value",
                    "Text",
                    Model.Estado
                 )
                )%>
                <%: Html.ValidationMessageFor(model => model.Estado)%>
            </div>
            
            <div class="comentario">
                <%: Html.TextAreaFor(model => model.Comentarios) %>
                <%: Html.ValidationMessageFor(model => model.Comentarios) %>
            </div>

            

        </fieldset>
        
        <input type="submit" value="Guardar" class="boton_der" />
    <% } %>

    <div>
         <%--<%: Html.ActionLink("Volver", "Index", "Documentos", new {idCandidato = ViewData["idCandidato"], tipo = 2}, new { @class = "boton_izq" })%>--%>
    </div>

</asp:Content>

