﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
 Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.ComunidadAutonoma>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Nueva Comunidad Autonoma
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Añadir nueva comunidad autonoma</h2>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
       

        <fieldset>
            <legend>Rellenar los siguientes campos</legend>
            
            <div class="editor-label" style="display: none">
                <%: Html.LabelFor(model => model.Id) %>
            </div>
            <div class="editor-field" style="display: none">
                <%: Html.TextBoxFor(model => model.Id) %>
                <%: Html.ValidationMessageFor(model => model.Id) %>
            </div>
            
            <div class="izq">
                <%: Html.Label("Nombre de la comunidad autonoma") %>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.NombreComunidadAutonoma) %>
                <%: Html.ValidationMessageFor(model => model.NombreComunidadAutonoma) %>
            </div>
            
            
        </fieldset>
        
                <input type="submit" class="boton_der" value="Guardar" />
    <% } %>

    <div>
        <%--<%: Html.ActionLink("Volver", "Index", null, new { @class = "boton_izq" })%>--%>
    </div>
<%--    diegos solution    <script type="text/javascript">
                $("#form0 .boton").bind("click", function () {
                    if ($('input#NombreComunidadAutonoma').val() == '' /*||
                    $('input#NombreComunidadAutonoma').val() == ''*/) {
                        alert('Debe rellenar el campo.');
                        return false;
                    } else {
                        return true;
                    }
                });
        </script>--%>
</asp:Content>

