﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Subir foto
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Subir foto</h2>
    
    
    <form action="" method="post" enctype="multipart/form-data">
        
        
        <div>
            <label for="file">Archivo:</label>
            <input type="file" name="file" id="file"/>
        </div>
        <input type="submit"class="boton" value="Subir documento" style="margin-top:20px;" />

    </form>
</asp:Content>
