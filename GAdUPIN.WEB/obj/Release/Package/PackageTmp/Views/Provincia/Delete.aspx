﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.Provincia>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Eliminar provincia
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Eliminar</h2>

    <h3>Una vez eliminado, el registro no es puede recuperar.</h3>
    <fieldset>
        
        
        <div class="display-label" style="display: none">Id</div>
        <div class="display-field"  style="display: none"><%: Model.Id %></div>
        
        <div class="display-label"> <%: Html.LabelFor(model => model.NombreProvincia) %></div>
        <div class="display-field"> <%: Model.NombreProvincia %></div>
        
        <div class="display-label"> <%: Html.LabelFor(model => model.ComunidadAutonoma.NombreComunidadAutonoma) %></div>
        <div class="display-field"><%: Model.ComunidadAutonoma.NombreComunidadAutonoma %></div>
        
        <div class="display-label"  style="display: none">ID Comunidad Autonoma</div>
        <div class="display-field"  style="display: none"><%: Model.ComunidadAutonoma.Id %></div>

    </fieldset>
    <% using (Html.BeginForm()) { %>
        <p>
		    <input type="submit" class="boton_der" value="Confirmar" /> 
		    <%--<%: Html.ActionLink("Volver", "Index", new{ id = Model.ComunidadAutonoma.Id}, new { @class="boton_izq"}) %>--%>
        </p>
    <% } %>

</asp:Content>

