﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.Nacionalidad>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Eliminar nacionalidad
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Eliminar</h2>

    <h3>¿Seguro que quiere eliminar el registro?</h3>
    <h4>No es aconsejable borrar un registro si ya es usado por otra tabla, puede causar problemas de integridad</h4>
    <fieldset>
        
        <div class="display-label" style="display: none">Id</div>
        <div class="display-field" style="display: none"><%: Model.Id %></div>
        
        <div class="display-label">Nacionalidad</div>
        <div class="display-field"><%: Model.NombreNacionalidad %></div>
        
        <div class="display-label">Visado</div>
        <div class="display-field"><%: Model.Visado %></div>
        
    </fieldset>
    <% using (Html.BeginForm()) { %>
        
		    
        <input type="submit" value="Confirmar" class="boton_der" /> 
        
    <div>
        <%--<%: Html.ActionLink("Volver", "Index", "Nacionalidad", null, new { @class="boton_izq"}) %>--%>
    </div>
    <% } %>

</asp:Content>

