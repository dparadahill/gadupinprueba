﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.Nacionalidad>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Editar nacionalidad
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Editar nacionalidad</h2>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
            <legend>Fields</legend>
            
            <div class="editor-label" style="display: none">
                <%: Html.LabelFor(model => model.Id) %>
            </div>
            <div class="editor-field" style="display: none">
                <%: Html.TextBoxFor(model => model.Id) %>
                <%: Html.ValidationMessageFor(model => model.Id) %>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.NombreNacionalidad) %>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.NombreNacionalidad) %>
                <%: Html.ValidationMessageFor(model => model.NombreNacionalidad) %>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.Visado) %>
            </div>
            <div class="der">
                <%: Html.CheckBoxFor(model => model.Visado) %>
                <%: Html.ValidationMessageFor(model => model.Visado) %>
            </div>
            
            
            
            
        </fieldset>
            <input type="submit" value="Guardar" class="boton_der" />
    <% } %>

    <div>
       <%--<%: Html.ActionLink("Volver", "Index", "Nacionalidad", null, new { @class = "boton_izq" })%>--%>
    </div>

</asp:Content>

