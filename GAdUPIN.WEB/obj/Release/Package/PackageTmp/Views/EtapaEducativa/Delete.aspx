﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
 Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.EtapaEducativa>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Eliminar etapa educativa
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Eliminar</h2>

    <h3>¿Seguro que quiere eliminar el registro?</h3>
    <h4>No es aconsejable borrar un registro si ya es usado por otra tabla, puede causar problemas de integridad</h4>
    <fieldset>
        <legend>Campos</legend>
        
        <div class="display-label" style="display: none">Id</div>
        <div class="display-field" style="display: none"><%: Model.Id %></div>
        
        <div class="display-label">Etapa Educativa</div>
        <div class="display-field"><%: Model.EtapaEducativaNombre %></div>
        
    </fieldset>
    <% using (Html.BeginForm()) { %>
 
    
    <div>
        <input type="submit" value="Confirmar" class="boton_der"/>        
          <%--<%: Html.ActionLink("Volver", "Index", "EtapaEducativa", null, new { @class = "boton_izq" })%>--%>

    </div>
    <% } %>

</asp:Content>

