﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
 Inherits="System.Web.Mvc.ViewPage<IEnumerable<GAdUPIN.WEB.ViewModels.CandidatoAsignarPlazaListVM>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Cubrir plaza
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 
    <h2>Cubrir plaza </h2>
     <table id="Jtable" class="display" style="text-align: left">
        <thead>
            <tr>
                <th>
                    Código
                </th>
                <th>
                    Nombre
                </th>
                <th>
                    Sexo
                </th>
                <th>
                    Nac.
                </th>
                <th>
                    C.A. Pref.
                </th>
                <th>
                    Prov. Pref.
                </th>
                <th>
                    Etapa Ed. Pref.
                </th>
                <th>
                    Alojamiento Pref.
                </th>
                <th>
                    Disponibilidad
                </th>
                <th></th>
            </tr>
        </thead>

   
        <tbody>
            <tr>
                 <% foreach (var item in Model) { %>
                <td>
                    <%: Html.DisplayFor(m => item.Candidato.Codigo)%>
                </td>
                <td>
                    <%: item.Candidato.Nombre + " " +  item.Candidato.Apellido1 + " " + item.Candidato.Apellido2%>
                </td>
                <td>
                    <%: item.Candidato.Sexo ? "Masculino" : "Femenino"%>
                </td>
                <td>
                    <%: Html.DisplayFor(m => item.Candidato.Nacionalidad.NombreNacionalidad)%>
                </td>
                <td>
                    <%: Html.DisplayFor(m => item.Candidato.ComunidadAutonomaPreferida.NombreComunidadAutonoma)%>
                </td>
                <td>
                    <%: Html.DisplayFor(m => item.Candidato.ProvinciaPreferida.NombreProvincia)%>
                </td>
                <td>
                    <%: Html.DisplayFor(m => item.Candidato.EtapaEducativaPreferida.EtapaEducativaNombre)%>
                </td>
                <td>
                    <%: Html.DisplayFor(m => item.Candidato.TipoAlojamientoPreferido.TipoAlojamientoNombre)%>
                </td>
                <td>
                    <%: Html.DisplayFor(m => item.Candidato.FechaDisponibilidad)%>
                </td>
                <td>
                <% if (item.Candidato.PlazasDeColegiosAsignadas == null || item.Candidato.PlazasDeColegiosAsignadas.Count == 0)
                   { %>
                    <%: Html.ActionLink("Cubrir plaza", "CubrirPlaza", new { idCandidato = item.Candidato.Id, idPlaza = item.IdPlaza })%> 
                <% }
                else { %>
                    <%: Html.ActionLink("Ya cubre una plaza", "DetailsCandidato", "Candidato" ,new { id=item.Candidato.Id }, null) %> 
                <% } %>
                    
                </td>
            </tr>  
            <% } %> 
        </tbody>
    </table>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $.fn.dataTableExt.sErrMode = 'throw';
            $('#Jtable').dataTable({
                "oLanguage": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    </script>
   
</asp:Content>

