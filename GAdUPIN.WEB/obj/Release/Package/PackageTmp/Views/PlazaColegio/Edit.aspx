﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.WEB.ViewModels.PlazaColegioNewEditVM>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Editar Plaza
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <% if (Model.PlazaColegio.PlazaContratada) {%>
        <%--<%: Html.ActionLink("Liberar plaza", "DesasignarPlaza", new {id = Model.PlazaColegio.Id}, new {@class = "botonGris", @style = "float:right"}) %>  --%>
        *Liberación de plaza temporalmente desabilitada*
    <% } %>
    

    <h2>Editar</h2>

   <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <fieldset style="clear: both">
            
 
             <div class="display-label" >
                <%: Html.Label("Colegio ") %> 
                <%: Html.DisplayFor(model => model.PlazaColegio.Colegio.NombreColegio)%>
                <%: Html.ValidationMessageFor(model => model.PlazaColegio.Colegio.NombreColegio)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.PlazaColegio.CodigoPlaza) %>  
            </div>
            <div class="der">
                <%: Html.DisplayFor(model => model.PlazaColegio.CodigoPlaza) %>  
            </div>
            <div class="izq" style="font-weight: bold">
                Candidato
            </div>
            <div class="der">
                <% if (Model.PlazaColegio.CandidatoAsignado == null) { %>
                    <%: Html.ActionLink("Cubrir", "ListarCandidatosParaAsignacion", new {idPlaza = Model.PlazaColegio.Id}) %> 
                <% } 
                else { %>
                <%: Html.ActionLink(Model.CandidatoAsignado.Nombre + " " + Model.CandidatoAsignado.Apellido1 + " " + Model.CandidatoAsignado.Apellido2, "DetailsCandidato", "Candidato", new { id = Model.CandidatoAsignado.Id }, null)%> 
                <% } %>
                <%: Html.ValidationMessageFor(model => model.CandidatoAsignado)%>  
            </div>
            <div class="izq" style="margin-top:5px; clear:left">
                <%: Html.LabelFor(model => model.PlazaColegio.PlazaContratada) %>
            </div>
            <div class="der" style="margin-top:5px ">
                <%: Html.CheckBoxFor(model => model.PlazaColegio.PlazaContratada, new {disabled = "disabled", })%>
                <%: Html.ValidationMessageFor(model => model.PlazaColegio.PlazaContratada)%>
            </div>
            
            <div class="izq" style="display: none">
                <%: Html.LabelFor(model => model.SelectedColegio) %>
            </div>
            <div class="der" style="display: none">
                <%: Html.TextBoxFor(model => model.SelectedColegio)%>
                <%: Html.ValidationMessageFor(model => model.SelectedColegio)%>
            </div>
            
            <div class="izq" style="display: none">
                <%: Html.LabelFor(model => model.PlazaColegio.Id) %>
            </div>
            <div class="der" style="display: none">
                <%: Html.TextBoxFor(model => model.PlazaColegio.Id)%>
                <%: Html.ValidationMessageFor(model => model.PlazaColegio.Id)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.PlazaColegio.BecaCuantia) %>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.PlazaColegio.BecaCuantia)%>
                <%: Html.ValidationMessageFor(model => model.PlazaColegio.BecaCuantia)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.PlazaColegio.NumeroHorasPlaza) %>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.PlazaColegio.NumeroHorasPlaza)%>
                <%: Html.ValidationMessageFor(model => model.PlazaColegio.NumeroHorasPlaza)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.PlazaColegio.FechaInicio) %>
            </div>
            <div class="der">
                <%: Html.EditorFor(model => model.PlazaColegio.FechaInicio)%>
                <%: Html.ValidationMessageFor(model => model.PlazaColegio.FechaInicio)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.PlazaColegio.FechaFin) %>
            </div>
            <div class="der">
                <%: Html.EditorFor(model => model.PlazaColegio.FechaFin)%>
                <%: Html.ValidationMessageFor(model => model.PlazaColegio.FechaFin)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.PlazaColegio.EtapaEducativa) %>
            </div>
            <div class="der">
                <%: @Html.DropDownListFor(model => model.PlazaColegio.EtapaEducativa.Id,
                       new SelectList(Model.EtapasEducativas,
                        "Id", "EtapaEducativaNombre", Model.EtapaEducativaActual))
                %>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.PlazaColegio.PreferenciaSexo) %>
            </div>
            <div class="der">  
                <%: Html.DropDownListFor(model => model.PlazaColegio.PreferenciaSexo, new SelectList(
                    new[]
                    {
                        new {Value = "Masculino", Text = "Masculino"},
                        new {Value = "Femenino", Text = "Femenino"},    
                        new {Value = "Indiferente", Text = "Indiferente"},
                    },
                    "Value",
                    "Text",
                    Model.PreferenciaSexoActual
                 )
                )%>
                <%: Html.ValidationMessageFor(model => model.PlazaColegio.PreferenciaSexo)%>
            </div>
            
            
            
            
            
            
            <div class="izq" >
                <%: Html.LabelFor(model => model.PlazaColegio.NacionalidadPreferente)%>
            </div>
            <div class="der">  
                <%: @Html.DropDownListFor(model => model.SelectedNacionalidad,
                        new SelectList(Model.Nacionalidades,
                            "Id", "NombreNacionalidad"))%> 
            </div>  
            
            <div class="izq">
                <%: Html.LabelFor(model => model.PlazaColegio.CondicionesOfrecidas) %>
            </div>
            <div class="der">
                 <%: Html.DropDownListFor(model => model.PlazaColegio.CondicionesOfrecidas, new SelectList(
                    new[]
                    {
                        new {Value = "Ninguna", Text = "Ninguna"},
                        new {Value = "Servicio comedor", Text = "Servicio comedor"},
                        new {Value = "Servicio desayuno", Text = "Servicio desayuno"},    
                        new {Value = "Transporte escolar", Text = "Transporte escolar"},     
                        new {Value = "Piso gratuito", Text = "Piso gratuito"},     
                        new {Value = "Alojamiento familia", Text = "Alojamiento familia"},     
                    },
                    "Value",
                    "Text",
                    1
                 )
                )%>
                
            </div>

            <div class="izq">
                <%: Html.LabelFor(model => model.PlazaColegio.DatoAdicional) %>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.PlazaColegio.DatoAdicional)%>
                <%: Html.ValidationMessageFor(model => model.PlazaColegio.DatoAdicional)%>
            </div>
            
               

        </fieldset>
        
        <fieldset style="clear:both">
                <legend>
                      <%: Html.LabelFor(model => model.PlazaColegio.Comentarios) %>
                </legend>
                
                <div class="comentario">
                    <%: Html.TextAreaFor(model => model.PlazaColegio.Comentarios)%>
                </div>
        </fieldset>
        <input type="submit" value="Guardar" class="boton_der" />
        
    <% } %>

    <div>
       <%--<%: Html.ActionLink("Volver", "Index", new { id = Model.PlazaColegio.Colegio.Id }, new { @class = "boton_izq" })%>    --%>
    </div>
    
   <script type="text/javascript">

       $('#form0').submit(function () {
           var obj;
           obj = $('#PlazaColegio_EtapaEducativa_Id');
           if (obj.val() == "") {
               alert("Debe elegir una etapa educativa");
               obj.focus();
               return false;
           }
           return true;
       });
     </script>
     
     <script type="text/javascript">
         $(function () {
             $('#PlazaColegio_FechaInicio,' +
               '#PlazaColegio_FechaFin'
                ).datepicker({
                    changeYear: true,
                    yearRange: "1960:2050",
                    dateFormat: "yy-mm-dd",
                    onSelect: function (dateText, inst) {
                        //                                alert(dateText);
                    }
                });
         });
      </script>
</asp:Content>