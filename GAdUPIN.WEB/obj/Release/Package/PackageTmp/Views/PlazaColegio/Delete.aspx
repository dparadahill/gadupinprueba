﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
 Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.PlazaColegio>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Eliminar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Eliminar</h2>

    <h3 class="warning">¡Una vez eliminado, el registro no se puede recuperar!</h3>
    <fieldset>
       
        
        <div class="display-label" style="display: none"> Id </div>
        <div class="display-label" style="display: none"> <%: Model.Id %></div>
        
        <div class="display-label">Fecha Inicio</div>
        <div class="display-field"><%: Html.DisplayFor(c => c.FechaInicio)%></div>
        
        <div class="display-label">Fecha Fin</div>
        <div class="display-field"><%: Html.DisplayFor(c => c.FechaFin)%></div>
        
        <div class="display-label">Cuantia Beca</div>
        <div class="display-field"><%: Model.BecaCuantia %></div>
        
        <div class="display-label">Entrevista</div>
        <div class="display-field"><%: Model.Entrevista %></div>
        
        <div class="display-label">Condiciones Firmadas</div>
        <div class="display-field"><%: Model.CondicionesFirmadas %></div>
        
        <div class="display-label">Comentarios</div>
        <div class="display-field"><%: Model.Comentarios %></div>
        
        <div class="display-label">Plaza Contratada</div>
        <div class="display-field"><%: Model.PlazaContratada %></div>
        
        <div class="display-label">Numero Horas</div>
        <div class="display-field"><%: Model.NumeroHorasPlaza %></div>
        
    </fieldset>
    <% using (Html.BeginForm()) { %>
        <p>
		    <input type="submit" class="boton_der" value="Confirmar" /> 
		    <%--<%: Html.ActionLink("Volver", "Index", new { id = Model.Colegio.Id }, new { @class = "boton_izq" })%>--%>
            
        </p>
    <% } %>

</asp:Content>

