﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.WEB.ViewModels.EncargadoNewEditVM>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Nuevo Encargado
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Nuevo encargado</h2>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <fieldset>
            
            <div class="izq" style="display: none">
                <%: Html.LabelFor(model => model.Encargado.Id) %>
            </div>
            <div class="der" style="display: none">
                <%: Html.TextBoxFor(model => model.Encargado.Id) %>
                <%: Html.ValidationMessageFor(model => model.Encargado.Id)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.Encargado.DNI)%>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.Encargado.DNI)%>
                <%: Html.ValidationMessageFor(model => model.Encargado.DNI)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.Encargado.Nombre)%>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.Encargado.Nombre)%>
                <%: Html.ValidationMessageFor(model => model.Encargado.Nombre)%>
            </div>
            
           <div class="izq">
                <%: Html.LabelFor(model => model.Encargado.Apellido1)%>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.Encargado.Apellido1)%>
                <%: Html.ValidationMessageFor(model => model.Encargado.Apellido1)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.Encargado.Apellido2)%>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.Encargado.Apellido2)%>
                <%: Html.ValidationMessageFor(model => model.Encargado.Apellido2)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.Encargado.Email)%>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.Encargado.Email)%>
                <%: Html.ValidationMessageFor(model => model.Encargado.Email)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.Encargado.ComunidadAutonoma.NombreComunidadAutonoma)%>
            </div>
            <div class="der">
                <%: @Html.DropDownListFor(model => model.Encargado.ComunidadAutonoma.Id,
                           new SelectList(Model.ComunidadesAutonomas,
                            "Id", "NombreComunidadAutonoma", 1), "*Seleccione Comunidad Autonóma*")
                %>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.Encargado.TelefonoDeContacto)%>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.Encargado.TelefonoDeContacto)%>
                <%: Html.ValidationMessageFor(model => model.Encargado.TelefonoDeContacto)%>
            </div>
        
        
        </fieldset>
            <input type="submit" value="Guardar" class="boton_der" />
    <% } %>

    <div>
        <%--<%: Html.ActionLink("Volver", "Index", "Encargado", null, new { @class = "boton_izq" })%>--%>
    </div>
    
    <script type="text/javascript">

        $('#form0').submit(function () {
            var obj;
            obj = $('#Encargado_ComunidadAutonoma_Id');
            if (obj.val() == "") {
                alert("Debe elegir una comunidad autónoma");
                obj.focus();
                return false;
            }
            return true;
        });
       
    </script>
</asp:Content>

