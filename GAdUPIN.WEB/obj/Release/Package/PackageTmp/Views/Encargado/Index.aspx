﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<IEnumerable<GAdUPIN.CORE.Domain.Encargado>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Encargados
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Encargados</h2>

    <table id="Jtable" class="display" style="text-align: left" >
        <thead>
            <tr>
                <th>
                    Encargado
                </th>
                <th>
                    Comunidad Autonoma
                </th>
                <th>
                    Responsable por
                </th>
                <th></th>
            </tr>
        </thead>
    
    <tbody>
            <tr>
                <% foreach (var item in Model) { %>
                <td>
                    <%: item.Nombre + " " + item.Apellido1 + " " + item.Apellido2 %>
                </td>
                <td>
                    <%: item.ComunidadAutonoma.NombreComunidadAutonoma %>
                </td>
                <td>
                    <%: Html.ActionLink("Ver candidatos", "ResponsablePor", new { id=item.Id}) %> 
                </td>
                <td>
                    <%: Html.ActionLink("Editar", "Edit", new { id=item.Id}) %> |
                    <%: Html.ActionLink("Eliminar", "Delete", new { id = item.Id })%>
                </td>
            </tr>
        <% } %>
    </tbody>
    </table>

    <script type="text/javascript">
        $(document).ready(function () {
            $.fn.dataTableExt.sErrMode = 'throw';
            $('#Jtable').dataTable({
                "sDom": 'T<"clear">lfrtip',
                "oTableTools": {
                    "sSwfPath": "../../Content/DataTables-1.9.4/extras/TableTools/media/swf/copy_csv_xls.swf",
                    "aButtons": ["copy", "csv", "print"]
                },
                "oLanguage": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    </script>
    <p style="clear: left; margin-top:30px;">
        <%: Html.ActionLink("Añadir", "Create", null, new { @class = "boton_der" })%>
        <%--<%: Html.ActionLink("Volver", "Index", "Candidato", null, new { @class = "boton_izq"})%>--%>
    </p>

</asp:Content>

