﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GAdUPIN.WEB.ViewModels.CandidatosCerradosListVM>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Fichas Cerradas
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Fichas cerradas</h2>

    <table id="Jtable" class="display" style="text-align: left" >
        <thead>
            <tr>
                <th style="display: none">
                    IdCandidato
                </th>
                <th style="display: none">
                    IdFicha
                </th>
                <th>
                    Código
                </th>
                <th>
                    Nombre
                </th>
                <th>
                    Apellido
                </th>
                <th>
                    Motivo Cierre
                </th>
                <th>
                    Contactar
                </th>
                <%--<th>
                    ValidateRequest
                </th>--%>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <% foreach (var item in Model) { %>
                <td style="display: none">
                    <%: item.IdCandidato %>
                </td>
                <td style="display: none">
                    <%: item.IdFicha %>
                </td>
                <td>
                    <%: item.CodigoCandidato %>
                </td>
                <td>
                    <%: item.CandidatoNombre %>
                </td>
                <td>
                    <%: item.CandidatoApellido1 %>
                </td>
                <td>
                    <%: item.MotivoCierreFicha %>
                </td>
                <td>
                    
                    <%: Html.DisplayFor(m => item.AnyoContacto)%>
                </td>
                <%--<td>
                    <%: item.ValidateRequest %>
                </td>--%>
                <td>
                    <%: Html.ActionLink("Ver Ficha", "DetailsCandidato", new { id=item.IdCandidato }) %> 
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
    

 <script type="text/javascript">
     $(document).ready(function () {
         $.fn.dataTableExt.sErrMode = 'throw';
         $('#Jtable').dataTable({
             "sDom": 'T<"clear">lfrtip',
             "oTableTools": {
                 "sSwfPath": "../../Content/DataTables-1.9.4/extras/TableTools/media/swf/copy_csv_xls.swf",
                 "aButtons": ["copy", "csv", "print"]
             },
             "oLanguage": {
                 "sProcessing": "Procesando...",
                 "sLengthMenu": "Mostrar _MENU_ registros",
                 "sZeroRecords": "No se encontraron resultados",
                 "sEmptyTable": "Ningún dato disponible en esta tabla",
                 "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                 "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                 "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                 "sInfoPostFix": "",
                 "sSearch": "Buscar:",
                 "sUrl": "",
                 "sInfoThousands": ",",
                 "sLoadingRecords": "Cargando...",
                 "oPaginate": {
                     "sFirst": "Primero",
                     "sLast": "Último",
                     "sNext": "Siguiente",
                     "sPrevious": "Anterior"
                 },
                 "oAria": {
                     "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                     "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                 }
             }
         });
     });
    </script>
   

    

</asp:Content>

