﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
         Inherits="System.Web.Mvc.ViewPage<GAdUPIN.WEB.ViewModels.CandidatoDetailsVM>" %>



<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Detalles del Candidato
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        
        <% if (Model.Candidato.fichaCerrada != null && Model.Candidato.fichaCerrada)
           { %>
            <h3 class="warning">¡Esta ficha se encuentra cerrada! Revisar motivo y comentarios abajo </h3>
        <% } %>

        <% if (Model.IdPlaza != Guid.Parse("00000000-0000-0000-0000-000000000000"))
           { %>
            <h3 style="background: palegreen; color: black; text-align: center">¡Esta ficha se encuentra cubierta!</h3>
        <% } %> 

     
        <div ><h2 style="float: left"> Detalles del Candidato </h2>
            <fieldset style="float: right; margin-bottom: 20px; padding: 0 1em 0.3em 1em; text-align: center"  >
                <legend>Encargado</legend>
            
                <% if (Model.Candidato.Encargado != null)
                   { %>
                    <%: Html.ActionLink(Model.Candidato.Encargado.Nombre + " " + Model.Candidato.Encargado.Apellido1 + " " + Model.Candidato.Encargado.Apellido2, "ResponsablePor", "Encargado", new {id = Model.Candidato.Encargado.Id}, null) %> 
                
                <% }
                   else
                   { %>
                    <%: Html.ActionLink("Asignar", "AsignarEncargado", "Encargado", new {idCandidato = Model.Candidato.Id}, null) %> 
                <% } %>

            </fieldset>  
        </div>

        <% using (Html.BeginForm("DetailsCandidato", "Candidato", FormMethod.Post))
           { %>
 
       
 
            <fieldset style="clear: right; display: inline; float: left; margin-bottom: 7px; width: 46.5%;">
                <legend> Datos Generales </legend>
            

                <div class="izq" style="display: none">
                    <%: Html.LabelFor(model => model.Candidato.Id) %>
                </div>
                <div class="der" style="display: none">
                    <%: Html.TextBoxFor(model => model.Candidato.Id) %>
                    <%: Html.ValidationMessageFor(model => model.Candidato.Id) %>
                </div>
        
                <div class="izq"  >
                    <%: Html.LabelFor(model => model.Candidato.Codigo) %>
                </div>
                <div class="der" >
                    <%: Html.DisplayFor(model => model.Candidato.Codigo) %>
                </div>
        

                <div class="izq" >
                    <%: Html.LabelFor(model => model.Candidato.Nombre) %>
                </div>
                <div class="der" >
                    <%: Html.TextBoxFor(model => model.Candidato.Nombre) %>
                    <%: Html.ValidationMessageFor(model => model.Candidato.Nombre) %>
                </div>
        
                <div class="izq" >
                    <%: Html.LabelFor(model => model.Candidato.Apellido1) %>
                </div>
                <div class="der" >
                    <%: Html.TextBoxFor(model => model.Candidato.Apellido1) %>
                    <%: Html.ValidationMessageFor(model => model.Candidato.Apellido1) %>
                </div>
        
                <div class="izq" >
                    <%: Html.LabelFor(model => model.Candidato.Apellido2) %>
                </div>
                <div class="der" >
                    <%: Html.TextBoxFor(model => model.Candidato.Apellido2) %>
                    <%: Html.ValidationMessageFor(model => model.Candidato.Apellido2) %>
                </div>
        
                <div class="izq" >
                    <%: Html.LabelFor(model => model.Candidato.Email) %>
                </div>
                <div class="der" >
                    <%: Html.TextBoxFor(model => model.Candidato.Email) %>
                    <%: Html.ValidationMessageFor(model => model.Candidato.Email) %>
                </div>
        
                <div class="izq" >
                    <%: Html.LabelFor(model => model.Candidato.TelefonoDeContacto) %>
                </div>
                <div class="der" >
                    <%: Html.TextBoxFor(model => model.Candidato.TelefonoDeContacto) %>
                    <%: Html.ValidationMessageFor(model => model.Candidato.TelefonoDeContacto) %>
                </div>
        
                <div class="izq" >
                    <%: Html.LabelFor(model => model.Candidato.TelefonoEspanol) %>
                </div>
                <div class="der" >
                    <%: Html.TextBoxFor(model => model.Candidato.TelefonoEspanol) %>
                    <%: Html.ValidationMessageFor(model => model.Candidato.TelefonoEspanol) %>
                </div>
        
                <div class="izq" >
                    <%: Html.LabelFor(model => model.Candidato.Carrera) %>
                </div>
                <div class="der" >
                    <%: Html.TextBoxFor(model => model.Candidato.Carrera) %>
                    <%: Html.ValidationMessageFor(model => model.Candidato.Carrera) %>
                </div>
        
                <div class="izq" >
                    <%: Html.LabelFor(model => model.Candidato.Skype) %>
                </div>
                <div class="der" >
                    <%: Html.TextBoxFor(model => model.Candidato.Skype) %>
                    <%: Html.ValidationMessageFor(model => model.Candidato.Skype) %>
                </div>
        
                <div class="izq" >
                    <%: Html.LabelFor(model => model.Candidato.FechaAlta) %>
                </div>
                <div class="der" >
                    <%: Html.EditorFor(model => model.Candidato.FechaAlta) %>
                </div>
        
                <div class="izq" >
                    <%: Html.LabelFor(model => model.Candidato.FechaNac) %>
                </div>
                <div class="der" >
                    <%: Html.EditorFor(model => model.Candidato.FechaNac) %>
                    <%: Html.ValidationMessageFor(model => model.Candidato.FechaNac) %>
                </div>
        
                <div class="izq" >
                    <%: Html.LabelFor(model => model.Candidato.FechaLlegadaEspana) %>
                </div>
                <div class="der" >
                    <%: Html.EditorFor(model => model.Candidato.FechaLlegadaEspana) %>
                    <%: Html.ValidationMessageFor(model => model.Candidato.FechaLlegadaEspana) %>
                </div>
        
                <div class="izq" >
                    <%: Html.LabelFor(model => model.Candidato.FechaDisponibilidad) %>
                </div>
                <div class="der" >
                    <%: Html.EditorFor(model => model.Candidato.FechaDisponibilidad) %>
                </div>
        
        
        
                <div class="izq" >
                    <%: Html.LabelFor(model => model.Candidato.Sexo) %>
                </div>
                <div class="der" >
                    <%: Html.DropDownListFor(model => model.Candidato.Sexo, new SelectList(
                            new[]
                            {
                                new {Value = "true", Text = "Masculino"},
                                new {Value = "false", Text = "Femenino"}
                            },
                            "Value",
                            "Text",
                            Model
                            )
                            ) %>
                </div>
        
                <div class="izq">
                    <%: Html.LabelFor(model => model.Candidato.Procedencia) %>
                </div>
                <div class="der">
                    <%: @Html.DropDownListFor(model => model.ProcedenciaActual,
                            new SelectList(Model.Procedencias,
                                "Id", "NombreProcedencia", Model.ProcedenciaActual)) %>
                    <%--<%: Html.ValidationMessageFor(model => model.Candidato.Procedencia.NombreProcedencia) %>--%>
                </div>
        
                <div class="izq">
                    <%: Html.LabelFor(model => model.Candidato.Nacionalidad) %>
                </div>
                <div class="der">
                    <%: @Html.DropDownListFor(model => model.Candidato.Nacionalidad.Id,
                            new SelectList(Model.Nacionalidades,
                                "Id", "NombreNacionalidad", Model.NacionalidadActual)) %>
                    <%--<%: Html.ValidationMessageFor(model => model.Candidato.Nacionalidad.NombreNacionalidad) %>--%>
                </div>
          

            </fieldset>
    
            <%--Subir foto de perfil--%>
            <div style="border-style: solid; clear: right; float: right; height: 180px; margin-right: 9em; width: 180px;"  >
                <%--<%:  Model.Candidato.Codigo %>--%>
    
                <%--<asp:Image runat="server" ImageUrl="<%# Container.DataItem  %>"/>--%>
                <%--<%= Html.i("previewImage", Model.Candidato.fotoPerfilPath, new { width = 100, height = 100 })%>  --%>
                <%--<img src="<%= Html.Image("verImagen",, new {width=100, height=100} %>"/>--%>
                
                <asp:Image ID="Image1"
                           runat="server"
                           AlternateText="FotoPerfil"
                           ImageUrl= "http://placehold.it/180x180"  /> 
                           <%--ImageUrl= "<%# Model.Candidato.fotoPerfilPath %>"  /> --%>
                           
            </div>
            <div style="clear: right; margin-right: 13.5em">
                <%: Html.ActionLink("Subir foto", "SubirFoto", "FotoPerfil", new {id = Model.IdCandidato}, new {@class = "boton_der", disabled = "disabled", style = "float:right;margin-top:7px;"}) %>
            </div>

            <fieldset  style="clear: right; display: inline; float: right; height: 17em; margin-bottom: 7px; width: 46.5%;">  
        
                <legend > Preferencias </legend>
            
                <div class="izq" >
                    <%: Html.LabelFor(model => model.Candidato.TipoAlojamientoPreferido) %>
                </div>
                <div style="display: inline; float: left;" >
                    <%: @Html.DropDownListFor(model => model.SelectedTipoAlojamiento,
                            new SelectList(Model.TiposAlojamientos,
                                "Id", "TipoAlojamientoNombre")) %> 
                </div>  


                <div class="izq" style="display: inline; float: left;" >
                    Comunidad
                </div>
                <div class="izq" style="clear: left; display: none;">
                    <%: Html.LabelFor(model => model.SelectedComunidadAutonoma) %>
                </div>
        
                <div style="display: inline; float: left;" >
               
                    <%: @Html.DropDownListFor(model => model.SelectedComunidadAutonoma, Model.ComunidadesAutonomas) %> 
                </div> 
        
                <div class="izq" style="display: none">
                    <%: Html.LabelFor(model => model.SelectedProvincia) %>
                </div>  
                <div class="izq">Provincia</div>
                <div style="display: inline; float: left;" >
               
                    <%: @Html.DropDownListFor(model => model.SelectedProvincia, Model.Provincias, new {disabled = "disabled"}) %> 
                        
                    <%--<select id="SelectedProvincia" name="SelectedProvincia" style="display: none"></select>--%>
                </div>        
            
                     
                      
                <div class="izq" >
                    <%: Html.LabelFor(model => model.Candidato.EtapaEducativaPreferida) %>
                </div>
                <div style="display: inline; float: left;" >
                    <%: @Html.DropDownListFor(model => model.SelectedEtapaEducativa,
                            new SelectList(Model.EtapasEducativas,
                                "Id", "EtapaEducativaNombre")) %>
                </div>   
                <div > <%: Html.TextAreaFor(model => model.Candidato.ComentariosProcedencia) %> </div>

                <%--     <div style="float:left; display: inline" >
                <%: @Html.TextAreaFor(model => model.Candidato.ComentariosProcedencia, new { @style = "width: 194px; height: 69px;" })%> 
                <%: Html.ValidationMessageFor(model => model.Candidato.ComentariosProcedencia) %>   
            </div>  --%>
            

        

            </fieldset>
    

            <%-- 
    <fieldset style="clear:right "  >
    <legend> Formación </legend>
        <table style="width: 100%; text-align: left; ">
            <tr>
                <td style="padding-right: 5px;">
                    <%: Html.LabelFor(model => model.Candidato.CLIL)%></td>
                <td style="padding-right: 17px;">
                    <%: Html.CheckBoxFor(model => model.Candidato.CLIL)%></td>
                <td style="padding-right: 5px;">
                    <%: Html.LabelFor(model => model.Candidato.TeflCelta)%></td>
                <td style="padding-right: 17px;">
                    <%: Html.CheckBoxFor(model => model.Candidato.TeflCelta)%></td>
                <td style="padding-right: 5px;">
                    <%: Html.LabelFor(model => model.Candidato.Espanol)%></td>
                <td style="padding-right: 17px;">
                    <%: Html.CheckBoxFor(model => model.Candidato.Espanol)%></td>
            </tr>
        </table>
    </fieldset>--%>
   
    
            <div style="clear: left">
                <%-- <fieldset style="clear:left;">--%>
                <%--<fieldset style="clear:left; float:left; width: 46.5%; ">  BORRADO PORQUE YA SE INCLUYE EN EL FIELDSET ANTERIOR
            <legend>CV</legend>
            <%: Html.LabelFor(model => model.Candidato.TeflCelta)%>
            <%: Html.CheckBoxFor(model => model.Candidato.TeflCelta, new { disabled="disabled"})%>
        </fieldset>--%>

                <fieldset style="float: right; margin-bottom: 7px; width: 46.5%; height: 16em" >
                    <legend>Colegio</legend>
                    
                    <div class="izq" style="clear: left" >
                        <%: Html.LabelFor(model => model.Candidato.EstadoFicha) %>
                    </div>
                    <div style="display: inline; float: left;">
                        <%: Html.DropDownListFor(model => model.Candidato.EstadoFicha, new SelectList(
                                new[]
                                {
                                    new {Value = "Sin enviar", Text = "Sin enviar"},
                                    new {Value = "Condiciones enviadas", Text = "Condiciones enviadas"},
                                    new {Value = "Condiciones aceptadas", Text = "Condiciones aceptadas"},
                                    new {Value = "Condiciones rechazadas", Text = "Condiciones rechazadas"},
                                    new {Value = "Entrevista colegio concertada", Text = "Entrevista Colegio concertada"},
                                    new {Value = "Entrevista colegio realizada", Text = "Entrevista colegio realizada"},
                                    new {Value = "Plaza ofrecida (colegio)", Text = "Plaza ofrecida (colegio)"},
                                    new {Value = "Plaza rechazada (colegio)", Text = "Plaza rechazada (colegio)"},
                                    new {Value = "Plaza rechazada (candidato)", Text = "Plaza rechazada (candidato)"},
                                    new {Value = "Plaza aceptada (candidato)", Text = "Plaza Aceptada (candidato)"}
                                },
                                "Value",
                                "Text",
                                Model
                                )
                                ) %>
                    </div>
                    
                    <div style="clear:left">
                        <table style="text-align: left; width: 100%;">
                                    <tr>
                                        <td > <%: Html.LabelFor(model => model.Candidato.EntrevistaColegio) %></td>
                                        <td > <%: Html.EditorFor(model => model.Candidato.EntrevistaColegio) %></td>
                                        <td > Hora</td>
                                        <td > <%:Html.TextBoxFor(model => model.Candidato.HoraEntrevistaNativo, new {style = "text-align: center; width:50px;"}) %></td>
                                    </tr>
                            </table>
                        </div>
                    <div style="clear: left">
                        
                        <% if (Model.Candidato.PlazasDeColegiosAsignadas == null && Model.Candidato.PlazasDeColegiosAsignadas == null || Model.Candidato.PlazasDeColegiosAsignadas.Count == 0)
                           { %>
                           <% if (Model.Candidato.PlazaOfrecida == null ){ %>
                            <%: Html.ActionLink("Ofrecer plaza", "ListarPlazasParaAsignacion", "PlazaColegio", new { idCandidato = Model.Candidato.Id, cubrir = false }, null)%> 
                            <% } %>
                             <% }
                                if (Model.Candidato.PlazasDeColegiosAsignadas == null && Model.Candidato.PlazaOfrecida != null && Model.Candidato.PlazasDeColegiosAsignadas.Count == 0)
                               { %>
                                   <%--<%: Html.ActionLink("Ver plaza ofrecida", "Edit", "PlazaColegio", new {id = Model.Candidato.PlazaOfrecida.Id}, null) %> --%>
                               
                            
                                    <table style="text-align: left; width: 100%;">
                                        <tr>
                                            <td> Plaza Ofrecida: </td>
                                            <td> <%: Html.ActionLink(Model.Candidato.PlazaOfrecida.CodigoPlaza, "Edit", "PlazaColegio", new { id = Model.Candidato.PlazaOfrecida.Id, disabled = "disabled", style = "width:70px;" }, null)%></td>
                                            <td><%: Html.LabelFor(model => model.Candidato.PlazaOfrecida.Colegio.NombreColegio) %></td>
                                            <td><%: Html.TextBoxFor(model => model.Candidato.PlazaOfrecida.Colegio.NombreColegio, new { disabled = "disabled", style="text-align: center" })%></td>
                                        </tr>
                                    </table>
                            <% } %>
                    </div>
                
                    

                    <div >
                        <% if (Model.IdPlaza == Guid.Parse("00000000-0000-0000-0000-000000000000"))
                           { %>
                            <%: Html.ActionLink("Cubrir plaza", "ListarPlazasParaAsignacion", "PlazaColegio", new {idCandidato = Model.Candidato.Id, cubrir = true}, null) %> 
                        <% }
                           else
                           { %>
                            <%--<%: Html.ActionLink("Ver plaza cubierta", "Edit", "PlazaColegio", new {id = Model.IdPlaza}, null) %> --%>
                            
                          
                            
                                <table style="text-align: left; width: 100%;">
                                    <tr>
                                        <td>Plaza Cubierta:</td>
                                        <td> <%: Html.ActionLink(Model.Candidato.PlazasDeColegiosAsignadas.Select(p => p.CodigoPlaza).FirstOrDefault(), "Edit", "PlazaColegio", new { id = Model.IdPlaza, disabled = "disabled", style = "width:70px;" }, null)%></td>
                                        <td><%: Html.LabelFor(model => model.NombreColegioPlazaCubierta) %></td>
                                        <td><%: Html.TextBoxFor(model => model.NombreColegioPlazaCubierta, new { disabled = "disabled", style = "text-align: center" })%></td>
                                    </tr>
                                    
                                </table>
                            
                        <% } %>
                        <div >
                            <div > <%: Html.TextAreaFor(model => model.Candidato.ComentariosColegio) %> </div>
                        </div>
                    </div>
                    
                    <div class="izq" style="display: none">IdColegio</div>
                    <div class="izq" style="display: none"><%: Html.TextBoxFor(model => model.IdColegio, new {disabled = "disabled"}) %></div>
                
                    <% if (Model.Candidato.Colegio != null)
                       { %>
                        <div class="izq">  
                            <%: Html.DisplayFor(item => item.Candidato.Colegio) %>
                        </div>
                        <div class="izq">
                            <%: Html.TextBoxFor(model => model.NombreColegioPlazaCubierta, new {disabled = "disabled"}) %>
                        </div>
                    <% } %>
                </fieldset>

                <fieldset style="clear: left; float: left; height: 16em; margin-bottom: 7px; width: 46.5%;">
                    <legend>Test</legend>
                    <div style="display: inline; float: left; padding-right: 5px;" >
                        Realizado
                    </div>
                    <div style="display: inline; float: left;" >
                        <%: Html.CheckBoxFor(model => model.Candidato.TestRealizado) %>
                    </div>
                    <table style="background-color: #E0E0E0; text-align: left; width: 100%; ">
                        <tr>
                            <td style="overflow: hidden; padding-right: 5px; white-space: nowrap">
                                Ajuste al puesto</td>
                            <td style="padding-right: 20px;">
                                <%: Html.TextBoxFor(model => model.Candidato.AjusteAlPuesto, new {style = "text-align:center;width:20px;"}) %></td>
                            <td style="overflow: hidden; padding-right: 5px; white-space: nowrap">
                                Deseabilidad Social</td>
                            <td >
                                <%: Html.TextBoxFor(model => model.Candidato.DeseabilidadSocial, new {style = "text-align:center;width:20px;"}) %> %</td>
                        </tr>
                    </table>

                    
                    <div style="margin-top: 7px;"> <%: Html.TextAreaFor(model => model.Candidato.ComentariosTest) %> </div>
            
                </fieldset>
        
                <fieldset style="float: right; margin-bottom: 7px; width: 46.5%;">
                    <legend>Documentación Matrícula

            
                        <%: @Html.DropDownListFor(model => model.SelectedUniversidadDeRegistro,
                                new SelectList(Model.UniversidadesDeRegistro,
                                    "Id", "NombreUniversidadDeRegistro")) %> 
            
                
                    </legend>
                    <fieldset>
                        <legend>Aportada</legend>    
                        <legend> Visado 
                            <%: @Html.DropDownListFor(model => model.SelectedEstadoVisado,
                                    new SelectList(Model.EstadosVisado,
                                        "Id", "EstadoVisadoNombre")) %>
                        </legend>
                        
                        
                        
                            
                        <%--<div style="clear:left; padding-top: 5px;">Comentarios</div>--%>
                        <div > <%: Html.TextAreaFor(model => model.Candidato.ComentariosVisado) %> </div>

                        <table style="text-align: left; width: 100%;">
                            <tr>
                                <td>
                                    Pasaporte</td>
                                <td style="padding-right: 17px;">
                                    <%: Html.CheckBoxFor(model => model.Candidato.Pasaporte) %></td>
                                <td colspan="2">
                                    <% if (Model.Pasaporte)
                                       { %>
                                        <%: Html.TextBoxFor(model => model.Candidato.NumeroPasaporte) %>
                                    <% }
                                       else
                                       { %>
                                        <%: Html.TextBoxFor(model => model.Candidato.NumeroPasaporte, new {disabled = "disabled",}) %>
                                    <% } %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Título</td>
                                <td style="padding-right: 17px;">
                                    <%: Html.CheckBoxFor(model => model.Candidato.TituloRecibido) %></td>
                                <td>
                                    Cert. Penales</td>
                                <td style="padding-right: 17px;">
                                    <%: Html.CheckBoxFor(model => model.Candidato.CertificadoPenalesRecibido) %></td>
                            </tr>
 
                        </table>
               
                    
                    

                    </fieldset>

                    <fieldset >
                        <legend>Inscripción</legend>    
                        <%: Html.ActionLink("Gestionar", "Index", "Documentos", new { idCandidato = Model.Candidato.Id, tipo = 1 }, new { @style = "float:right" })%>
                  

                    </fieldset>
                    <%--<table style="width: 100%; text-align: left; ">
                <tr>
                    <td style="padding-right: 5px; overflow: hidden; white-space: nowrap; width: 20px;">
                        Doc. Inscripción</td>
                    <td style="padding-right: 20px; width: 20px;">
                        <%: Html.CheckBoxFor(model => model.DocInscripcion, new { disabled = "disabled", style="padding-left: 5px;" })%></td>
                    <td style="padding-right: 5px; overflow: hidden; white-space: nowrap; width: 20px;">
                        Matrícula</td>
                    <td >
                        <%: Html.CheckBoxFor(model => model.Matricula, new { disabled = "disabled", style="padding-left: 5px;" })%></td>
                </tr>
            </table>--%>
               
                </fieldset>

                <fieldset style="clear: left; float: left; margin-bottom: 7px; width: 46.5%; ">
                    <legend>Entrevista</legend>
                    <table style="background-color: #E0E0E0; margin-bottom: .5em; text-align: left; width: 100%;">
                        <tr>
                            <td style="overflow: hidden; padding-right: 5px; white-space: nowrap"> Entrevista Nativo</td>
                            <td style="padding-right: 20px;"><%: Html.EditorFor(model => model.Candidato.FechaEntrevistaNativo, new {style = "text-align: center"}) %></td>
                            <td style="overflow: hidden; padding-right: 5px; white-space: nowrap"> Hora  <%:Html.TextBoxFor(model => model.Candidato.HoraEntrevistaNativo, new {style = "text-align: center; width:50px;"}) %></td>
                            
                            <td style="overflow: hidden; padding-right: 5px; white-space: nowrap"> ¿Realizada? </td>
                            <td > <%: Html.CheckBoxFor(model => model.Candidato.EntrevistaConNativo, new {style = "text-align:center;width:20px;"}) %> </td>
                        </tr>
                        <tr>
                            <td style="overflow: hidden; padding-right: 5px; white-space: nowrap"> Entrevista UP</td>
                            <td style="padding-right: 20px;"><%: Html.EditorFor(model => model.Candidato.FechaEntrevistaUP, new {style = "text-align: center"}) %></td>
                            <td style="overflow: hidden; padding-right: 5px; white-space: nowrap"> Hora  <%:Html.TextBoxFor(model => model.Candidato.HoraEntrevistaUp, new {style = "text-align: center; width:50px;"}) %></td>
                            <td style="overflow: hidden; padding-right: 5px; white-space: nowrap"> ¿Realizada? </td>
                            <td > <%: Html.CheckBoxFor(model => model.Candidato.EntrevistaConUp, new {style = "text-align:center;width:20px;"}) %> </td>
                        </tr>
                    </table>
           
                    <div style="float: right">
                        <% if (Model.IdInforme == Guid.Parse("00000000-0000-0000-0000-000000000000"))
                           { %>
                            <%: Html.ActionLink("Crear Informe", "Create", "InformeEntrevista", new {id = Model.IdCandidato}, null) %> 
                        <% }
                           else
                           { %>
                            <%: Html.ActionLink("Ver Informe", "Details", "InformeEntrevista", new {id = Model.IdInforme}, null) %> 
                        <% } %>
                    </div>
                    <div style="display: inline; float: left; padding-right: 5px;" >
                        Informe Completado
                    </div>
                    <div style="display: inline; float: left;" >
                        <%: Html.CheckBoxFor(model => model.InformeCompletado, new {disabled = "disabled"}) %>
                    </div>
                    <table style="text-align: left; width: 100%;">
                        <tr>
                            <td style="overflow: hidden; padding-right: 5px; white-space: nowrap">
                                Fecha
                            </td>
                            <td style="padding-right: 20px;">
                                <%: Html.TextBoxFor(model => model.InformeFechaCompletado, new {style = "text-align:center;width:68px;", disabled = "disabled"}) %> 
                            </td>
                            <td style="overflow: hidden; padding-right: 5px; white-space: nowrap">
                                Val. Global
                            </td>
                            <td >
                                <%: Html.TextBoxFor(model => model.InformeValoracionGlobal, new {style = "text-align:center;width:20px;", disabled = "disabled"}) %>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            
            
                <fieldset style="clear: left; float: left; margin: 0; width: 46.5%; ">
                    <legend>Experiencia</legend>
                    <div >
                        <div class="izq" style="display: none">
                            <%: Html.LabelFor(model => model.Candidato.Id) %>
                        </div>
                        <div class="der" style="display: none">
                            <%: Html.TextBoxFor(model => model.Candidato.Id) %>
                        </div>
                        <div class="izq" style="clear: left" >
                            <%: Html.LabelFor(model => model.Candidato.CantidadDeExperiencia) %>
                        </div>
                        <div style="display: inline; float: left;">
                            <%: Html.DropDownListFor(model => model.Candidato.CantidadDeExperiencia, new SelectList(
                                    new[]
                                    {
                                        new {Value = "*No indicada*", Text = "*No indicada*"},
                                        new {Value = "Mucha", Text = "Mucha"},
                                        new {Value = "Poca", Text = "Poca"},
                                        new {Value = "Nada", Text = "Nada"}  
                                    },
                                    "Value",
                                    "Text",
                                    1
                                    )
                             )%> 
                        </div>
                        <table style="text-align: left; width: 100%; clear: left">
                            <tr>
                               <td style="padding-right: 5px;">
                                    <%: Html.LabelFor(model => model.Candidato.TC) %></td>
                                <td style="padding-right: 17px;">
                                    <%: Html.CheckBoxFor(model => model.Candidato.TC) %></td>
                                <td style="padding-right: 5px;">
                                    <%: Html.LabelFor(model => model.Candidato.Voluntario) %></td>
                                <td style="padding-right: 17px;">
                                    <%: Html.CheckBoxFor(model => model.Candidato.Voluntario) %></td>
                                <td style="padding-right: 5px;">
                                    <%: Html.LabelFor(model => model.Candidato.Abroad) %></td>
                                <td style="padding-right: 17px;">
                                    <%: Html.CheckBoxFor(model => model.Candidato.Abroad) %></td>
                            </tr>
                        </table>
                        <div style="margin-top: 7px; clear: left"><%: Html.LabelFor(model => model.Candidato.Destacar) %> <%: Html.TextBoxFor(model => model.Candidato.Destacar) %></div>
                         <div class="comentario">
                            <%: Html.TextAreaFor(model => model.Candidato.ComentariosExperiencia) %>
                        </div>

                        <%--<table style="text-align: left; width: 100%;">
                            <tr>
                                <td style="padding-right: 5px;">
                                    <%: Html.LabelFor(model => model.Candidato.Experiencia.Colegio) %></td>
                                <td style="padding-right: 17px;">
                                    <%: Html.CheckBoxFor(model => model.Candidato.Experiencia.Colegio) %></td>
                                <td style="padding-right: 5px;">
                                    <%: Html.LabelFor(model => model.Candidato.Experiencia.Academia) %></td>
                                <td style="padding-right: 17px;">
                                    <%: Html.CheckBoxFor(model => model.Candidato.Experiencia.Academia) %></td>
                                <td style="padding-right: 5px;">
                                    <%: Html.LabelFor(model => model.Candidato.Experiencia.Campamento) %></td>
                                <td style="padding-right: 17px;">
                                    <%: Html.CheckBoxFor(model => model.Candidato.Experiencia.Campamento) %></td>
                                <td style="padding-right: 5px;">
                                    <%: Html.LabelFor(model => model.Candidato.Experiencia.Adultos) %></td>
                                <td >
                                    <%: Html.CheckBoxFor(model => model.Candidato.Experiencia.Adultos) %></td>
                            </tr>
                            <tr>
                                <td style="padding-right: 7px;">
                                    <%: Html.LabelFor(model => model.Candidato.Experiencia.Ninos) %></td>
                                <td style="padding-right: 17px;">
                                    <%: Html.CheckBoxFor(model => model.Candidato.Experiencia.Ninos) %></td>
                                <td style="padding-right: 7px;">
                                    <%: Html.LabelFor(model => model.Candidato.Experiencia.Voluntariado) %></td>
                                <td style="padding-right: 17px;">
                                    <%: Html.CheckBoxFor(model => model.Candidato.Experiencia.Voluntariado) %></td>
                                <td style="padding-right: 5px;">
                                    <%: Html.LabelFor(model => model.Candidato.Experiencia.Up) %></td>
                                <td style="padding-right: 17px;">
                                    <%: Html.CheckBoxFor(model => model.Candidato.Experiencia.Up) %></td>
                                <td style="padding-right: 5px;">
                                    <%: Html.LabelFor(model => model.Candidato.Experiencia.Paises) %></td>
                                <td style="padding-right: 5px;">
                                    <%: Html.CheckBoxFor(model => model.Candidato.Experiencia.Paises) %></td>
                   
                            </tr>
                        </table>--%>
                    </div>  
                    
                </fieldset>
                
            </div>
    
    
            <fieldset style="clear: both">
                <legend>
                    <%: Html.LabelFor(model => model.Candidato.ComentariosGenerales) %>
                </legend>
                <div class="editor-label" style="display: none">
                    <%: Html.LabelFor(model => model.Candidato.ComentariosGenerales) %>
                </div>
                <div class="comentario">
                    <%: Html.TextAreaFor(model => model.Candidato.ComentariosGenerales) %>
                </div>
            </fieldset>
    
    
            <fieldset  style="clear: both">
                <legend>Trámites</legend>
                <div style="text-align: right">
                    <%: Html.ActionLink("Gestionar", "Index", "Documentos", new {idCandidato = Model.IdCandidato, tipo = 2}, null) %>
                    
                </div>
                <table style="text-align: left; width: 100%;">
                </table>
               
            </fieldset>

            <br/>
            <hr class="fancy-line"/>
    
            <br/>
            <div style="text-align: center">
                <%: Html.LabelFor(model => model.Candidato.fichaCerrada) %>
                <%: Html.CheckBoxFor(model => model.Candidato.fichaCerrada) %>
            </div>
        
            <% if (Model.Candidato.fichaCerrada == false)
               { %>
        <fieldset id="fieldCerrado" >
            <% }
               else
               { %>
            
                <% } %>

                <legend>Cerrar Ficha</legend>
       
                <br/>
                <div style="display: inline; float: left; padding-right: 5px;" >
                    <%: Html.LabelFor(model => model.Candidato.MotivoCierreFicha) %>
                </div>

                <div style="display: inline; float: left;" >
                    <%: @Html.DropDownListFor(model => model.SelectedMotivoCierreFicha,
                            new SelectList(Model.MotivosCierreFicha,
                                "Id", "Motivo"), new {disabled = "disabled"}) %> 
                </div>

                <div style="display: inline; float: left; padding-left: 5px; padding-right: 5px;" >
                    <%: Html.LabelFor(model => model.Candidato.anyoContacto) %>
                </div>
                <div style="display: inline; float: left; padding-right: 5px;" >
                    <%: Html.EditorFor(model => model.Candidato.anyoContacto) %>
                    <%: Html.ValidationMessageFor(model => model.Candidato.anyoContacto) %>
                </div>  
                <br/>
        
                <div class="editor-label">
                    <%: Html.LabelFor(model => model.Candidato.ComentariosMotivoCierre) %>
                </div>
                <div class="comentario">
                    <%: Html.TextAreaFor(model => model.Candidato.ComentariosMotivoCierre) %>
                </div>

            </fieldset>
    
   
                <p>
                    <%--<%: Html.ActionLink("Volver", "Index", "Candidato", null, new {@class = "boton_der", style = "float:left;"}) %>--%>
                    <input type="submit" class="boton" value="Guardar Cambios" id="BotonGuardar" style="float: right; " disabled="disabled"/>
                </p>
                
<% } %>         
</form>         
      
    <div id="actualizado" class="actualizado"  >Guardando</div> 
 
<script type="text/javascript">
        $(document).ready(function() {
            $('#SelectedComunidadAutonoma').change(function() {
                $.ajaxSetup({ cache: false });
                var selectedItem = $(this).val();
                if (selectedItem == "" || selectedItem == 0) {
                    //Do nothing or hide...?    
                } else {
                    $.ajax({
                        url: '<%= @Url.Action("GetProvinciasByComunidad", "Colegio") %>',
                        data: { id: selectedItem },
                        dataType: "json",
                        type: "POST",
                        error: function(xhr, status, err) {
                            var error = jQuery.parseJSON(xhr.responseText).Message;
                            alert(error);
                        },
                        success: function(data) {
                            var items = "";
                            $.each(data, function(i, item) {
                                items += "<option value=\"" +
                                    item.Id + "\">" +
                                    item.Name + "</option>";
                            });

                            $("#SelectedProvincia").html(items);
                            $("#SelectedProvincia").prop('disabled', false);
                            $("#SelectedProvincia2").hide();
                            $("#SelectedProvincia").show();
                        }
                    });
                }
            });
        });
</script>
    
    

<script type="text/javascript">
        var ori = '';
        $(document).ready(function() {
            $('#BotonGuardar').prop("disabled", true);
            $('#Candidato_Nombre, ' +
                '#Candidato_Apellido1,' +
                '#Candidato_Apellido2, ' +
                '#Candidato_Email, ' +
                '#Candidato_TelefonoDeContacto, ' +
                '#Candidato_TelefonoEspanol, ' +
                '#Candidato_FechaNac, ' +
                '#Candidato_Nacionalidad_Id, ' +
                '#Candidato_Skype, ' +
                '#Candidato_Procedencia_Id, ' +
                '#Candidato_Sexo, ' +
                '#Candidato_FechaAlta,' +
                '#Candidato_FechaDisponibilidad,' +
                '#SelectedTipoAlojamiento,' +
                '#SelectedComunidadAutonoma,' +
                '#SelectedProvincia,' +
                '#SelectedEtapaEducativa,' +
                '#Candidato_ComentariosProcedencia,' +
                '#Candidato_TeflCelta,' +
                '#Candidato_TestRealizado,' +
                '#Candidato_AjusteAlPuesto,' +
                '#Candidato_DeseabilidadSocial,' +
                '#Candidato_ComentariosTest,' +
                '#Candidato_ComentariosGenerales,' +
                '#Candidato_CLIL,' +
                '#Candidato_TeflCelta,' +
                '#Candidato_Espanol,' +
                '#SelectedEstadoVisado,' +
                '#Candidato_ComentariosVisado,' +
                '#Candidato_Carrera,' +
                '#SelectedMotivoCierreFicha,' +
                '#Candidato_fichaCerrada, ' +
                '#Candidato_ComentariosMotivoCierre,' +
                '#ProcedenciaActual,' +
                '#Candidato_FechaLlegadaEspana,' +
                '#Candidato_FechaEntrevistaNativo,' +
                '#Candidato_FechaEntrevistaUP,' +
                '#Candidato_EntrevistaConNativo,' +
                '#Candidato_EntrevistaConUp,' +
                '#SelectedUniversidadDeRegistro,' +
                '#Candidato_Pasaporte,' +
                '#Candidato_TituloRecibido,' +
                '#Candidato_CertificadoPenalesRecibido,' +
                '#Candidato_NumeroPasaporte,' +
                '#Candidato_CondicionesColegioFirmadas,' +
                '#Candidato_EntrevistaColegio,' +
                '#Candidato_CantidadDeExperiencia,' +
                '#Candidato_TC,' +
                '#Candidato_Voluntario,' +
                '#Candidato_Abroad,' +
                '#Candidato_Destacar,' +
                '#Candidato_ComentariosExperiencia,'+
                '#Candidato_ComentariosColegio,'+
                '#Candidato_HoraEntrevistaNativo' 
            ).change(function() {
                $('#BotonGuardar').prop("disabled", false);
            });
            $('#BotonGuardar').click(function() {
                ori = $('input').val();
                $(this).hide();
                $(this).prop("disabled", true);
                $('#actualizado').show('fast');
                $("#actualizado").fadeOut(4000);
            });
        });
</script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#SelectedMotivoCierreFicha').prop("disabled", true);
            $('#Candidato_ComentariosMotivoCierre').prop("disabled", true);
            $('#Candidato_anyoContacto').prop("disabled", true);
            $('#Candidato_fichaCerrada'
            ).change(function() {
                $('#SelectedMotivoCierreFicha').prop("disabled", false);
                $('#Candidato_ComentariosMotivoCierre').prop("disabled", false);
                $('#Candidato_anyoContacto').prop("disabled", false);
            });
        });
    </script>
    
    
    <script type="text/javascript">
        $(document).ready(function() {
            $('#Candidato_NumeroPasaporte').prop("disabled", true);
            $('#Candidato_Pasaporte'
            ).change(function() {
                $('#Candidato_NumeroPasaporte').prop("disabled", false);

            });
        });
    </script>
    
    
    <script type="text/javascript">
        $(document).ready(function() {
            $('#fieldCerrado').prop("hidden", true);
            $('#Candidato_fichaCerrada'
            ).change(function() {
                $('#fieldCerrado').prop("hidden", false);
            });
        });
    </script>

</asp:Content>