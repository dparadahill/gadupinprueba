﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.Procedencia>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Nuevo canal de procedencia
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Nuevo canal de procedencia</h2>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <fieldset>
            
            <div class="izq" style="display: none">
                <%: Html.LabelFor(model => model.Id) %>
            </div>
            <div class="der" style="display: none">
                <%: Html.TextBoxFor(model => model.Id) %>
                <%: Html.ValidationMessageFor(model => model.Id) %>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.NombreProcedencia) %>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.NombreProcedencia) %>
                <%: Html.ValidationMessageFor(model => model.NombreProcedencia) %>
            </div>
            
        </fieldset>

        <input type="submit" class="boton_der" value="Guardar" />
    <% } %>

    <div>
        <%--<%: Html.ActionLink("Volver", "Index", "Procedencia", null, new { @class = "boton_izq" })%>--%>
    </div>

</asp:Content>

