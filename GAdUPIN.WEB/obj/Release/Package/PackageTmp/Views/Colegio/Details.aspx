﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.WEB.ViewModels.ColegioDetailsVM>" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Detalles del Colegio
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% if(Model.Colegio.Desactivado ){ %>
            <h3 style="color:red; background:antiquewhite; text-align: center"> Este colegio se encuentra desactivado </h3>
        <% } %> 
        <fieldset style="float: right; " >
        <%: Html.ActionLink("Gestionar Contactos", "Index", "ContactoColegio", new { id = Model.Colegio.Id }, new { @class = "boton" })%>
        <% if(Model.Colegio.Desactivado == false){ %>
            <%: Html.ActionLink("Desactivar colegio", "ConfirmarDesactivacion", "Colegio", new { id = Model.Colegio.Id }, new { @class = "botonGris" })%>
            <% } %>   
        <% else{ %>
            <%: Html.ActionLink("Activar colegio", "ConfirmarActivacion", "Colegio", new { id = Model.Colegio.Id }, new { @class = "boton" })%>
            <% } %>
        </fieldset>  

<div style="float:left"><h2>Detalles del colegio <%: Model.Colegio.NombreColegio %></h2></div>
<form id="Form1" runat="server" method="post"> 


<%--<% using (Html.BeginForm("Details", "Colegio", FormMethod.Post)){ %>--%>
    <%--<%: Html.ValidationSummary(true) %>--%>
    <fieldset style=" clear: right">
        <legend>Datos Generales</legend>
                   
        <div class="izq" style="display: none">
            <%: Html.HiddenFor(model => model.Colegio.Id) %>
            <%: Html.LabelFor(model => model.Colegio.Id)%>
        </div>
        <div class="display-field" style="display: none">
            <%: Html.DisplayFor(model => model.Colegio.Id)%>
        </div>
        
        <div id="Div1" class="actualizado" >Guardado </div>
        
        

        <div class="izq" >
            <%: Html.LabelFor(model => model.Colegio.NombreColegio)%>
        </div>
        <div style="float: left; display: inline" >
            <%: Html.TextBoxFor(model => model.Colegio.NombreColegio)%>
        </div>
        
        <div class="izq" >
            <%: Html.LabelFor(model => model.Colegio.DireccionColegio)%>
        </div>
        <div style="float: left; display: inline" >
            <%: Html.TextBoxFor(model => model.Colegio.DireccionColegio)%>
        </div>
        
        <div class="izq" style="float:left; display: inline" >
                Comunidad
        </div>
        <div class="izq" style="display: none">
            <%: Html.LabelFor(model => model.SelectedComunidadAutonoma)%>
        </div>

        <div style="float:left; display: inline" >
                <%: @Html.DropDownListFor(model => model.SelectedComunidadAutonoma, Model.ComunidadesAutonomas)%> 
        </div> 
        
        <div class="der" style="display: none">
            <%: Html.LabelFor(model => model.SelectedProvincia)%>
        </div>
        <div class="izq">Provincia</div>
        <div style="float: left; display: inline" >
                <%: @Html.DropDownListFor(model => model.SelectedProvincia, Model.Provincias, new {disabled="disabled"})%> 
                        
                <%--<select id="SelectedProvincia" name="SelectedProvincia" style="display: none"></select>--%>
        </div>      
         
                   
        <div class="izq" style="clear: left"  >
            <%: Html.LabelFor(model => model.Colegio.Localidad)%>
        </div>
        <div style="float: left; display: inline" >
            <%: Html.TextBoxFor(model => model.Colegio.Localidad)%>
        </div>

        <div class="izq" >
            <%: Html.LabelFor(model => model.Colegio.CodigoPostal)%>
        </div>
        <div style="float: left; display: inline" >
            <%: Html.EditorFor(model => model.Colegio.CodigoPostal)%>
        </div>
        
        <div class="izq" >
            <%: Html.LabelFor(model => model.Colegio.Telefono)%>
        </div>
        <div style="float: left; display: inline" >
            <%: Html.TextBoxFor(model => model.Colegio.Telefono)%>
        </div>
        
        <div class="izq" >
            <%: Html.LabelFor(model => model.Colegio.CIF)%>
        </div>
        <div style="float: left; display: inline" >
            <%: Html.TextBoxFor(model => model.Colegio.CIF)%>
        </div>

        <div class="izq">
            <%: Html.LabelFor(model => model.Colegio.ColegioContratado)%>
            <%: Html.CheckBoxFor(model => model.Colegio.ColegioContratado)%>
        </div>
        
        <div style="float: left; display: inline" >
                <%: Html.LabelFor(model => model.Colegio.FechaDeContrato)%>
                <%: Html.EditorFor(model => model.Colegio.FechaDeContrato)%>
                <%: Html.ValidationMessageFor(model => model.Colegio.FechaDeContrato)%>
            </div>
            
         <div class="izq" style="clear: left" >
            <%: Html.LabelFor(model => model.Colegio.Web)%>
         </div>
         <div style="float: left; display: inline" >
            <%: Html.TextBoxFor(model => model.Colegio.Web)%>
         </div>
         <div class="izq" >
            <%: Html.LabelFor(model => model.Colegio.Email)%>
         </div>
         <div style="float: left; display: inline" >
            <%: Html.TextBoxFor(model => model.Colegio.Email)%>
         </div>
    </fieldset>
 
    <fieldset style="clear:both "  >
         <legend>Plazas</legend>
   
    <table style="width: 100%; margin-bottom: 10px; text-align: center; table-layout: fixed ">
      <tr style="text-align: center">
        <th style="text-align: center">
            Total</th>
        <th style="text-align: center">
            Cubiertas   </th>
        <th style="text-align: center">
            Pendientes </th>
      </tr>
      <tr>
        <td >
            <%: Html.DisplayFor(model => model.TotalDePlazas) %>
        </td>
        <td >
            <%: Html.DisplayFor(model => model.PlazasCubiertas) %>
        </td>
        <td >
            <%: Html.DisplayFor(model => model.PlazasDisponibles) %>
        </td>
      </tr>
    </table>
         <%: Html.ActionLink("Gestionar Plazas", "Index", "PlazaColegio", new { id = Model.Colegio.Id }, new { @style = "float: right", @class = "boton" })%>
    </fieldset>

   <%-- <fieldset>
         <legend>Contactos</legend>
         <%: Html.ActionLink("Gestionar Contactos", "../ContactoColegio/Index", "Contacto", new { id = Model.Colegio.Id }, new { @style = "float: right ", @class = "boton" })%>
    </fieldset>--%>
    
    <fieldset style="clear:left">
        <legend>Comentarios</legend>
        <div class="comentario">
            <%: Html.TextAreaFor(model => model.Colegio.Comentarios)%>
        </div>   
    </fieldset>

    <p>
         <%--<%: Html.ActionLink("Volver", "Index", null , new { @class="boton"})%>--%>
    </p>
    
    
         
     <p>
           <input type="submit" class="boton" value="Guardar Cambios" id="BotonGuardarColegio" style="float:right;" disabled="disabled" onclick="detail"/>
           
     </p>  
          
</form>
 <div id="actualizado" class="actualizado">Guardando</div>
        
          
<script type="text/javascript">
              var ori = '';
              $(document).ready(function () {
                  $('#BotonGuardarColegio').prop("disabled", true);
                  $('#Colegio_NombreColegio, ' +
                              '#Colegio_DireccionColegio,' +
                              '#Colegio_CodigoPostal,' +
                              '#Colegio_Telefono, ' +
                              '#SelectedComunidadAutonoma, ' +
                              '#SelectedProvincia, ' +
                              '#SelectedProvincia2, ' +
                              '#Colegio_ColegioContratado,' +
                              '#Colegio_CIF,' +
                              '#Colegio_Comentarios,' +
                              '#Colegio_Localidad,' +
                              '#Colegio_Web,' +
                              '#Colegio_Email'
                            ).change(function () {
                                $('#BotonGuardarColegio').prop("disabled", false);
                            });
                  $('#BotonGuardarColegio').click(function () {
                      ori = $('input').val();
                      $(this).hide();
                      $(this).prop("disabled", true);
                      $('#actualizado').show('fast');
                      $("#actualizado").fadeOut(4000);
                  });
              });
          </script>


<script type="text/javascript">
    $(document).ready(function () {
        $('#SelectedComunidadAutonoma').change(function () {
            $.ajaxSetup({ cache: false });
            var selectedItem = $(this).val();
            if (selectedItem == "" || selectedItem == 0) {
                //Do nothing or hide...?    
            } else {
                $.ajax({
                    url: '<%= @Url.Action("GetProvinciasByComunidad", "Colegio") %>',
                    data: { id: selectedItem },
                    dataType: "json",
                    type: "POST",
                    error: function (xhr, status, err) {
                        var error = jQuery.parseJSON(xhr.responseText).Message;
                        alert(error);
                    },
                    success: function (data) {
                        var items = "";
                        $.each(data, function (i, item) {
                            items += "<option value=\"" +
                                   item.Id + "\">" +
                                   item.Name + "</option>";
                        });

                        $("#SelectedProvincia").html(items);
                        $("#SelectedProvincia").prop('disabled', false);
                        $("#SelectedProvincia2").hide();
                        $("#SelectedProvincia").show();
                    }
                });
            }
        });
    });
   </script>
            

</asp:Content>

