﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<IEnumerable<GAdUPIN.WEB.ViewModels.ColegioListVM>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Listado de colegios
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  

    <h2>Colegios Desactivados</h2>
    
    <table id="Jtable" class="display" style="text-align: left" >
        <thead>
            <tr>
                <th>
                    CIF
                </th>
                <th>
                    Colegio
                </th>
                <th>
                    Teléfono
                </th>
                <th>
                    Comunidad Autonoma
                </th>
                <th>
                    Provincia
                </th>
                <%--<th>Contratado</th>  
                <th>Plazas Disponibles</th> --%> 
                <th></th>
            </tr>
        </thead>
 
        <tbody>
          <tr>
            <% foreach (var item in Model) { %>
                    <td>
                        <%: item.CIF %>
                    </td>
                    <td>
                        <%: item.NombreColegio %>
                    </td>
                    <td>
                        <%: item.Telefono %>
                    </td>
                    <td>
                        <%: item.NombreComunidadAutonoma%>
                    </td>
                    <td>
                        <%: item.NombreProvincia%>
                    </td>
                    <%--<td>
                        <%: item.Contratado ? "Si":"No" %>
                    </td>
                    <td>
                         <%: item.PlazasDisponibles %>
                    </td>--%>
                    <td>
                      <%--  <%: Html.ActionLink("Modificar", "Edit", new { id=item.IdColegio  }) %> |--%>
                        <%: Html.ActionLink("Gestionar", "Details", new {id=item.IdColegio })%> <br/>
                    </td>
            </tr>
            <% } %>
        </tbody>
    
    </table>
   
    <script type="text/javascript">
        $(document).ready(function () {
            $.fn.dataTableExt.sErrMode = 'throw';
            $('#Jtable').dataTable({
                "sDom": 'T<"clear">lfrtip',
                "oTableTools": {
                    "sSwfPath": "../../Content/DataTables-1.9.4/extras/TableTools/media/swf/copy_csv_xls.swf",
                    "aButtons": ["copy", "csv", "print"]
                },
                "oLanguage": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    </script>

     <p style="clear: both;margin-top: 20px; float:right;">
        <%: Html.ActionLink("Nuevo Colegio", "Create",null, new { @float="right", @class = "boton"}) %>
    </p>

</asp:Content>

