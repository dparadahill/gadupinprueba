﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.WEB.ViewModels.ProvinciaNewEditVM>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Editar provincia
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Editar datos de la provincia</h2>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
            <legend>Campos</legend>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Provincia.ComunidadAutonoma) %>
            </div>
            <div class="editor-field">
                <%: @Html.DropDownListFor(model => model.SelectedComunidadAutonoma,
                        new SelectList(Model.ComunidadesAutonomas,
                        "Id", "NombreComunidadAutonoma"))%>
                <%: Html.ValidationMessageFor(model => model.ComunidadesAutonomas) %>
            </div>
            
            <div class="editor-label" style="display: none">
                <%: Html.LabelFor(model => model.IdComunidadAutonoma) %>
            </div>
            <div class="editor-field" style="display: none">
                <%: Html.TextBoxFor(model => model.IdComunidadAutonoma) %>
                <%: Html.ValidationMessageFor(model => model.IdComunidadAutonoma) %>
            </div>
            
            <div class="editor-label"  style="display: none">
                <%: Html.LabelFor(model => model.Provincia.Id) %>
            </div>
            <div class="editor-field"  style="display: none">
                <%: Html.TextBoxFor(model => model.Provincia.Id)%>
                <%: Html.ValidationMessageFor(model => model.Provincia.Id)%>
            </div>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.Provincia.NombreProvincia) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Provincia.NombreProvincia)%>
                <%: Html.ValidationMessageFor(model => model.Provincia.NombreProvincia)%>
            </div>
            
            

            <p>
                <input type="submit" class="boton" value="Guardar" />
            </p>
        </fieldset>

    <% } %>

    <div>
        <%: Html.ActionLink("Volver", "Index", new{id = Model.Provincia.ComunidadAutonoma.Id}, new {@class="boton_der"}) %>
    </div>

</asp:Content>

