﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.WEB.ViewModels.ProvinciaNewEditVM>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Nueva provincia
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Añadir nueva provincia</h2>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <fieldset>
            <legend>Rellenar campos</legend>
            <div class="izq" style="display: none" >
                <%: Html.DisplayFor(model => model.Provincia.ComunidadAutonoma.Id) %>
            </div>
            <div class="izq" style="display: none">
                <%: Html.TextBoxFor(model => model.Provincia.ComunidadAutonoma.Id) %>
            </div>
            <div class="display-label" >
                <%: Html.Label("Comunidad Autonoma: ") %> 
                <%: Html.DisplayFor(model => model.Provincia.ComunidadAutonoma.NombreComunidadAutonoma)%>
                <%: Html.ValidationMessageFor(model => model.Provincia.ComunidadAutonoma.NombreComunidadAutonoma)%>
            </div>
            <div class="izq" style="display: none">
                <%: Html.LabelFor(model => model.Provincia.Id) %>
            </div>
            <div class="der" style="display: none">
                <%: Html.TextBoxFor(model => model.Provincia.Id)%>
                <%: Html.ValidationMessageFor(model => model.Provincia.Id)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.Provincia.NombreProvincia)%>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.Provincia.NombreProvincia)%>
                <%: Html.ValidationMessageFor(model => model.Provincia.NombreProvincia)%>
            </div>

            
            <p>
                <input type="submit" class="boton" value="Guardar" />
            </p>
        </fieldset>

    <% } %>

    <div>
        <%: Html.ActionLink("Volver", "Index", new { id = Model.Provincia.ComunidadAutonoma.Id }, new { @class = "boton_der" })%>    
    </div>

  

</asp:Content>

