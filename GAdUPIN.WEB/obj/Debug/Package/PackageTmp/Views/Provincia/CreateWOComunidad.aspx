﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.WEB.ViewModels.ProvinciaNewEditVM>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Nueva provincia
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Nueva provincia</h2>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <fieldset>
            <legend>Campos</legend>
            
            
            <div class="editor-label">
                <%: Html.Label("Seleccione Comunidad") %>
            </div>
            <div class="editor-field">
                <%: @Html.DropDownListFor(model => model.IdComunidadAutonoma,
                       new SelectList(Model.ComunidadesAutonomas,
                       "Id", "NombreComunidadAutonoma", 0), "*Seleccione la comunidad*")
                %>
            </div>
            
            <div class="editor-label" style="display:none">
                <%: Html.LabelFor(model => model.Provincia.Id) %>
            </div>
            <div class="editor-field" style="display:none">
                <%: Html.TextBoxFor(model => model.Provincia.Id)%>
                <%: Html.ValidationMessageFor(model => model.Provincia.Id)%>
            </div>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.Provincia.NombreProvincia) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Provincia.NombreProvincia)%>
                <%: Html.ValidationMessageFor(model => model.Provincia.NombreProvincia)%>
            </div>
            

            
            <p>
                <input type="submit" class="boton" value="Crear" />
            </p>
        </fieldset>

    <% } %>

    <div>
        <%: Html.ActionLink("Volver", "Index", new { id = Model.IdComunidadAutonoma }, new { @class = "boton" }
       ) %>
    </div>
    
      <script type="text/javascript">

          $('#form0').submit(function () {
              var obj;
              obj = $('#IdComunidadAutonoma');
              if (obj.val() == "") {
                  alert("Debe elegir una comunidad autonoma");
                  obj.focus();
                  return false;
              }
              return true;
          });
       
     </script>

</asp:Content>

