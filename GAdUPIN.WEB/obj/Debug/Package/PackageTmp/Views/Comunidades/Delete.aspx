﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.ComunidadAutonoma>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Eliminar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Eliminar comunidad autonoma</h2>

    <h3>Seguro que desea borrar esta comunidad autonoma?</h3>
    <fieldset>
        <legend>Fields</legend>
        
        <div class="display-label" style="display: none">Id</div>
        <div class="display-field" style="display: none"><%: Model.Id %></div>
        
        <div class="display-label">Nombre</div>
        <div class="display-field"><%: Model.NombreComunidadAutonoma %></div>
        
    </fieldset>
    <% using (Html.BeginForm()) { %>
        <p>
		    <input type="submit" class="boton" value="Confirmar" /> 
		    <%: Html.ActionLink("Volver", "Index", null, new { @class = "boton" })%>
        </p>
    <% } %>

</asp:Content>

