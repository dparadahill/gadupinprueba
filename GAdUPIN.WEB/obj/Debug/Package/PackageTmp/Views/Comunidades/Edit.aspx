﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.ComunidadAutonoma>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Editar Comunidad Autonoma
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Editar Comunidad Autonoma</h2>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
            <legend>Fields</legend>
            
            <div class="editor-label" style="display: none">
                <%: Html.LabelFor(model => model.Id) %>
            </div>
            <div class="editor-field" style="display: none">
                <%: Html.TextBoxFor(model => model.Id) %>
                <%: Html.ValidationMessageFor(model => model.Id) %>
            </div>
            
            <div class="editor-label">
                <%: Html.Label("Comunidad Autonoma") %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.NombreComunidadAutonoma) %>
                <%: Html.ValidationMessageFor(model => model.NombreComunidadAutonoma) %>
            </div>
            
            <p>
                <input type="submit" class="boton" value="Guardar" />
            </p>
        </fieldset>
    <% } %>
    

    <div>
        <%: Html.ActionLink("Volver", "Index", null, new { @class = "boton_der" })%>
    </div>

</asp:Content>

