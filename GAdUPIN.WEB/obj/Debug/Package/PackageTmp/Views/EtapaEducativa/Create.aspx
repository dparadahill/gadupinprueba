﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.EtapaEducativa>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Nueva Etapa Educativa
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Nueva etapa educativa</h2>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <fieldset>
            
            <div class="izq" style="display: none">
                <%: Html.LabelFor(model => model.Id) %>
            </div>
            <div class="der" style="display: none">
                <%: Html.TextBoxFor(model => model.Id) %>
                <%: Html.ValidationMessageFor(model => model.Id) %>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.EtapaEducativaNombre) %>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.EtapaEducativaNombre) %>
                <%: Html.ValidationMessageFor(model => model.EtapaEducativaNombre) %>
            </div>
            
            <p>
                <input type="submit" value="Crear" class="boton"/>
            </p>
        </fieldset>

    <% } %>

    <div>
        <%: Html.ActionLink("Volver", "Index", "EtapaEducativa", null, new { @class = "boton_der" })%>
    </div>

</asp:Content>

