﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.TipoAlojamiento>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Eliminar tipo de alojamiento    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Eliminar</h2>

    <h3>Una vez eliminado, el registro no es puede recuperar.</h3>
    <fieldset>
        
        <div class="izq" style="display: none">Id</div>
        <div class="der" style="display: none"><%: Model.Id %></div>
        
        <div class="izq">Tipo de alojamiento</div>
        <div class="der"><%: Model.TipoAlojamientoNombre %></div>
        
    </fieldset>
    <% using (Html.BeginForm()) { %>
        <p>
		    <input type="submit" value="Eliminar" class="boton" /> 
		    <%: Html.ActionLink("Volver", "Index", null, new { @class="boton"}) %>
        </p>
    <% } %>

</asp:Content>

