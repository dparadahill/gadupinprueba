﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.WEB.ViewModels.CandidatoDetailsVM>" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Detalles del Candidato
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
<fieldset style="float: right; " >
     <legend>Gestionar</legend>
<%: Html.ActionLink("Documentos", "../Documento/Index", "Documento", new { id = Model.IdCandidato }, new { @class = "boton" })%>

    </fieldset>           
<div style="float:left"><h2> Detalles del Candidato </h2>

</div>


<% using (Html.BeginForm("DetailsCandidato", "Candidato", FormMethod.Post)) {%>
       
 
    <fieldset style="float:left; display: inline; width: 50%; margin:0; clear: right">
      <legend> Datos Generales </legend>
            

         <div class="izq" style="display: none">
            <%: Html.LabelFor(model => model.Candidato.Id)%>
        </div>
        <div class="der" style="display: none">
            <%: Html.TextBoxFor(model => model.Candidato.Id)%>
            <%: Html.ValidationMessageFor(model => model.Candidato.Id)%>
        </div>
        
        <div class="izq" >
            <%: Html.LabelFor(model => model.Candidato.Codigo)%>
        </div>
        <div class="der" >
            <%: Html.DisplayFor(model => model.Candidato.Codigo) %>
        </div>
        
        <div class="izq" >
            <%: Html.LabelFor(model => model.Candidato.Nombre)%>
        </div>
        <div class="der" >
            <%: Html.TextBoxFor(model => model.Candidato.Nombre) %>
            <%: Html.ValidationMessageFor(model => model.Candidato.Nombre) %>
        </div>
        
        <div class="izq" >
            <%: Html.LabelFor(model => model.Candidato.Apellido1)%>
        </div>
        <div class="der" >
            <%: Html.TextBoxFor(model => model.Candidato.Apellido1) %>
            <%: Html.ValidationMessageFor(model => model.Candidato.Apellido1) %>
        </div>
        
        <div class="izq" >
            <%: Html.LabelFor(model => model.Candidato.Apellido2)%>
        </div>
        <div class="der" >
            <%: Html.TextBoxFor(model => model.Candidato.Apellido2) %>
            <%: Html.ValidationMessageFor(model => model.Candidato.Apellido2) %>
        </div>
        
        <div class="izq" >
            <%: Html.LabelFor(model => model.Candidato.Email)%>
        </div>
        <div class="der" >
            <%: Html.TextBoxFor(model => model.Candidato.Email) %>
            <%: Html.ValidationMessageFor(model => model.Candidato.Email) %>
        </div>
        
        <div class="izq" >
            <%: Html.LabelFor(model => model.Candidato.TelefonoDeContacto)%>
        </div>
        <div class="der" >
            <%: Html.TextBoxFor(model => model.Candidato.TelefonoDeContacto) %>
            <%: Html.ValidationMessageFor(model => model.Candidato.TelefonoDeContacto) %>
        </div>
        
        <div class="izq" >
            <%: Html.LabelFor(model => model.Candidato.TelefonoEspanol)%>
        </div>
        <div class="der" >
            <%: Html.TextBoxFor(model => model.Candidato.TelefonoEspanol) %>
            <%: Html.ValidationMessageFor(model => model.Candidato.TelefonoEspanol) %>
        </div>
        
        <div class="izq" >
            <%: Html.LabelFor(model => model.Candidato.FechaNac)%>
        </div>
        <div class="der" >
            <%: Html.EditorFor(model => model.Candidato.FechaNac)%>
            <%: Html.ValidationMessageFor(model => model.Candidato.FechaNac)%>
        </div>

        <div class="izq">
                <%: Html.LabelFor(model => model.Candidato.Nacionalidad) %>
            </div>
        <div class="der">
            <%: @Html.DropDownListFor(model => model.Candidato.Nacionalidad.Id,
                    new SelectList(Model.Nacionalidades,
                    "Id", "NombreNacionalidad", Model.NacionalidadActual))%>
            <%--<%: Html.ValidationMessageFor(model => model.Candidato.Nacionalidad.NombreNacionalidad) %>--%>
        </div>
        
  
        <div class="izq" >
            <%: Html.LabelFor(model => model.Candidato.Sexo)%>
        </div>
        <div class="der" >
            <%: Html.DropDownListFor(model => model.Candidato.Sexo, new SelectList(
                    new[]
                    {
                        new {Value = "true", Text = "Masculino"},
                        new {Value = "false", Text = "Femenino"}    
                    },
                    "Value",
                    "Text",
                    Model
                 )
                )%>
            <%--<%: Html.ValidationMessageFor(model => model.Candidato.Sexo )%>--%>
        </div>
        <br/>    
        <br/>   
        <br/>    
        <br/>    
     </fieldset>
    
    <fieldset style="clear:right "  >
        
        <legend > Preferencias </legend>
            
            <div class="izq" >
                <%: Html.LabelFor(model => model.Candidato.TipoAlojamientoPreferido)%>
            </div>
            <div style="float:left; display: inline" >
                <%: @Html.DropDownListFor(model => model.SelectedTipoAlojamiento,
                        new SelectList(Model.TiposAlojamientos,
                            "Id", "TipoAlojamientoNombre"))%> 
                            
            </div>  


        <div class="izq" style="float:left; display: inline" >
                Geográfica
        </div>
        <div class="izq" style="display: none; clear:left">
            <%: Html.LabelFor(model => model.SelectedComunidadAutonoma)%>
        </div>
        <div style="float:left; display: inline" >
               
                <%: @Html.DropDownListFor(model => model.SelectedComunidadAutonoma,
                        new SelectList(Model.ComunidadesAutonomas,
                        "Id", "NombreComunidadAutonoma"))%> 
        </div>  
        
        <div class="izq" style="display: none">
            <%: Html.LabelFor(model => model.SelectedProvincia)%>
        </div>
        <div style="float:left; display: inline; margin-left:8px;" >
               
                <%: @Html.DropDownListFor(model => model.SelectedProvincia,
                        new SelectList(Model.Provincias,
                        "Id", "NombreProvincia", Model.Candidato.ProvinciaPreferida), new {disabled="disabled"})%> 
                        
                <%--<select id="SelectedProvincia" name="SelectedProvincia" style="display: none"></select>--%>
        </div>            
            
                     
                      
             <div class="izq" >
                <%: Html.LabelFor(model => model.Candidato.EtapaEducativaPreferida)%>
            </div>
            <div style="float:left; display: inline" >
                <%: @Html.DropDownListFor(model => model.SelectedEtapaEducativa,
                        new SelectList(Model.EtapasEducativas,
                            "Id", "EtapaEducativaNombre"))%>
            </div>   
            <div class="izq" >
                <%: Html.LabelFor(model => model.Candidato.ComentariosProcedencia)%>
            </div>
            <div style="float:left; display: inline" >
                <%: @Html.TextAreaFor(model => model.Candidato.ComentariosProcedencia)%> 
                            
            </div>  

    </fieldset>
    
    
    <fieldset style="clear:right "  >
    <legend> Datos Adicionales</legend>
        <div class="izq" >
            <%: Html.LabelFor(model => model.Candidato.Skype)%>
        </div>
        <div class="der" >
            <%: Html.TextBoxFor(model => model.Candidato.Skype) %>
            <%: Html.ValidationMessageFor(model => model.Candidato.Skype) %>
        </div>
        
        <div class="izq" >
            <%: Html.LabelFor(model => model.Candidato.FechaAlta)%>
        </div>
        <div class="der" >
            <%: Html.EditorFor(model => model.Candidato.FechaAlta)%>
            <%--<%: Html.ValidationMessageFor(model => model.Candidato.FechaAlta) %>--%>
        </div>
        
        <div class="izq" >
            <%: Html.LabelFor(model => model.Candidato.FechaDisponibilidad)%>
        </div>
        <div class="der" >
            <%: Html.EditorFor(model => model.Candidato.FechaDisponibilidad)%>
            <%--<%: Html.ValidationMessageFor(model => model.Candidato.FechaDisponibilidad) %>--%>
        </div>  
        
        <div class="izq">
            <%: Html.LabelFor(model => model.Candidato.Procedencia)%>
        </div>
        <div class="der">
            <%: @Html.DropDownListFor(model => model.Candidato.Procedencia.Id,
                    new SelectList(Model.Procedencias,
                        "Id", "NombreProcedencia", Model.ProcedenciaActual))%>
            <%--<%: Html.ValidationMessageFor(model => model.Candidato.Procedencia.NombreProcedencia) %>--%>
        </div>
    </fieldset>
    
    
    <div style="clear: left">
   <%-- <fieldset style="clear:left;">--%>
        <fieldset style="clear:left; float:left; width: 45%; ">
            <legend>CV</legend>
            <%: Html.LabelFor(model => model.Candidato.TeflCelta)%>
            <%: Html.CheckBoxFor(model => model.Candidato.TeflCelta, new { disabled="disabled"})%>
        </fieldset>

            <fieldset style="float:right; width: 45%">
                <legend>Colegio</legend>
                <div style="float:right">
                <% if (Model.IdPlaza == Guid.Parse("00000000-0000-0000-0000-000000000000")) { %>
                    <%: Html.ActionLink("Asignar plaza", "Index", "Colegio") %> 
                <% }
                else { %>
                    <%: Html.ActionLink("Ver plaza asignada", "Edit", "PlazaColegio", new {id = Model.IdPlaza}, null) %> 
                <% } %>
            </div>
                <div class="izq" style="display: none">IdColegio</div>
                <div class="izq" style="display: none"><%: Html.TextBoxFor(model => model.IdColegio, new {disabled = "disabled"})%></div>
                
                <div class="izq">Colegio</div>
                <div class="izq"><%: Html.TextBoxFor(model => model.NombreColegio, new {disabled = "disabled"})%></div>
                
                <div class="izq">Condiciones del Colegio</div>
                <div class="izq"><%: Html.TextBoxFor(model => model.CondicionesColegio, new {disabled = "disabled"})%></div>
                
                <div class="izq">Entrevista con Colegio</div>
                <div class="izq"><%: Html.TextBoxFor(model => model.EntrevistaColegio, new {disabled = "disabled"})%></div>
            </fieldset>

        <fieldset style="clear:left; float:left; width: 45%; ">
            <legend>Test</legend>
           <div style="float:left; display: inline; padding-right: 5px;" >
                Realizado
           </div>
           <div style="float:left; display: inline" >
                <%: Html.CheckBoxFor(model => model.Candidato.TestRealizado)%>
            </div>
            <table style="width: 100%; text-align: left; background-color: #E0E0E0">
                <tr>
                    <td style="padding-right: 5px; overflow: hidden; white-space: nowrap">
                        Ajuste al puesto</td>
                    <td style="padding-right: 20px;">
                        <%: Html.TextBoxFor(model => model.Candidato.AjusteAlPuesto, new { style = "width:20px; text-align: center" })%></td>
                    <td style="padding-right: 5px; overflow: hidden; white-space: nowrap">
                        Deseabilidad Social</td>
                    <td >
                        <%: Html.TextBoxFor(model => model.Candidato.DeseabilidadSocial, new { style = "width:20px; text-align: center" })%> %</td>
                </tr>
            </table>

            <div style="clear:left; padding-top: 5px;">Comentarios Test</div>
            <div > <%: Html.TextAreaFor(model => model.Candidato.ComentariosTest)%> </div>
            
        </fieldset>
        
            <fieldset style="float:right; width: 45%">
                <legend>Matricula USJ</legend>
                <table style="width: 100%; text-align: left; ">
                <tr>
                    <td style="padding-right: 5px; overflow: hidden; white-space: nowrap; width: 20px;">
                        Doc. Inscripción</td>
                    <td style="padding-right: 20px; width: 20px;">
                        <%: Html.CheckBoxFor(model => model.DocInscripcion, new { disabled = "disabled", style="padding-left: 5px;" })%></td>
                    <td style="padding-right: 5px; overflow: hidden; white-space: nowrap; width: 20px;">
                        Matrícula</td>
                    <td >
                        <%: Html.CheckBoxFor(model => model.Matricula, new { disabled = "disabled", style="padding-left: 5px;" })%></td>
                </tr>
            </table>
               
            </fieldset>

        <fieldset style="clear:left; float:left; width: 45%; ">
            <legend>Entrevista</legend>
            <div style="float:right">
                <% if (Model.IdInforme == Guid.Parse("00000000-0000-0000-0000-000000000000")) { %>
                    <%: Html.ActionLink("Crear Informe", "Create", "InformeEntrevista", new {id = Model.IdCandidato}, null) %> 
                <% }
                else { %>
                    <%: Html.ActionLink("Ver Informe", "Details", "InformeEntrevista", new {id = Model.IdInforme}, null) %> 
                <% } %>
            </div>
            <div style="float:left; display: inline; padding-right: 5px;" >
                Informe Completado
           </div>
           <div style="float:left; display: inline" >
                <%: Html.CheckBoxFor(model => model.InformeCompletado, new {disabled = "disabled"})%>
            </div>
            <table style="width: 100%; text-align: left; ">
                   <tr>
                       <td style="padding-right: 5px; overflow: hidden; white-space: nowrap">
                           Fecha</td>
                       <td style="padding-right: 20px;">
                           <%: Html.TextBoxFor(model => model.InformeFechaCompletado, new { style = "width:68px; text-align: center", disabled = "disabled" })%> </td>
                       <td style="padding-right: 5px; overflow: hidden; white-space: nowrap">
                            Val. Global</td>
                       <td >
                           <%: Html.TextBoxFor(model => model.InformeValoracionGlobal, new { style = "width:20px; text-align: center", disabled = "disabled" })%></td>
                    </tr>
                </table>
        </fieldset>
            
            <fieldset style="float:right; width: 45%">
                <legend>Documentacion</legend>
                <table style="width: 100%; text-align: left; ">
                   <tr>
                       <td>
                           Pasaporte</td>
                       <td style="padding-right: 17px;">
                           <%: Html.CheckBoxFor(model => model.Pasaporte, new { disabled = "disabled", })%></td>
                       <td>
                           Titulo</td>
                       <td style="padding-right: 17px;">
                           <%: Html.CheckBoxFor(model => model.Titulo, new { disabled = "disabled", })%></td>
                    </tr>
                    <tr>
                        <td>
                           Cert. Penales</td>
                       <td style="padding-right: 17px;">
                           <%: Html.CheckBoxFor(model => model.CertificadoPenales, new { disabled = "disabled", })%></td>
                       <td>
                           Carta Admisión UP</td>
                       <td style="padding-right: 17px;">
                           <%: Html.CheckBoxFor(model => model.CartaAdmisionUP, new { disabled = "disabled", })%></td>
                   </tr>
                   <tr>
                       <td style="padding-right: 7px;">
                           Claves Acceso USJ</td>
                       <td style="padding-right: 17px;">
                           <%: Html.CheckBoxFor(model => model.ClavesAccesoUSJ, new { disabled = "disabled", })%></td>
                       <td style="padding-right: 7px;">
                           Anexo Convenio</td>
                       <td style="padding-right: 17px;">
                           <%: Html.CheckBoxFor(model => model.AnexoConvenio, new { disabled = "disabled", })%></td>
                   </tr>
                   <tr>
                       <td>
                           NIE</td>
                       <td style="padding-right: 17px;">
                           <%: Html.CheckBoxFor(model => model.NIE, new { disabled = "disabled", })%></td>
                       <td>
                           Empadronamiento</td>
                       <td style="padding-right: 17px;">
                           <%: Html.CheckBoxFor(model => model.Empadronamiento, new { disabled = "disabled", })%></td>
                   </tr>
                   <tr>
                       <td style="padding-right: 7px;">
                           Alta Seg. Social</td>
                       <td style="padding-right: 17px;">
                           <%: Html.CheckBoxFor(model => model.AltaSegSocial, new { disabled = "disabled", })%></td>
                       <td style="padding-right: 7px;">
                           Alta Fiscal (Mod.30)</td>
                       <td style="padding-right: 17px;">
                           <%: Html.CheckBoxFor(model => model.AltaFiscal_Modelo30, new { disabled = "disabled", })%></td>
                   </tr>
               </table>
               
            </fieldset>

        <fieldset style="clear:left; float:left; width: 45%; ">
        <div >
            <%: Html.LabelFor(model => model.Candidato.Experiencia)%>
            <div class="izq" style="display: none">
                <%: Html.LabelFor(model => model.Candidato.Experiencia.Id)%>
            </div>
        <div class="der" style="display: none">
            <%: Html.TextBoxFor(model => model.Candidato.Experiencia.Id)%>
            <%--<%: Html.ValidationMessageFor(model => model.Candidato.Experiencia.Id)%>--%>
        </div>
               <table style="width: 100%; text-align: left; ">
                   <tr>
                       <td style="padding-right: 5px;">
                           <%: Html.LabelFor(model => model.Candidato.Experiencia.Colegio)%></td>
                       <td style="padding-right: 17px;">
                           <%: Html.CheckBoxFor(model => model.Candidato.Experiencia.Colegio)%></td>
                       <td style="padding-right: 5px;">
                           <%: Html.LabelFor(model => model.Candidato.Experiencia.Academia)%></td>
                       <td style="padding-right: 17px;">
                           <%: Html.CheckBoxFor(model => model.Candidato.Experiencia.Academia)%></td>
                       <td style="padding-right: 5px;">
                           <%: Html.LabelFor(model => model.Candidato.Experiencia.Campamento)%></td>
                       <td style="padding-right: 17px;">
                           <%: Html.CheckBoxFor(model => model.Candidato.Experiencia.Campamento)%></td>
                       <td style="padding-right: 5px;">
                           <%: Html.LabelFor(model => model.Candidato.Experiencia.Adultos)%></td>
                       <td >
                           <%: Html.CheckBoxFor(model => model.Candidato.Experiencia.Adultos)%></td>
                    </tr>
                    <tr>
                       <td style="padding-right: 7px;">
                           <%: Html.LabelFor(model => model.Candidato.Experiencia.Ninos)%></td>
                       <td style="padding-right: 17px;">
                           <%: Html.CheckBoxFor(model => model.Candidato.Experiencia.Ninos)%></td>
                       <td style="padding-right: 7px;">
                           <%: Html.LabelFor(model => model.Candidato.Experiencia.Voluntariado)%></td>
                       <td style="padding-right: 17px;">
                           <%: Html.CheckBoxFor(model => model.Candidato.Experiencia.Voluntariado)%></td>
                       <td style="padding-right: 5px;">
                           <%: Html.LabelFor(model => model.Candidato.Experiencia.Up)%></td>
                       <td style="padding-right: 17px;">
                           <%: Html.CheckBoxFor(model => model.Candidato.Experiencia.Up)%></td>
                       <td style="padding-right: 5px;">
                           &nbsp;</td>
                       <td >
                           &nbsp;</td>
                   
                   </tr>
               </table>
           </div>  
          
        </fieldset>
 <%--   </fieldset>--%>
    </div>


    <fieldset style="clear:left">
        <div class="editor-label">
            <%: Html.LabelFor(model => model.Candidato.ComentariosGenerales)%>
        </div>
        <div class="comentario">
            <%: Html.TextAreaFor(model => model.Candidato.ComentariosGenerales)%>
        </div>
    </fieldset>
    
   
   <p>
       <input type="submit" class="boton" value="Guardar Cambios" id="BotonGuardarCandidato" style="clear:left; margin-top:13px;" disabled="disabled"/>
   </p>
   <% } %>
   </form>
   <div id="actualizado" class="actualizado" >Guardado</div> 
 
   <%: Html.ActionLink("Volver", "Index", "Candidato", null, new { @class = "boton_der", style="margin-top:7px;" })%>

   <script type="text/javascript">
       $(document).ready(function () {
           $('#SelectedComunidadAutonoma').change(function () {
               $.ajaxSetup({ cache: false });
               var selectedItem = $(this).val();
               if (selectedItem == "" || selectedItem == 0) {
                   //Do nothing or hide...?    
               } else {
                   $.ajax({
                       url: '<%= @Url.Action("GetProvinciasByComunidad", "Colegio") %>',
                       data: { id: selectedItem },
                       dataType: "json",
                       type: "POST",
                       error: function (xhr, status, err) {
                           var error = jQuery.parseJSON(xhr.responseText).Message;
                           alert(error);
                       },
                       success: function (data) {
                           var items = "";
                           $.each(data, function (i, item) {
                               items += "<option value=\"" +
                                                      item.Id + "\">" +
                                                      item.Name + "</option>";
                           });

                           $("#SelectedProvincia").html(items);
                           $("#SelectedProvincia").prop('disabled', false);
                           $("#SelectedProvincia2").hide();
                           $("#SelectedProvincia").show();
                       }
                   });
               }
           });
       });
                      </script>

    <script type="text/javascript">
        var ori = '';
        $(document).ready(function () {
            $('#BotonGuardarCandidato').prop("disabled", true);
            $('#Candidato_Nombre, ' +
                              '#Candidato_Apellido1,' +
                              '#Candidato_Apellido2, ' +
                              '#Candidato_Email, ' +
                              '#Candidato_TelefonoDeContacto, ' +
                              '#Candidato_TelefonoEspanol, ' +
                              '#Candidato_FechaNac, ' +
                              '#Candidato_Nacionalidad_Id, ' +
                              '#Candidato_Skype, ' +
                              '#Candidato_Procedencia_Id, ' +
                              '#Candidato_Sexo, ' +
                              '#Candidato_FechaAlta,' +
                              '#Candidato_FechaDisponibilidad,' + 
                              '#SelectedTipoAlojamiento,' + 
                              '#SelectedComunidadAutonoma,' + 
                              '#SelectedProvincia,' +
                              '#SelectedEtapaEducativa' 
                            ).change(function () {
                                $('#BotonGuardarCandidato').prop("disabled", false);
                            });
            $('#BotonGuardarCandidato').click(function () {
                ori = $('input').val();
                $(this).prop("disabled", true);
                $('#actualizado').show('slow');
                $("#actualizado").fadeOut(1600);
            });
        });
    </script>
    
    
</asp:Content>

