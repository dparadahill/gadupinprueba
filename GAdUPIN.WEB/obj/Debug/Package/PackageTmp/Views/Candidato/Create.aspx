﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<GAdUPIN.WEB.ViewModels.CandidatoNewEditVM>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Añadir Nuevo Candidato
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Nuevo Candidato</h2>
    <% using (Html.BeginForm())
       {%>
    <%: Html.ValidationSummary(true) %>
    <fieldset>
        <div class="izq" style="display: none">
            <%: Html.LabelFor(model => model.Candidato.Id)%>
        </div>
        <div class="der" style="display: none">
            <%: Html.TextBoxFor(model => model.Candidato.Id) %>
            <%: Html.ValidationMessageFor(model => model.Candidato.Id)%>
        </div>
        <div class="izq">
            <%: Html.LabelFor(model => model.Candidato.Codigo)%>
        </div>
        <div class="der">
            <%: Html.DisplayFor(model => model.Candidato.Codigo)%>
        </div>
        <div class="izq">
            <%: Html.LabelFor(model => model.Candidato.Nombre)%>
        </div>
        <div class="der">
            <%: Html.TextBoxFor(model => model.Candidato.Nombre)%>
            <%: Html.ValidationMessageFor(model => model.Candidato.Nombre)%>
        </div>
        <div class="izq">
            <%: Html.LabelFor(model => model.Candidato.Apellido1)%>
        </div>
        <div class="der">
            <%: Html.TextBoxFor(model => model.Candidato.Apellido1)%>
            <%: Html.ValidationMessageFor(model => model.Candidato.Apellido1)%>
        </div>
        <div class="izq">
            <%: Html.LabelFor(model => model.Candidato.Apellido2)%>
        </div>
        <div class="der">
            <%: Html.TextBoxFor(model => model.Candidato.Apellido2)%>
            <%: Html.ValidationMessageFor(model => model.Candidato.Apellido2)%>
        </div>
        <div class="izq">
            <%: Html.LabelFor(model => model.Candidato.Email)%>
        </div>
        <div class="der">
            <%: Html.TextBoxFor(model => model.Candidato.Email)%>
            <%: Html.ValidationMessageFor(model => model.Candidato.Email)%>
        </div>
        <div class="izq">
            <%: Html.LabelFor(model => model.Candidato.TelefonoDeContacto)%>
        </div>
        <div class="der">
            <%: Html.TextBoxFor(model => model.Candidato.TelefonoDeContacto)%>
            <%: Html.ValidationMessageFor(model => model.Candidato.TelefonoDeContacto)%>
        </div>
        <div class="izq">
            <%: Html.LabelFor(model => model.Candidato.TelefonoEspanol)%>
        </div>
        <div class="der">
            <%: Html.TextBoxFor(model => model.Candidato.TelefonoEspanol)%>
            <%: Html.ValidationMessageFor(model => model.Candidato.TelefonoEspanol)%>
        </div>
        <div class="izq">
            <%: Html.LabelFor(model => model.Candidato.FechaNac)%>
        </div>
        <div class="der">
            <%: Html.EditorFor(model => model.Candidato.FechaNac)%>
            <%: Html.ValidationMessageFor(model => model.Candidato.FechaNac)%>
        </div>
        <div class="izq">
            <%: Html.LabelFor(model => model.Candidato.Skype)%>
        </div>
        <div class="der">
            <%: Html.TextBoxFor(model => model.Candidato.Skype)%>
            <%: Html.ValidationMessageFor(model => model.Candidato.Skype)%>
        </div>
        <div class="izq">
            <%: Html.LabelFor(model => model.Candidato.FechaAlta)%>
        </div>
        <div class="der">
            <%: Html.EditorFor(model => model.Candidato.FechaAlta)%>
            <%: Html.ValidationMessageFor(model => model.Candidato.FechaAlta)%>
        </div>
        <div class="izq">
            <%: Html.LabelFor(model => model.Candidato.FechaDisponibilidad)%>
        </div>
        <div class="der">
            <%: Html.EditorFor(model => model.Candidato.FechaDisponibilidad)%>
            <%: Html.ValidationMessageFor(model => model.Candidato.FechaDisponibilidad)%>
        </div>
        <div class="izq">
            <%: Html.LabelFor(model => model.Candidato.Nacionalidad.NombreNacionalidad)%>
        </div>
        <div class="der">
            <%: @Html.DropDownListFor(model => model.Candidato.Nacionalidad.Id,
                       new SelectList(Model.Nacionalidades,
                        "Id", "NombreNacionalidad", 1), "*Seleccione Nacionalidad*")
            %>
        </div>
        <div class="izq">
            <%: Html.LabelFor(model => model.Candidato.Procedencia.NombreProcedencia)%>
        </div>
        <div class="der">
            <%: @Html.DropDownListFor(model => model.Candidato.Procedencia.Id,
                       new SelectList(Model.Procedencias,
                            "Id", "NombreProcedencia", 1), "*Seleccione Canal*")
            %>
        </div>
        <div class="izq">
            <%: Html.LabelFor(model => model.Candidato.Sexo)%>
        </div>
        <div class="der">
            <%: Html.DropDownListFor(model => model.Candidato.Sexo, new SelectList(
                    new[]
                    {
                        new {Value = "true", Text = "Masculino"},
                        new {Value = "false", Text = "Femenino"},    
                    },
                    "Value",
                    "Text",
                    Model
                 )
                )%>
            <%: Html.ValidationMessageFor(model => model.Candidato.Sexo)%>
        </div>
        <script type="text/javascript">
            $(function () {
                $('#Candidato_FechaAlta,' +
                          '#Candidato_FechaNac' +
                          '#Candidato_FechaDisponibilidad'
                            ).datepicker({
                                onSelect: function (dateText, inst) {
                                    //                                alert(dateText);
                                }
                            });
            });
        </script>
        <p>
            <input type="submit" value="Crear" class="boton" />
        </p>
    </fieldset>
    <% } %>
    <div>
        <%: Html.ActionLink("Volver a la lista", "Index", null, new { @class = "boton_der" })%>
    </div>
    <script type="text/javascript">

        $('#form0').submit(function () {
            var obj;
            obj = $('#Candidato_Nacionalidad_Id');
            if (obj.val() == "") {
                alert("Debe elegir una nacionalidad");
                obj.focus();
                return false;
            }
            return true;
        });
       
    </script>
    <script type="text/javascript">

        $('#form0').submit(function () {
            var obj2;
            obj2 = $('#Candidato_Procedencia_Id');
            if (obj2.val() == "") {
                alert("Debe elegir una procedencia");
                obj2.focus();
                return false;
            }
            return true;
        });
    </script>
</asp:Content>
