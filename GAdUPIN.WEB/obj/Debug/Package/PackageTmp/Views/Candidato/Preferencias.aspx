﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
 Inherits="System.Web.Mvc.ViewPage<IEnumerable<GAdUPIN.WEB.ViewModels.CandidatoPreferenciasListVM>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Preferencias
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<fieldset style="float: right; margin-bottom: 50px;">
     <legend>Gestionar</legend>
<%: Html.ActionLink("Etapas Educativas", "../EtapaEducativa/Index", "Comunidades", new{  @class="boton"})%>
<%: Html.ActionLink("Tipos de alojamiento", "../TipoAlojamiento/Index", "Comunidades", new{  @class="boton"})%>
</fieldset>   
    <h2>Preferencias</h2>

    <table id="Jtable" class="display" >
        <thead>
            <tr>
                <th style="display: none">
                    IdCandidato
                </th>
                <th style="display: none">
                    IdPreferencia
                </th>
                <th>
                    Código
                </th>
                <th>
                    Nombre
                </th>
                <th>
                    Apellidos
                </th>
                <th>
                    Alojamiento
                </th>
                <th>
                    Comunidad Autónoma
                </th>
                <th>
                    Provincia
                </th>
                <th>
                    Etapa Educativa
                </th>
                <th>
                    Disponibilidad
                </th>
                <th></th>
            </tr>
        </thead>

    
        <tbody>
            <tr>
                <% foreach (var item in Model) { %>
                <td style="display: none">
                    <%: item.IdCandidato %>
                </td>
                <td style="display: none">
                    <%: item.IdPreferencia %>
                </td>
                <td>
                    <%: item.CodigoCandidato %>
                </td>
                <td>
                    <%: item.CandidatoNombre  %>
                </td>
                <td>
                    <%: item.CandidatoApellido1 + " " + item.CandidatoApellido2 %>
                </td>
                <td>
                    <%: item.TipoAlojamiento %>
                </td>
                <td>
                    <%: item.ComunidadAutonoma %>
                </td>
                <td>
                     <%: item.Provincia %>
                </td>
                <td>
                     <%: item.EtapaEducativa %>
                </td>
                <td>
                     <%: Html.DisplayFor(m => item.FechaDisponibilidad)%>
                </td>
                <td>
                    <%: Html.ActionLink("Ver ficha", "DetailsCandidato", new { id=item.IdCandidato }) %> 
                </td>
            </tr>
             <% } %>
        </tbody>
    </table>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#Jtable').dataTable({
                "oLanguage": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    </script>
   

    

</asp:Content>

