﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.WEB.ViewModels.InformeEntrevistaNewEdit>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Nuevo Informe
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Informe de entrevista para: <%: Html.Label(Model.Candidato.Nombre+ " " + Model.Candidato.Apellido1 + " " + Model.Candidato.Apellido2)%></h2>
   
    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <%: Html.HiddenFor(model => model.CandidatoSeleccionado) %>


        <fieldset>
            <legend>Datos Generales</legend>
                <div class="editor-label" style="display: none">
                    <%: Html.LabelFor(model => model.Informe.Id) %>
                </div>
                <div class="editor-field" style="display: none">
                    <%: Html.TextBoxFor(model => model.Informe.Id) %>
                    <%: Html.ValidationMessageFor(model => model.Informe.Id) %>
                </div>
                
                <div class="editor-label">
                    <%: Html.LabelFor(model => model.Informe.Fecha) %>
                    <%: Html.EditorFor(model => model.Informe.Fecha)%>
                    <%: Html.ValidationMessageFor(model => model.Informe.Fecha) %>
                </div>
 
             
                <div class="editor-label" style="clear:left">
                    <%: Html.LabelFor(model => model.Informe.Titulacion) %>
                </div>
                <div class="editor-field" style="width:100%">  
                    <%: Html.TextBoxFor(model => model.Informe.Titulacion, new {style="width:100%"}) %>
                    <%: Html.ValidationMessageFor(model => model.Informe.Titulacion) %>
                </div>
                
               
                <div class="editor-label">
                    <%: Html.LabelFor(model => model.Informe.ObsGenerales) %>
                </div>
                <div class="comentario">
                    <%: Html.TextAreaFor(model => model.Informe.ObsGenerales)%>
                    <%: Html.ValidationMessageFor(model => model.Informe.ObsGenerales) %>
                </div>
            
                <div class="editor-label">
                    <%: Html.LabelFor(model => model.Informe.AnalisisEquipoUP) %>
                </div>
                <div class="comentario">
                    <%: Html.TextAreaFor(model => model.Informe.AnalisisEquipoUP) %>
                    <%: Html.ValidationMessageFor(model => model.Informe.AnalisisEquipoUP) %>
                </div>
        </fieldset>  
        
        <fieldset>
            <legend>Valoración General</legend> 
             
                <div class="nombre">
                    <%: Html.LabelFor(model => model.Informe.CumpleRequisitos) %>
                </div>
                <div class="campo">  
                    <%: Html.DropDownListFor(model => model.Informe.CumpleRequisitos, new SelectList(
                        new[]
                        {
                            new {Value = "true", Text = "Si"},
                            new {Value = "false", Text = "No"},    
                        },
                        "Value",
                        "Text",
                        Model
                     )
                    )%>
                    <%: Html.ValidationMessageFor(model => model.Informe.CumpleRequisitos) %>
                </div>
            
                <div class="nombre">
                    <%: Html.LabelFor(model => model.Informe.ValGlobal) %>
                </div>
                <div class="campo">  
                    <%: Html.DropDownListFor(model => model.Informe.ValGlobal, new SelectList(Enumerable.Range(0,10),0))%>
                    <%: Html.ValidationMessageFor(model => model.Informe.Candidato.Sexo)%>
                </div>
            
                <div class="nombre">
                    <%: Html.LabelFor(model => model.Informe.ValRequisitos) %>
                </div>
                <div class="campo">
                    <%: Html.DropDownListFor(model => model.Informe.ValRequisitos, new SelectList(Enumerable.Range(0, 10), 0))%>
                    <%: Html.ValidationMessageFor(model => model.Informe.ValRequisitos) %>
                </div>
            
                <div class="nombre" style="clear:both">
                    <%: Html.LabelFor(model => model.Informe.ValInteres) %>
                </div>
                <div class="campo">
                    <%: Html.DropDownListFor(model => model.Informe.ValInteres, new SelectList(Enumerable.Range(0, 10), 0))%>
                    <%: Html.ValidationMessageFor(model => model.Informe.ValInteres) %>
                </div>
            
                <div class="nombre">
                    <%: Html.LabelFor(model => model.Informe.ValEducacion) %>
                </div>
                <div class="campo">
                    <%: Html.DropDownListFor(model => model.Informe.ValEducacion, new SelectList(Enumerable.Range(0, 10), 0))%>
                    <%: Html.ValidationMessageFor(model => model.Informe.ValEducacion) %>
                </div>
            
                <div class="nombre">
                    <%: Html.LabelFor(model => model.Informe.ValHabProfesionales) %>
                </div>
                <div class="campo">
                    <%: Html.DropDownListFor(model => model.Informe.ValHabProfesionales, new SelectList(Enumerable.Range(0, 10), 0))%>
                    <%: Html.ValidationMessageFor(model => model.Informe.ValHabProfesionales) %>
                </div>
            
                <div class="editor-label" style="clear:both">
                    <%: Html.LabelFor(model => model.Informe.ComentariosInteres) %>
                </div>
                <div class="comentario">
                    <%: Html.TextAreaFor(model => model.Informe.ComentariosInteres)%>
                    <%: Html.ValidationMessageFor(model => model.Informe.ComentariosInteres) %>
                </div>
            </fieldset>
        
        <fieldset>
            <legend>Valoración Lingüística</legend> 
                <div class="nombre">
                    <%: Html.LabelFor(model => model.Informe.ValClaridadDelIdioma) %>
                </div>
                <div class="campo">
                    <%: Html.DropDownListFor(model => model.Informe.ValClaridadDelIdioma, new SelectList(Enumerable.Range(0, 10), 0))%>
                    <%: Html.ValidationMessageFor(model => model.Informe.ValClaridadDelIdioma) %>
                </div>
            
                <div class="nombre">
                    <%: Html.LabelFor(model => model.Informe.ValExpresividadSoltura) %>
                </div>
                <div class="campo">
                    <%: Html.DropDownListFor(model => model.Informe.ValExpresividadSoltura, new SelectList(Enumerable.Range(0, 10), 0))%>
                    <%: Html.ValidationMessageFor(model => model.Informe.ValExpresividadSoltura) %>
                </div>
            
                <div class="nombre">
                    <%: Html.LabelFor(model => model.Informe.ValConfianzaPersonal) %>
                </div>
                <div class="campo">
                    <%: Html.DropDownListFor(model => model.Informe.ValConfianzaPersonal, new SelectList(Enumerable.Range(0, 10), 0))%>
                    <%: Html.ValidationMessageFor(model => model.Informe.ValConfianzaPersonal) %>
                </div>
            
                <div class="editor-label" style="clear: both; padding-top:10px;">
                    <%: Html.LabelFor(model => model.Informe.ComentariosAnalisisLinguistico) %>
                </div>
                <div class="comentario">
                    <%: Html.TextAreaFor(model => model.Informe.ComentariosAnalisisLinguistico)%>
                    <%: Html.ValidationMessageFor(model => model.Informe.ComentariosAnalisisLinguistico) %>
                </div>
        </fieldset>
        
        <fieldset>
            <legend>Preferencias del candidato</legend> 

        </fieldset>

        <fieldset>
            <legend>Información Adicional</legend> 
                <div class="editor-label">
                    <%: Html.LabelFor(model => model.Informe.Idiomas) %>
                </div>
                <div class="comentario">
                    <%: Html.TextAreaFor(model => model.Informe.Idiomas, new{@class="comentarioOpt"})%>
                    <%: Html.ValidationMessageFor(model => model.Informe.Idiomas) %>
                </div>
            
                <div class="editor-label">
                    <%: Html.LabelFor(model => model.Informe.CondicionesDelPrograma) %>
                </div>
                <div class="comentario">
                    <%: Html.TextAreaFor(model => model.Informe.CondicionesDelPrograma, new{@class="comentarioOpt"})%>
                    <%: Html.ValidationMessageFor(model => model.Informe.CondicionesDelPrograma) %>
                </div>
            
                <div class="editor-label">
                    <%: Html.LabelFor(model => model.Informe.Referencias) %>
                </div>
                <div class="comentario">
                    <%: Html.TextAreaFor(model => model.Informe.Referencias, new{@class="comentarioOpt"})%>
                    <%: Html.ValidationMessageFor(model => model.Informe.Referencias) %>
                </div>
            
                <div class="editor-label">
                    <%: Html.LabelFor(model => model.Informe.SituacionEconomica) %>
                </div>
                <div class="comentario">
                    <%: Html.TextAreaFor(model => model.Informe.SituacionEconomica, new{@class="comentarioOpt"})%>
                    <%: Html.ValidationMessageFor(model => model.Informe.SituacionEconomica) %>
                </div>
            
                <div class="editor-label">
                    <%: Html.LabelFor(model => model.Informe.DietaAlimentacion) %>
                </div>
                <div class="comentario">
                    <%: Html.TextAreaFor(model => model.Informe.DietaAlimentacion, new{@class="comentarioOpt"})%>
                    <%: Html.ValidationMessageFor(model => model.Informe.DietaAlimentacion) %>
                </div>
            
                <div class="editor-label">
                    <%: Html.LabelFor(model => model.Informe.Religion) %>
                </div>
                <div class="comentario">
                    <%: Html.TextAreaFor(model => model.Informe.Religion, new{@class="comentarioOpt"})%>
                    <%: Html.ValidationMessageFor(model => model.Informe.Religion) %>
                </div>
       </fieldset>
       
       <fieldset>
            
                <div class="editor-label">
                    <%: Html.LabelFor(model => model.Informe.Otros) %>
                </div>
                <div class="comentario">
                    <%: Html.TextAreaFor(model => model.Informe.Otros)%>
                    <%: Html.ValidationMessageFor(model => model.Informe.Otros) %>
                </div>
        </fieldset>
        
        <fieldset >
            
                <div style="float:right">
                    Informe Completado
                
                
                    <%: Html.CheckBoxFor(model => model.Informe.Completado)%>
                    <%: Html.ValidationMessageFor(model => model.Informe.Completado) %>
                </div>
        </fieldset>
                <p>
                    <input type="submit" value="Guardar" class="boton"/>
                </p>
       

    <% } %>
    
    <div>
        <%: Html.ActionLink("Volver a la lista", "Index", null, new { @class = "boton_der" })%>
    </div>
 

</asp:Content>

