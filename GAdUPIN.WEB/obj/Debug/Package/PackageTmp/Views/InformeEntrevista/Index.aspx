﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<IEnumerable<GAdUPIN.WEB.ViewModels.InformeEntrevistaListVM>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Informes
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Informes</h2>

    <table id="Jtable" class="display" >
        <thead>
            <tr>
                <th>
                    Cod. Candidato
                </th>
                <th>
                    Candidato
                </th>
                <th>
                    Informe completado
                </th>
                <th>
                    Fecha completado
                </th>
                <th>
                    Valoración Global
                </th>
                <th>
                    Informe
                </th>

            </tr>
        </thead>
    
        <tbody>
            <tr>
                <% foreach (var item in Model) { %>
                <td>
                    <%: Html.DisplayFor(m=>item.CodigoCandidato) %>
                </td>
                <td>
                    <%: Html.Label(item.NombreCandidato + " " + item.Apellido1 + " " + item.Apellido2)%>
                </td>
                <td>
                    <%: item.InformeCompletado ? "Si" : "No"%>
                </td>
                <td>
                    <%: Html.DisplayFor(m=>item.FechaCompletado) %>
                </td>
                <td>
                    <%: Html.DisplayFor(m=>item.ValGlobal) %>
                </td>

                <td>
                <% if (item.IdInforme == Guid.Parse("00000000-0000-0000-0000-000000000000")) { %>
                    <%: Html.ActionLink("Crear Informe", "Create", new { id=item.IdCandidato }) %> 
                <% }
                else { %>
                    <%: Html.ActionLink("Ver Informe", "Details", new { id=item.IdInforme }) %> 
                <% } %>
                </td>
            </tr>
        <% } %>
        </tbody>
    </table>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $('#Jtable').dataTable({
                "sDom": 'T<"clear">lfrtip',
                "oTableTools": {
                    "sSwfPath": "../../Content/DataTables-1.9.4/extras/TableTools/media/swf/copy_csv_xls.swf"
                },
                "oLanguage": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    </script>

    <p style="clear: left;margin-top: 30px;">
        <%: Html.ActionLink("Crear", "Create", null, new{@class="boton"}) %>
    </p>

</asp:Content>

