﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.Nacionalidad>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Eliminar nacionalidad
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Eliminar</h2>

    <h3>Una vez eliminado, el registro no es puede recuperar.</h3>
    <fieldset>
        
        <div class="display-label" style="display: none">Id</div>
        <div class="display-field" style="display: none"><%: Model.Id %></div>
        
        <div class="display-label">Nacionalidad</div>
        <div class="display-field"><%: Model.NombreNacionalidad %></div>
        
        <div class="display-label">Visado</div>
        <div class="display-field"><%: Model.Visado %></div>
        
    </fieldset>
    <% using (Html.BeginForm()) { %>
        <p>
		    <input type="submit" value="Confirmar" /> 
        </p>
        
    <div>
        <%: Html.ActionLink("Volver", "Index", new { @class="boton"}) %>
    </div>
    <% } %>

</asp:Content>

