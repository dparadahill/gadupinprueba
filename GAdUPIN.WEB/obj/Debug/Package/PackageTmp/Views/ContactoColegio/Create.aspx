﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.ContactoColegio>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Nuevo Contacto del Colegio
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Nuevo contacto del colegio</h2>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <fieldset>
            <legend>Fields</legend>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Id) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Id) %>
                <%: Html.ValidationMessageFor(model => model.Id) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.NombreContactoColegio) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.NombreContactoColegio) %>
                <%: Html.ValidationMessageFor(model => model.NombreContactoColegio) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Apellido1) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Apellido1) %>
                <%: Html.ValidationMessageFor(model => model.Apellido1) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Apellido2) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Apellido2) %>
                <%: Html.ValidationMessageFor(model => model.Apellido2) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Email) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Email) %>
                <%: Html.ValidationMessageFor(model => model.Email) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.TelefonoDeContacto) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.TelefonoDeContacto) %>
                <%: Html.ValidationMessageFor(model => model.TelefonoDeContacto) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.ContactoPrincipal) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.ContactoPrincipal) %>
                <%: Html.ValidationMessageFor(model => model.ContactoPrincipal) %>
            </div>
            
            <p>
                <input type="submit" value="Crear" />
            </p>
        </fieldset>

    <% } %>

    <div>
        <%: Html.ActionLink("Volver", "Index") %>
    </div>

</asp:Content>

