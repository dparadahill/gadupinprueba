﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<IEnumerable<GAdUPIN.WEB.ViewModels.DocumentacionRequeridaListVM>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Documentación del candidato
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Documentos</h2>

    <table id="Jtable" class="display" >
        <thead>
        <tr>
            <th style="display: none">
                Id
            </th>
            <th>
                Documento
            </th>
            <th>
                Fecha Subida
            </th>
            
            <th></th>
        </tr>
        </thead>


        <tbody>
        <tr>
            <% foreach (var item in Model) { %>
            <td style="display: none">
                <%: item.IdDoc %>
            </td>
            <td>
                <%: item.NombreDocumento %>
            </td>
            <td>
                <%: item.FechaSubida %>
            </td>
            
           
           
            <td>
                <% if (item.NombreDocumento == "Sin subir") { %>
                  <%--  No mostrar nada --%>
                <% } %>
                <% else { %>
                
                <%: Html.ActionLink("Descargar", "Descargar", new {  path =item.Link, id = item.IdCandidato }, new {AjaxOptions = "POST"}) %> |
                <%: Html.ActionLink("Eliminar", "Delete", new {  id=item.IdDoc }) %> 
                <% } %>
            </td>
        </tr>
         <% } %>
        </tbody>
    
   

    </table>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#Jtable').dataTable({
                "oLanguage": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    </script>
    <p style="clear: left; margin-top:30px;">
       <%: Html.ActionLink("Subir nuevo", "../Documento/Upload", new {id = Model.First().IdCandidato} ,new{  @class="boton"})%>
    </p>

</asp:Content>

