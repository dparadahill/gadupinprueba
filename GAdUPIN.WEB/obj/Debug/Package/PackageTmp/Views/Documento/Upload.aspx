﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Subir documento
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Subir documento</h2>
    
    
    <form action="" method="post" enctype="multipart/form-data">
        
        <div>
            <label for="NombreDoc">Nombre Documento</label>
            <input type="text" name="NombreDoc" /> 
        </div>
        <div>
            <label for="file">Archivo:</label>
            <input type="file" name="file" id="file"/>
        </div>
        <input type="submit"/>

    </form>
</asp:Content>
