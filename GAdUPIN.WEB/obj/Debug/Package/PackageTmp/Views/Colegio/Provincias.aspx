﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GAdUPIN.CORE.Domain.Provincia>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Provincias
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% if (Model.Count() != 0)
       {%>
    <h2>Prinvicas de: <%: Model.First().ComunidadAutonoma.NombreComunidadAutonoma%> </h2>
    <% } 
    else
       {%>
    <h2>No existen localidades para dicha comunidad autonoma</h2>
    <%}%>

    <table>
        <tr>
            <th></th>
            <th>
                Id
            </th>
            <th>
                Provincia
            </th>
        </tr>

    <% foreach (var item in Model) { %>
    
        <tr>
            <td>
                <%: Html.ActionLink("Modificar", "Edit", new { id = item.Id })%> |
                <%: Html.ActionLink("Eliminar", "Delete", new { id = item.Id })%>
            </td>
            <td>
                <%: item.Id %>
            </td>
            <td>
                <%: item.NombreProvincia %>
            </td>
        </tr>
    
    <% } %>

    </table>

    <p>
        <%: Html.ActionLink("Añadir", "Create") %>
    </p>

</asp:Content>

