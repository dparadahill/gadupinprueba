﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.WEB.ViewModels.ColegioDetailsVM>" %>
<%@ Import Namespace="GAdUPIN.WEB.ViewModels" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Detalles
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Detalles del Colegio</h2>
    
<% using (Html.BeginForm("UpdateColegio", "Colegio", FormMethod.Post)){ %>
    <%: Html.ValidationSummary(true) %>
    <fieldset>
        <legend>Detalles del colegio</legend>
        
            <%: Html.Partial("_EditColegio", Model) %>
            
            
            
            <input type="submit" class="boton" value="Guardar Cambios" id="BotonGuardarColegio" style="float:right; display: none" disabled="disabled"/>
   
        <script type="text/javascript">
            var ori = '';
            $(document).ready(function () {
                $('#BotonGuardarColegio').prop("disabled", true);
                $('#BotonGuardarColegio').show();
                $('#Colegio_NombreColegio, ' +
                              '#Colegio_DireccionColegio,' +
                              '#Colegio_CodigoPostal,' +
                              '#Colegio_Telefono, ' +
                              '#SelectedComunidadAutonoma, ' +
                              '#SelectedProvincia, ' +
                              '#SelectedProvincia2, ' +
                              '#Colegio_Contratado' +
                              '#Colegio_CIF'
                            ).change(function () {
                                $('#BotonGuardarColegio').prop("disabled", false);
                            });
                $('#BotonGuardarColegio').click(function () {
                    ori = $('input').val();
                    $(this).prop("disabled", true);
                    $('#actualizado').show('slow');
                    $("#actualizado").fadeOut(1600);
                });
            });
          </script>
    </fieldset>
 <% } %>
    
    <fieldset>
         <legend>Plazas</legend>
         <%: Html.ActionLink("Gestionar Plazas", "../PlazaColegio/Index", "Plaza", new { id = Model.Colegio.Id }, new { @style = "float: right ", @class = "boton" })%>

    </fieldset>

    <fieldset>
         <legend>Contactos</legend>
         <%: Html.ActionLink("Gestionar Contactos", "../Contacto/Index", "Contacto", new{ @style="float: right ", @class="boton"})%>

    </fieldset>
    <p>
         <%: Html.ActionLink("Volver", "Index", null , new { @class="boton"})%>
    </p>

</asp:Content>

