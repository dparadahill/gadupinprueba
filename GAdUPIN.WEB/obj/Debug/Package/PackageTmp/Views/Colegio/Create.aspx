﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.WEB.ViewModels.ColegioNewEditVM>" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Nuevo Colegio
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<% Html.EnableClientValidation(); %>
    <h2>Nuevo Colegio</h2>
    
     <% using (Html.BeginForm(null, null, FormMethod.Post, new { id = "form0" })) {%>
        <%: Html.ValidationSummary(true) %>

        <fieldset>
            

            <div class="izq" style="display:none">
                <%: Html.LabelFor(model => model.Colegio.Id) %>
            </div>
            <div class="der" style="display:none">
                <%: Html.TextBoxFor(model => model.Colegio.Id)%>
                <%: Html.ValidationMessageFor(model => model.Colegio.Id)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.Colegio.CIF)%>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.Colegio.CIF)%>
                <%: Html.ValidationMessageFor(model => model.Colegio.CIF)%>
            </div>

            <div class="izq">
                <%: Html.LabelFor(model => model.Colegio.NombreColegio)%>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.Colegio.NombreColegio)%>
                <%: Html.ValidationMessageFor(model => model.Colegio.NombreColegio)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.Colegio.Telefono)%>
            </div>
                <div class="der">
                <%: Html.TextBoxFor(model => model.Colegio.Telefono)%>
                <%: Html.ValidationMessageFor(model => model.Colegio.Telefono)%>
            </div>

            <div class="izq">
                <%: Html.LabelFor(model => model.Colegio.DireccionColegio)%>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.Colegio.DireccionColegio)%>
                <%: Html.ValidationMessageFor(model => model.Colegio.DireccionColegio)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.Colegio.CodigoPostal)%>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.Colegio.CodigoPostal)%>
                <%: Html.ValidationMessageFor(model => model.Colegio.CodigoPostal)%>
            </div>

            <div class="izq" >
               
                <%: @Html.DropDownListFor(model => model.SelectedComunidadAutonoma,
                        new SelectList(Model.ComunidadesAutonomas,
                        "Id", "NombreComunidadAutonoma", 0), "*Seleccione comunidad autónoma*")%> 

            </div>            

            
            <div class="der" >
               <select id="SelectedProvincia" name="SelectedProvincia" style="display: none">
               </select>
               <%: Html.ValidationMessageFor(model => model.Colegio.ComunidadAutonoma)%>
            </div>
            
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $('#SelectedComunidadAutonoma').change(function () {
                                 $.ajaxSetup({ cache: false });
                                 var selectedItem = $(this).val();
                                 if (selectedItem == "" || selectedItem == 0) {
                                     //Do nothing or hide...?    
                                 } else {
                                     $.ajax({
                                         url: '<%= @Url.Action("GetProvinciasByComunidad", "Colegio") %>',
                                         data: { id: selectedItem },
                                         dataType: "json",
                                         type: "POST",
                                         error: function (xhr, status, err) {
                                             var error = jQuery.parseJSON(xhr.responseText).Message;
                                             alert(error);
                                         },
                                         success: function (data) {
                                             var items = "";
                                             items = '<option value="" selected="selected">*Selecione provincia*</option>';
                                             $.each(data, function (i, item) {
                                                 items += "<option value=\"" +
                                                     item.Id + "\">" +
                                                     item.Name + "</option>";
                                             });

                                             $("#SelectedProvincia").html(items);
                                             $("#SelectedProvincia").show();
                                         }
                                     });
                                 }
                             });
                         });
                     </script>

            
            <div class="izq" style="clear:both">
                <%: Html.LabelFor(model => model.Colegio.ColegioContratado)%>
                <%: Html.CheckBoxFor(model => model.Colegio.ColegioContratado)%>
                <%: Html.ValidationMessageFor(model => model.Colegio.ColegioContratado)%>
            </div>
            

            

            <p>
                <input type="submit" class="boton" value="Guardar" style="clear:both; float:right"/>
            </p>
        </fieldset>

    <% } %>

    <div>
        <%: Html.ActionLink("Volver", "Index", null, new { @class = "boton_der" })%>
    </div>

    
    <script type="text/javascript">
        
            $('#form0').submit(function () {
                var obj;
                obj = $('#SelectedComunidadAutonoma');
                if (obj.val() == "") {
                    alert("Debe elegir una comunidad autonoma");
                    obj.focus();
                    return false;
                }
                return true;
            });
       
     </script>
     
     <script type="text/javascript">

         $('#form0').submit(function () {
             var obj;
             obj = $('#SelectedProvincia');
             if (obj.val() == "") {
                 alert("Debe elegir una provincia");
                 obj.focus();
                 return false;
             }
             return true;
         });
       
     </script>
</asp:Content>
