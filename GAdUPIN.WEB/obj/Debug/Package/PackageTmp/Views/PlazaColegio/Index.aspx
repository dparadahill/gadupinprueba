﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<IEnumerable<GAdUPIN.WEB.ViewModels.PlazaColegioListVM>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Plazas del colegio
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm())
   { %>
        <%: Html.ValidationSummary(true) %>
    <% if (Model.Count() != 0)
       { %>
    <h2>Plazas del colegio: <%: Model.First().NombreColegio %> </h2>
    <% }
       else
       { %>
    <h2>No existen plazas disponibles</h2>
    <% } %>
    <table id="Jtable" class="display">
        <thead>
            <tr>
                <th style="display: none">>
                    IdPlaza
                </th >
                <th style="display: none">>
                    IdColegio
                </th>
                <th>
                    Cuantía Beca
                </th>
                <th>
                    Horas
                </th>
                <th>
                    Fecha Inicio
                </th>
                <th>
                    Fecha Fin
                </th>
                <th>
                    Cubierta
                </th>
                <th>
                    Etapa Educativa
                </th>
                <th>
                    Preferencia Sexo
                </th>
                <th>
                    Cubierta
                </th>
                <th></th>
            </tr>
        </thead>

    
        <tbody>
            <tr>
                <% foreach (var item in Model) { %>
                <td style="display: none">
                    <%: item.Id %>
                </td>
                <td style="display: none">
                    <%: item.IdColegio %>
                </td>
                <td>
                    <%: item.CuantiaBeca %>
                </td>
                <td>
                    <%: item.NumeroHoras %>
                </td>
                <td>
                    <%: Html.DisplayFor(p => item.FechaInicio) %>
                </td>
                <td>
                    <%: Html.DisplayFor(p => item.FechaFin) %>
                </td>
                <td>
                    <%: item.PlazaContratada ? "Si" : "No"%>
                </td>
                <td>
                    <%:  Html.DisplayFor(p => item.EtapaEducativa.EtapaEducativaNombre)%>
                </td>
                <td>
                    <%: item.PreferenciaSexo ? "Masculino" : "Femenino"%>
                </td>
                <td>
                <% if (item.CubiertaPor == null) { %>
                    <%: Html.ActionLink("Asignar", "ListarCandidatosParaAsignacion", new {idPlaza = item.Id}) %> 
                <% }
                else { %>
                    <%: Html.ActionLink(item.CubiertaPor.Nombre, "DetailsCandidato", "Candidato" ,new { id=item.CubiertaPor.Id }, null) %> 
                <% } %>
                    
                </td>
                <td>
                    <%: Html.ActionLink("Modificar", "Edit", new { id=item.Id}) %> |
                    <%--<%: Html.ActionLink("Details", "Details", new { id = item.Id })%> |--%>
                    <%: Html.ActionLink("Eliminar", "Delete", new { id = item.Id })%>
                </td>
            </tr>
             <% } %>
        </tbody>
    </table>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#Jtable').dataTable({
                "oLanguage": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    </script>

    <% } %>

    <p style="clear: left;margin-top: 30px;">
        <%if (Model.Count() != 0)
          { %>
        <%: Html.ActionLink("Añadir", "Create", new { colegio = Model.First().IdColegio}, new {@class="boton"}) %>
        <% } %>
        <%else 
          { %>
        <%: Html.ActionLink("Añadir primer plaza", "CreateWOColegio", null, new { @class = "boton" })%>
       
        <% } %>
        <%-- <%: Html.ActionLink("Volver", "Details", "Colegio", new { id =  Model.First().IdColegio }, new { @class = "boton" })%>    --%>
    </p>
    

</asp:Content>

