﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.WEB.ViewModels.PlazaColegioNewEditVM>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Nueva plaza de colegio
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Nueva plaza de colegio</h2>


     <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <fieldset>
            <legend>Rellenar campos</legend>

            
             <div class="izq" >
                <%: Html.Label("Seleccione colegio") %> 
             </div>
             <div class="der">
                <%: @Html.DropDownListFor(model => model.SelectedColegio,
                       new SelectList(Model.Colegios,
                       "Id", "NombreColegio", 0), "*Seleccione colegio*")
                %>
            </div>
            
            <div class="izq" style="display: none">
                <%: Html.LabelFor(model => model.PlazaColegio.Id) %>
            </div>
            <div class="der" style="display: none">
                <%: Html.TextBoxFor(model => model.PlazaColegio.Id)%>
                <%: Html.ValidationMessageFor(model => model.PlazaColegio.Id)%>
            </div>

            <div class="izq">
                <%: Html.LabelFor(model => model.PlazaColegio.CuantiaBeca) %>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.PlazaColegio.CuantiaBeca)%>
                <%: Html.ValidationMessageFor(model => model.PlazaColegio.CuantiaBeca)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.PlazaColegio.FechaInicio) %>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.PlazaColegio.FechaInicio)%>
                <%: Html.ValidationMessageFor(model => model.PlazaColegio.FechaInicio)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.PlazaColegio.FechaFin) %>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.PlazaColegio.FechaFin)%>
                <%: Html.ValidationMessageFor(model => model.PlazaColegio.FechaFin)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.PlazaColegio.PlazaContratada) %>
            </div>
            <div class="der">
                <%: Html.CheckBoxFor(model => model.PlazaColegio.PlazaContratada)%>
                <%: Html.ValidationMessageFor(model => model.PlazaColegio.PlazaContratada)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.PlazaColegio.EtapaEducativa)%>
            </div>
            <div class="der">
                <%: @Html.DropDownListFor(model => model.PlazaColegio.EtapaEducativa.Id,
                       new SelectList(Model.EtapasEducativas,
                        "Id", "EtapaEducativaNombre", 1), "*Seleccione etapa educativa*")
                %>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.PlazaColegio.PreferenciaSexo) %>
            </div>
            <div class="der">  
                <%: Html.DropDownListFor(model => model.PlazaColegio.PreferenciaSexo, new SelectList(
                    new[]
                    {
                        new {Value = "true", Text = "Masculino"},
                        new {Value = "false", Text = "Femenino"},    
                    },
                    "Value",
                    "Text",
                    Model
                 )
                )%>
                <%: Html.ValidationMessageFor(model => model.PlazaColegio.PreferenciaSexo)%>
            </div>

            
<%--            <div class="editor-label">
                <%: Html.LabelFor(model => model.ValidateRequest) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.ValidateRequest) %>
                <%: Html.ValidationMessageFor(model => model.ValidateRequest) %>
            </div>--%>
            
            <p>
                <input type="submit" value="Crear" class="boton" style="float:right" />
            </p>
        </fieldset>

    <% } %>


    <div>
        <%: Html.ActionLink("Volver", "Index", new { id = Model.SelectedColegio }, new { @class = "boton" }) %>
    </div>
    
    <script type="text/javascript">

        $('#form0').submit(function () {
            var obj;
            obj = $('#SelectedColegio');
            if (obj.val() == "") {
                alert("Debe elegir una comunidad autonoma");
                obj.focus();
                return false;
            }
            return true;
        });
       
     </script>
     
     <script type="text/javascript">
         $(function () {
             $('#PlazaColegio_FechaInicio,' +
               '#PlazaColegio_FechaFin'
                ).datepicker({
                    changeYear: true,
                    yearRange: "1960:2050",
                    dateFormat: "yy-mm-dd",
                    onSelect: function (dateText, inst) {
                //                                alert(dateText);
                                }
                            });
         });
                </script>

</asp:Content>

