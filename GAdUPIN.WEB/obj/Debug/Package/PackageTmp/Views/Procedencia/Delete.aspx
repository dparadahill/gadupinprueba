﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.Procedencia>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Eliminar canal de procedencia
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Eliminar</h2>

    <h3>Una vez eliminado, el registro no es puede recuperar.</h3>
    <fieldset>
        
        <div class="display-label" style="display: none">Id</div>
        <div class="display-field" style="display: none"><%: Model.Id %></div>
        
        <div class="display-label">Canal de procedencia</div>
        <div class="display-field"><%: Model.NombreProcedencia %></div>
        
    </fieldset>
    <% using (Html.BeginForm()) { %>
        <p>
		    <input type="submit" class="boton" value="Confirmar" /> 
		    <%: Html.ActionLink("Volver", "Index", new { @class="boton"}) %>
        </p>
    <% } %>

</asp:Content>

