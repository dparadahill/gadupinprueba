﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<IEnumerable<GAdUPIN.CORE.Domain.Procedencia>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Canales de procedencia
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Canales de procedencia</h2>

   <table id="Jtable" class="display" >
        <thead>
            <tr>
                <th>
                    Canal
                </th>
                <th></th>
            </tr>
        </thead>
        
        <tbody>
            <tr>
            <% foreach (var item in Model) { %>
                <td>
                    <%: item.NombreProcedencia %>
                </td>
                <td>
                    <%: Html.ActionLink("Editar", "Edit", new { id=item.Id }) %> |
                    <%: Html.ActionLink("Eliminar", "Delete", new { id=item.Id })%>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#Jtable').dataTable({
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    }
                });
            });
    </script>

    <p style="clear: left; margin-top:30px;">
        <%: Html.ActionLink("Añadir", "Create", null, new { @class = "boton" })%>
        <%: Html.ActionLink("Volver", "Index", "Candidato", null, new { @class = "boton", style="float:right" })%>
    </p>

</asp:Content>

