﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection.Emit;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.Routing;
using GAdUPIN.DAL;
using GAdUPIN.Infrastructure;

namespace GAdUPIN.WEB
{
    // Nota: para obtener instrucciones sobre cómo habilitar el modo clásico de IIS6 o IIS7, 
    // visite http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(
                "Default", // NombreEncargado de ruta
                "{controller}/{action}/{id}", // URL con parámetros
                new { controller = "Account", action = "Logon", id = UrlParameter.Optional } // Valores predeterminados de parámetro
            );

        }

        protected void Application_Start()
        {
            
            AreaRegistration.RegisterAllAreas();

            RegisterRoutes(RouteTable.Routes);
            Bootstrapper.Initialise();
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<DBContext>()); 
            //Database.SetInitializer<DBContext>(null);
        }

        //protected void Application_Start()
        //{ 
        //  Database.SetInititalizer<aa>(New RecreateDatabaseIfModelChanges<aa>());
        
        //}
    }
}