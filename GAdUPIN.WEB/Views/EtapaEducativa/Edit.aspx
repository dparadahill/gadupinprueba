﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.EtapaEducativa>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Editar Etapa Educativa
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Editar etapa educativa</h2>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
            <legend>Modificar etapa educativa</legend>
            
            <div class="editor-label" style="display: none">
                <%: Html.LabelFor(model => model.Id) %>
            </div>
            <div class="editor-field" style="display: none">
                <%: Html.TextBoxFor(model => model.Id) %>
                <%: Html.ValidationMessageFor(model => model.Id) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.EtapaEducativaNombre) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.EtapaEducativaNombre) %>
                <%: Html.ValidationMessageFor(model => model.EtapaEducativaNombre) %>
            </div>
            
            
        </fieldset>
         
        <input type="submit" value="Guardar"  class="boton_der" />
       
    <% } %>

    <div>
        <%--<%: Html.ActionLink("Volver", "Index", "EtapaEducativa", null, new { @class = "boton_izq" })%>--%>
    </div>

</asp:Content>

