﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<GAdUPIN.CORE.Domain.UniversidadDeRegistro>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Universidades de Registro
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Universidades de Registro</h2>

     <table id="Jtable" class="display" style="text-align: left">
        <thead>
            <tr>
                <th style="display: none">
                    Id
                </th>
                <th>
                    Nombre
                </th>
                <th>
                    ¿Visible?
                </th>
                <th></th>
            </tr>
        </thead>

    <% foreach (var item in Model) { %>
        <tbody>
            <tr>
            
                <td style="display: none">
                    <%: item.Id %>
                </td>
                <td>
                    <%: item.NombreUniversidadDeRegistro %>
                </td>
                <td>
                    <%: item.Visible ? "Si" : "No"%>
                </td>
                <td>
                    <%: Html.ActionLink("Editar", "Edit", new { id=item.Id}) %> |
                    <%: Html.ActionLink("Eliminar", "Delete", new { id = item.Id })%>
                </td>
            </tr>
        </tbody>    
    <% } %>

    </table>
    
    
    <script type="text/javascript">
        $(document).ready(function () {
            $.fn.dataTableExt.sErrMode = 'throw';
            $('#Jtable').dataTable({
                "sDom": 'T<"clear">lfrtip',
                "oTableTools": {
                    "sSwfPath": "../../Content/DataTables-1.9.4/extras/TableTools/media/swf/copy_csv_xls.swf",
                    "aButtons": ["copy", "csv", "print"]
                },
                "oLanguage": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    </script>

    <p style="clear: both;margin-top: 20px; float:right;">
        <%: Html.ActionLink("Nueva", "Create", null, new{@class="boton"}) %>
    </p>

</asp:Content>

