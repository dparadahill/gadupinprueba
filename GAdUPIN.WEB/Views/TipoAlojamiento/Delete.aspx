﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.TipoAlojamiento>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Eliminar tipo de alojamiento    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Eliminar</h2>

    <h3>¿Seguro que quiere eliminar el registro?</h3>
    <h4>No es aconsejable borrar un registro si ya es usado por otra tabla, puede causar problemas de integridad</h4>
    <fieldset>
        
        <div class="izq" style="display: none">Id</div>
        <div class="der" style="display: none"><%: Model.Id %></div>
        
        <div class="izq">Tipo de alojamiento</div>
        <div class="der"><%: Model.TipoAlojamientoNombre %></div>
        
    </fieldset>
    <% using (Html.BeginForm()) { %>
        <p>
		    <input type="submit" value="Confirmar" class="boton_der" /> 
		    <%--<%: Html.ActionLink("Volver", "Index", "TipoAlojamiento", null, new { @class = "boton_izq" })%>--%>
        </p>
    <% } %>

</asp:Content>

