﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
 Inherits="System.Web.Mvc.ViewPage<IEnumerable<GAdUPIN.CORE.Domain.Candidato>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Candidatos
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%-- <fieldset style="float: right; margin-bottom: 50px;" >
     <legend>Gestionar</legend>
<%: Html.ActionLink("Nacionalidades", "Index", "Nacionalidad", new{ @class="boton"})%>
<%: Html.ActionLink("Canales de Procedencia", "Index", "Procedencia", new{ @class="boton"})%>
<%: Html.ActionLink("Encargados", "Index", "Encargado", new{ @class="boton"})%>
    </fieldset> --%>  
    
    <div id="demo"></div>
    
    <h2>Candidatos</h2>
     <table id="Jtable" class="display" style="text-align: left">
        <thead>
            <tr>
                
                <th>
                    Código
                </th>
                <th>
                    Nombre
                </th>
                <th>
                    Apellidos
                </th>
                <th>
                    Email
                </th>
                <%--<th>
                    # de contacto ES
                </th>--%>
                <th>
                    Nacionalidad
                </th>
                <%--<th>
                    Skype
                </th>
                <th>
                    Sexo
                </th>--%>
                <th>
                   ¿Cubierto?
                </th>
                <%--<th>
                    Disponibilidad
                </th>--%>
                <th>
                    Estado
                </th>
                <th></th>
            </tr>
        </thead>

   
        <tbody>
            <tr>
                 <% foreach (var item in Model) { %>
                <td>
                    <%: Html.DisplayFor(m=>item.Codigo) %>
                </td>
                <td>
                    <%: Html.DisplayFor(m=>item.Nombre) %>
                </td>
                <td>
                    <%: item.Apellido1  %>
                </td>
                <td>
                    <%: Html.DisplayFor(m=>item.Email) %>
                </td>
                <%--<td>
                    <%: Html.DisplayFor(m=>item.TelefonoDeContacto) %>
                </td>--%>
                <td>
                    <%: Html.DisplayFor(m=>item.Nacionalidad.NombreNacionalidad) %>
                </td>
                <%--<td>
                    <%: Html.DisplayFor(m=>item.Skype) %>
                </td>
                <td>
                    <%: item.Sexo ? "Masculino" : "Femenino"%>
                </td>--%>
                <td>
                <% if (item.PlazasDeColegiosAsignadas != null && item.PlazasDeColegiosAsignadas.Count != 0)
                   { %>
                    Si
                <% }  %>
                <% else{  %>    
                    No
                <% }  %>
                </td>
                <td>
                    <%: Html.DisplayFor(m=>item.EstadoFicha) %>
                </td>
                <td>
                    <%: Html.ActionLink(" Ver ficha", "DetailsCandidato", new { id = item.Id })%> 
                </td>
            </tr>  
            <% } %> 
        </tbody>
    </table>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $.fn.dataTableExt.sErrMode = 'throw';
                $('#Jtable').dataTable({
                    "sDom": 'T<"clear">lfrtip',
                    "oTableTools": {
                        "sSwfPath": "../../Content/DataTables-1.9.4/extras/TableTools/media/swf/copy_csv_xls.swf",
                        "aButtons": ["copy", "csv", "print"]
		                            },
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    }
                });
            });
    </script>


    <p style="clear: both;margin-top: 20px; float:right;">
        <%: Html.ActionLink("Nuevo", "Create", null, new{@class="boton"}) %>
    </p>
   
   

</asp:Content>

