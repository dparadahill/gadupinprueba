﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<IEnumerable<GAdUPIN.WEB.ViewModels.CandidatoPerfilesListVM>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Perfil del candidato
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Perfiles de los candidatos</h2>

     <table id="Jtable" class="display" style="text-align: left">
        <thead>
            <tr>
                <th>
                    Código
                </th>
                <th>
                    Nombre
                </th>
                <th>
                    Edad
                </th>
                <th>
                    Carrera
                </th>
                <th>
                   Experiencia
                </th>
                <th>
                    T/C
                </th>
                <th>
                    Test
                </th>
                <th>
                    Destacar
                </th>
                <th></th>
            </tr>
        </thead>

   
        <tbody>
            <tr>
                 <% foreach (var item in Model) { %>
                <td>
                    <%: Html.DisplayFor(m=>item.Codigo) %>
                </td>
                <td>
                    <%: Html.DisplayFor(m => item.Nombre)%>
                </td>
                <td>
                    <% if (item.Edad == 0) {%>
                        
                   <%} else { %>
                        <%: Html.DisplayFor(m=>item.Edad) %>
                   <% } %>
                    
                </td>
                <td>
                    <%: Html.DisplayFor(m=>item.Carrera) %>
                </td>
                <td>
                    <%: Html.DisplayFor(m => item.CantidadExperiencia)%>
                </td>
                <td>
                    <%: item.TC ? "Si" : "No"%>
                </td>
                <td>
                   <% if (item.Test == "0 0%")
                      {%>
                        
                   <%} else { %>
                        <%: Html.DisplayFor(m => item.Test) %>
                   <% } %>
                </td>
                <td>
                    <%: Html.DisplayFor(m => item.Destacar) %>
                </td>
                <td>
                    <%: Html.ActionLink(" Ver ficha", "DetailsCandidato", new { id = item.Id })%> 
                </td>
            </tr>  
            <% } %> 
        </tbody>
    </table>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $.fn.dataTableExt.sErrMode = 'throw';
            $('#Jtable').dataTable({
                "sDom": 'T<"clear">lfrtip',
                "oTableTools": {
                    "sSwfPath": "../../Content/DataTables-1.9.4/extras/TableTools/media/swf/copy_csv_xls.swf",
                    "aButtons": ["copy", "csv", "print"]
                },
                "oLanguage": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    </script>

</asp:Content>

