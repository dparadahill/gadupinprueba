﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%
    if (Request.IsAuthenticated) {
%>
        Usuario: <b><%: Page.User.Identity.Name %></b>
        <%--[ <%: Html.ActionLink("Acceder", "Index", "Candidato") %> ]--%>
         <br/>
         [<%: Html.ActionLink("Cerrar sesión", "LogOff", "Account") %>]
       <%-- [ <%: Html.ActionLink("Entrar a la admninistración", "Index", "Candidato") %> ]--%>
<%
    }
    else {
%> 
        [ <%: Html.ActionLink("Iniciar sesión", "LogOn", "Account") %> ]
<%
    }
%>
