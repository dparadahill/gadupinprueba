﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.Procedencia>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Eliminar canal de procedencia
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Eliminar</h2>

    <h3>¿Seguro que quiere eliminar el registro?</h3>
    <h4>No es aconsejable borrar un registro si ya es usado por otra tabla, puede causar problemas de integridad</h4>
    <fieldset>
        
        <div class="display-label" style="display: none">Id</div>
        <div class="display-field" style="display: none"><%: Model.Id %></div>
        
        <div class="display-label">Canal de procedencia</div>
        <div class="display-field"><%: Model.NombreProcedencia %></div>
        
    </fieldset>
    <% using (Html.BeginForm()) { %>
        <p>
		    <input type="submit" class="boton_der" value="Confirmar" /> 
		    <%--<%: Html.ActionLink("Volver", "Index", null, new { @class="boton_izq"}) %>--%>
        </p>
    <% } %>

</asp:Content>

