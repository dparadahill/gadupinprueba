﻿<%@ Control Language="C#" 
Inherits="System.Web.Mvc.ViewUserControl<GAdUPIN.WEB.ViewModels.ColegioDetailsVM>"%>

        <div class="izq" style="display: none">
            <%: Html.HiddenFor(model => model.Colegio.Id) %>
            <%: Html.LabelFor(model => model.Colegio.Id)%>
        </div>
        <div class="display-field" style="display: none">
            <%: Html.DisplayFor(model => model.Colegio.Id)%>
        </div>
        
        <div id="actualizado" class="actualizado" >Guardado </div>
        
        <div class="izq" >
            <%: Html.LabelFor(model => model.Colegio.CIF)%>
        </div>
        <div class="display-field" >
            <%: Html.TextBoxFor(model => model.Colegio.CIF)%>
        </div>

        <div class="izq" >
            <%: Html.LabelFor(model => model.Colegio.NombreColegio)%>
        </div>
        <div class="display-field" >
            <%: Html.TextBoxFor(model => model.Colegio.NombreColegio)%>
        </div>
        
        <div class="izq" >
            <%: Html.LabelFor(model => model.Colegio.DireccionColegio)%>
        </div>
        <div class="display-field"  >
            <%: Html.TextBoxFor(model => model.Colegio.DireccionColegio)%>
        </div>
        
        <div class="izq" style="display: none">
            <%: Html.LabelFor(model => model.SelectedComunidadAutonoma)%>
        </div>
        <div class="izq" >
               
                <%: @Html.DropDownListFor(model => model.SelectedComunidadAutonoma,
                        new SelectList(Model.ComunidadesAutonomas,
                        "Id", "NombreComunidadAutonoma"))%> 
        </div>  
        
        <div class="izq" style="display: none">
            <%: Html.LabelFor(model => model.SelectedProvincia)%>
        </div>
        <div class="display-field" >
               
                <%: @Html.DropDownListFor(model => model.SelectedProvincia,
                        new SelectList(Model.Provincias,
                        "Id", "NombreProvincia"), new{disabled="disabled", @id="SelectedProvincia2"})%> 
                <select id="SelectedProvincia" name="SelectedProvincia" style="display: none"></select>
        </div>            
            
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $('#SelectedComunidadAutonoma').change(function () {
                                 $.ajaxSetup({ cache: false });
                                 var selectedItem = $(this).val();
                                 if (selectedItem == "" || selectedItem == 0) {
                                     //Do nothing or hide...?    
                                 } else {
                                     $.ajax({
                                         url: '<%= @Url.Action("GetProvinciasByComunidad", "Colegio") %>',
                                         data: { id: selectedItem },
                                         dataType: "json",
                                         type: "POST",
                                         error: function (xhr, status, err) {
                                             var error = jQuery.parseJSON(xhr.responseText).Message;
                                             alert(error);
                                         },
                                         success: function (data) {
                                             var items = "";
                                             $.each(data, function (i, item) {
                                                 items += "<option value=\"" +
                                                      item.Id + "\">" +
                                                      item.Name + "</option>";
                                             });

                                             $("#SelectedProvincia").html(items);
                                             $("#SelectedProvincia2").hide();
                                             $("#SelectedProvincia").show();
                                         }
                                     });
                                 }
                             });
                         });
                      </script>


        <div class="izq" >
            <%: Html.LabelFor(model => model.Colegio.CodigoPostal)%>
        </div>
        <div class="display-field"  >
            <%: Html.TextBoxFor(model => model.Colegio.CodigoPostal)%>
        </div>
        
        <div class="izq">
            <%: Html.LabelFor(model => model.Colegio.Telefono)%>
        </div>
        <div class="display-field">
            <%: Html.TextBoxFor(model => model.Colegio.Telefono)%>
        </div>
        
        <div class="izq">
            <%: Html.LabelFor(model => model.Colegio.PlazasDisponibles)%>
        </div>
        <div class="display-field">
            <%: Html.DisplayFor(model => model.Colegio.PlazasDisponibles)%>
            <%: Html.HiddenFor(model => model.Colegio.PlazasDisponibles)%>
        </div>
        
        <div class="izq">
            <%: Html.LabelFor(model => model.Colegio.ColegioContratado)%>
        </div>
        <div class="display-field">
            <%: Html.CheckBoxFor(model => model.Colegio.ColegioContratado)%>
        </div>
        
      