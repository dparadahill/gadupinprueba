﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.Colegio>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Activar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <h3>El colegio se pasará al listado de colegios activos</h3>
    <fieldset>
        <legend>Campos</legend>
        
        <div class="display-label" style="display: none">Id</div>
        <div class="display-field" style="display: none"><%: Model.Id %></div>
        
        <div class="display-label" style="font-weight: bold">CIF</div>
        <div class="display-field"><%: Model.CIF %></div>

        <div class="display-label" style="font-weight: bold">Colegio</div>
        <div class="display-field"><%: Model.NombreColegio %></div>
        
        <div class="display-label" style="font-weight: bold">Dirección</div>
        <div class="display-field"><%: Model.DireccionColegio %></div>
        
        <div class="display-label" style="font-weight: bold">Código Postal</div>
        <div class="display-field"><%: Model.CodigoPostal %></div>
        
        <div class="display-label" style="font-weight: bold">Teléfono</div>
        <div class="display-field"><%: Model.Telefono %></div>
        
       
    </fieldset>
    <% using (Html.BeginForm()) { %>
    <%--<%: Html.ActionLink("Volver", "Index", null, new{@class="boton", style="margin-top:7px; float:left;" })%>--%>
        <p style="margin-top:13px; float:right">
		    <input type="submit" class="boton" value="Activar" />
		    
        </p>
    <% } %>

</asp:Content>

