﻿<%--<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.WEB.ViewModels.ColegioNewEditVM>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Editar Colegio
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Editar colegio</h2>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
            <legend>Datos</legend>
            
            <div class="editor-label" style="display:none">
                <%: Html.LabelFor(model => model.Colegio.Id) %>
            </div>
            <div class="editor-field" style="display:none">
                <%: Html.TextBoxFor(model => model.Colegio.Id)%>
                <%: Html.ValidationMessageFor(model => model.Colegio.Id)%>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Colegio.CIF)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Colegio.NombreColegio)%>
                <%: Html.ValidationMessageFor(model => model.Colegio.NombreColegio)%>
            </div>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.Colegio.NombreColegio)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Colegio.NombreColegio)%>
                <%: Html.ValidationMessageFor(model => model.Colegio.NombreColegio)%>
            </div>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.Colegio.DireccionColegio)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Colegio.DireccionColegio)%>
                <%: Html.ValidationMessageFor(model => model.Colegio.DireccionColegio)%>
            </div>
            
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Colegio.CodigoPostal)%>
                <%: Html.ValidationMessageFor(model => model.Colegio.CodigoPostal)%>
            </div>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.Colegio.Telefono)%>
            </div>
                <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Colegio.Telefono)%>
                <%: Html.ValidationMessageFor(model => model.Colegio.Telefono)%>
            </div>
            
            <div class="editor-field" >
               
                <%: @Html.DropDownListFor(model => model.SelectedComunidadAutonoma,
                        new SelectList(Model.ComunidadesAutonomas,
                        "Id", "NombreComunidadAutonoma", 1), "*Seleccione comunidad autónoma*")%> 
                        
               <select id="SelectedProvincia" name="SelectedProvincia"></select>
                                    

              <script type="text/javascript">
                  $(document).ready(function () {
                      $('#SelectedComunidadAutonoma').change(function () {
                          $.ajaxSetup({ cache: false });
                          var selectedItem = $(this).val();
                          if (selectedItem == "" || selectedItem == 0) {
                              //Do nothing or hide...?    
                          } else {
                              $.ajax({
                                  url: '<%= @Url.Action("GetProvinciasByComunidad", "Colegio") %>',
                                  data: { id: selectedItem },
                                  dataType: "json",
                                  type: "POST",
                                  error: function (xhr, status, err) {
                                      var error = jQuery.parseJSON(xhr.responseText).Message;
                                      alert(error);
                                  },
                                  success: function (data) {
                                      var items = "";
                                      $.each(data, function (i, item) {
                                          items += "<option value=\"" +
                                              item.Id + "\">" +
                                              item.Name + "</option>";
                                      });

                                      $("#SelectedProvincia").html(items);
                                  }
                              });
                          }
                      });
                  });
              </script>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Colegio.ColegioContratado)%>
                <%: Html.CheckBoxFor(model => model.Colegio.ColegioContratado)%>
                <%: Html.ValidationMessageFor(model => model.Colegio.ColegioContratado)%>
            </div>
            
            <p>
                <input type="submit" value="Save" />
            </p>
        </fieldset>

    <% } %>

    <div>
        <%: Html.ActionLink("Volver", "Index", null, new { @class = "boton_der" })%>
    </div>

</asp:Content>

--%>