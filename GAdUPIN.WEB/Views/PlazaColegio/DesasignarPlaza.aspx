﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
 Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.PlazaColegio>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Desasignar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Desasignar <% string.Format("{0}{1}{2}", Model.CandidatoAsignado.Nombre, Model.CandidatoAsignado.Apellido1, Model.CandidatoAsignado.Apellido2); %></h2>

    <fieldset>     
        <div class="display-label" style="display: none">Id</div>
        <div class="display-field" style="display: none"><%: Model.Id %></div>
        
        <div class="display-label"> Al confirmar la desasignación, se liberarán tanto la plaza del colegio como el candidato</div>
       
    </fieldset>
    <% using (Html.BeginForm()) { %>
        <p>
		    <input type="submit" class="botonGris" value="Confirmar" /> 
		    <%--<%: Html.ActionLink("Volver", "Edit" ,new { id=Model.Id }, new {@class = "boton_izq"}) %> --%>
            
        </p>
    <% } %>

</asp:Content>

