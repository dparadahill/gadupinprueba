﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<IEnumerable<GAdUPIN.WEB.ViewModels.PlazaColegioListVM>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Plazas del colegio
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm())
   { %>
    <%: Html.ValidationSummary(true) %>
    <% if (Model.Count() != 0)
       { %>
    <h2>Plazas del colegio <%: Model.First().NombreColegio %> </h2>
    <% }
       else
       { %>
    <h2>No existen plazas disponibles</h2>
    <% } %>
    <table id="Jtable" class="display" style="text-align: left">
        <thead>
            <tr>
                <th>
                    Código
                </th>
                <th>
                    Cuantía
                </th>
                <th>
                    Inicio
                </th>
                <th>
                    Etapa Ed.
                </th>
                <th>
                    Condiciones
                </th>
                <th>
                    Pref. Sexo
                </th>
                <th>
                    Adicional
                </th>
                <th>
                    ¿Cubierta?
                </th>
                <th></th>
            </tr>
        </thead>

    
        <tbody>
            <tr>
                <% foreach (var item in Model) { %>
                <td>
                    <%: item.CodigoPlaza %>
                </td>
                <td>
                    <%: item.CuantiaBeca%>
                </td>
                <td>
                    <%: Html.DisplayFor(p => item.FechaInicio) %>
                </td>
                <td>
                    <%: Html.DisplayFor(p => item.EtapaEducativa.EtapaEducativaNombre)%>
                </td>
                <td>
                    <%: item.Condiciones %>
                </td>
                <td>
                    <%: item.PreferenciaSexo %>
                </td>
                <td>
                    <%: item.DatoAdicional %>
                </td>
                <td>
                <% if (item.CubiertaPor == null) { %>
                    <%: Html.ActionLink("Cubrir", "ListarCandidatosParaAsignacion", new {idPlaza = item.Id}) %> 
                <% }
                else { %>
                    <%: Html.ActionLink(item.CubiertaPor.Codigo, "DetailsCandidato", "Candidato" ,new { id=item.CubiertaPor.Id }, null) %> 
                <% } %>
                    
                </td>
                <td>
                    <%: Html.ActionLink("Modificar", "Edit", new { id=item.Id}) %> |
                    <%--<%: Html.ActionLink("Details", "Details", new { id = item.Id })%> |--%>
                    <%: Html.ActionLink("Eliminar", "Delete", new { id = item.Id })%>
                </td>
            </tr>
             <% } %>
        </tbody>
    </table>
    <script type="text/javascript">
        $(document).ready(function () {
            $.fn.dataTableExt.sErrMode = 'throw';
            $('#Jtable').dataTable({
                "sDom": 'T<"clear">lfrtip',
                "oTableTools": {
                    "sSwfPath": "../../Content/DataTables-1.9.4/extras/TableTools/media/swf/copy_csv_xls.swf",
                    "aButtons": ["copy", "csv", "print"]
                },
                "oLanguage": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    </script>

    <% } %>

    <p style="clear: left;margin-top: 30px;">
        <%if (Model.Count() != 0)
          { %>
        <%: Html.ActionLink("Añadir", "Create", new { colegio = Model.First().IdColegio}, new {@class="boton_der"}) %>
        <% } %>
        <%else 
          { %>
        <%: Html.ActionLink("Añadir", "Create", new { colegio = ViewData["idColegio"]}, new {@class="boton_der"}) %>
       
        <% } %>
    </p>
    <div>
        
        <% if (Model.Count() != 0)
       { %>
            <%--<%: Html.ActionLink("Volver", "Details", "Colegio", new{id = Model.First().IdColegio}, new {@class="boton_izq"}) %>--%>
        <% } %>
       
    </div>
    

</asp:Content>

