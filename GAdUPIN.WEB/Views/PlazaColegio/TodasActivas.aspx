﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<IEnumerable<GAdUPIN.WEB.ViewModels.PlazaColegioListVM>>" %>
<%--Inherits="System.Web.Mvc.ViewPage<IEnumerable<GAdUPIN.CORE.Domain.PlazaColegio>>" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Plazas Activas
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Plazas Activas</h2>

    <table id="Jtable" class="display" style="text-align: left" >
        <thead>
            <tr>
            
                <th style="display: none">
                    IdPlaza
                </th>
                <th style="display: none">
                    IdColegio
                </th>
                <th>
                    Colegio
                </th>
                <th>
                    Comunidad
                </th>
<%--                <th>
                    Cubierta
                </th>--%>
                <th>
                    Fecha Inicio
                </th>
                <th>
                    Etapa Educativa
                </th>
                <th>
                    Nacionalidad
                </th>
                <th>
                    Adicional
                </th>
                <th>
                    Candidato
                </th>
                <th></th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <% foreach (var item in Model) { %>
                <td style="display: none">
                    <%: item.Id %>
                </td>
                <td style="display: none">
                    <%: item.IdColegio %>
                </td>
                <td>
                    <%: Html.ActionLink(item.NombreColegio, "Details", "Colegio", new { id = item.IdColegio }, null)%>
                    <%--<%: Html.ActionLink(item.CubiertaPor.Nombre, "DetailsCandidato", "Candidato" ,new { id=item.CubiertaPor.Id }, null) %> --%>
                    
                </td>
                <td>
                    <%: item.NombreComunidadAutonoma %>
                </td>
<%--                <td>
                    <%: item.PlazaContratada ? "Si" : "No"%>
                </td>--%>
                <td>
                    <%: Html.DisplayFor(p => item.FechaInicio) %>
                </td>
                <td>
                    <%: item.EtapaEducativa.EtapaEducativaNombre %>
                </td>
                <td>
                    <%: Html.DisplayFor(p => item.NacionalidadPreferente.NombreNacionalidad) %>
                </td>
                <td>
                    <%: Html.DisplayFor(p => item.DatoAdicional) %>
                </td>
                <td>
                    <% if (item.PlazaContratada == false) { %>
                        <%: Html.ActionLink("Cubrir", "ListarCandidatosParaAsignacion", new {idPlaza = item.Id}) %> 
                    <% }
                    else if (item.CodigoCandidato != "") { %>
                        <%: Html.ActionLink(item.CodigoCandidato, "DetailsCandidato", "Candidato" ,new { id = item.IdCandidato }, null) %> 
                    <% } %>
                </td>
                <td>
                    <%--<%: Html.ActionLink("Ver colegio", "Details", "Colegio" ,new {  id=item.Colegio.Id })%> |--%>
                    <%: Html.ActionLink("Ver plaza", "Edit", "PlazaColegio",new { id=item.Id }, null)%>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>

<script type="text/javascript">
    $(document).ready(function () {
        $.fn.dataTableExt.sErrMode = 'throw';
        $('#Jtable').dataTable({
            "sDom": 'T<"clear">lfrtip',
            "oTableTools": {
                "sSwfPath": "../../Content/DataTables-1.9.4/extras/TableTools/media/swf/copy_csv_xls.swf",
                "aButtons": ["copy", "csv", "print"]
            },
            "oLanguage": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });
    });
    </script>

</asp:Content>

