﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.ContactoColegio>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Eliminar contacto de colegio
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Eliminar contacto de colegio</h2>

    <h3>¿Seguro que quiere eliminar el registro?</h3>
    
    <fieldset>
        
        <div class="display-label" style="display: none">Id</div>
        <div class="display-label" style="display: none"><%: Model.Id %></div>
        
        <div class="display-label">Nombre</div>
        <div class="display-field"><%: Model.NombreContactoColegio + " " + Model.Apellido1 + " " + Model.Apellido2%></div>
        
        <div class="display-label">Email</div>
        <div class="display-field"><%: Model.Email %></div>
        
        <div class="display-label">Teléfono</div>
        <div class="display-field"><%: Model.TelefonoDeContacto %></div>
        
        <div class="display-label">¿Contacto principal?</div>
        <div class="display-field"><%: Model.ContactoPrincipal ? "Si" : "No" %></div>
        
        <div class="display-label">DNI</div>
        <div class="display-field"><%: Model.DNI %></div>
        
        <div class="display-label">Tipo</div>
        <div class="display-field"><%: Model.Tipo %></div>
        
        <div class="display-label">Comentarios</div>
        <div class="display-field"><%: Model.ComentariosContacto %></div>
    </fieldset>
    <% using (Html.BeginForm()) { %>
        <p>
		    <input type="submit" value="Eliminar" class="boton_der"/> 
		    <%--<%: Html.ActionLink("Volver", "Index", "ContactoColegio", new { id = Model.Colegio.Id}, new { @class = "boton_izq" })%>--%>
        </p>
    <% } %>

</asp:Content>

