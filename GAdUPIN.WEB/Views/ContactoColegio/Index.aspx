﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<IEnumerable<GAdUPIN.CORE.Domain.ContactoColegio>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Contactos
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Contactos</h2>
    
    <table id="Jtable" class="display" style="text-align: left">
        <thead>
            <tr>
                <%--<th>
                    Principal
                </th>--%>
                <th>
                    DNI
                </th>
                <th>
                    Nombre
                </th>
                <th>
                    Apellidos
                </th>
                <th>
                    Email
                </th>
                <th>
                    Teléfono
                </th>
                <th>
                    Tipo
                </th>
                <th>
                    ¿Principal?
                </th>
                <th></th>
            </tr>
        </thead>
    
    <tbody>
            <tr>
                <% foreach (var item in Model) { %>
              <%--  <td>
                    <%: item.ContactoPrincipal? "Si" : "No"%>
                </td>--%>
                <td>
                    <%: item.DNI %>
                </td>
                <td>
                    <%: item.NombreContactoColegio %>
                </td>
                <td>
                    <%: item.Apellido1 + " " + item.Apellido2 %>
                </td>
                <td>
                    <%: item.Email %>
                </td>
                <td>
                    <%: item.TelefonoDeContacto %>
                </td>
                <td>
                    <%: item.Tipo %>
                </td>
                <td>
                    <%: item.ContactoPrincipal ? "Si" : "No"%>
                </td>
                <td>
                    <%: Html.ActionLink("Editar", "Edit", new { id=item.Id}) %> |
                    <%: Html.ActionLink("Eliminar", "Delete", new { id = item.Id })%>
                </td>
            </tr>
        <% } %>
    </tbody>
    </table>

    <script type="text/javascript">
        $(document).ready(function () {
            $.fn.dataTableExt.sErrMode = 'throw';
            $('#Jtable').dataTable({
                "sDom": 'T<"clear">lfrtip',
                "oTableTools": {
                    "sSwfPath": "../../Content/DataTables-1.9.4/extras/TableTools/media/swf/copy_csv_xls.swf",
                    "aButtons": ["copy", "csv", "print"]
                },
                "oLanguage": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    </script>
    <p style="clear: left; margin-top:30px;">
        
        <%: Html.ActionLink("Añadir", "Create", new { idColegio = ViewData["idColegio"]} , new { @class = "boton_der" })%>
        <%--<%: Html.ActionLink("Volver", "Details", "Colegio", new { id = ViewData["idColegio"] }, new { @class = "boton_izq" })%>--%>
        
    </p>

</asp:Content>

