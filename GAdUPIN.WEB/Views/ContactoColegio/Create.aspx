﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.WEB.ViewModels.ContactoColegioNewEditVM>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Nuevo Contacto de colegio
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Nuevo contacto de colegio</h2>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <fieldset>
            
            <div class="izq" style="display: none">
                <%: Html.LabelFor(model => model.ContactoColegio.Id) %>
            </div>
            <div class="der" style="display: none">
                <%: Html.TextBoxFor(model => model.ContactoColegio.Id) %>
                <%: Html.ValidationMessageFor(model => model.ContactoColegio.Id)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.ContactoColegio.DNI)%>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.ContactoColegio.DNI)%>
                <%: Html.ValidationMessageFor(model => model.ContactoColegio.DNI)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.ContactoColegio.NombreContactoColegio)%>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.ContactoColegio.NombreContactoColegio)%>
                <%: Html.ValidationMessageFor(model => model.ContactoColegio.NombreContactoColegio)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.ContactoColegio.Apellido1)%>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.ContactoColegio.Apellido1)%>
                <%: Html.ValidationMessageFor(model => model.ContactoColegio.Apellido1)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.ContactoColegio.Apellido2)%>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.ContactoColegio.Apellido2)%>
                <%: Html.ValidationMessageFor(model => model.ContactoColegio.Apellido2)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.ContactoColegio.Email)%>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.ContactoColegio.Email)%>
                <%: Html.ValidationMessageFor(model => model.ContactoColegio.Email)%>
            </div>
            
            <div class="izq">
                <%: Html.LabelFor(model => model.ContactoColegio.TelefonoDeContacto)%>
            </div>
            <div class="der">
                <%: Html.TextBoxFor(model => model.ContactoColegio.TelefonoDeContacto)%>
                <%: Html.ValidationMessageFor(model => model.ContactoColegio.TelefonoDeContacto)%>
            </div>
           
           <div class="izq" style="clear:both">
                <%: Html.LabelFor(model => model.ContactoColegio.ContactoPrincipal)%>
            </div>
            <div class="der">
                <%: Html.CheckBoxFor(model => model.ContactoColegio.ContactoPrincipal)%>
                <%: Html.ValidationMessageFor(model => model.ContactoColegio.ContactoPrincipal)%>
            </div>
            
            <div class="izq" style="clear:both">
                <%: Html.LabelFor(model => model.ContactoColegio.Tipo)%>
            </div>
            <div class="der">  
                <%: Html.DropDownListFor(model => model.ContactoColegio.Tipo, new SelectList(
                    new[]
                    {
                        new {Value = "Director", Text = "Director"},      
                        new {Value = "Administrador", Text = "Administrador"},      
                        new {Value = "Jefe de departamento", Text = "Jefe de departamento"},      
                        new {Value = "Profesor", Text = "Profesor"},      
                        new {Value = "Representante", Text = "Representante"},
                        new {Value = "Tutor", Text = "Tutor"},    
                        new {Value = "Otros", Text = "Otros"}  
                    },
                    "Value",
                    "Text",
                    Model
                 )
                )%>
                <%: Html.ValidationMessageFor(model => model.ContactoColegio.Tipo)%>
            </div>
        
        
        </fieldset>
        
        <fieldset style="clear:both">
                <legend>
                      <%: Html.LabelFor(model => model.ContactoColegio.ComentariosContacto) %>
                </legend>
                
                <div class="comentario">
                    <%: Html.TextAreaFor(model => model.ContactoColegio.ComentariosContacto)%>
                </div>
        </fieldset>
        

            <input type="submit" value="Guardar" class="boton_der" />
    <% } %>

    <div>
        <%--<%: Html.ActionLink("Volver", "Index", "ContactoColegio", new { id = Model.IdColegio }, new { @class = "boton_izq" })%>--%>
    </div>
    
    <script type="text/javascript">

        $('#form0').submit(function () {
            var obj;
            obj = $('#ContactoColegio_ComunidadAutonoma_Id');
            if (obj.val() == "") {
                alert("Debe elegir una comunidad autónoma");
                obj.focus();
                return false;
            }
            return true;
        });
       
    </script>
</asp:Content>

