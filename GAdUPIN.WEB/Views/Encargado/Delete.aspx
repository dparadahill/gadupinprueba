﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
 Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.Encargado>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Eliminar encargado
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Eliminar</h2>

    <h3>¿Seguro que quiere eliminar el registro?</h3>
    <h4>No es aconsejable borrar un registro si ya es usado por otra tabla, puede causar problemas de integridad</h4>
    <fieldset>
        <legend>Campos</legend>
        
        <div class="display-label" style="display: none">Id</div>
        <div class="display-field" style="display: none"><%: Model.Id %></div>
             
        <div class="izq">
            <%: Html.LabelFor(model => model.DNI) %>
        </div>
        <div class="der">
            <%: Html.DisplayFor(model => model.DNI)%>
            <%: Html.ValidationMessageFor(model => model.DNI) %>
        </div>
            
        <div class="izq">
            <%: Html.LabelFor(model => model.Nombre) %>
        </div>
        <div class="der">
            <%: Html.DisplayFor(model => model.Nombre)%>
            <%: Html.ValidationMessageFor(model => model.Nombre) %>
        </div>
            
        <div class="izq">
            <%: Html.LabelFor(model => model.Apellido1) %>
        </div>
        <div class="der">
            <%: Html.DisplayFor(model => model.Apellido1)%>
            <%: Html.ValidationMessageFor(model => model.Apellido1) %>
        </div>
            
        <div class="izq">
            <%: Html.LabelFor(model => model.Apellido2) %>
        </div>
        <div class="der">
            <%: Html.DisplayFor(model => model.Apellido2)%>
            <%: Html.ValidationMessageFor(model => model.Apellido2) %>
        </div>
            
        <div class="izq">
            <%: Html.LabelFor(model => model.Email) %>
        </div>
        <div class="der">
            <%: Html.DisplayFor(model => model.Email)%>
            <%: Html.ValidationMessageFor(model => model.Email) %>
        </div>
            
        <div class="izq">
            <%: Html.LabelFor(model => model.ComunidadAutonoma) %>
        </div>
        <div class="der">
            <%: Html.DisplayFor(model => model.ComunidadAutonoma.NombreComunidadAutonoma)%> 
            <%: Html.ValidationMessageFor(model => model.ComunidadAutonoma)%>
        </div>
        
        <div class="izq">
                <%: Html.LabelFor(model => model.TelefonoDeContacto)%>
            </div>
            <div class="der">
                <%: Html.DisplayFor(model => model.TelefonoDeContacto)%>
                <%: Html.ValidationMessageFor(model => model.TelefonoDeContacto)%>
            </div>
        
    </fieldset>
    <% using (Html.BeginForm()) { %>
 
    
    <div>
        <input type="submit" value="Confirmar" class="boton_der"/>        
          <%--<%: Html.ActionLink("Volver", "Index", "Encargado", null, new { @class = "boton_izq" })%>--%>

    </div>
    <% } %>

</asp:Content>

