﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<IEnumerable<GAdUPIN.CORE.Domain.Candidato>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% if (Model.Count() != 0){ %>
        <h2>Candidatos de <%: Model.First().Encargado.Nombre + " " + Model.First(). Encargado.Apellido1 + " " + Model.First().Encargado.Apellido2 %></h2>
    <% }
    else { %>
        <h2>El encargado no tiene candidatos asignados</h2>
    <% } %>
    

   <table id="Jtable" class="display" style="text-align: left" >
      <thead>
        <tr>
            <th style="display: none">
                Id
            </th>
            <th>
                Codigo
            </th>
            <th>
                Nombre
            </th>
            <th>
                Email
            </th>
            <th>
                Teléfono 
            </th>
            <th>
                Teléfono (ES)
            </th>
            <th>
                Skype
            </th>
            <th>
                Llegada a España
            </th>
            <th></th>
        </tr>
      </thead>

      <tbody>
        <% foreach (var item in Model) { %>
    
        <tr>
            
            <td style="display: none">
                <%: item.Id %>
            </td>
            <td>
                <%: item.Codigo %>
            </td>
            <td>
                <%: item.Nombre + " " + item.Apellido1 + " " + item.Apellido2%>
            </td>
            <td>
                <%: item.Email %>
            </td>
            <td>
                <%: item.TelefonoDeContacto %>
            </td>
            <td>
                <%: item.TelefonoEspanol %>
            </td>
            <td>
                <%: item.Skype %>
            </td>
            <td>
                <%: Html.DisplayFor(m => item.FechaLlegadaEspana)%>
            </td>
            <td>
                <%: Html.ActionLink("Ver", "DetailsCandidato", "Candidato",  new { id = item.Id }, null) %> |
                <%: Html.ActionLink("Desasignar", "DesasignarEncargado", new { idCandidato = item.Id})%> 
            </td>
        </tr>
      </tbody>
    
    <% } %>
    
    </table>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $.fn.dataTableExt.sErrMode = 'throw';
            $('#Jtable').dataTable({
                "sDom": 'T<"clear">lfrtip',
                "oTableTools": {
                    "sSwfPath": "../../Content/DataTables-1.9.4/extras/TableTools/media/swf/copy_csv_xls.swf",
                    "aButtons": ["copy", "csv", "print"]
                },
                "oLanguage": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    </script>
    <div style="clear: left">
    <%--<%: Html.ActionLink("Volver", "Index", "Encargado", null, new { @class = "boton_izq"})%>--%>
    </div>
</asp:Content>

