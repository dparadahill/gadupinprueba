﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using GAdUPIN.CORE.DAL;
using GAdUPIN.CORE.Domain;

// Post-compilacion, ruta Diego: 
//          copy /Y "$(SolutionDir)GAdUPIN.DAL\bin\Debug\GAdUPIN.DAL.dll" "D:\SI2\GAdUPIN\GAdUPIN.WEB\bin"



namespace GAdUPIN.DAL
{
    public class DBContext : DbContext, IDBContext //DbContext es del entity framework, es el paquete NuGet que hemos instalado
    {
        ///// <summary>
        ///// Constructor de la clase con nombre de cadena de conexión o cadena de conexión que llama al base
        ///// </summary>
        ///// <param name="nameOrConnectionString"></param>
        //public DBContext(string nameOrConnectionString) : //Si no pasamos nada tomará ApplicationServices por defecto
        //    base(nameOrConnectionString)
        //{
        //    //Hemos heredado del base (que es el padre) y hemos implementado uno de sus 7 constructores pasandole la cadena de conexión
        //}


        public DBContext() : //Si no pasamos nada tomará ApplicationServices por defecto
            base("name=ApplicationServices")
        {
            //System.Data.Entity.Database.SetInitializer<DbContext>(new DropCreateDatabaseAlways<DbContext>());
            //Hemos heredado del base (que es el padre) y hemos implementado uno de sus 7 constructores pasandole la cadena de conexión
        }

        private string schemaName = "uptea_ch";
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Admin>().ToTable("Admins", schemaName);
            modelBuilder.Entity<Candidato>().ToTable("Candidatos", schemaName);
            modelBuilder.Entity<Colegio>().ToTable("Colegios", schemaName);
            modelBuilder.Entity<ComunidadAutonoma>().ToTable("ComunidadesAutonomas", schemaName);
            modelBuilder.Entity<ContactoColegio>().ToTable("ContactosColegio", schemaName);
            modelBuilder.Entity<DocumentoDeTramite>().ToTable("DocumentosDeTramite", schemaName);
            modelBuilder.Entity<DocumentoInscripcion>().ToTable("DocumentosInscripcion", schemaName);
            modelBuilder.Entity<Encargado>().ToTable("Encargados", schemaName);
            modelBuilder.Entity<EstadoVisado>().ToTable("EstadosVisado", schemaName);
            modelBuilder.Entity<Estatus>().ToTable("Estatuses", schemaName);
            modelBuilder.Entity<EtapaEducativa>().ToTable("EtapasEducativas", schemaName);
            modelBuilder.Entity<Experiencia>().ToTable("Experiencias", schemaName);
            //modelBuilder.Entity<NuevaExperiencia>().ToTable("NuevasExperiencias", schemaName);
            modelBuilder.Entity<InformeEntrevistaEvaluacion>().ToTable("InformesEntrevistaEvaluacion", schemaName);
            modelBuilder.Entity<MotivoCierreFicha>().ToTable("MotivosCierreFicha", schemaName);
            modelBuilder.Entity<Nacionalidad>().ToTable("Nacionalidades", schemaName);
            modelBuilder.Entity<PlazaColegio>().ToTable("PlazaColegios", schemaName);
            modelBuilder.Entity<PreferenciasColegio>().ToTable("PreferenciasColegios", schemaName);
            modelBuilder.Entity<Procedencia>().ToTable("Procedencias", schemaName);
            modelBuilder.Entity<Provincia>().ToTable("Provincias", schemaName);
            modelBuilder.Entity<TipoAlojamiento>().ToTable("TiposAlojamientos", schemaName);
            modelBuilder.Entity<UniversidadDeRegistro>().ToTable("UniversidadesDeRegistro", schemaName);
        }


        /// <summary>
        /// Candidatos del contexto de datos
        /// </summary>
        //public IDbSet<Candidato> Candidatos { get; set; }
        
        public DbSet<Candidato> Candidatos { get; set; }
        /// <summary>
        /// Administradores del contexto de datos
        /// </summary>
        public IDbSet<Admin> Admins { get; set; }

        /// <summary>
        /// Colegios del contexto de datos
        /// </summary>
        public IDbSet<Colegio> Colegios { get; set; }

        /// <summary>
        /// Contactos de un colegio
        /// </summary>
        public IDbSet<ContactoColegio> ContactosDelColegio { get; set; }

        /// <summary>
        /// Encargados de los candidatos
        /// </summary>
        public IDbSet<Encargado> Encargados { get; set; }

        ///// <summary>
        ///// Entrevistadores
        ///// </summary>
        //public IDbSet<Entrevistador> Entrevistadores { get; set; }
        
        /// <summary>
        /// Experiencias que tiene un candidato
        /// </summary>
        //public IDbSet<NuevaExperiencia> Experiencias { get; set; }

        /// <summary>
        /// Plazas antiguas relacionadas a un colegio
        /// </summary>
        //public IDbSet<PlazaAntigua> PlazasAntiguas { get; set; }

        /// <summary>
        /// Plazas del colegio
        /// </summary>
        public IDbSet<PlazaColegio> PlazasDelColegio { get; set; }

        /// <summary>
        /// Preferencias que tiene el candidato
        /// </summary>
        //public IDbSet<PreferenciasColegio> PreferenciasesColegio { get; set; }

        /// <summary>
        /// Comunidades autonomas en españa
        /// </summary>
        public IDbSet<ComunidadAutonoma> ComunidadesAutonomas { get; set; }

        /// <summary>
        /// Provincias de españa
        /// </summary>
        public IDbSet<Provincia> Provincias { get; set; }

        /// <summary>
        /// Tipos de alojamientos
        /// </summary>
        public IDbSet<TipoAlojamiento> TipoAlojamientos { get; set; }

        /// <summary>
        /// Estatuses de los candidatos
        /// </summary>
        public IDbSet<Estatus> Estatuses { get; set; }

        /// <summary>
        /// Nacionalidades de los candidatos
        /// </summary>
        public IDbSet<Nacionalidad> Nacionalidades { get; set; }

        /// <summary>
        /// Entrevistas de los candidatos
        /// </summary>
        public IDbSet<InformeEntrevistaEvaluacion> InformeEntrevistaEvaluaciones { get; set; }

        /// <summary>
        /// Canales de procedencia de los candidatos
        /// </summary>
        public IDbSet<Procedencia> Procedencias { get; set; }

        /// <summary>
        /// Etapas educativas de los colegios
        /// </summary>
        public IDbSet<EtapaEducativa> EtapasEducativas { get; set; }
        
        
        /// <summary>
        /// Estados de un visado
        /// </summary>
        public IDbSet<EstadoVisado> EstadosVisado { get; set; }

        /// <summary>
        /// Motivo de cierre de una ficha de candidato
        /// </summary>
        public IDbSet<MotivoCierreFicha> MotivosCierreFicha { get; set; }

        /// <summary>
        /// Universidades de registro
        /// </summary>
        public IDbSet<UniversidadDeRegistro> UniversidadesDeRegistro { get; set; }

        // Listado de documentos requeridos por el candidato
        public IDbSet<DocumentoDeTramite> DocumentosDeTramite { get; set; }

        /// <summary>
        /// Documentos adjuntos del candidato
        /// </summary>
        public IDbSet<DocumentoInscripcion> DocumentosInscripcion { get; set; }
    }
}
