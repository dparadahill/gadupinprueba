﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Contracts;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;
using GAdUPIN.WEB.ViewModels;


namespace GAdUPIN.WEB.Controllers
{

    public class EmployeeController : Controller
    {
        //Hay que instanciar IoC
        private IEmployeeService _employeeService;
        private ICenterService _centerService;
        private INLogger _NLoggerService;

        public EmployeeController()
        {
            _employeeService =
                IoC.Current.GetContainer().Resolve(typeof(IEmployeeService), "")
                as IEmployeeService;

            _centerService =
                IoC.Current.GetContainer().Resolve(typeof(ICenterService), "")
                as ICenterService;
            
            _NLoggerService =
                IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
                as INLogger;
        }


        //
        // GET: /Employee/

        public ActionResult Index(Guid? id = null)
        {
            IList<EmployeeListViewModel> list = CreateViewModelList();
            _NLoggerService.Info("Visita a la página de empleados registrada");
            return View(list);
        }

        private IList<EmployeeListViewModel> CreateViewModelList()
        {
            IList<EmployeeListViewModel> result = new List<EmployeeListViewModel>();

            foreach (var employee in _employeeService.GetAllWithCenters())
            {
                result.Add(new EmployeeListViewModel()
                {
                    Id = employee.Id,
                    CenterName = employee.Center != null ? employee.Center.Name : string.Empty,
                    Name = string.Format("{0}", employee.Name)
                    
                    //Job = employee.JobPosition
                });
            }

            return result;
        }


        //
        // GET: /Employee/Details/
        public ActionResult Details(Guid id)
        {
            return View(_employeeService.Get(id));
        }

        
        // GET: /Employee/Create
        public ActionResult Create(Guid? id = null)
        {
            EmployeeNewEditViewModel employee = CreateViewModelNewEdit(id);
            return View(employee);

        }

        private EmployeeNewEditViewModel CreateViewModelNewEdit(Guid? id = null)
        {
            EmployeeNewEditViewModel result = new EmployeeNewEditViewModel();
            if (id == null)
            {
                result.Employee = new Employee() { Id = Guid.NewGuid() };
                result.Centers = _centerService.GetAll();
            }
            else if (id != null)
            {
                result.Employee = new Employee() { Id = Guid.NewGuid() };
                result.Centers = _centerService.GetAll();
              //  result.SelectedCenter = _centerService.Get(id);
            }
            return result;
        } 


        //
        // POST: /Employee/Create

        [HttpPost]
        public ActionResult Create(EmployeeNewEditViewModel employeeVisualModel)
        {
            try
            {
                // TODO: Add insert logic here
                employeeVisualModel.Employee.Center = _centerService.Get(employeeVisualModel.SelectedCenter);
                _employeeService.Add(employeeVisualModel.Employee);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Employee/Edit/5

       
        public ActionResult Edit(Guid id)
        {
            EmployeeNewEditViewModel vm = new EmployeeNewEditViewModel();
            vm.Employee = _employeeService.Get(id);
            vm.Centers = _centerService.GetAll();
            vm.SelectedCenter = vm.Employee.Center.Id;
            return View(vm);
        }

        //
        // POST: /Employee/Edit/5

        [HttpPost]
        public ActionResult Edit(EmployeeNewEditViewModel employeeVM)
        {
            try
            {
                // TODO: Add update logic here
                employeeVM.Employee.Center = _centerService.Get(employeeVM.SelectedCenter);
                _employeeService.Update(employeeVM.Employee);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Employee/Delete/5

        public ActionResult Delete(Guid id)
        {
            return View(_employeeService.Get(id));
        }

        //
        // POST: /Employee/Delete/5

        [HttpPost]
        public ActionResult Delete(Employee employee)
        {
            try
            {
                // TODO: Add delete logic here
                _employeeService.Remove(employee);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}


