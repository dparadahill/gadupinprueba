﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;
using GAdUPIN.WEB.ViewModels;

namespace GAdUPIN.WEB.Controllers
{
    //[Authorize(Roles = "admin")]
    public class JobPositionController : Controller
    {
        
        private IJobPositionService _jobPositionService;
        private ICenterService _centerService;
        private INLogger _NLoggerService;

        public JobPositionController()
        {
            _jobPositionService =
                IoC.Current.GetContainer().Resolve(typeof(IJobPositionService), "")
                as IJobPositionService;
                        _centerService =
                IoC.Current.GetContainer().Resolve(typeof(ICenterService), "")
                as ICenterService;

            _NLoggerService =
                IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
                as INLogger;

        }


        //
        // GET: /JobPosition/
        public ActionResult Index(Guid? id = null)
        {
            IList<JobPositionListVM> list = CreateViewModelList();
            _NLoggerService.Info("Visita a la pagina de puesto de trabajo registrada");
            return View(list);
            // return View(_jobPositionService.GetByCenter(_centerService.Get(id)));
        }

        private IList<JobPositionListVM> CreateViewModelList()
        {
            IList<JobPositionListVM> result = new List<JobPositionListVM>();

            foreach (var jobPosition in _jobPositionService.GetAllWithCenters())
            {
                result.Add(new JobPositionListVM()
                {
                    Id = jobPosition.Id,
                    JobPositionName = jobPosition.JobName != null ? jobPosition.JobName : string.Empty,
                    CenterName = jobPosition.Center != null ? jobPosition.Center.Name : string.Empty,
                    Description = jobPosition.Description
                   
                });
            }

            return result;
        }

        //
        // GET: /JobPosition/Details/
        public ActionResult Details(Guid id)
        {
            return View(_jobPositionService.Get(id));
        }
    


        //
        // GET: /JobPosition/Create        
        public ActionResult Create(Guid? id = null)
        {
            JobPositionNewEditVM jobPosition = CreateViewModelNewEdit();
           return View(jobPosition);
//           return View(_jobPositionService.GetByCenter(_centerService.Get(id)));
            
        }

        private JobPositionNewEditVM CreateViewModelNewEdit(Guid? id = null)
        {
            JobPositionNewEditVM result = new JobPositionNewEditVM();
            if (id == null)
            {
                result.JobPosition = new JobPosition() { Id = Guid.NewGuid() };
                result.Centers = _centerService.GetAll();
            }
            return result;
        } 
       
        //
        // POST: /JobPosition/Create
        [HttpPost]
        public ActionResult Create(JobPositionNewEditVM jobPositionVM)
        {
            try
            {
                // TODO: Add insert logic here
                jobPositionVM.JobPosition.Center = _centerService.Get(jobPositionVM.SelectedCenter);
                _jobPositionService.Add(jobPositionVM.JobPosition);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /JobPosition/Edit/5

        public ActionResult Edit(Guid id)
        {
            return View(_jobPositionService.Get(id));
            
        }

        //
        // POST: /JobPosition/Edit/5

        [HttpPost]
        public ActionResult Edit(Guid id, JobPosition jobPosition)
        {
            try
            {
                // TODO: Add update logic here
                _jobPositionService.Update(jobPosition);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /JobPosition/Delete/5

        public ActionResult Delete(Guid id)
        {
            return View(_jobPositionService.Get(id));
        }

        //
        // POST: /JobPosition/Delete/5

        [HttpPost]
        public ActionResult Delete(JobPosition jobPosition, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                _jobPositionService.Remove(jobPosition);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
