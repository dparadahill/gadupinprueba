﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;

namespace GAdUPIN.WEB.Controllers
{
    public class ColegioController : Controller
    {
        private IColegioService _colegioService;


        public ColegioController()
        {
            _colegioService = 
                IoC.Current.GetContainer().Resolve(typeof(IColegioService), "")
                as IColegioService;
        }

        //
        // GET: /Colegio/

        public ActionResult Index()
        {
            return View(_colegioService.GetAll());
        }

        //
        // GET: /Colegio/Details/5

        public ActionResult Details(Guid id)
        {
            return View(_colegioService.Get(id));
        }

        //
        // GET: /Colegio/Create

        public ActionResult Create()
        {
            Colegio colegio = new Colegio();
            colegio = new Colegio() {Id = Guid.NewGuid()};
            return View(colegio);

        } 

        //
        // POST: /Colegio/Create

        [HttpPost]
        public ActionResult Create(Colegio colegio)
        {
            try
            {
                _colegioService.Add(colegio);
                return View("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /Colegio/Edit/5
 
        public ActionResult Edit(Guid id)
        {
            return View(_colegioService.Get(id));
        }

        //
        // POST: /Colegio/Edit/5

        [HttpPost]
        public ActionResult Edit(Guid id, Colegio colegio)
        {
            try
            {
               _colegioService.Update(colegio);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Colegio/Delete/5
 
        public ActionResult Delete(Guid id)
        {
            return View(_colegioService.Get(id));
        }

        //
        // POST: /Colegio/Delete/5

        [HttpPost]
        public ActionResult Delete(Guid id, Colegio colegio)
        {
            try
            {
                _colegioService.Remove(colegio);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
