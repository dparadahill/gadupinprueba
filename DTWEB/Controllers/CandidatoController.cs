﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Contracts.Services;

using GAdUPIN.CORE.Domain;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.WEB.Controllers
{
    //[Authorize(Roles = "admin")]
    public class CandidatoController : Controller
    {

        private ICandidatoService _candidatoService;
        private INLogger _NLoggerService;

        public CandidatoController()
        {
            _candidatoService =
                IoC.Current.GetContainer().Resolve(typeof(ICandidatoService), "")
                as ICandidatoService;
            _NLoggerService =
                IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
                as INLogger;
        }
        //
        // GET: /Candidato/

        public ActionResult Index()
        {
            return View(_candidatoService.GetAll());
        }

        //
        // GET: /Candidato/Details/5

        public ActionResult DetailsCandidato(Guid id)
        {
            // TODO: I think this is right, check.
            return View(_candidatoService.Get(id));
        }

        //
        // GET: /Candidato/Create

        public ActionResult Create()
        {
            Candidato candidato = new Candidato();
            candidato = new Candidato() {Id = Guid.NewGuid()};
            return View(candidato);
        } 

        //
        // POST: /Candidato/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /Candidato/Edit/5
 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Candidato/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Candidato/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Candidato/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
