﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Contracts;
using GAdUPIN.CORE.Contracts.Services;
using GAdUPIN.CORE.Domain;
using System.Reflection;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;


namespace GAdUPIN.WEB.Controllers
{
    //[Authorize(Roles = "admin")]
    public class CenterController : Controller
    {
        private IEmployeeService _employeeService;
        private ICenterService _centerService;
        private IJobPositionService _jobPositionService;
        private IScheduleService _scheduleService;
        private INLogger _NLoggerService;

        public CenterController()
        {
            _scheduleService =
                IoC.Current.GetContainer().Resolve(typeof(IScheduleService), "")
                as IScheduleService;
            _centerService =
                IoC.Current.GetContainer().Resolve(typeof(ICenterService), "")
                as ICenterService;
            _employeeService =
                IoC.Current.GetContainer().Resolve(typeof(IEmployeeService), "")
                as IEmployeeService;
            _jobPositionService =
                IoC.Current.GetContainer().Resolve(typeof(IJobPositionService), "")
                as IJobPositionService;
            _NLoggerService =
                IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
                as INLogger;
        }


        //
        // GET: /Center/

        public ActionResult Index()
        {
            _NLoggerService.Info("Visita a la página del centro registrada");
            return View(_centerService.GetAll());
        }


        public ActionResult DetailsEmployee(Guid id)
        {
            //DetailsCenterVM detailCenter = CreateViewModelDetailsCenter(id);
            //IList<DetailsCenterVM> list = CreateViewModelCenter(id);
            return View(_employeeService.GetByCenter(_centerService.Get(id)));
        }

        public ActionResult DetailsSchedule(Guid id)
        {
            //DetailsCenterVM detailCenter = CreateViewModelDetailsCenter(id);
            //IList<DetailsCenterVM> list = CreateViewModelCenter(id);
            return View(_scheduleService.GetByCenter(_centerService.Get(id)));
        }


        //
        // GET: /Center/Details

        public ActionResult DetailsJobPosition(Guid id)
        {
            //DetailsCenterVM detailCenter = CreateViewModelDetailsCenter(id);
            //IList<DetailsCenterVM> list = CreateViewModelCenter(id);
            return View(_jobPositionService.GetByCenter(_centerService.Get(id)));
        }

        public ActionResult ManageSchedule()
        {
            return View(_scheduleService.GetAll());
        }


        //
        // GET: /Center/Create        
        public ActionResult Create()
        {
            Center center = new Center();
            center = new Center() { Id = Guid.NewGuid() };
            return View(center);
        }


        //
        // POST: /Center/Create

        [HttpPost]
        public ActionResult Create(Center center)
        {
            try
            {
                // TODO: Add insert logic here
                _centerService.Add(center);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Center/Edit/5

        public ActionResult Edit(Guid id)
        {
            return View(_centerService.Get(id));

        }

        //
        // POST: /Center/Edit/5

        [HttpPost]
        public ActionResult Edit(Guid id, Center center)
        {
            try
            {
                // TODO: Add update logic here
                _centerService.Update(center);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Center/Delete/5

        public ActionResult Delete(Guid id)
        {
            return View(_centerService.Get(id));
        }

        //
        // POST: /Center/Delete/5

        [HttpPost]
        public ActionResult Delete(Center center, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                _centerService.Remove(center);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

