﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;

namespace GAdUPIN.WEB.Controllers
{
    [HandleError]
    public class HomeController : Controller
    {
        private INLogger _NLoggerService;


        public HomeController()
        {
            _NLoggerService =
                IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
                as INLogger;
        }

        public ActionResult Index()
        {
            ViewData["Message"] = "ASP.NET MVC";
            /*
            // Nos conectamos al contexto de datos
            DBContext dbcontext = new DBContext();
            // Creamos un listado
            IList<Center> centers = dbcontext.Centers.ToList();
            IList<Employee> employees = dbcontext.Employees.ToList();
            IList<Shift> shifts = dbcontext.Shifts.ToList();
            */
            ViewData["Message2"] = "NLogger: Visita a la página principal registrada";
            _NLoggerService.Info("Visita a la página principal registrada");
            return RedirectToAction("Index", "Home");
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
