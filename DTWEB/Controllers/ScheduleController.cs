﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GAdUPIN.CORE.Contracts;
using GAdUPIN.CORE.Contracts.Services;

using GAdUPIN.CORE.Domain;

//using GAdUPIN.CORE.DAL;

//using System.Reflection;
using GAdUPIN.Infrastructure;
using GAdUPIN.Infrastructure.Logging;
using GAdUPIN.WEB.ViewModels;


namespace GAdUPIN.WEB.Controllers
{
    public class ScheduleController : Controller
    {

        IScheduleService _scheduleService;
        IEmployeeService _employeeService;
        ICenterService _centerService;
        IJobPositionService _jobPositionService;
        private INLogger _NLoggerService;

        public ScheduleController()
        {
            _scheduleService =
                IoC.Current.GetContainer().Resolve(typeof(IScheduleService), "")
                as IScheduleService;

            _employeeService =
                IoC.Current.GetContainer().Resolve(typeof(IEmployeeService), "")
                 as IEmployeeService;

            _centerService =
                IoC.Current.GetContainer().Resolve(typeof(ICenterService), "")
                as ICenterService;

            _jobPositionService =
                IoC.Current.GetContainer().Resolve(typeof(IJobPositionService), "")
                as IJobPositionService;
            
            _NLoggerService =
                IoC.Current.GetContainer().Resolve(typeof(INLogger), "")
                as INLogger;
        }

        //
        // GET: /Schedule/

        //public ActionResult Index(Center center)
        public ActionResult Index(Guid? id = null)
        {
            IList<ScheduleListVM> list = CreateViewModelList();
            _NLoggerService.Info("Visita a la página de horarios registrada");
            return View(list);
        }

        private IList<ScheduleListVM> CreateViewModelList()
        {
            IList<ScheduleListVM> result = new List<ScheduleListVM>();

            foreach (var schedule in _scheduleService.GetAllWithCenters())
            {
                result.Add(new ScheduleListVM()
                {
                    Id = schedule.Id,
                    CenterName = schedule.Center != null ? schedule.Center.Name : string.Empty,
                    Employee = schedule.AssignedTo != null ? schedule.AssignedTo.Name : string.Empty,
                    
                    Job = schedule.JobPosition != null ? schedule.JobPosition.JobName : string.Empty
                });
            }

            return result;
        }

        // GET: /Schedule/Create
        public ActionResult Create(Guid? id = null)
        {
            ScheduleNewEditVM schedule = CreateViewModelNewEdit(id);
            return View(schedule);

        }

        private ScheduleNewEditVM CreateViewModelNewEdit(Guid? id = null)
        {
            ScheduleNewEditVM result = new ScheduleNewEditVM();
            if (id == null)
            {
                result.Schedule = new Schedule() { Id = Guid.NewGuid() };
                result.Centers = _centerService.GetAll();
                result.Employees = _employeeService.GetAll();
                result.JobPositions = _jobPositionService.GetAll();
               // result.DaysOfWeek = DayOfWeek.Monday;
            //    result.CurrentDay = DateTime.Now.ToLongDateString();
            }
            return result;
        }


        //
        // POST: /Schedule/Create

        [HttpPost]
        public ActionResult Create(ScheduleNewEditVM scheduleNewEditVM)
        {
            try
            {
                // TODO: Add insert logic here
                scheduleNewEditVM.Schedule.Center = _centerService.Get(scheduleNewEditVM.SelectedCenter);
                scheduleNewEditVM.Schedule.JobPosition = _jobPositionService.Get(scheduleNewEditVM.SelectedJobPosition);
                scheduleNewEditVM.Schedule.AssignedTo = _employeeService.Get(scheduleNewEditVM.AssingedTo);

                _scheduleService.Add(scheduleNewEditVM.Schedule);
                return RedirectToAction("Index");
            }
            catch
            {
             //   _NLoggerService.Error("No se pudo crear : ", Assembly.GetExecutingAssembly().FullName);
                return View();
            }
        }

    }
}

