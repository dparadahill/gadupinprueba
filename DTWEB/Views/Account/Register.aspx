﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<GAdUPIN.WEB.Models.RegisterModel>" %>

<asp:Content ID="registerTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Registrarse
</asp:Content>

<asp:Content ID="registerContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Crear una nueva cuenta</h2>
    <p>
        Use el siguiente formulario para crear una nueva cuenta. 
    </p>
    <p>
        Las contraseñas deben tener una longitud mínima de <%: ViewData["PasswordLength"] %> caracteres.
    </p>

    <% using (Html.BeginForm()) { %>
        <%: Html.ValidationSummary(true, "No se creó la cuenta. Corrija los errores e inténtelo de nuevo.") %>
        <div>
            <fieldset class="centrar">
                <legend>Información de cuenta</legend>
             <div class="centrar">
                
                <div class="editor-label2">
                    <%: Html.LabelFor(m => m.UserName) %>
                </div>
                <div class="editor-field2">
                    <%: Html.TextBoxFor(m => m.UserName) %>
                    <%: Html.ValidationMessageFor(m => m.UserName) %>
                </div>
                
                <div class="editor-label2">
                    <%: Html.LabelFor(m => m.Email) %>
                </div>
                <div class="editor-field2">
                    <%: Html.TextBoxFor(m => m.Email) %>
                    <%: Html.ValidationMessageFor(m => m.Email) %>
                </div>
                
                <div class="editor-label2">
                    <%: Html.LabelFor(m => m.Password) %>
                </div>
                <div class="editor-field2">
                    <%: Html.PasswordFor(m => m.Password) %>
                    <%: Html.ValidationMessageFor(m => m.Password) %>
                </div>
                
                <div class="editor-label2">
                    <%: Html.LabelFor(m => m.ConfirmPassword) %>
                </div>
                <div class="editor-field2">
                    <%: Html.PasswordFor(m => m.ConfirmPassword) %>
                    <%: Html.ValidationMessageFor(m => m.ConfirmPassword) %>
                </div>
             </div>
                <p>
                    <input type="submit" value="Registrarse" />
                </p>
            </fieldset>
        </div>
    <% } %>
</asp:Content>
