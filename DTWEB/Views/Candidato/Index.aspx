﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
 Inherits="System.Web.Mvc.ViewPage<IEnumerable<GAdUPIN.CORE.Domain.Candidato>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Index</h2>

    <table>
        <tr>
            <th></th>
            <th style="display:none">
                Id
            </th>
            <th>
                Nombre
            </th>
            <th>
                Primer Apellido
            </th>
            <th>
                Segundo Apellido
            </th>
            <th>
                Email
            </th>
            <th>
                # de contacto
            </th>
            <th>
                Usuario
            </th>
            <th>
                Contraseña
            </th>
            <th>
                Edad
            </th>
            <th>
                Nacionalidad
            </th>
            <th>
                Skype
            </th>
            <th>
                Procedencia
            </th>
            <th>
                Sexo
            </th>
            <th>
                Fecha de Alta
            </th>
        </tr>

    <% foreach (var item in Model) { %>
    
        <tr>
            <td>
                <%: Html.ActionLink("Editar", "Edit", new { id = item.Id })%> |
                <%: Html.ActionLink(" Ver detalles", "DetailsCandidato", new { id = item.Id })%> |
            </td>
        
            <td style="display:none">
                <%: item.Id %>
            </td>
        
            <td>
                <%: item.Name %>
            </td>
            <td>
                <%: item.LastName1 %>
            </td>
            <td>
                <%: item.LastName2 %>
            </td>
            <td>
                <%: item.Email %>
            </td>
            <td>
                <%: item.TelefonoDeContacto %>
            </td>
            <td>
                <%: item.Usuario %>
            </td>
            <td>
                <%: item.Password %>
            </td>
            <td>
                <%: item.Edad %>
            </td>
            <td>
                <%: item.Nacionalidad %>
            </td>
            <td>
                <%: item.Skype %>
            </td>
            <td>
                <%: item.Procedencia %>
            </td>
            <td>
                <%: item.Sexo %>
            </td>
            <td>
                <%: item.FechaAlta %>
            </td>
        </tr>  
    
    <% } %>

    </table>

    <p>
        <%: Html.ActionLink("Crear", "Create") %>
    </p>

</asp:Content>

