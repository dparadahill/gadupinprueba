﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.Candidato>" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<h2>Detalles del Candidato</h2>
<% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
 
        <fieldset>
            <legend>Fields</legend>

    <div style="text-align: center">
    <table style="text-align: center">
        <tr class="editor-label" style="display:none" >
            <th>
            ID
            </th>
            <th class="editor-field" style="display:none" >
                <%: Html.TextBoxFor(model => model.Id) %>
                <%: Html.ValidationMessageFor(model => model.Id) %>
            </th>
        </tr>
        <tr class="editor-label"  >
            <th>
            Nombre
            </th>
            <th class="editor-field"  >
                <%: Html.TextBoxFor(model => model.Name) %>
                <%: Html.ValidationMessageFor(model => model.Name) %>
            </th>
        </tr>
        
        <tr class="editor-label"  >
            <th>
            Primer Apellido
            </th>
            <th class="editor-field"  >
                <%: Html.TextBoxFor(model => model.LastName1) %>
                <%: Html.ValidationMessageFor(model => model.LastName1) %>
            </th>
        </tr>
        <tr class="editor-label"  >
            <th>
            Segundo Apellido
            </th>
            <th class="editor-field"  >
                <%: Html.TextBoxFor(model => model.LastName2) %>
                <%: Html.ValidationMessageFor(model => model.LastName2) %>
            </th>
        </tr>
        <tr class="editor-label"  >
            <th>
            Email
            </th>
            <th class="editor-field"  >
                <%: Html.TextBoxFor(model => model.Email) %>
                <%: Html.ValidationMessageFor(model => model.Email) %>
            </th>
        </tr>
        <tr class="editor-label"  >
            <th>
            # de contacto
            </th>
            <th class="editor-field"  >
                <%: Html.TextBoxFor(model => model.TelefonoDeContacto) %>
                <%: Html.ValidationMessageFor(model => model.TelefonoDeContacto) %>
            </th>
        </tr>
        <tr class="editor-label"  >
            <th>
            Usuario
            </th>
            <th class="editor-field"  >
                <%: Html.TextBoxFor(model => model.Usuario) %>
                <%: Html.ValidationMessageFor(model => model.Usuario) %>
            </th>
        </tr>
        <tr class="editor-label"  >
            <th>
            Contrasena
            </th>
            <th class="editor-field"  >
                <%: Html.TextBoxFor(model => model.Password) %>
                <%: Html.ValidationMessageFor(model => model.Password) %>
            </th>
        </tr>
        
        <tr class="editor-label"  >
            <th>
            Edad
            </th>
            <th class="editor-field"  >
                <%: Html.TextBoxFor(model => model.Edad) %>
                <%: Html.ValidationMessageFor(model => model.Edad) %>
            </th>
        </tr>
        <tr class="editor-label"  >
            <th>
            Nacionalidad
            </th>
            <th class="editor-field"  >
                <%: Html.TextBoxFor(model => model.Nacionalidad) %>
                <%: Html.ValidationMessageFor(model => model.Nacionalidad) %>
            </th>
        </tr>
        <tr class="editor-label"  >
            <th>
            Skype
            </th>
            <th class="editor-field"  >
                <%: Html.TextBoxFor(model => model.Skype) %>
                <%: Html.ValidationMessageFor(model => model.Skype) %>
            </th>
        </tr>
        <tr class="editor-label"  >
            <th>
            Procedencia
            </th>
            <th class="editor-field"  >
                <%: Html.TextBoxFor(model => model.Procedencia) %>
                <%: Html.ValidationMessageFor(model => model.Procedencia) %>
            </th>
        </tr>
        <tr class="editor-label"  >
            <th>
            Sexo
            </th>
            <th class="editor-field"  >
                <%: Html.TextBoxFor(model => model.Sexo) %>
                <%: Html.ValidationMessageFor(model => model.Sexo) %>
            </th>
        </tr><tr class="editor-label"  >
            <th>
            Fecha Alta
            </th>
            <th class="editor-field"  >
                <%: Html.TextBoxFor(model => model.FechaAlta) %>
                <%: Html.ValidationMessageFor(model => model.FechaAlta) %>
            </th>
        </tr>

      </table>
      </div>
     

    <% foreach (var item in Model) { %>
    
        <tr>
            <td>
                <%: Html.ActionLink("Edit", "../Employee/Edit", new { id=item.Id  }) %> |
                <%: Html.ActionLink("Details", "../Employee/Details", new { id = item.Id })%> |
                <%: Html.ActionLink("Delete", "../Employee/Delete", new { id = item.Id })%>
            </td>
        </tr>  
        <tr>
            <td  >
                <%: item.Id %>
            </td>
        </tr>  
            <td>
                <%: item.Name %>
            </td>
            <td>
                <%: item.Apellido1 %>
            </td>
            <td>
                <%: item.Apellido2 %>
            </td>
            <td>
                <%: item.Email %>
            </td>
            <td>
                <%: item.TelefonoDeContacto %>
            </td>
            <td>
                <%: item.Usuario %>
            </td>
            <td>
                <%: item.Password %>
            </td>
            <td>
                <%: item.Edad %>
            </td>
            <td>
                <%: item.Nacionalidad %>
            </td>
            <td>
                <%: item.Skype %>
            </td>
            <td>
                <%: item.Procedencia %>
            </td>
            <td>
                <%: item.Sexo %>
            </td>
            <td>
                <%: item.FechaAlta %>
            </td>
        
     </fieldset>
    <% } %>

    </table>
    </div>
    <p>
        <%: Html.ActionLink("Crear", "../Candidato/Create" )%>
    </p>

</asp:Content>

