﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.Candidato>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Create
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Create</h2>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <fieldset>
            <legend>Fields</legend>
            
            <div class="editor-label" style="display:none" >
                <%: Html.LabelFor(model => model.Id) %>
            </div>
            <div class="editor-field" style="display:none">
                <%: Html.TextBoxFor(model => model.Id) %>
                <%: Html.ValidationMessageFor(model => model.Id) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Name) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Name) %>
                <%: Html.ValidationMessageFor(model => model.Name) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.LastName1) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.LastName1)%>
                <%: Html.ValidationMessageFor(model => model.LastName1)%>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.LastName2)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.LastName2)%>
                <%: Html.ValidationMessageFor(model => model.LastName2)%>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Email) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Email) %>
                <%: Html.ValidationMessageFor(model => model.Email) %>
            </div>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.TelefonoDeContacto) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.TelefonoDeContacto) %>
                <%: Html.ValidationMessageFor(model => model.TelefonoDeContacto) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Password) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Password) %>
                <%: Html.ValidationMessageFor(model => model.Password) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Edad) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Edad) %>
                <%: Html.ValidationMessageFor(model => model.Edad) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Nacionalidad) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Nacionalidad) %>
                <%: Html.ValidationMessageFor(model => model.Nacionalidad) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Skype) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Skype) %>
                <%: Html.ValidationMessageFor(model => model.Skype) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Procedencia) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Procedencia) %>
                <%: Html.ValidationMessageFor(model => model.Procedencia) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Sexo) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Sexo) %>
                <%: Html.ValidationMessageFor(model => model.Sexo) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.FechaAlta) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.FechaAlta) %>
                <%: Html.ValidationMessageFor(model => model.FechaAlta) %>
            </div>


            <p>
                <input type="submit" value="Crear" />
            </p>
        </fieldset>

    <% } %>

    <div>
        <%: Html.ActionLink("Volver a la lista", "Index") %>
    </div>

</asp:Content>

