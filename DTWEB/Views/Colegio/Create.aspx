﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.Colegio>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Nuevo colegio
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Dar de alta un nuevo colegio</h2>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <fieldset>
            <legend>Campos</legend>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Id) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Id) %>
                <%: Html.ValidationMessageFor(model => model.Id) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.NombreColegio) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.NombreColegio) %>
                <%: Html.ValidationMessageFor(model => model.NombreColegio) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.DireccionColegio) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.DireccionColegio) %>
                <%: Html.ValidationMessageFor(model => model.DireccionColegio) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.TelefonoContacto) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.TelefonoContacto) %>
                <%: Html.ValidationMessageFor(model => model.TelefonoContacto) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.ComunidadAutonoma) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.ComunidadAutonoma)%>
                <%: Html.ValidationMessageFor(model => model.ComunidadAutonoma)%>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.PlazasDisponibles) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.PlazasDisponibles)%>
                <%: Html.ValidationMessageFor(model => model.PlazasDisponibles)%>
            </div>
            
            <p>
                <input type="submit" value="Create" />
            </p>
        </fieldset>

    <% } %>

    <div>
        <%: Html.ActionLink("Volver", "Index") %>
    </div>

</asp:Content>

