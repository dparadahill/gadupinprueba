﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<GAdUPIN.CORE.Domain.Colegio>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Detalles del colegio
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Detalles del colegio</h2>

    <fieldset>
        <legend>Campos</legend>
        
        <div class="display-label" style="display: none">Id</div>
        <div class="display-field" style="display: none"><%: Model.Id %></div>
        
        <div class="display-label">Nombre del colegio</div>
        <div class="display-field"><%: Model.NombreColegio %></div>
        
        <div class="display-label">Direccion</div>
        <div class="display-field"><%: Model.DireccionColegio %></div>
        
        <div class="display-label">Teléfono</div>
        <div class="display-field"><%: Model.TelefonoContacto %></div>

        <div class="display-label">ComunidadAutonoma</div>
        <div class="display-field"><%: Model.ComunidadAutonoma %></div>

        <div class="display-label">PlazasDisponibles</div>
        <div class="display-field"><%: Model.PlazasDisponibles %></div>
        
    </fieldset>
    <p>
        <%: Html.ActionLink("Editar", "Edit", new { id = Model.Id })%> |
        <%: Html.ActionLink("Regresar", "Index") %>
    </p>

</asp:Content>

